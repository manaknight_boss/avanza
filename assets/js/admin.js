jQuery(document).ready(function() {
  if (document.getElementById("call_history_government_id")) {
    var options = {
      url: function(phrase) {
        return "/v1/api/admin/voters/autocomplete/" + phrase;
      },
      list: {
        onSelectItemEvent: function() {
          var value = $("#call_history_government_id").getSelectedItemData();
          $("#call_history_government_id_value")
            .val(value.government_id)
            .trigger("change");
          $("#call_history_voter_id_value")
            .val(value.id)
            .trigger("change");
        }
      },
      getValue: "government_id"
    };

    $("#call_history_government_id").easyAutocomplete(options);
  }

  if (document.getElementById("sms_history_government_id")) {
    var options = {
      url: function(phrase) {
        return "/v1/api/admin/voters/autocomplete/" + phrase;
      },
      list: {
        onSelectItemEvent: function() {
          var value = $("#sms_history_government_id").getSelectedItemData();
          $("#sms_history_government_id_value")
            .val(value.government_id)
            .trigger("change");
          $("#sms_history_voter_id_value")
            .val(value.id)
            .trigger("change");
        }
      },
      getValue: "government_id"
    };

    $("#sms_history_government_id").easyAutocomplete(options);
  }
  if (document.getElementById("sms_dashboard_chart")) {
    var ctx = document.getElementById("sms_dashboard_chart").getContext("2d");
    var chart = new Chart(ctx, {
      type: "doughnut",
      data: {
        datasets: [
          {
            data: [
              document
                .getElementById("total_sms_reach")
                .getAttribute("data-num"),
              document
                .getElementById("total_sms_no_response")
                .getAttribute("data-num"),
              document
                .getElementById("total_sms_sent")
                .getAttribute("data-num"),
              document.getElementById("total_sms_poll").getAttribute("data-num")
            ],
            backgroundColor: [
              "#0074D9",
              "#FF4136",
              "#2ECC40",
              "#FF851B",
              "#7FDBFF",
              "#B10DC9",
              "#FFDC00",
              "#001f3f",
              "#39CCCC",
              "#01FF70",
              "#85144b",
              "#F012BE",
              "#3D9970",
              "#111111",
              "#AAAAAA"
            ]
          }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
          "Contactados",
          "Total Electores No",
          "Enviados",
          "Encuestas"
        ]
      },
      options: {
        legend: {
          display: false
        }
      }
    });
  }
  if (document.getElementById("voice_dashboard_chart")) {
    var ctx = document.getElementById("voice_dashboard_chart").getContext("2d");
    var chart = new Chart(ctx, {
      type: "doughnut",
      data: {
        datasets: [
          {
            data: [
              document
                .getElementById("total_voice_reach")
                .getAttribute("data-num"),
              document
                .getElementById("total_voice_no_response")
                .getAttribute("data-num"),
              document
                .getElementById("total_voice_sent")
                .getAttribute("data-num"),
              document
                .getElementById("total_voice_poll")
                .getAttribute("data-num")
            ],
            backgroundColor: [
              "#0074D9",
              "#FF4136",
              "#2ECC40",
              "#FF851B",
              "#7FDBFF",
              "#B10DC9",
              "#FFDC00",
              "#001f3f",
              "#39CCCC",
              "#01FF70",
              "#85144b",
              "#F012BE",
              "#3D9970",
              "#111111",
              "#AAAAAA"
            ]
          }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
          "Contactados",
          "Total Electores No",
          "Llamadas Global",
          "Encuestas"
        ]
      },
      options: {
        legend: {
          display: false
        }
      }
    });
  }
  if (document.getElementById("email_dashboard_chart")) {
    var ctx = document.getElementById("email_dashboard_chart").getContext("2d");
    var chart = new Chart(ctx, {
      type: "doughnut",
      data: {
        datasets: [
          {
            data: [
              document
                .getElementById("total_email_reach")
                .getAttribute("data-num"),
              document
                .getElementById("total_email_sent")
                .getAttribute("data-num")
            ],
            backgroundColor: [
              "#0074D9",
              "#FF4136",
              "#2ECC40",
              "#FF851B",
              "#7FDBFF",
              "#B10DC9",
              "#FFDC00",
              "#001f3f",
              "#39CCCC",
              "#01FF70",
              "#85144b",
              "#F012BE",
              "#3D9970",
              "#111111",
              "#AAAAAA"
            ]
          }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: ["Contactados", "Enviados"]
      },
      options: {
        legend: {
          display: false
        }
      }
    });
  }
  if (document.getElementById("poll_dashboard_chart")) {
    var ctx = document.getElementById("poll_dashboard_chart").getContext("2d");
    var chart = new Chart(ctx, {
      type: "doughnut",
      data: {
        datasets: [
          {
            data: [
              document
                .getElementById("total_poll_voice")
                .getAttribute("data-num"),
              document
                .getElementById("total_poll_sms")
                .getAttribute("data-num"),
              document
                .getElementById("total_poll_response_1")
                .getAttribute("data-num"),
              document
                .getElementById("total_poll_response_2")
                .getAttribute("data-num"),
              document
                .getElementById("total_poll_response_3")
                .getAttribute("data-num")
            ],
            backgroundColor: [
              "#0074D9",
              "#FF4136",
              "#2ECC40",
              "#FF851B",
              "#7FDBFF",
              "#B10DC9",
              "#FFDC00",
              "#001f3f",
              "#39CCCC",
              "#01FF70",
              "#85144b",
              "#F012BE",
              "#3D9970",
              "#111111",
              "#AAAAAA"
            ]
          }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
          "Encuestas voz",
          "Encuestas SMS",
          "Resp./Opcion 1",
          "Resp./Opcion 2",
          "Resp./Opcion 3"
        ]
      },
      options: {
        legend: {
          display: false
        }
      }
    });
  }
});
