jQuery(document).ready(function() {
  if (document.getElementById("circunscripcion_1_dashboard_chart")) {
    var ctx = document
      .getElementById("circunscripcion_1_dashboard_chart")
      .getContext("2d");
    var chart = new Chart(ctx, {
      type: "doughnut",
      data: {
        datasets: [
          {
            data: [
              document
                .getElementById("avanza_circunscripcion_1_turnout")
                .getAttribute("data-num"),
              document
                .getElementById("other_circunscripcion_1_turnout")
                .getAttribute("data-num")
            ],
            backgroundColor: [
              "#0074D9",
              "#FF4136",
              "#2ECC40",
              "#FF851B",
              "#7FDBFF",
              "#B10DC9",
              "#FFDC00",
              "#001f3f",
              "#39CCCC",
              "#01FF70",
              "#85144b",
              "#F012BE",
              "#3D9970",
              "#111111",
              "#AAAAAA"
            ]
          }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: ["% Voto Avanza", "% Voto General"]
      },
      options: {
        legend: {
          display: false
        }
      }
    });
  }
  if (document.getElementById("circunscripcion_2_dashboard_chart")) {
    var ctx = document
      .getElementById("circunscripcion_2_dashboard_chart")
      .getContext("2d");
    var chart = new Chart(ctx, {
      type: "doughnut",
      data: {
        datasets: [
          {
            data: [
              document
                .getElementById("avanza_circunscripcion_2_turnout")
                .getAttribute("data-num"),
              document
                .getElementById("other_circunscripcion_2_turnout")
                .getAttribute("data-num")
            ],
            backgroundColor: [
              "#0074D9",
              "#FF4136",
              "#2ECC40",
              "#FF851B",
              "#7FDBFF",
              "#B10DC9",
              "#FFDC00",
              "#001f3f",
              "#39CCCC",
              "#01FF70",
              "#85144b",
              "#F012BE",
              "#3D9970",
              "#111111",
              "#AAAAAA"
            ]
          }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: ["% Voto Avanza", "% Voto General"]
      },
      options: {
        legend: {
          display: false
        }
      }
    });
  }
  if (document.getElementById("circunscripcion_3_dashboard_chart")) {
    var ctx = document
      .getElementById("circunscripcion_3_dashboard_chart")
      .getContext("2d");
    var chart = new Chart(ctx, {
      type: "doughnut",
      data: {
        datasets: [
          {
            data: [
              document
                .getElementById("avanza_circunscripcion_3_turnout")
                .getAttribute("data-num"),
              document
                .getElementById("other_circunscripcion_3_turnout")
                .getAttribute("data-num")
            ],
            backgroundColor: [
              "#0074D9",
              "#FF4136",
              "#2ECC40",
              "#FF851B",
              "#7FDBFF",
              "#B10DC9",
              "#FFDC00",
              "#001f3f",
              "#39CCCC",
              "#01FF70",
              "#85144b",
              "#F012BE",
              "#3D9970",
              "#111111",
              "#AAAAAA"
            ]
          }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: ["% Voto Avanza", "% Voto General"]
      },
      options: {
        legend: {
          display: false
        }
      }
    });
  }
  if (document.getElementById("circunscripcion_n_dashboard_chart")) {
    var ctx = document
      .getElementById("circunscripcion_n_dashboard_chart")
      .getContext("2d");
    var chart = new Chart(ctx, {
      type: "doughnut",
      data: {
        datasets: [
          {
            data: [
              document
                .getElementById("not_voted_circunscripcion")
                .getAttribute("data-num"),
              document
                .getElementById("voted_circunscripcion")
                .getAttribute("data-num")
            ],
            backgroundColor: [
              "#0074D9",
              "#FF4136",
              "#2ECC40",
              "#FF851B",
              "#7FDBFF",
              "#B10DC9",
              "#FFDC00",
              "#001f3f",
              "#39CCCC",
              "#01FF70",
              "#85144b",
              "#F012BE",
              "#3D9970",
              "#111111",
              "#AAAAAA"
            ]
          }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: ["Total por Votar", "Total Votaron"]
      },
      options: {
        legend: {
          display: false
        }
      }
    });
  }
  if (document.getElementById("all_dashboard_chart")) {
    var ctx = document.getElementById("all_dashboard_chart").getContext("2d");
    var chart = new Chart(ctx, {
      type: "doughnut",
      data: {
        datasets: [
          {
            data: [
              document
                .getElementById("turnout_percentage_avanza")
                .getAttribute("data-num"),
              document
                .getElementById("turnout_percentage_other")
                .getAttribute("data-num")
            ],
            backgroundColor: [
              "#0074D9",
              "#FF4136",
              "#2ECC40",
              "#FF851B",
              "#7FDBFF",
              "#B10DC9",
              "#FFDC00",
              "#001f3f",
              "#39CCCC",
              "#01FF70",
              "#85144b",
              "#F012BE",
              "#3D9970",
              "#111111",
              "#AAAAAA"
            ]
          }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: ["Avanza", "No Avanza"]
      },
      options: {
        legend: {
          display: false
        }
      }
    });
  }
  if (document.getElementById("circunscripcion_1_dashboard_chart_a")) {
    var ctx = document
      .getElementById("circunscripcion_1_dashboard_chart_a")
      .getContext("2d");
    var chart = new Chart(ctx, {
      type: "doughnut",
      data: {
        datasets: [
          {
            data: [
              document
                .getElementById("not_voted_circunscripcion_1")
                .getAttribute("data-num"),
              document
                .getElementById("voted_circunscripcion_1")
                .getAttribute("data-num")
            ],
            backgroundColor: [
              "#0074D9",
              "#FF4136",
              "#2ECC40",
              "#FF851B",
              "#7FDBFF",
              "#B10DC9",
              "#FFDC00",
              "#001f3f",
              "#39CCCC",
              "#01FF70",
              "#85144b",
              "#F012BE",
              "#3D9970",
              "#111111",
              "#AAAAAA"
            ]
          }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: ["Total por Votar", "Total Votaron"]
      },
      options: {
        legend: {
          display: false
        }
      }
    });
  }
  if (document.getElementById("circunscripcion_1_dashboard_chart_b")) {
    var ctx = document
      .getElementById("circunscripcion_1_dashboard_chart_b")
      .getContext("2d");
    var chart = new Chart(ctx, {
      type: "doughnut",
      data: {
        datasets: [
          {
            data: [
              document
                .getElementById("not_voted_circunscripcion_2")
                .getAttribute("data-num"),
              document
                .getElementById("voted_circunscripcion_2")
                .getAttribute("data-num")
            ],
            backgroundColor: [
              "#0074D9",
              "#FF4136",
              "#2ECC40",
              "#FF851B",
              "#7FDBFF",
              "#B10DC9",
              "#FFDC00",
              "#001f3f",
              "#39CCCC",
              "#01FF70",
              "#85144b",
              "#F012BE",
              "#3D9970",
              "#111111",
              "#AAAAAA"
            ]
          }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: ["Total por Votar", "Total Votaron"]
      },
      options: {
        legend: {
          display: false
        }
      }
    });
  }
  if (document.getElementById("circunscripcion_1_dashboard_chart_c")) {
    var ctx = document
      .getElementById("circunscripcion_1_dashboard_chart_c")
      .getContext("2d");
    var chart = new Chart(ctx, {
      type: "doughnut",
      data: {
        datasets: [
          {
            data: [
              document
                .getElementById("not_voted_circunscripcion_3")
                .getAttribute("data-num"),
              document
                .getElementById("voted_circunscripcion_3")
                .getAttribute("data-num")
            ],
            backgroundColor: [
              "#0074D9",
              "#FF4136",
              "#2ECC40",
              "#FF851B",
              "#7FDBFF",
              "#B10DC9",
              "#FFDC00",
              "#001f3f",
              "#39CCCC",
              "#01FF70",
              "#85144b",
              "#F012BE",
              "#3D9970",
              "#111111",
              "#AAAAAA"
            ]
          }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: ["Total por Votar", "Total Votaron"]
      },
      options: {
        legend: {
          display: false
        }
      }
    });
  }
});
