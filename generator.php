<?php
include_once './templates/builder/App_builder.php';

echo '<hr/>';
echo '<h1>SAAS Deployment</h1>';
echo '<a href="/generator.php?action=start">Start Build</a><br/>';
echo '<a href="/generator.php?action=clean">Clean Build</a><br/>';
echo '<a href="/generator.php?action=rebuild">ReBuild</a><br/>';
echo '<a href="/generator.php?action=random">Random Key</a><br/>';
echo '<a href="/generator.php?action=crud">Crud Builder</a><br/>';

$action = !empty($_GET['action']) ? $_GET['action'] : '';

$raw_configuration = file_get_contents('configuration.json');

if ($action == 'start') {
    $builder = new App_builder($raw_configuration);
    $builder->build();
}

if ($action == 'clean') {
    $builder = new App_builder($raw_configuration);
    $builder->destroy();
}

if ($action == 'random') {
    echo '<h1>Random Key</h1>';
    echo '<p>' . sha1(uniqid() . time()). '</p>';
}

if ($action == 'rebuild') {
    $builder = new App_builder($raw_configuration);
    $builder->destroy();
    $builder = new App_builder($raw_configuration);
    $builder->build();
}

if ($action == 'crud') {
    $raw_configuration = file_get_contents('crud.json');
    $builder = new App_builder($raw_configuration);
    $builder->destroy();
}

echo '<h3>Controller:</h3>';
echo '<pre>' . json_encode(json_decode('{"route":"&sol;settings","name":"setting","model":"setting","controller":"Setting_controller.php","override":"","override_add":"","override_edit":"","override_view":"","override_list":"","paginate":true,"portal":"admin","is_crud":true,"is_add":true,"is_edit":true,"is_delete":true,"is_list":true,"is_view":true,"method":"","is_menu":true,"menu_name":"Orders","is_filter":true,"listing_fields":["key","type","value"],"filter_fields":["key","value"],"add_fields":["key","type","value"],"edit_fields":["key","value"],"view_fields":["key","value"]}', TRUE), JSON_PRETTY_PRINT) . '</pre>';
echo '{"route":"&sol;settings","name":"setting","model":"setting","controller":"Setting_controller.php","override":"","override_add":"","override_edit":"","override_view":"","override_list":"","paginate":true,"portal":"admin","is_crud":true,"is_add":true,"is_edit":true,"is_delete":true,"is_list":true,"is_view":true,"method":"","is_menu":true,"menu_name":"Orders","is_filter":true,"listing_fields":["key","type","value"],"filter_fields":["key","value"],"add_fields":["key","type","value"],"edit_fields":["key","value"],"view_fields":["key","value"]}';
echo '<h3>Model:</h3>';
echo '<pre>' . json_encode(json_decode('{"name":"z","timestamp":true,"migration":true,"unique": [],"field":[["y","string",[{"limit":50}],"x","required",""]],"method":"","join":[],"mapping":{"status":{"0":"inactive","1":"active"}},"pre":"","post":"","count":"","override":"","seed": []}', TRUE), JSON_PRETTY_PRINT) . '</pre>';
echo '{"name":"z","timestamp":true,"migration":true,"unique": [],"field":[["y","string",[{"limit":50}],"x","required",""]],"method":"","join":[],"mapping":{"status":{"0":"inactive","1":"active"}},"pre":"","post":"","count":"","override":"","seed": []}';