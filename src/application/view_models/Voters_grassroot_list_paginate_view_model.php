<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Voters List Paginate View Model
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Voters_grassroot_list_paginate_view_model
{
    protected $_library;
    protected $_base_url = '';
    protected $_heading = 'Voters';
    protected $_total_rows = 0;
    protected $_per_page = 10;
    protected $_page;
    protected $_num_links = 5;
    protected $_column = ['ID', 'Circunscripcion','Nombre','Recinto','Colegio','Coordinador','Votaron','Accion'];
    protected $_list = [];
    protected $_links = '';
    protected $_entity;

    public function __construct($entity, $library, $base_url)
    {
        $this->_entity = $entity;
        $this->_library = $library;
		$this->_base_url = str_replace('/0', '/', $base_url);
    }

    /**
     * set_total_rows function
     *
     * @param integer $total_rows
     * @return void
     */
    public function set_total_rows ($total_rows)
    {
        $this->_total_rows = $total_rows;
    }

    /**
     * set_per_page function
     *
     * @param integer $per_page
     * @return void
     */
    public function set_per_page ($per_page)
    {
        $this->_per_page = $per_page;
    }

    /**
     * set_page function
     *
     * @param integer $page
     * @return void
     */
    public function set_page ($page)
    {
        $this->_page = $page;
    }

    /**
     * set_list function
     *
     * @param mixed $list
     * @return void
     */
    public function set_list ($list)
    {
        $this->_list = $list;
    }

    /**
     * get_total_rows function
     *
     * @return integer
     */
    public function get_total_rows ()
    {
        return $this->_total_rows;
    }

    /**
     * get_per_page function
     *
     * @return integer
     */
    public function get_per_page ()
    {
        return $this->_per_page;
    }

    /**
     * get_page function
     *
     * @return integer
     */
    public function get_page ()
    {
        return $this->_page;
    }

    /**
     * num_links function
     *
     * @return integer
     */
    public function get_num_links ()
    {
        return $this->_num_links;
    }

    /**
     * num_pages function
     *
     * @return integer
     */
    public function get_num_page ()
    {
        $num = ceil($this->_total_rows / $this->_per_page);
        return ($num > 0) ? (int) $num : 1;
    }

    public function image_or_file ($file)
    {
        $images = ['.jpg','.png', '.gif', '.jpeg', '.bmp'];
        $is_image = FALSE;
        if ($this->strposa($file, $images))
        {
            return "<div class='mkd-image-container'><img class='img-fluid' src='{$file}'/></div>";
        }

        return "<a href='{$file}' target='__blank'>{$file}</a>";
    }

    /**
     * Strpos for array
     *
     * @param string $haystack
     * @param array $needle
     * @return boolean
     */
    private function strposa($haystack, $needle)
    {
        foreach($needle as $query)
        {
            if(strpos($haystack, $query) !== FALSE)
            {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * get_list function
     *
     * @return mixed
     */
    public function get_list ()
    {
        $this->_library->initialize([
            'reuse_query_string' => TRUE,
            'base_url' => $this->_base_url,
            'total_rows' => $this->_total_rows,
            'per_page' => $this->_per_page,
            'num_links' => $this->_num_links,
            'full_tag_open' => '<ul class="pagination justify-content-end">',
            'full_tag_close' => '</ul>',
            'attributes' => ['class' => 'page-link'],
            'first_link' => FALSE,
            'last_link' => FALSE,
            'first_tag_open' => '<li class="page-item">',
            'first_tag_close' => '</li>',
            'prev_link' => '&laquo',
            'prev_tag_open' => '<li class="page-item">',
            'prev_tag_close' => '</li>',
            'next_link' => '&raquo',
            'next_tag_open' => '<li class="page-item">',
            'next_tag_close' => '</li>',
            'last_tag_open' => '<li class="page-item">',
            'last_tag_close' => '</li>',
            'cur_tag_open' => '<li class="page-item active"><a href="#" class="page-link">',
            'cur_tag_close' => '<span class="sr-only">(current)</span></a></li>',
            'num_tag_open' => '<li class="page-item">',
            'num_tag_close' => '</li>'
        ]);
        return $this->_list;
    }

    /**
     * get_links function
     *
     * @return mixed
     */
    public function get_links ()
    {
        $this->_links = $this->_library->create_links();
        return $this->_links;
    }

    /**
     * set_heading function
     *
     * @param string $heading
     * @return void
     */
    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    /**
     * get_heading function
     *
     * @return string
     */
    public function get_heading ()
    {
        return $this->_heading;
    }

    public function set_column ($column)
    {
        $this->_column = $column;
    }

    public function get_column ()
    {
        return $this->_column;
    }

    public function set_source ($source)
    {
        $this->_source = $source;
    }

    public function get_source ()
    {
        return $this->_source;
    }

	public function get_circunscripcion ()
	{
		return $this->_circunscripcion;
	}

	public function set_circunscripcion ($circunscripcion)
	{
		$this->_circunscripcion = $circunscripcion;
	}

	public function get_government_id ()
	{
		return $this->_government_id;
	}

	public function set_government_id ($government_id)
	{
		$this->_government_id = $government_id;
	}

	public function get_recinto ()
	{
		return $this->_recinto;
	}

	public function set_recinto ($recinto)
	{
		$this->_recinto = $recinto;
	}

	public function get_colegio ()
	{
		return $this->_colegio;
	}

	public function set_colegio ($colegio)
	{
		$this->_colegio = $colegio;
	}

	public function get_has_voted_municipal_2020 ()
	{
		return $this->_has_voted_municipal_2020;
	}

	public function set_has_voted_municipal_2020 ($has_voted_municipal_2020)
	{
		$this->_has_voted_municipal_2020 = $has_voted_municipal_2020;
	}

	public function get_id ()
	{
		return $this->_id;
	}

	public function set_id ($id)
	{
		$this->_id = $id;
	}

	public function member_pld_mapping ()
	{
		return $this->_entity->member_pld_mapping();

	}

	public function is_vote_external_mapping ()
	{
		return $this->_entity->is_vote_external_mapping();

	}

	public function is_cordinator_mapping ()
	{
		return $this->_entity->is_cordinator_mapping();

	}

	public function gender_mapping ()
	{
		return $this->_entity->gender_mapping();

	}

	public function married_status_mapping ()
	{
		return $this->_entity->married_status_mapping();

	}

	public function phone_type_1_mapping ()
	{
		return $this->_entity->phone_type_1_mapping();

	}

	public function phone_type_2_mapping ()
	{
		return $this->_entity->phone_type_2_mapping();

	}

	public function to_json ()
	{
		$list = $this->get_list();

		$clean_list = [];

		foreach ($list as $key => $value)
		{
			$list[$key]->member_pld = $this->member_pld_mapping()[$value->member_pld];
			$list[$key]->is_vote_external = $this->is_vote_external_mapping()[$value->is_vote_external];
			$list[$key]->is_cordinator = $this->is_cordinator_mapping()[$value->is_cordinator];
			$list[$key]->gender = $this->gender_mapping()[$value->gender];
			$list[$key]->married_status = $this->married_status_mapping()[$value->married_status];
			$list[$key]->phone_type_1 = $this->phone_type_1_mapping()[$value->phone_type_1];
			$list[$key]->phone_type_2 = $this->phone_type_2_mapping()[$value->phone_type_2];
			$clean_list_entry = [];
			$clean_list[] = $clean_list_entry;
		}

		return [
			'page' => $this->get_page(),
			'num_page' => $this->get_num_page(),
			'num_item' => $this->get_total_rows(),
			'item' => $clean_list
		];
	}

}