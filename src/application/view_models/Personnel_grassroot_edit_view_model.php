<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Edit Personnel View Model
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Personnel_grassroot_edit_view_model
{
    protected $_entity;
	protected $_id;
	protected $_staff_lead_name;
	protected $_staff_lead_government_id;
	protected $_transmitter_name;
	protected $_transmitter_government_id;
	protected $_delegate_1_name;
	protected $_delegate_1_government_id;
	protected $_delegate_2_name;
	protected $_delegate_2_government_id;
	protected $_runner_1_name;
	protected $_runner_1_government_id;
	protected $_runner_2_name;
	protected $_runner_2_government_id;
	protected $_staff_lead_present;
	protected $_transmitter_present;
	protected $_delegate_1_present;
	protected $_delegate_2_present;
	protected $_runner_1_present;
	protected $_runner_2_present;


    public function __construct($entity)
    {
        $this->_entity = $entity;
    }

    public function get_entity ()
    {
        return $this->_entity;
    }

    /**
     * set_heading function
     *
     * @param string $heading
     * @return void
     */
    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    /**
     * get_heading function
     *
     * @return string
     */
    public function get_heading ()
    {
        return $this->_heading;
    }

    public function set_model ($model)
    {
        $this->_model = $model;
		$this->_id = $model->id;
		$this->_staff_lead_name = $model->staff_lead_name;
		$this->_staff_lead_government_id = $model->staff_lead_government_id;
		$this->_transmitter_name = $model->transmitter_name;
		$this->_transmitter_government_id = $model->transmitter_government_id;
		$this->_delegate_1_name = $model->delegate_1_name;
		$this->_delegate_1_government_id = $model->delegate_1_government_id;
		$this->_delegate_2_name = $model->delegate_2_name;
		$this->_delegate_2_government_id = $model->delegate_2_government_id;
		$this->_runner_1_name = $model->runner_1_name;
		$this->_runner_1_government_id = $model->runner_1_government_id;
		$this->_runner_2_name = $model->runner_2_name;
		$this->_runner_2_government_id = $model->runner_2_government_id;
		$this->_staff_lead_present = $model->staff_lead_present;
		$this->_transmitter_present = $model->transmitter_present;
		$this->_delegate_1_present = $model->delegate_1_present;
		$this->_delegate_2_present = $model->delegate_2_present;
		$this->_runner_1_present = $model->runner_1_present;
		$this->_runner_2_present = $model->runner_2_present;

    }

	public function staff_lead_present_mapping ()
	{
		return $this->_entity->staff_lead_present_mapping();

	}

	public function transmitter_present_mapping ()
	{
		return $this->_entity->transmitter_present_mapping();

	}

	public function delegate_1_present_mapping ()
	{
		return $this->_entity->delegate_1_present_mapping();

	}

	public function delegate_2_present_mapping ()
	{
		return $this->_entity->delegate_2_present_mapping();

	}

	public function runner_1_present_mapping ()
	{
		return $this->_entity->runner_1_present_mapping();

	}

	public function runner_2_present_mapping ()
	{
		return $this->_entity->runner_2_present_mapping();

	}

	public function get_staff_lead_name ()
	{
		return $this->_staff_lead_name;
	}

	public function set_staff_lead_name ($staff_lead_name)
	{
		$this->_staff_lead_name = $staff_lead_name;
	}

	public function get_staff_lead_government_id ()
	{
		return $this->_staff_lead_government_id;
	}

	public function set_staff_lead_government_id ($staff_lead_government_id)
	{
		$this->_staff_lead_government_id = $staff_lead_government_id;
	}

	public function get_transmitter_name ()
	{
		return $this->_transmitter_name;
	}

	public function set_transmitter_name ($transmitter_name)
	{
		$this->_transmitter_name = $transmitter_name;
	}

	public function get_transmitter_government_id ()
	{
		return $this->_transmitter_government_id;
	}

	public function set_transmitter_government_id ($transmitter_government_id)
	{
		$this->_transmitter_government_id = $transmitter_government_id;
	}

	public function get_delegate_1_name ()
	{
		return $this->_delegate_1_name;
	}

	public function set_delegate_1_name ($delegate_1_name)
	{
		$this->_delegate_1_name = $delegate_1_name;
	}

	public function get_delegate_1_government_id ()
	{
		return $this->_delegate_1_government_id;
	}

	public function set_delegate_1_government_id ($delegate_1_government_id)
	{
		$this->_delegate_1_government_id = $delegate_1_government_id;
	}

	public function get_delegate_2_name ()
	{
		return $this->_delegate_2_name;
	}

	public function set_delegate_2_name ($delegate_2_name)
	{
		$this->_delegate_2_name = $delegate_2_name;
	}

	public function get_delegate_2_government_id ()
	{
		return $this->_delegate_2_government_id;
	}

	public function set_delegate_2_government_id ($delegate_2_government_id)
	{
		$this->_delegate_2_government_id = $delegate_2_government_id;
	}

	public function get_runner_1_name ()
	{
		return $this->_runner_1_name;
	}

	public function set_runner_1_name ($runner_1_name)
	{
		$this->_runner_1_name = $runner_1_name;
	}

	public function get_runner_1_government_id ()
	{
		return $this->_runner_1_government_id;
	}

	public function set_runner_1_government_id ($runner_1_government_id)
	{
		$this->_runner_1_government_id = $runner_1_government_id;
	}

	public function get_runner_2_name ()
	{
		return $this->_runner_2_name;
	}

	public function set_runner_2_name ($runner_2_name)
	{
		$this->_runner_2_name = $runner_2_name;
	}

	public function get_runner_2_government_id ()
	{
		return $this->_runner_2_government_id;
	}

	public function set_runner_2_government_id ($runner_2_government_id)
	{
		$this->_runner_2_government_id = $runner_2_government_id;
	}

	public function get_staff_lead_present ()
	{
		return $this->_staff_lead_present;
	}

	public function set_staff_lead_present ($staff_lead_present)
	{
		$this->_staff_lead_present = $staff_lead_present;
	}

	public function get_transmitter_present ()
	{
		return $this->_transmitter_present;
	}

	public function set_transmitter_present ($transmitter_present)
	{
		$this->_transmitter_present = $transmitter_present;
	}

	public function get_delegate_1_present ()
	{
		return $this->_delegate_1_present;
	}

	public function set_delegate_1_present ($delegate_1_present)
	{
		$this->_delegate_1_present = $delegate_1_present;
	}

	public function get_delegate_2_present ()
	{
		return $this->_delegate_2_present;
	}

	public function set_delegate_2_present ($delegate_2_present)
	{
		$this->_delegate_2_present = $delegate_2_present;
	}

	public function get_runner_1_present ()
	{
		return $this->_runner_1_present;
	}

	public function set_runner_1_present ($runner_1_present)
	{
		$this->_runner_1_present = $runner_1_present;
	}

	public function get_runner_2_present ()
	{
		return $this->_runner_2_present;
	}

	public function set_runner_2_present ($runner_2_present)
	{
		$this->_runner_2_present = $runner_2_present;
	}

	public function get_id ()
	{
		return $this->_id;
	}

	public function set_id ($id)
	{
		$this->_id = $id;
	}

}