<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Personnel List Paginate View Model
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Personnel_grassroot_list_paginate_view_model
{
    protected $_library;
    protected $_base_url = '';
    protected $_heading = 'Personnel';
    protected $_total_rows = 0;
    protected $_format_layout = '';
    protected $_per_page = 10;
    protected $_page;
    protected $_num_links = 5;
    protected $_column = ['Colegio','Personal','Presente','Acción'];
    protected $_field_column = ['collegio','runner_2_present','runner_2_present',''];
    protected $_list = [];
    protected $_links = '';
    protected $_sort_base_url = '';
    protected $_order_by = '';
    protected $_sort = '';
    protected $_entity;

    public function __construct($entity, $library, $base_url)
    {
        $this->_entity = $entity;
        $this->_library = $library;
		$this->_base_url = str_replace('/0', '/', $base_url);
    }

    /**
     * set_total_rows function
     *
     * @param integer $total_rows
     * @return void
     */
    public function set_total_rows ($total_rows)
    {
        $this->_total_rows = $total_rows;
    }

    /**
     * set_per_page function
     *
     * @param integer $per_page
     * @return void
     */
    public function set_per_page ($per_page)
    {
        $this->_per_page = $per_page;
    }
    /**
     * format_layout function
     *
     * @param integer $_format_layout
     * @return void
     */
    public function set_format_layout ($_format_layout)
    {
        $this->_format_layout = $_format_layout;
    }

    /**
     * set_order_by function
     *
     * @param string $order_by
     * @return void
     */
    public function set_order_by ($order_by)
    {
        $this->_order_by = $order_by;
    }

    /**
     * set_sort function
     *
     * @param string $sort
     * @return void
     */
    public function set_sort ($sort)
    {
        $this->_sort = $sort;
    }

    /**
     * set_sort_base_url function
     *
     * @param string $sort_base_url
     * @return void
     */
    public function set_sort_base_url ($sort_base_url)
    {
        $this->_sort_base_url = $sort_base_url;
    }

    /**
     * set_page function
     *
     * @param integer $page
     * @return void
     */
    public function set_page ($page)
    {
        $this->_page = $page;
    }

    /**
     * set_list function
     *
     * @param mixed $list
     * @return void
     */
    public function set_list ($list)
    {
        $this->_list = $list;
    }

    /**
     * get_total_rows function
     *
     * @return integer
     */
    public function get_total_rows ()
    {
        return $this->_total_rows;
    }

    /**
     * get_format_layout function
     *
     * @return integer
     */
    public function get_format_layout ()
    {
        return $this->_format_layout;
    }

    /**
     * get_per_page function
     *
     * @return integer
     */
    public function get_per_page ()
    {
        return $this->_per_page;
    }

    /**
     * get_page function
     *
     * @return integer
     */
    public function get_page ()
    {
        return $this->_page;
    }

    /**
     * num_links function
     *
     * @return integer
     */
    public function get_num_links ()
    {
        return $this->_num_links;
    }

    /**
     * set_order_by function
     *
     */
    public function get_order_by ()
    {
        return $this->_order_by;
    }

    /**
     * get_field_column function
     *
     */
    public function get_field_column ()
    {
        return $this->_field_column;
    }

    /**
     * set_sort function
     *
     */
    public function get_sort ()
    {
        return $this->_sort;
    }

    /**
     * set_sort_base_url function
     *
     */
    public function get_sort_base_url ()
    {
        return $this->_sort_base_url;
    }

    /**
     * num_pages function
     *
     * @return integer
     */
    public function get_num_page ()
    {
        $num = ceil($this->_total_rows / $this->_per_page);
        return ($num > 0) ? (int) $num : 1;
    }

    public function image_or_file ($file)
    {
        $images = ['.jpg','.png', '.gif', '.jpeg', '.bmp'];
        $is_image = FALSE;
        if ($this->strposa($file, $images))
        {
            return "<div class='mkd-image-container'><img class='img-fluid' src='{$file}' onerror=\"if (this.src != '/uploads/placeholder.jpg') this.src = '/uploads/placeholder.jpg';\"/></div>";
        }

        return "<a href='{$file}' target='__blank'>{$file}</a>";
    }

    /**
     * Strpos for array
     *
     * @param string $haystack
     * @param array $needle
     * @return boolean
     */
    private function strposa($haystack, $needle)
    {
        foreach($needle as $query)
        {
            if(strpos($haystack, $query) !== FALSE)
            {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * get_list function
     *
     * @return mixed
     */
    public function get_list ()
    {
        $this->_library->initialize([
            'reuse_query_string' => TRUE,
            'base_url' => $this->_base_url,
            'total_rows' => $this->_total_rows,
            'per_page' => $this->_per_page,
            'num_links' => $this->_num_links,
            'full_tag_open' => '<ul class="pagination justify-content-end">',
            'full_tag_close' => '</ul>',
            'attributes' => ['class' => 'page-link'],
            'first_link' => FALSE,
            'last_link' => FALSE,
            'first_tag_open' => '<li class="page-item">',
            'first_tag_close' => '</li>',
            'prev_link' => '&laquo',
            'prev_tag_open' => '<li class="page-item">',
            'prev_tag_close' => '</li>',
            'next_link' => '&raquo',
            'next_tag_open' => '<li class="page-item">',
            'next_tag_close' => '</li>',
            'last_tag_open' => '<li class="page-item">',
            'last_tag_close' => '</li>',
            'cur_tag_open' => '<li class="page-item active"><a href="#" class="page-link">',
            'cur_tag_close' => '<span class="sr-only">(current)</span></a></li>',
            'num_tag_open' => '<li class="page-item">',
            'num_tag_close' => '</li>'
        ]);
        return $this->_list;
    }

    /**
     * get_links function
     *
     * @return mixed
     */
    public function get_links ()
    {
        $this->_links = $this->_library->create_links();
        return $this->_links;
    }

    /**
     * set_heading function
     *
     * @param string $heading
     * @return void
     */
    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    /**
     * get_heading function
     *
     * @return string
     */
    public function get_heading ()
    {
        return $this->_heading;
    }

    public function set_column ($column)
    {
        $this->_column = $column;
    }

    public function get_column ()
    {
        return $this->_column;
    }

	public function get_recinto ()
	{
		return $this->_recinto;
	}

	public function set_recinto ($recinto)
	{
		$this->_recinto = $recinto;
	}

	public function get_collegio ()
	{
		return $this->_collegio;
	}

	public function set_collegio ($collegio)
	{
		$this->_collegio = $collegio;
	}

	public function get_staff_lead_name ()
	{
		return $this->_staff_lead_name;
	}

	public function set_staff_lead_name ($staff_lead_name)
	{
		$this->_staff_lead_name = $staff_lead_name;
	}

	public function get_staff_lead_government_id ()
	{
		return $this->_staff_lead_government_id;
	}

	public function set_staff_lead_government_id ($staff_lead_government_id)
	{
		$this->_staff_lead_government_id = $staff_lead_government_id;
	}

	public function get_transmitter_name ()
	{
		return $this->_transmitter_name;
	}

	public function set_transmitter_name ($transmitter_name)
	{
		$this->_transmitter_name = $transmitter_name;
	}

	public function get_transmitter_government_id ()
	{
		return $this->_transmitter_government_id;
	}

	public function set_transmitter_government_id ($transmitter_government_id)
	{
		$this->_transmitter_government_id = $transmitter_government_id;
	}

	public function get_delegate_1_name ()
	{
		return $this->_delegate_1_name;
	}

	public function set_delegate_1_name ($delegate_1_name)
	{
		$this->_delegate_1_name = $delegate_1_name;
	}

	public function get_delegate_1_government_id ()
	{
		return $this->_delegate_1_government_id;
	}

	public function set_delegate_1_government_id ($delegate_1_government_id)
	{
		$this->_delegate_1_government_id = $delegate_1_government_id;
	}

	public function get_delegate_2_name ()
	{
		return $this->_delegate_2_name;
	}

	public function set_delegate_2_name ($delegate_2_name)
	{
		$this->_delegate_2_name = $delegate_2_name;
	}

	public function get_delegate_2_government_id ()
	{
		return $this->_delegate_2_government_id;
	}

	public function set_delegate_2_government_id ($delegate_2_government_id)
	{
		$this->_delegate_2_government_id = $delegate_2_government_id;
	}

	public function get_runner_1_name ()
	{
		return $this->_runner_1_name;
	}

	public function set_runner_1_name ($runner_1_name)
	{
		$this->_runner_1_name = $runner_1_name;
	}

	public function get_runner_1_government_id ()
	{
		return $this->_runner_1_government_id;
	}

	public function set_runner_1_government_id ($runner_1_government_id)
	{
		$this->_runner_1_government_id = $runner_1_government_id;
	}

	public function get_runner_2_name ()
	{
		return $this->_runner_2_name;
	}

	public function set_runner_2_name ($runner_2_name)
	{
		$this->_runner_2_name = $runner_2_name;
	}

	public function get_runner_2_government_id ()
	{
		return $this->_runner_2_government_id;
	}

	public function set_runner_2_government_id ($runner_2_government_id)
	{
		$this->_runner_2_government_id = $runner_2_government_id;
	}

	public function get_staff_lead_present ()
	{
		return $this->_staff_lead_present;
	}

	public function set_staff_lead_present ($staff_lead_present)
	{
		$this->_staff_lead_present = $staff_lead_present;
	}

	public function get_transmitter_present ()
	{
		return $this->_transmitter_present;
	}

	public function set_transmitter_present ($transmitter_present)
	{
		$this->_transmitter_present = $transmitter_present;
	}

	public function get_delegate_1_present ()
	{
		return $this->_delegate_1_present;
	}

	public function set_delegate_1_present ($delegate_1_present)
	{
		$this->_delegate_1_present = $delegate_1_present;
	}

	public function get_delegate_2_present ()
	{
		return $this->_delegate_2_present;
	}

	public function set_delegate_2_present ($delegate_2_present)
	{
		$this->_delegate_2_present = $delegate_2_present;
	}

	public function get_runner_1_present ()
	{
		return $this->_runner_1_present;
	}

	public function set_runner_1_present ($runner_1_present)
	{
		$this->_runner_1_present = $runner_1_present;
	}

	public function get_runner_2_present ()
	{
		return $this->_runner_2_present;
	}

	public function set_runner_2_present ($runner_2_present)
	{
		$this->_runner_2_present = $runner_2_present;
	}

	public function get_id ()
	{
		return $this->_id;
	}

	public function set_id ($id)
	{
		$this->_id = $id;
	}

	public function staff_lead_present_mapping ()
	{
		return $this->_entity->staff_lead_present_mapping();

	}

	public function transmitter_present_mapping ()
	{
		return $this->_entity->transmitter_present_mapping();

	}

	public function delegate_1_present_mapping ()
	{
		return $this->_entity->delegate_1_present_mapping();

	}

	public function delegate_2_present_mapping ()
	{
		return $this->_entity->delegate_2_present_mapping();

	}

	public function runner_1_present_mapping ()
	{
		return $this->_entity->runner_1_present_mapping();

	}

	public function runner_2_present_mapping ()
	{
		return $this->_entity->runner_2_present_mapping();

	}

	public function to_json ()
	{
		$list = $this->get_list();

		$clean_list = [];

		foreach ($list as $key => $value)
		{
			$list[$key]->staff_lead_present = $this->staff_lead_present_mapping()[$value->staff_lead_present];
			$list[$key]->transmitter_present = $this->transmitter_present_mapping()[$value->transmitter_present];
			$list[$key]->delegate_1_present = $this->delegate_1_present_mapping()[$value->delegate_1_present];
			$list[$key]->delegate_2_present = $this->delegate_2_present_mapping()[$value->delegate_2_present];
			$list[$key]->runner_1_present = $this->runner_1_present_mapping()[$value->runner_1_present];
			$list[$key]->runner_2_present = $this->runner_2_present_mapping()[$value->runner_2_present];
			$clean_list_entry = [];
			$clean_list_entry['recinto'] = $list[$key]->recinto;
			$clean_list_entry['collegio'] = $list[$key]->collegio;
			$clean_list_entry['staff_lead_name'] = $list[$key]->staff_lead_name;
			$clean_list_entry['staff_lead_government_id'] = $list[$key]->staff_lead_government_id;
			$clean_list_entry['transmitter_name'] = $list[$key]->transmitter_name;
			$clean_list_entry['transmitter_government_id'] = $list[$key]->transmitter_government_id;
			$clean_list_entry['delegate_1_name'] = $list[$key]->delegate_1_name;
			$clean_list_entry['delegate_1_government_id'] = $list[$key]->delegate_1_government_id;
			$clean_list_entry['delegate_2_name'] = $list[$key]->delegate_2_name;
			$clean_list_entry['delegate_2_government_id'] = $list[$key]->delegate_2_government_id;
			$clean_list_entry['runner_1_name'] = $list[$key]->runner_1_name;
			$clean_list_entry['runner_1_government_id'] = $list[$key]->runner_1_government_id;
			$clean_list_entry['runner_2_name'] = $list[$key]->runner_2_name;
			$clean_list_entry['runner_2_government_id'] = $list[$key]->runner_2_government_id;
			$clean_list_entry['staff_lead_present'] = $list[$key]->staff_lead_present;
			$clean_list_entry['transmitter_present'] = $list[$key]->transmitter_present;
			$clean_list_entry['delegate_1_present'] = $list[$key]->delegate_1_present;
			$clean_list_entry['delegate_2_present'] = $list[$key]->delegate_2_present;
			$clean_list_entry['runner_1_present'] = $list[$key]->runner_1_present;
			$clean_list_entry['runner_2_present'] = $list[$key]->runner_2_present;
			$clean_list[] = $clean_list_entry;
		}

		return [
			'page' => $this->get_page(),
			'num_page' => $this->get_num_page(),
			'num_item' => $this->get_total_rows(),
			'item' => $clean_list
		];
	}

}