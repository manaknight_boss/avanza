<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Final List Paginate View Model
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Final_grassroot_list_paginate_view_model
{
    protected $_library;
    protected $_base_url = '';
    protected $_heading = 'Final';
    protected $_total_rows = 0;
    protected $_format_layout = '';
    protected $_per_page = 10;
    protected $_page;
    protected $_num_links = 5;
    protected $_column = ['Colegio','Archivo','Fecha','Acción'];
    protected $_field_column = ['table','file','created_at',''];
    protected $_list = [];
    protected $_links = '';
    protected $_sort_base_url = '';
    protected $_order_by = '';
    protected $_sort = '';
    protected $_entity;

    public function __construct($entity, $library, $base_url)
    {
        $this->_entity = $entity;
        $this->_library = $library;
		$this->_base_url = str_replace('/0', '/', $base_url);
    }

    /**
     * set_total_rows function
     *
     * @param integer $total_rows
     * @return void
     */
    public function set_total_rows ($total_rows)
    {
        $this->_total_rows = $total_rows;
    }

    /**
     * set_per_page function
     *
     * @param integer $per_page
     * @return void
     */
    public function set_per_page ($per_page)
    {
        $this->_per_page = $per_page;
    }
    /**
     * format_layout function
     *
     * @param integer $_format_layout
     * @return void
     */
    public function set_format_layout ($_format_layout)
    {
        $this->_format_layout = $_format_layout;
    }

    /**
     * set_order_by function
     *
     * @param string $order_by
     * @return void
     */
    public function set_order_by ($order_by)
    {
        $this->_order_by = $order_by;
    }

    /**
     * set_sort function
     *
     * @param string $sort
     * @return void
     */
    public function set_sort ($sort)
    {
        $this->_sort = $sort;
    }

    /**
     * set_sort_base_url function
     *
     * @param string $sort_base_url
     * @return void
     */
    public function set_sort_base_url ($sort_base_url)
    {
        $this->_sort_base_url = $sort_base_url;
    }

    /**
     * set_page function
     *
     * @param integer $page
     * @return void
     */
    public function set_page ($page)
    {
        $this->_page = $page;
    }

    /**
     * set_list function
     *
     * @param mixed $list
     * @return void
     */
    public function set_list ($list)
    {
        $this->_list = $list;
    }

    /**
     * get_total_rows function
     *
     * @return integer
     */
    public function get_total_rows ()
    {
        return $this->_total_rows;
    }

    /**
     * get_format_layout function
     *
     * @return integer
     */
    public function get_format_layout ()
    {
        return $this->_format_layout;
    }

    /**
     * get_per_page function
     *
     * @return integer
     */
    public function get_per_page ()
    {
        return $this->_per_page;
    }

    /**
     * get_page function
     *
     * @return integer
     */
    public function get_page ()
    {
        return $this->_page;
    }

    /**
     * num_links function
     *
     * @return integer
     */
    public function get_num_links ()
    {
        return $this->_num_links;
    }

    /**
     * set_order_by function
     *
     */
    public function get_order_by ()
    {
        return $this->_order_by;
    }

    /**
     * get_field_column function
     *
     */
    public function get_field_column ()
    {
        return $this->_field_column;
    }

    /**
     * set_sort function
     *
     */
    public function get_sort ()
    {
        return $this->_sort;
    }

    /**
     * set_sort_base_url function
     *
     */
    public function get_sort_base_url ()
    {
        return $this->_sort_base_url;
    }

    /**
     * num_pages function
     *
     * @return integer
     */
    public function get_num_page ()
    {
        $num = ceil($this->_total_rows / $this->_per_page);
        return ($num > 0) ? (int) $num : 1;
    }

    public function image_or_file ($file)
    {
        $images = ['.jpg','.png', '.gif', '.jpeg', '.bmp'];
        $is_image = FALSE;
        if ($this->strposa($file, $images))
        {
            return "<div class='mkd-image-container'><img class='img-fluid' src='{$file}' onerror=\"if (this.src != '/uploads/placeholder.jpg') this.src = '/uploads/placeholder.jpg';\"/></div>";
        }

        return "<a href='{$file}' target='__blank'>{$file}</a>";
    }

    /**
     * Strpos for array
     *
     * @param string $haystack
     * @param array $needle
     * @return boolean
     */
    private function strposa($haystack, $needle)
    {
        foreach($needle as $query)
        {
            if(strpos($haystack, $query) !== FALSE)
            {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * get_list function
     *
     * @return mixed
     */
    public function get_list ()
    {
        $this->_library->initialize([
            'reuse_query_string' => TRUE,
            'base_url' => $this->_base_url,
            'total_rows' => $this->_total_rows,
            'per_page' => $this->_per_page,
            'num_links' => $this->_num_links,
            'full_tag_open' => '<ul class="pagination justify-content-end">',
            'full_tag_close' => '</ul>',
            'attributes' => ['class' => 'page-link'],
            'first_link' => FALSE,
            'last_link' => FALSE,
            'first_tag_open' => '<li class="page-item">',
            'first_tag_close' => '</li>',
            'prev_link' => '&laquo',
            'prev_tag_open' => '<li class="page-item">',
            'prev_tag_close' => '</li>',
            'next_link' => '&raquo',
            'next_tag_open' => '<li class="page-item">',
            'next_tag_close' => '</li>',
            'last_tag_open' => '<li class="page-item">',
            'last_tag_close' => '</li>',
            'cur_tag_open' => '<li class="page-item active"><a href="#" class="page-link">',
            'cur_tag_close' => '<span class="sr-only">(current)</span></a></li>',
            'num_tag_open' => '<li class="page-item">',
            'num_tag_close' => '</li>'
        ]);
        return $this->_list;
    }

    /**
     * get_links function
     *
     * @return mixed
     */
    public function get_links ()
    {
        $this->_links = $this->_library->create_links();
        return $this->_links;
    }

    /**
     * set_heading function
     *
     * @param string $heading
     * @return void
     */
    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    /**
     * get_heading function
     *
     * @return string
     */
    public function get_heading ()
    {
        return $this->_heading;
    }

    public function set_column ($column)
    {
        $this->_column = $column;
    }

    public function get_column ()
    {
        return $this->_column;
    }

	public function get_id ()
	{
		return $this->_id;
	}

	public function set_id ($id)
	{
		$this->_id = $id;
	}

	public function to_json ()
	{
		$list = $this->get_list();

		$clean_list = [];

		foreach ($list as $key => $value)
		{
			$clean_list_entry = [];
			$clean_list_entry['id'] = $list[$key]->id;
			$clean_list_entry['file'] = $list[$key]->file;
			$clean_list_entry['table'] = $list[$key]->table;
			$clean_list_entry['created_at'] = $list[$key]->created_at;
			$clean_list_entry['updated_at'] = $list[$key]->updated_at;
			$clean_list_entry['mayor_1'] = $list[$key]->mayor_1;
			$clean_list_entry['mayor_count_1'] = $list[$key]->mayor_count_1;
			$clean_list_entry['mayor_2'] = $list[$key]->mayor_2;
			$clean_list_entry['mayor_count_2'] = $list[$key]->mayor_count_2;
			$clean_list_entry['mayor_3'] = $list[$key]->mayor_3;
			$clean_list_entry['mayor_count_3'] = $list[$key]->mayor_count_3;
			$clean_list_entry['mayor_4'] = $list[$key]->mayor_4;
			$clean_list_entry['mayor_count_4'] = $list[$key]->mayor_count_4;
			$clean_list_entry['mayor_5'] = $list[$key]->mayor_5;
			$clean_list_entry['mayor_count_5'] = $list[$key]->mayor_count_5;
			$clean_list_entry['mayor_6'] = $list[$key]->mayor_6;
			$clean_list_entry['mayor_count_6'] = $list[$key]->mayor_count_6;
			$clean_list_entry['mayor_7'] = $list[$key]->mayor_7;
			$clean_list_entry['mayor_count_7'] = $list[$key]->mayor_count_7;
			$clean_list_entry['mayor_8'] = $list[$key]->mayor_8;
			$clean_list_entry['mayor_count_8'] = $list[$key]->mayor_count_8;
			$clean_list_entry['mayor_9'] = $list[$key]->mayor_9;
			$clean_list_entry['mayor_count_9'] = $list[$key]->mayor_count_9;
			$clean_list_entry['mayor_10'] = $list[$key]->mayor_10;
			$clean_list_entry['mayor_count_10'] = $list[$key]->mayor_count_10;
			$clean_list_entry['senator_1'] = $list[$key]->senator_1;
			$clean_list_entry['senator_count_1'] = $list[$key]->senator_count_1;
			$clean_list_entry['senator_2'] = $list[$key]->senator_2;
			$clean_list_entry['senator_count_2'] = $list[$key]->senator_count_2;
			$clean_list_entry['senator_3'] = $list[$key]->senator_3;
			$clean_list_entry['senator_count_3'] = $list[$key]->senator_count_3;
			$clean_list_entry['senator_4'] = $list[$key]->senator_4;
			$clean_list_entry['senator_count_4'] = $list[$key]->senator_count_4;
			$clean_list_entry['senator_5'] = $list[$key]->senator_5;
			$clean_list_entry['senator_count_5'] = $list[$key]->senator_count_5;
			$clean_list_entry['senator_6'] = $list[$key]->senator_6;
			$clean_list_entry['senator_count_6'] = $list[$key]->senator_count_6;
			$clean_list_entry['senator_7'] = $list[$key]->senator_7;
			$clean_list_entry['senator_count_7'] = $list[$key]->senator_count_7;
			$clean_list_entry['senator_8'] = $list[$key]->senator_8;
			$clean_list_entry['senator_count_8'] = $list[$key]->senator_count_8;
			$clean_list_entry['senator_9'] = $list[$key]->senator_9;
			$clean_list_entry['senator_count_9'] = $list[$key]->senator_count_9;
			$clean_list_entry['senator_10'] = $list[$key]->senator_10;
			$clean_list_entry['senator_count_10'] = $list[$key]->senator_count_10;
			$clean_list_entry['regional_1'] = $list[$key]->regional_1;
			$clean_list_entry['regional_count_1'] = $list[$key]->regional_count_1;
			$clean_list_entry['regional_2'] = $list[$key]->regional_2;
			$clean_list_entry['regional_count_2'] = $list[$key]->regional_count_2;
			$clean_list_entry['regional_3'] = $list[$key]->regional_3;
			$clean_list_entry['regional_count_3'] = $list[$key]->regional_count_3;
			$clean_list_entry['regional_4'] = $list[$key]->regional_4;
			$clean_list_entry['regional_count_4'] = $list[$key]->regional_count_4;
			$clean_list_entry['regional_5'] = $list[$key]->regional_5;
			$clean_list_entry['regional_count_5'] = $list[$key]->regional_count_5;
			$clean_list_entry['regional_6'] = $list[$key]->regional_6;
			$clean_list_entry['regional_count_6'] = $list[$key]->regional_count_6;
			$clean_list_entry['regional_7'] = $list[$key]->regional_7;
			$clean_list_entry['regional_count_7'] = $list[$key]->regional_count_7;
			$clean_list_entry['regional_8'] = $list[$key]->regional_8;
			$clean_list_entry['regional_count_8'] = $list[$key]->regional_count_8;
			$clean_list_entry['regional_9'] = $list[$key]->regional_9;
			$clean_list_entry['regional_count_9'] = $list[$key]->regional_count_9;
			$clean_list_entry['regional_10'] = $list[$key]->regional_10;
			$clean_list_entry['regional_count_10'] = $list[$key]->regional_count_10;
			$clean_list_entry['regional_11'] = $list[$key]->regional_11;
			$clean_list_entry['regional_count_11'] = $list[$key]->regional_count_11;
			$clean_list_entry['regional_12'] = $list[$key]->regional_12;
			$clean_list_entry['regional_count_12'] = $list[$key]->regional_count_12;
			$clean_list_entry['regional_13'] = $list[$key]->regional_13;
			$clean_list_entry['regional_count_13'] = $list[$key]->regional_count_13;
			$clean_list_entry['regional_14'] = $list[$key]->regional_14;
			$clean_list_entry['regional_count_14'] = $list[$key]->regional_count_14;
			$clean_list_entry['regional_15'] = $list[$key]->regional_15;
			$clean_list_entry['regional_count_15'] = $list[$key]->regional_count_15;
			$clean_list[] = $clean_list_entry;
		}

		return [
			'page' => $this->get_page(),
			'num_page' => $this->get_num_page(),
			'num_item' => $this->get_total_rows(),
			'item' => $clean_list
		];
	}

}