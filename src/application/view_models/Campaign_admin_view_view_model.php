<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * View Campaign View Model
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Campaign_admin_view_view_model
{
    protected $_entity;
    protected $_model;
	protected $_id;
	protected $_name;
	protected $_type;
	protected $_status;
	protected $_circunscripcion;
	protected $_age;
	protected $_recinto;
	protected $_source_id;
	protected $_content;
	protected $_college;
	protected $_gender;
	protected $_married_status;
	protected $_total;


    public function __construct($entity)
    {
        $this->_entity = $entity;
    }

    public function get_entity ()
    {
        return $this->_entity;
    }

    /**
     * set_heading function
     *
     * @param string $heading
     * @return void
     */
    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    /**
     * get_heading function
     *
     * @return string
     */
    public function get_heading ()
    {
        return $this->_heading;
    }

    public function set_model ($model)
    {
        $this->_model = $model;
		$this->_id = $model->id;
		$this->_name = $model->name;
		$this->_type = $model->type;
		$this->_status = $model->status;
		$this->_circunscripcion = $model->circunscripcion;
		$this->_age = $model->age;
		$this->_recinto = $model->recinto;
		$this->_source_id = $model->source_id;
		$this->_content = $model->content;
		$this->_college = $model->college;
		$this->_gender = $model->gender;
		$this->_married_status = $model->married_status;
		$this->_total = $model->total;

    }

	public function source_id_mapping ()
	{
		return $this->_entity->source_id_mapping();

	}

	public function type_mapping ()
	{
		return $this->_entity->type_mapping();

	}

	public function status_mapping ()
	{
		return $this->_entity->status_mapping();

	}

	public function circunscripcion_mapping ()
	{
		return $this->_entity->circunscripcion_mapping();

	}

	public function gender_mapping ()
	{
		return $this->_entity->gender_mapping();

	}

	public function married_status_mapping ()
	{
		return $this->_entity->married_status_mapping();

	}

	public function age_mapping ()
	{
		return $this->_entity->age_mapping();

	}

	public function get_id ()
	{
		return $this->_id;
	}

	public function set_id ($id)
	{
		$this->_id = $id;
	}

	public function get_name ()
	{
		return $this->_name;
	}

	public function set_name ($name)
	{
		$this->_name = $name;
	}

	public function get_type ()
	{
		return $this->_type;
	}

	public function set_type ($type)
	{
		$this->_type = $type;
	}

	public function get_status ()
	{
		return $this->_status;
	}

	public function set_status ($status)
	{
		$this->_status = $status;
	}

	public function get_circunscripcion ()
	{
		return $this->_circunscripcion;
	}

	public function set_circunscripcion ($circunscripcion)
	{
		$this->_circunscripcion = $circunscripcion;
	}

	public function get_age ()
	{
		return $this->_age;
	}

	public function set_age ($age)
	{
		$this->_age = $age;
	}

	public function get_recinto ()
	{
		return $this->_recinto;
	}

	public function set_recinto ($recinto)
	{
		$this->_recinto = $recinto;
	}

	public function get_source_id ()
	{
		return $this->_source_id;
	}

	public function set_source_id ($source_id)
	{
		$this->_source_id = $source_id;
	}

	public function get_content ()
	{
		return $this->_content;
	}

	public function set_content ($content)
	{
		$this->_content = $content;
	}

	public function get_college ()
	{
		return $this->_college;
	}

	public function set_college ($college)
	{
		$this->_college = $college;
	}

	public function get_gender ()
	{
		return $this->_gender;
	}

	public function set_gender ($gender)
	{
		$this->_gender = $gender;
	}

	public function get_married_status ()
	{
		return $this->_married_status;
	}

	public function set_married_status ($married_status)
	{
		$this->_married_status = $married_status;
	}

	public function get_total ()
	{
		return $this->_total;
	}

	public function set_total ($total)
	{
		$this->_total = $total;
	}

	public function to_json ()
	{
		return [
		'id' => $this->get_id(),
		'name' => $this->get_name(),
		'type' => $this->get_type(),
		'status' => $this->get_status(),
		'circunscripcion' => $this->get_circunscripcion(),
		'age' => $this->get_age(),
		'recinto' => $this->get_recinto(),
		'source_id' => $this->get_source_id(),
		'content' => $this->get_content(),
		'college' => $this->get_college(),
		'gender' => $this->get_gender(),
		'married_status' => $this->get_married_status(),
		'total' => $this->get_total(),
		];
	}

}