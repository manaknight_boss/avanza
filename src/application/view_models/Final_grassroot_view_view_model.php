<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * View Final View Model
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Final_grassroot_view_view_model
{
    protected $_entity;
    protected $_model;
	protected $_id;
	protected $_table;
	protected $_file;
	protected $_file_id;
	protected $_created_at;
	protected $_mayor_1;
	protected $_mayor_count_1;
	protected $_mayor_2;
	protected $_mayor_count_2;
	protected $_mayor_3;
	protected $_mayor_count_3;
	protected $_mayor_4;
	protected $_mayor_count_4;
	protected $_mayor_5;
	protected $_mayor_count_5;
	protected $_mayor_6;
	protected $_mayor_count_6;
	protected $_mayor_7;
	protected $_mayor_count_7;
	protected $_mayor_8;
	protected $_mayor_count_8;
	protected $_mayor_9;
	protected $_mayor_count_9;
	protected $_mayor_10;
	protected $_mayor_count_10;
	protected $_senator_1;
	protected $_senator_count_1;
	protected $_senator_2;
	protected $_senator_count_2;
	protected $_senator_3;
	protected $_senator_count_3;
	protected $_senator_4;
	protected $_senator_count_4;
	protected $_senator_5;
	protected $_senator_count_5;
	protected $_senator_6;
	protected $_senator_count_6;
	protected $_senator_7;
	protected $_senator_count_7;
	protected $_senator_8;
	protected $_senator_count_8;
	protected $_senator_9;
	protected $_senator_count_9;
	protected $_senator_10;
	protected $_senator_count_10;
	protected $_regional_1;
	protected $_regional_count_1;
	protected $_regional_2;
	protected $_regional_count_2;
	protected $_regional_3;
	protected $_regional_count_3;
	protected $_regional_4;
	protected $_regional_count_4;
	protected $_regional_5;
	protected $_regional_count_5;
	protected $_regional_6;
	protected $_regional_count_6;
	protected $_regional_7;
	protected $_regional_count_7;
	protected $_regional_8;
	protected $_regional_count_8;
	protected $_regional_9;
	protected $_regional_count_9;
	protected $_regional_10;
	protected $_regional_count_10;
	protected $_regional_11;
	protected $_regional_count_11;
	protected $_regional_12;
	protected $_regional_count_12;
	protected $_regional_13;
	protected $_regional_count_13;
	protected $_regional_14;
	protected $_regional_count_14;
	protected $_regional_15;
	protected $_regional_count_15;


    public function __construct($entity)
    {
        $this->_entity = $entity;
    }

    public function get_entity ()
    {
        return $this->_entity;
    }

    /**
     * set_heading function
     *
     * @param string $heading
     * @return void
     */
    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    /**
     * get_heading function
     *
     * @return string
     */
    public function get_heading ()
    {
        return $this->_heading;
    }

    public function set_model ($model)
    {
        $this->_model = $model;
		$this->_id = $model->id;
		$this->_table = $model->table;
		$this->_file = $model->file;
		$this->_created_at = $model->created_at;
		$this->_mayor_1 = $model->mayor_1;
		$this->_mayor_count_1 = $model->mayor_count_1;
		$this->_mayor_2 = $model->mayor_2;
		$this->_mayor_count_2 = $model->mayor_count_2;
		$this->_mayor_3 = $model->mayor_3;
		$this->_mayor_count_3 = $model->mayor_count_3;
		$this->_mayor_4 = $model->mayor_4;
		$this->_mayor_count_4 = $model->mayor_count_4;
		$this->_mayor_5 = $model->mayor_5;
		$this->_mayor_count_5 = $model->mayor_count_5;
		$this->_mayor_6 = $model->mayor_6;
		$this->_mayor_count_6 = $model->mayor_count_6;
		$this->_mayor_7 = $model->mayor_7;
		$this->_mayor_count_7 = $model->mayor_count_7;
		$this->_mayor_8 = $model->mayor_8;
		$this->_mayor_count_8 = $model->mayor_count_8;
		$this->_mayor_9 = $model->mayor_9;
		$this->_mayor_count_9 = $model->mayor_count_9;
		$this->_mayor_10 = $model->mayor_10;
		$this->_mayor_count_10 = $model->mayor_count_10;
		$this->_senator_1 = $model->senator_1;
		$this->_senator_count_1 = $model->senator_count_1;
		$this->_senator_2 = $model->senator_2;
		$this->_senator_count_2 = $model->senator_count_2;
		$this->_senator_3 = $model->senator_3;
		$this->_senator_count_3 = $model->senator_count_3;
		$this->_senator_4 = $model->senator_4;
		$this->_senator_count_4 = $model->senator_count_4;
		$this->_senator_5 = $model->senator_5;
		$this->_senator_count_5 = $model->senator_count_5;
		$this->_senator_6 = $model->senator_6;
		$this->_senator_count_6 = $model->senator_count_6;
		$this->_senator_7 = $model->senator_7;
		$this->_senator_count_7 = $model->senator_count_7;
		$this->_senator_8 = $model->senator_8;
		$this->_senator_count_8 = $model->senator_count_8;
		$this->_senator_9 = $model->senator_9;
		$this->_senator_count_9 = $model->senator_count_9;
		$this->_senator_10 = $model->senator_10;
		$this->_senator_count_10 = $model->senator_count_10;
		$this->_regional_1 = $model->regional_1;
		$this->_regional_count_1 = $model->regional_count_1;
		$this->_regional_2 = $model->regional_2;
		$this->_regional_count_2 = $model->regional_count_2;
		$this->_regional_3 = $model->regional_3;
		$this->_regional_count_3 = $model->regional_count_3;
		$this->_regional_4 = $model->regional_4;
		$this->_regional_count_4 = $model->regional_count_4;
		$this->_regional_5 = $model->regional_5;
		$this->_regional_count_5 = $model->regional_count_5;
		$this->_regional_6 = $model->regional_6;
		$this->_regional_count_6 = $model->regional_count_6;
		$this->_regional_7 = $model->regional_7;
		$this->_regional_count_7 = $model->regional_count_7;
		$this->_regional_8 = $model->regional_8;
		$this->_regional_count_8 = $model->regional_count_8;
		$this->_regional_9 = $model->regional_9;
		$this->_regional_count_9 = $model->regional_count_9;
		$this->_regional_10 = $model->regional_10;
		$this->_regional_count_10 = $model->regional_count_10;
		$this->_regional_11 = $model->regional_11;
		$this->_regional_count_11 = $model->regional_count_11;
		$this->_regional_12 = $model->regional_12;
		$this->_regional_count_12 = $model->regional_count_12;
		$this->_regional_13 = $model->regional_13;
		$this->_regional_count_13 = $model->regional_count_13;
		$this->_regional_14 = $model->regional_14;
		$this->_regional_count_14 = $model->regional_count_14;
		$this->_regional_15 = $model->regional_15;
		$this->_regional_count_15 = $model->regional_count_15;

    }

	public function get_table ()
	{
		return $this->_table;
	}

	public function set_table ($table)
	{
		$this->_table = $table;
	}

	public function get_file ()
	{
		return $this->_file;
	}

	public function set_file ($file)
	{
		$this->_file = $file;
	}

	public function get_file_id ()
	{
		return $this->_file_id;
	}

	public function set_file_id ($file)
	{
		$this->_file_id = $file;
	}

	public function get_created_at ()
	{
		return $this->_created_at;
	}

	public function set_created_at ($created_at)
	{
		$this->_created_at = $created_at;
	}

	public function get_mayor_1 ()
	{
		return $this->_mayor_1;
	}

	public function set_mayor_1 ($mayor_1)
	{
		$this->_mayor_1 = $mayor_1;
	}

	public function get_mayor_count_1 ()
	{
		return $this->_mayor_count_1;
	}

	public function set_mayor_count_1 ($mayor_count_1)
	{
		$this->_mayor_count_1 = $mayor_count_1;
	}

	public function get_mayor_2 ()
	{
		return $this->_mayor_2;
	}

	public function set_mayor_2 ($mayor_2)
	{
		$this->_mayor_2 = $mayor_2;
	}

	public function get_mayor_count_2 ()
	{
		return $this->_mayor_count_2;
	}

	public function set_mayor_count_2 ($mayor_count_2)
	{
		$this->_mayor_count_2 = $mayor_count_2;
	}

	public function get_mayor_3 ()
	{
		return $this->_mayor_3;
	}

	public function set_mayor_3 ($mayor_3)
	{
		$this->_mayor_3 = $mayor_3;
	}

	public function get_mayor_count_3 ()
	{
		return $this->_mayor_count_3;
	}

	public function set_mayor_count_3 ($mayor_count_3)
	{
		$this->_mayor_count_3 = $mayor_count_3;
	}

	public function get_mayor_4 ()
	{
		return $this->_mayor_4;
	}

	public function set_mayor_4 ($mayor_4)
	{
		$this->_mayor_4 = $mayor_4;
	}

	public function get_mayor_count_4 ()
	{
		return $this->_mayor_count_4;
	}

	public function set_mayor_count_4 ($mayor_count_4)
	{
		$this->_mayor_count_4 = $mayor_count_4;
	}

	public function get_mayor_5 ()
	{
		return $this->_mayor_5;
	}

	public function set_mayor_5 ($mayor_5)
	{
		$this->_mayor_5 = $mayor_5;
	}

	public function get_mayor_count_5 ()
	{
		return $this->_mayor_count_5;
	}

	public function set_mayor_count_5 ($mayor_count_5)
	{
		$this->_mayor_count_5 = $mayor_count_5;
	}

	public function get_mayor_6 ()
	{
		return $this->_mayor_6;
	}

	public function set_mayor_6 ($mayor_6)
	{
		$this->_mayor_6 = $mayor_6;
	}

	public function get_mayor_count_6 ()
	{
		return $this->_mayor_count_6;
	}

	public function set_mayor_count_6 ($mayor_count_6)
	{
		$this->_mayor_count_6 = $mayor_count_6;
	}

	public function get_mayor_7 ()
	{
		return $this->_mayor_7;
	}

	public function set_mayor_7 ($mayor_7)
	{
		$this->_mayor_7 = $mayor_7;
	}

	public function get_mayor_count_7 ()
	{
		return $this->_mayor_count_7;
	}

	public function set_mayor_count_7 ($mayor_count_7)
	{
		$this->_mayor_count_7 = $mayor_count_7;
	}

	public function get_mayor_8 ()
	{
		return $this->_mayor_8;
	}

	public function set_mayor_8 ($mayor_8)
	{
		$this->_mayor_8 = $mayor_8;
	}

	public function get_mayor_count_8 ()
	{
		return $this->_mayor_count_8;
	}

	public function set_mayor_count_8 ($mayor_count_8)
	{
		$this->_mayor_count_8 = $mayor_count_8;
	}

	public function get_mayor_9 ()
	{
		return $this->_mayor_9;
	}

	public function set_mayor_9 ($mayor_9)
	{
		$this->_mayor_9 = $mayor_9;
	}

	public function get_mayor_count_9 ()
	{
		return $this->_mayor_count_9;
	}

	public function set_mayor_count_9 ($mayor_count_9)
	{
		$this->_mayor_count_9 = $mayor_count_9;
	}

	public function get_mayor_10 ()
	{
		return $this->_mayor_10;
	}

	public function set_mayor_10 ($mayor_10)
	{
		$this->_mayor_10 = $mayor_10;
	}

	public function get_mayor_count_10 ()
	{
		return $this->_mayor_count_10;
	}

	public function set_mayor_count_10 ($mayor_count_10)
	{
		$this->_mayor_count_10 = $mayor_count_10;
	}

	public function get_senator_1 ()
	{
		return $this->_senator_1;
	}

	public function set_senator_1 ($senator_1)
	{
		$this->_senator_1 = $senator_1;
	}

	public function get_senator_count_1 ()
	{
		return $this->_senator_count_1;
	}

	public function set_senator_count_1 ($senator_count_1)
	{
		$this->_senator_count_1 = $senator_count_1;
	}

	public function get_senator_2 ()
	{
		return $this->_senator_2;
	}

	public function set_senator_2 ($senator_2)
	{
		$this->_senator_2 = $senator_2;
	}

	public function get_senator_count_2 ()
	{
		return $this->_senator_count_2;
	}

	public function set_senator_count_2 ($senator_count_2)
	{
		$this->_senator_count_2 = $senator_count_2;
	}

	public function get_senator_3 ()
	{
		return $this->_senator_3;
	}

	public function set_senator_3 ($senator_3)
	{
		$this->_senator_3 = $senator_3;
	}

	public function get_senator_count_3 ()
	{
		return $this->_senator_count_3;
	}

	public function set_senator_count_3 ($senator_count_3)
	{
		$this->_senator_count_3 = $senator_count_3;
	}

	public function get_senator_4 ()
	{
		return $this->_senator_4;
	}

	public function set_senator_4 ($senator_4)
	{
		$this->_senator_4 = $senator_4;
	}

	public function get_senator_count_4 ()
	{
		return $this->_senator_count_4;
	}

	public function set_senator_count_4 ($senator_count_4)
	{
		$this->_senator_count_4 = $senator_count_4;
	}

	public function get_senator_5 ()
	{
		return $this->_senator_5;
	}

	public function set_senator_5 ($senator_5)
	{
		$this->_senator_5 = $senator_5;
	}

	public function get_senator_count_5 ()
	{
		return $this->_senator_count_5;
	}

	public function set_senator_count_5 ($senator_count_5)
	{
		$this->_senator_count_5 = $senator_count_5;
	}

	public function get_senator_6 ()
	{
		return $this->_senator_6;
	}

	public function set_senator_6 ($senator_6)
	{
		$this->_senator_6 = $senator_6;
	}

	public function get_senator_count_6 ()
	{
		return $this->_senator_count_6;
	}

	public function set_senator_count_6 ($senator_count_6)
	{
		$this->_senator_count_6 = $senator_count_6;
	}

	public function get_senator_7 ()
	{
		return $this->_senator_7;
	}

	public function set_senator_7 ($senator_7)
	{
		$this->_senator_7 = $senator_7;
	}

	public function get_senator_count_7 ()
	{
		return $this->_senator_count_7;
	}

	public function set_senator_count_7 ($senator_count_7)
	{
		$this->_senator_count_7 = $senator_count_7;
	}

	public function get_senator_8 ()
	{
		return $this->_senator_8;
	}

	public function set_senator_8 ($senator_8)
	{
		$this->_senator_8 = $senator_8;
	}

	public function get_senator_count_8 ()
	{
		return $this->_senator_count_8;
	}

	public function set_senator_count_8 ($senator_count_8)
	{
		$this->_senator_count_8 = $senator_count_8;
	}

	public function get_senator_9 ()
	{
		return $this->_senator_9;
	}

	public function set_senator_9 ($senator_9)
	{
		$this->_senator_9 = $senator_9;
	}

	public function get_senator_count_9 ()
	{
		return $this->_senator_count_9;
	}

	public function set_senator_count_9 ($senator_count_9)
	{
		$this->_senator_count_9 = $senator_count_9;
	}

	public function get_senator_10 ()
	{
		return $this->_senator_10;
	}

	public function set_senator_10 ($senator_10)
	{
		$this->_senator_10 = $senator_10;
	}

	public function get_senator_count_10 ()
	{
		return $this->_senator_count_10;
	}

	public function set_senator_count_10 ($senator_count_10)
	{
		$this->_senator_count_10 = $senator_count_10;
	}

	public function get_regional_1 ()
	{
		return $this->_regional_1;
	}

	public function set_regional_1 ($regional_1)
	{
		$this->_regional_1 = $regional_1;
	}

	public function get_regional_count_1 ()
	{
		return $this->_regional_count_1;
	}

	public function set_regional_count_1 ($regional_count_1)
	{
		$this->_regional_count_1 = $regional_count_1;
	}

	public function get_regional_2 ()
	{
		return $this->_regional_2;
	}

	public function set_regional_2 ($regional_2)
	{
		$this->_regional_2 = $regional_2;
	}

	public function get_regional_count_2 ()
	{
		return $this->_regional_count_2;
	}

	public function set_regional_count_2 ($regional_count_2)
	{
		$this->_regional_count_2 = $regional_count_2;
	}

	public function get_regional_3 ()
	{
		return $this->_regional_3;
	}

	public function set_regional_3 ($regional_3)
	{
		$this->_regional_3 = $regional_3;
	}

	public function get_regional_count_3 ()
	{
		return $this->_regional_count_3;
	}

	public function set_regional_count_3 ($regional_count_3)
	{
		$this->_regional_count_3 = $regional_count_3;
	}

	public function get_regional_4 ()
	{
		return $this->_regional_4;
	}

	public function set_regional_4 ($regional_4)
	{
		$this->_regional_4 = $regional_4;
	}

	public function get_regional_count_4 ()
	{
		return $this->_regional_count_4;
	}

	public function set_regional_count_4 ($regional_count_4)
	{
		$this->_regional_count_4 = $regional_count_4;
	}

	public function get_regional_5 ()
	{
		return $this->_regional_5;
	}

	public function set_regional_5 ($regional_5)
	{
		$this->_regional_5 = $regional_5;
	}

	public function get_regional_count_5 ()
	{
		return $this->_regional_count_5;
	}

	public function set_regional_count_5 ($regional_count_5)
	{
		$this->_regional_count_5 = $regional_count_5;
	}

	public function get_regional_6 ()
	{
		return $this->_regional_6;
	}

	public function set_regional_6 ($regional_6)
	{
		$this->_regional_6 = $regional_6;
	}

	public function get_regional_count_6 ()
	{
		return $this->_regional_count_6;
	}

	public function set_regional_count_6 ($regional_count_6)
	{
		$this->_regional_count_6 = $regional_count_6;
	}

	public function get_regional_7 ()
	{
		return $this->_regional_7;
	}

	public function set_regional_7 ($regional_7)
	{
		$this->_regional_7 = $regional_7;
	}

	public function get_regional_count_7 ()
	{
		return $this->_regional_count_7;
	}

	public function set_regional_count_7 ($regional_count_7)
	{
		$this->_regional_count_7 = $regional_count_7;
	}

	public function get_regional_8 ()
	{
		return $this->_regional_8;
	}

	public function set_regional_8 ($regional_8)
	{
		$this->_regional_8 = $regional_8;
	}

	public function get_regional_count_8 ()
	{
		return $this->_regional_count_8;
	}

	public function set_regional_count_8 ($regional_count_8)
	{
		$this->_regional_count_8 = $regional_count_8;
	}

	public function get_regional_9 ()
	{
		return $this->_regional_9;
	}

	public function set_regional_9 ($regional_9)
	{
		$this->_regional_9 = $regional_9;
	}

	public function get_regional_count_9 ()
	{
		return $this->_regional_count_9;
	}

	public function set_regional_count_9 ($regional_count_9)
	{
		$this->_regional_count_9 = $regional_count_9;
	}

	public function get_regional_10 ()
	{
		return $this->_regional_10;
	}

	public function set_regional_10 ($regional_10)
	{
		$this->_regional_10 = $regional_10;
	}

	public function get_regional_count_10 ()
	{
		return $this->_regional_count_10;
	}

	public function set_regional_count_10 ($regional_count_10)
	{
		$this->_regional_count_10 = $regional_count_10;
	}

	public function get_regional_11 ()
	{
		return $this->_regional_11;
	}

	public function set_regional_11 ($regional_11)
	{
		$this->_regional_11 = $regional_11;
	}

	public function get_regional_count_11 ()
	{
		return $this->_regional_count_11;
	}

	public function set_regional_count_11 ($regional_count_11)
	{
		$this->_regional_count_11 = $regional_count_11;
	}

	public function get_regional_12 ()
	{
		return $this->_regional_12;
	}

	public function set_regional_12 ($regional_12)
	{
		$this->_regional_12 = $regional_12;
	}

	public function get_regional_count_12 ()
	{
		return $this->_regional_count_12;
	}

	public function set_regional_count_12 ($regional_count_12)
	{
		$this->_regional_count_12 = $regional_count_12;
	}

	public function get_regional_13 ()
	{
		return $this->_regional_13;
	}

	public function set_regional_13 ($regional_13)
	{
		$this->_regional_13 = $regional_13;
	}

	public function get_regional_count_13 ()
	{
		return $this->_regional_count_13;
	}

	public function set_regional_count_13 ($regional_count_13)
	{
		$this->_regional_count_13 = $regional_count_13;
	}

	public function get_regional_14 ()
	{
		return $this->_regional_14;
	}

	public function set_regional_14 ($regional_14)
	{
		$this->_regional_14 = $regional_14;
	}

	public function get_regional_count_14 ()
	{
		return $this->_regional_count_14;
	}

	public function set_regional_count_14 ($regional_count_14)
	{
		$this->_regional_count_14 = $regional_count_14;
	}

	public function get_regional_15 ()
	{
		return $this->_regional_15;
	}

	public function set_regional_15 ($regional_15)
	{
		$this->_regional_15 = $regional_15;
	}

	public function get_regional_count_15 ()
	{
		return $this->_regional_count_15;
	}

	public function set_regional_count_15 ($regional_count_15)
	{
		$this->_regional_count_15 = $regional_count_15;
	}

	public function get_id ()
	{
		return $this->_id;
	}

	public function set_id ($id)
	{
		$this->_id = $id;
	}

	public function to_json ()
	{
		return [
		'table' => $this->get_table(),
		'file' => $this->get_file(),
		'created_at' => $this->get_created_at(),
		'mayor_1' => $this->get_mayor_1(),
		'mayor_count_1' => $this->get_mayor_count_1(),
		'mayor_2' => $this->get_mayor_2(),
		'mayor_count_2' => $this->get_mayor_count_2(),
		'mayor_3' => $this->get_mayor_3(),
		'mayor_count_3' => $this->get_mayor_count_3(),
		'mayor_4' => $this->get_mayor_4(),
		'mayor_count_4' => $this->get_mayor_count_4(),
		'mayor_5' => $this->get_mayor_5(),
		'mayor_count_5' => $this->get_mayor_count_5(),
		'mayor_6' => $this->get_mayor_6(),
		'mayor_count_6' => $this->get_mayor_count_6(),
		'mayor_7' => $this->get_mayor_7(),
		'mayor_count_7' => $this->get_mayor_count_7(),
		'mayor_8' => $this->get_mayor_8(),
		'mayor_count_8' => $this->get_mayor_count_8(),
		'mayor_9' => $this->get_mayor_9(),
		'mayor_count_9' => $this->get_mayor_count_9(),
		'mayor_10' => $this->get_mayor_10(),
		'mayor_count_10' => $this->get_mayor_count_10(),
		'senator_1' => $this->get_senator_1(),
		'senator_count_1' => $this->get_senator_count_1(),
		'senator_2' => $this->get_senator_2(),
		'senator_count_2' => $this->get_senator_count_2(),
		'senator_3' => $this->get_senator_3(),
		'senator_count_3' => $this->get_senator_count_3(),
		'senator_4' => $this->get_senator_4(),
		'senator_count_4' => $this->get_senator_count_4(),
		'senator_5' => $this->get_senator_5(),
		'senator_count_5' => $this->get_senator_count_5(),
		'senator_6' => $this->get_senator_6(),
		'senator_count_6' => $this->get_senator_count_6(),
		'senator_7' => $this->get_senator_7(),
		'senator_count_7' => $this->get_senator_count_7(),
		'senator_8' => $this->get_senator_8(),
		'senator_count_8' => $this->get_senator_count_8(),
		'senator_9' => $this->get_senator_9(),
		'senator_count_9' => $this->get_senator_count_9(),
		'senator_10' => $this->get_senator_10(),
		'senator_count_10' => $this->get_senator_count_10(),
		'regional_1' => $this->get_regional_1(),
		'regional_count_1' => $this->get_regional_count_1(),
		'regional_2' => $this->get_regional_2(),
		'regional_count_2' => $this->get_regional_count_2(),
		'regional_3' => $this->get_regional_3(),
		'regional_count_3' => $this->get_regional_count_3(),
		'regional_4' => $this->get_regional_4(),
		'regional_count_4' => $this->get_regional_count_4(),
		'regional_5' => $this->get_regional_5(),
		'regional_count_5' => $this->get_regional_count_5(),
		'regional_6' => $this->get_regional_6(),
		'regional_count_6' => $this->get_regional_count_6(),
		'regional_7' => $this->get_regional_7(),
		'regional_count_7' => $this->get_regional_count_7(),
		'regional_8' => $this->get_regional_8(),
		'regional_count_8' => $this->get_regional_count_8(),
		'regional_9' => $this->get_regional_9(),
		'regional_count_9' => $this->get_regional_count_9(),
		'regional_10' => $this->get_regional_10(),
		'regional_count_10' => $this->get_regional_count_10(),
		'regional_11' => $this->get_regional_11(),
		'regional_count_11' => $this->get_regional_count_11(),
		'regional_12' => $this->get_regional_12(),
		'regional_count_12' => $this->get_regional_count_12(),
		'regional_13' => $this->get_regional_13(),
		'regional_count_13' => $this->get_regional_count_13(),
		'regional_14' => $this->get_regional_14(),
		'regional_count_14' => $this->get_regional_count_14(),
		'regional_15' => $this->get_regional_15(),
		'regional_count_15' => $this->get_regional_count_15(),
		];
	}

}