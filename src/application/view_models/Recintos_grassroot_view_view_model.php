<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * View Recintos View Model
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Recintos_grassroot_view_view_model
{
    protected $_entity;
    protected $_model;
	protected $_id;
	protected $_recinto;
	protected $_recinto_name;
	protected $_collegio;
	protected $_coordinator_name;
	protected $_coordinator_government_id;
	protected $_assigned;
	protected $_personel;
	protected $_present;


    public function __construct($entity)
    {
        $this->_entity = $entity;
    }

    public function get_entity ()
    {
        return $this->_entity;
    }

    /**
     * set_heading function
     *
     * @param string $heading
     * @return void
     */
    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    /**
     * get_heading function
     *
     * @return string
     */
    public function get_heading ()
    {
        return $this->_heading;
    }

    public function set_model ($model)
    {
        $this->_model = $model;
		$this->_id = $model->id;
		$this->_recinto = $model->recinto;
		$this->_recinto_name = $model->recinto_name;
		$this->_collegio = $model->collegio;
		$this->_coordinator_name = $model->coordinator_name;
		$this->_coordinator_government_id = $model->coordinator_government_id;
		$this->_assigned = $model->assigned;
		$this->_personel = $model->personel;
		$this->_present = $model->present;

    }

	public function assigned_mapping ()
	{
		return $this->_entity->assigned_mapping();

	}

	public function present_mapping ()
	{
		return $this->_entity->present_mapping();

	}

	public function get_recinto ()
	{
		return $this->_recinto;
	}

	public function set_recinto ($recinto)
	{
		$this->_recinto = $recinto;
	}

	public function get_recinto_name ()
	{
		return $this->_recinto_name;
	}

	public function set_recinto_name ($recinto_name)
	{
		$this->_recinto_name = $recinto_name;
	}

	public function get_collegio ()
	{
		return $this->_collegio;
	}

	public function set_collegio ($collegio)
	{
		$this->_collegio = $collegio;
	}

	public function get_coordinator_name ()
	{
		return $this->_coordinator_name;
	}

	public function set_coordinator_name ($coordinator_name)
	{
		$this->_coordinator_name = $coordinator_name;
	}

	public function get_coordinator_government_id ()
	{
		return $this->_coordinator_government_id;
	}

	public function set_coordinator_government_id ($coordinator_government_id)
	{
		$this->_coordinator_government_id = $coordinator_government_id;
	}

	public function get_assigned ()
	{
		return $this->_assigned;
	}

	public function set_assigned ($assigned)
	{
		$this->_assigned = $assigned;
	}

	public function get_personel ()
	{
		return $this->_personel;
	}

	public function set_personel ($personel)
	{
		$this->_personel = $personel;
	}

	public function get_present ()
	{
		return $this->_present;
	}

	public function set_present ($present)
	{
		$this->_present = $present;
	}

	public function get_id ()
	{
		return $this->_id;
	}

	public function set_id ($id)
	{
		$this->_id = $id;
	}

	public function to_json ()
	{
		return [
		'recinto' => $this->get_recinto(),
		'recinto_name' => $this->get_recinto_name(),
		'collegio' => $this->get_collegio(),
		'coordinator_name' => $this->get_coordinator_name(),
		'coordinator_government_id' => $this->get_coordinator_government_id(),
		'assigned' => $this->get_assigned(),
		'personel' => $this->get_personel(),
		'present' => $this->get_present(),
		];
	}

}