<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Voters List Paginate View Model
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Voters_admin_list_paginate_view_model
{
    protected $_library;
    protected $_base_url = '';
    protected $_heading = 'Voters';
    protected $_total_rows = 0;
    protected $_format_layout = '';
    protected $_per_page = 10;
    protected $_page;
    protected $_num_links = 5;
    protected $_column = ['id','avanza_member_id','government_id','first_name','last_name','married_status','gender','date_of_birth','place_of_birth','member_pld','circunscripcion','age','address','address_number','recinto','recinto_name','recinto_address','colegio','sector','sector_name','voting_sub_sector_code','phone_1','phone_type_1','phone_2','phone_type_2','email','facebook','twitter','instagram','vote_in_primary','cordinator_id','is_cordinator','occupation_id','government_sponsor','is_vote_external','parent_voting_table','voter_turn','has_voted_municipal_2020','has_voted_general_2020','has_contact_call','has_contact_call_poll','has_contact_sms','has_contact_sms_poll','has_contact_email','Acción'];
    protected $_field_column = ['id','avanza_member_id','government_id','first_name','last_name','married_status','gender','date_of_birth','place_of_birth','member_pld','circunscripcion','age','address','address_number','recinto','recinto_name','recinto_address','colegio','sector','sector_name','voting_sub_sector_code','phone_1','phone_type_1','phone_2','phone_type_2','email','facebook','twitter','instagram','vote_in_primary','cordinator_id','is_cordinator','occupation_id','government_sponsor','is_vote_external','parent_voting_table','voter_turn','has_voted_municipal_2020','has_voted_general_2020','has_contact_call','has_contact_call_poll','has_contact_sms','has_contact_sms_poll','has_contact_email',''];
    protected $_list = [];
    protected $_links = '';
    protected $_sort_base_url = '';
    protected $_order_by = '';
    protected $_sort = '';
    protected $_entity;

    public function __construct($entity, $library, $base_url)
    {
        $this->_entity = $entity;
        $this->_library = $library;
		$this->_base_url = str_replace('/0', '/', $base_url);
    }

    /**
     * set_total_rows function
     *
     * @param integer $total_rows
     * @return void
     */
    public function set_total_rows ($total_rows)
    {
        $this->_total_rows = $total_rows;
    }

    /**
     * set_per_page function
     *
     * @param integer $per_page
     * @return void
     */
    public function set_per_page ($per_page)
    {
        $this->_per_page = $per_page;
    }
    /**
     * format_layout function
     *
     * @param integer $_format_layout
     * @return void
     */
    public function set_format_layout ($_format_layout)
    {
        $this->_format_layout = $_format_layout;
    }

    /**
     * set_order_by function
     *
     * @param string $order_by
     * @return void
     */
    public function set_order_by ($order_by)
    {
        $this->_order_by = $order_by;
    }

    /**
     * set_sort function
     *
     * @param string $sort
     * @return void
     */
    public function set_sort ($sort)
    {
        $this->_sort = $sort;
    }

    /**
     * set_sort_base_url function
     *
     * @param string $sort_base_url
     * @return void
     */
    public function set_sort_base_url ($sort_base_url)
    {
        $this->_sort_base_url = $sort_base_url;
    }

    /**
     * set_page function
     *
     * @param integer $page
     * @return void
     */
    public function set_page ($page)
    {
        $this->_page = $page;
    }

    /**
     * set_list function
     *
     * @param mixed $list
     * @return void
     */
    public function set_list ($list)
    {
        $this->_list = $list;
    }

    /**
     * get_total_rows function
     *
     * @return integer
     */
    public function get_total_rows ()
    {
        return $this->_total_rows;
    }

    /**
     * get_format_layout function
     *
     * @return integer
     */
    public function get_format_layout ()
    {
        return $this->_format_layout;
    }

    /**
     * get_per_page function
     *
     * @return integer
     */
    public function get_per_page ()
    {
        return $this->_per_page;
    }

    /**
     * get_page function
     *
     * @return integer
     */
    public function get_page ()
    {
        return $this->_page;
    }

    /**
     * num_links function
     *
     * @return integer
     */
    public function get_num_links ()
    {
        return $this->_num_links;
    }

    /**
     * set_order_by function
     *
     */
    public function get_order_by ()
    {
        return $this->_order_by;
    }

    /**
     * get_field_column function
     *
     */
    public function get_field_column ()
    {
        return $this->_field_column;
    }

    /**
     * set_sort function
     *
     */
    public function get_sort ()
    {
        return $this->_sort;
    }

    /**
     * set_sort_base_url function
     *
     */
    public function get_sort_base_url ()
    {
        return $this->_sort_base_url;
    }

    /**
     * num_pages function
     *
     * @return integer
     */
    public function get_num_page ()
    {
        $num = ceil($this->_total_rows / $this->_per_page);
        return ($num > 0) ? (int) $num : 1;
    }

    public function image_or_file ($file)
    {
        $images = ['.jpg','.png', '.gif', '.jpeg', '.bmp'];
        $is_image = FALSE;
        if ($this->strposa($file, $images))
        {
            return "<div class='mkd-image-container'><img class='img-fluid' src='{$file}' onerror=\"if (this.src != '/uploads/placeholder.jpg') this.src = '/uploads/placeholder.jpg';\"/></div>";
        }

        return "<a href='{$file}' target='__blank'>{$file}</a>";
    }

    /**
     * Strpos for array
     *
     * @param string $haystack
     * @param array $needle
     * @return boolean
     */
    private function strposa($haystack, $needle)
    {
        foreach($needle as $query)
        {
            if(strpos($haystack, $query) !== FALSE)
            {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * get_list function
     *
     * @return mixed
     */
    public function get_list ()
    {
        $this->_library->initialize([
            'reuse_query_string' => TRUE,
            'base_url' => $this->_base_url,
            'total_rows' => $this->_total_rows,
            'per_page' => $this->_per_page,
            'num_links' => $this->_num_links,
            'full_tag_open' => '<ul class="pagination justify-content-end">',
            'full_tag_close' => '</ul>',
            'attributes' => ['class' => 'page-link'],
            'first_link' => FALSE,
            'last_link' => FALSE,
            'first_tag_open' => '<li class="page-item">',
            'first_tag_close' => '</li>',
            'prev_link' => '&laquo',
            'prev_tag_open' => '<li class="page-item">',
            'prev_tag_close' => '</li>',
            'next_link' => '&raquo',
            'next_tag_open' => '<li class="page-item">',
            'next_tag_close' => '</li>',
            'last_tag_open' => '<li class="page-item">',
            'last_tag_close' => '</li>',
            'cur_tag_open' => '<li class="page-item active"><a href="#" class="page-link">',
            'cur_tag_close' => '<span class="sr-only">(current)</span></a></li>',
            'num_tag_open' => '<li class="page-item">',
            'num_tag_close' => '</li>'
        ]);
        return $this->_list;
    }

    /**
     * get_links function
     *
     * @return mixed
     */
    public function get_links ()
    {
        $this->_links = $this->_library->create_links();
        return $this->_links;
    }

    /**
     * set_heading function
     *
     * @param string $heading
     * @return void
     */
    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    /**
     * get_heading function
     *
     * @return string
     */
    public function get_heading ()
    {
        return $this->_heading;
    }

    public function set_column ($column)
    {
        $this->_column = $column;
    }

    public function get_column ()
    {
        return $this->_column;
    }

	public function get_id ()
	{
		return $this->_id;
	}

	public function set_id ($id)
	{
		$this->_id = $id;
	}

	public function get_avanza_member_id ()
	{
		return $this->_avanza_member_id;
	}

	public function set_avanza_member_id ($avanza_member_id)
	{
		$this->_avanza_member_id = $avanza_member_id;
	}

	public function get_government_id ()
	{
		return $this->_government_id;
	}

	public function set_government_id ($government_id)
	{
		$this->_government_id = $government_id;
	}

	public function get_first_name ()
	{
		return $this->_first_name;
	}

	public function set_first_name ($first_name)
	{
		$this->_first_name = $first_name;
	}

	public function get_last_name ()
	{
		return $this->_last_name;
	}

	public function set_last_name ($last_name)
	{
		$this->_last_name = $last_name;
	}

	public function get_married_status ()
	{
		return $this->_married_status;
	}

	public function set_married_status ($married_status)
	{
		$this->_married_status = $married_status;
	}

	public function get_gender ()
	{
		return $this->_gender;
	}

	public function set_gender ($gender)
	{
		$this->_gender = $gender;
	}

	public function get_date_of_birth ()
	{
		return $this->_date_of_birth;
	}

	public function set_date_of_birth ($date_of_birth)
	{
		$this->_date_of_birth = $date_of_birth;
	}

	public function get_place_of_birth ()
	{
		return $this->_place_of_birth;
	}

	public function set_place_of_birth ($place_of_birth)
	{
		$this->_place_of_birth = $place_of_birth;
	}

	public function get_member_pld ()
	{
		return $this->_member_pld;
	}

	public function set_member_pld ($member_pld)
	{
		$this->_member_pld = $member_pld;
	}

	public function get_circunscripcion ()
	{
		return $this->_circunscripcion;
	}

	public function set_circunscripcion ($circunscripcion)
	{
		$this->_circunscripcion = $circunscripcion;
	}

	public function get_age ()
	{
		return $this->_age;
	}

	public function set_age ($age)
	{
		$this->_age = $age;
	}

	public function get_address ()
	{
		return $this->_address;
	}

	public function set_address ($address)
	{
		$this->_address = $address;
	}

	public function get_address_number ()
	{
		return $this->_address_number;
	}

	public function set_address_number ($address_number)
	{
		$this->_address_number = $address_number;
	}

	public function get_recinto ()
	{
		return $this->_recinto;
	}

	public function set_recinto ($recinto)
	{
		$this->_recinto = $recinto;
	}

	public function get_recinto_name ()
	{
		return $this->_recinto_name;
	}

	public function set_recinto_name ($recinto_name)
	{
		$this->_recinto_name = $recinto_name;
	}

	public function get_recinto_address ()
	{
		return $this->_recinto_address;
	}

	public function set_recinto_address ($recinto_address)
	{
		$this->_recinto_address = $recinto_address;
	}

	public function get_colegio ()
	{
		return $this->_colegio;
	}

	public function set_colegio ($colegio)
	{
		$this->_colegio = $colegio;
	}

	public function get_sector ()
	{
		return $this->_sector;
	}

	public function set_sector ($sector)
	{
		$this->_sector = $sector;
	}

	public function get_sector_name ()
	{
		return $this->_sector_name;
	}

	public function set_sector_name ($sector_name)
	{
		$this->_sector_name = $sector_name;
	}

	public function get_voting_sub_sector_code ()
	{
		return $this->_voting_sub_sector_code;
	}

	public function set_voting_sub_sector_code ($voting_sub_sector_code)
	{
		$this->_voting_sub_sector_code = $voting_sub_sector_code;
	}

	public function get_phone_1 ()
	{
		return $this->_phone_1;
	}

	public function set_phone_1 ($phone_1)
	{
		$this->_phone_1 = $phone_1;
	}

	public function get_phone_type_1 ()
	{
		return $this->_phone_type_1;
	}

	public function set_phone_type_1 ($phone_type_1)
	{
		$this->_phone_type_1 = $phone_type_1;
	}

	public function get_phone_2 ()
	{
		return $this->_phone_2;
	}

	public function set_phone_2 ($phone_2)
	{
		$this->_phone_2 = $phone_2;
	}

	public function get_phone_type_2 ()
	{
		return $this->_phone_type_2;
	}

	public function set_phone_type_2 ($phone_type_2)
	{
		$this->_phone_type_2 = $phone_type_2;
	}

	public function get_email ()
	{
		return $this->_email;
	}

	public function set_email ($email)
	{
		$this->_email = $email;
	}

	public function get_facebook ()
	{
		return $this->_facebook;
	}

	public function set_facebook ($facebook)
	{
		$this->_facebook = $facebook;
	}

	public function get_twitter ()
	{
		return $this->_twitter;
	}

	public function set_twitter ($twitter)
	{
		$this->_twitter = $twitter;
	}

	public function get_instagram ()
	{
		return $this->_instagram;
	}

	public function set_instagram ($instagram)
	{
		$this->_instagram = $instagram;
	}

	public function get_vote_in_primary ()
	{
		return $this->_vote_in_primary;
	}

	public function set_vote_in_primary ($vote_in_primary)
	{
		$this->_vote_in_primary = $vote_in_primary;
	}

	public function get_cordinator_id ()
	{
		return $this->_cordinator_id;
	}

	public function set_cordinator_id ($cordinator_id)
	{
		$this->_cordinator_id = $cordinator_id;
	}

	public function get_is_cordinator ()
	{
		return $this->_is_cordinator;
	}

	public function set_is_cordinator ($is_cordinator)
	{
		$this->_is_cordinator = $is_cordinator;
	}

	public function get_occupation_id ()
	{
		return $this->_occupation_id;
	}

	public function set_occupation_id ($occupation_id)
	{
		$this->_occupation_id = $occupation_id;
	}

	public function get_government_sponsor ()
	{
		return $this->_government_sponsor;
	}

	public function set_government_sponsor ($government_sponsor)
	{
		$this->_government_sponsor = $government_sponsor;
	}

	public function get_is_vote_external ()
	{
		return $this->_is_vote_external;
	}

	public function set_is_vote_external ($is_vote_external)
	{
		$this->_is_vote_external = $is_vote_external;
	}

	public function get_parent_voting_table ()
	{
		return $this->_parent_voting_table;
	}

	public function set_parent_voting_table ($parent_voting_table)
	{
		$this->_parent_voting_table = $parent_voting_table;
	}

	public function get_voter_turn ()
	{
		return $this->_voter_turn;
	}

	public function set_voter_turn ($voter_turn)
	{
		$this->_voter_turn = $voter_turn;
	}

	public function get_has_voted_municipal_2020 ()
	{
		return $this->_has_voted_municipal_2020;
	}

	public function set_has_voted_municipal_2020 ($has_voted_municipal_2020)
	{
		$this->_has_voted_municipal_2020 = $has_voted_municipal_2020;
	}

	public function get_has_voted_general_2020 ()
	{
		return $this->_has_voted_general_2020;
	}

	public function set_has_voted_general_2020 ($has_voted_general_2020)
	{
		$this->_has_voted_general_2020 = $has_voted_general_2020;
	}

	public function get_has_contact_call ()
	{
		return $this->_has_contact_call;
	}

	public function set_has_contact_call ($has_contact_call)
	{
		$this->_has_contact_call = $has_contact_call;
	}

	public function get_has_contact_call_poll ()
	{
		return $this->_has_contact_call_poll;
	}

	public function set_has_contact_call_poll ($has_contact_call_poll)
	{
		$this->_has_contact_call_poll = $has_contact_call_poll;
	}

	public function get_has_contact_sms ()
	{
		return $this->_has_contact_sms;
	}

	public function set_has_contact_sms ($has_contact_sms)
	{
		$this->_has_contact_sms = $has_contact_sms;
	}

	public function get_has_contact_sms_poll ()
	{
		return $this->_has_contact_sms_poll;
	}

	public function set_has_contact_sms_poll ($has_contact_sms_poll)
	{
		$this->_has_contact_sms_poll = $has_contact_sms_poll;
	}

	public function get_has_contact_email ()
	{
		return $this->_has_contact_email;
	}

	public function set_has_contact_email ($has_contact_email)
	{
		$this->_has_contact_email = $has_contact_email;
	}

	public function member_pld_mapping ()
	{
		return $this->_entity->member_pld_mapping();

	}

	public function is_vote_external_mapping ()
	{
		return $this->_entity->is_vote_external_mapping();

	}

	public function is_cordinator_mapping ()
	{
		return $this->_entity->is_cordinator_mapping();

	}

	public function gender_mapping ()
	{
		return $this->_entity->gender_mapping();

	}

	public function married_status_mapping ()
	{
		return $this->_entity->married_status_mapping();

	}

	public function phone_type_1_mapping ()
	{
		return $this->_entity->phone_type_1_mapping();

	}

	public function phone_type_2_mapping ()
	{
		return $this->_entity->phone_type_2_mapping();

	}

	public function to_json ()
	{
		$list = $this->get_list();

		$clean_list = [];

		foreach ($list as $key => $value)
		{
			$list[$key]->member_pld = $this->member_pld_mapping()[$value->member_pld];
			$list[$key]->is_vote_external = $this->is_vote_external_mapping()[$value->is_vote_external];
			$list[$key]->is_cordinator = $this->is_cordinator_mapping()[$value->is_cordinator];
			$list[$key]->gender = $this->gender_mapping()[$value->gender];
			$list[$key]->married_status = $this->married_status_mapping()[$value->married_status];
			$list[$key]->phone_type_1 = $this->phone_type_1_mapping()[$value->phone_type_1];
			$list[$key]->phone_type_2 = $this->phone_type_2_mapping()[$value->phone_type_2];
			$clean_list_entry = [];
			$clean_list_entry['id'] = $list[$key]->id;
			$clean_list_entry['avanza_member_id'] = $list[$key]->avanza_member_id;
			$clean_list_entry['government_id'] = $list[$key]->government_id;
			$clean_list_entry['first_name'] = $list[$key]->first_name;
			$clean_list_entry['last_name'] = $list[$key]->last_name;
			$clean_list_entry['married_status'] = $list[$key]->married_status;
			$clean_list_entry['gender'] = $list[$key]->gender;
			$clean_list_entry['date_of_birth'] = $list[$key]->date_of_birth;
			$clean_list_entry['place_of_birth'] = $list[$key]->place_of_birth;
			$clean_list_entry['member_pld'] = $list[$key]->member_pld;
			$clean_list_entry['circunscripcion'] = $list[$key]->circunscripcion;
			$clean_list_entry['age'] = $list[$key]->age;
			$clean_list_entry['address'] = $list[$key]->address;
			$clean_list_entry['address_number'] = $list[$key]->address_number;
			$clean_list_entry['recinto'] = $list[$key]->recinto;
			$clean_list_entry['recinto_name'] = $list[$key]->recinto_name;
			$clean_list_entry['recinto_address'] = $list[$key]->recinto_address;
			$clean_list_entry['colegio'] = $list[$key]->colegio;
			$clean_list_entry['sector'] = $list[$key]->sector;
			$clean_list_entry['sector_name'] = $list[$key]->sector_name;
			$clean_list_entry['voting_sub_sector_code'] = $list[$key]->voting_sub_sector_code;
			$clean_list_entry['phone_1'] = $list[$key]->phone_1;
			$clean_list_entry['phone_type_1'] = $list[$key]->phone_type_1;
			$clean_list_entry['phone_2'] = $list[$key]->phone_2;
			$clean_list_entry['phone_type_2'] = $list[$key]->phone_type_2;
			$clean_list_entry['email'] = $list[$key]->email;
			$clean_list_entry['facebook'] = $list[$key]->facebook;
			$clean_list_entry['twitter'] = $list[$key]->twitter;
			$clean_list_entry['instagram'] = $list[$key]->instagram;
			$clean_list_entry['vote_in_primary'] = $list[$key]->vote_in_primary;
			$clean_list_entry['cordinator_id'] = $list[$key]->cordinator_id;
			$clean_list_entry['is_cordinator'] = $list[$key]->is_cordinator;
			$clean_list_entry['occupation_id'] = $list[$key]->occupation_id;
			$clean_list_entry['government_sponsor'] = $list[$key]->government_sponsor;
			$clean_list_entry['is_vote_external'] = $list[$key]->is_vote_external;
			$clean_list_entry['parent_voting_table'] = $list[$key]->parent_voting_table;
			$clean_list_entry['voter_turn'] = $list[$key]->voter_turn;
			$clean_list_entry['has_voted_municipal_2020'] = $list[$key]->has_voted_municipal_2020;
			$clean_list_entry['has_voted_general_2020'] = $list[$key]->has_voted_general_2020;
			$clean_list_entry['has_contact_call'] = $list[$key]->has_contact_call;
			$clean_list_entry['has_contact_call_poll'] = $list[$key]->has_contact_call_poll;
			$clean_list_entry['has_contact_sms'] = $list[$key]->has_contact_sms;
			$clean_list_entry['has_contact_sms_poll'] = $list[$key]->has_contact_sms_poll;
			$clean_list_entry['has_contact_email'] = $list[$key]->has_contact_email;
			$clean_list[] = $clean_list_entry;
		}

		return [
			'page' => $this->get_page(),
			'num_page' => $this->get_num_page(),
			'num_item' => $this->get_total_rows(),
			'item' => $clean_list
		];
	}

}