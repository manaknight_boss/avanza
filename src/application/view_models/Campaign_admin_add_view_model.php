<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Add Campaign View Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Campaign_admin_add_view_model
{
    protected $_entity;

    public function __construct($entity)
    {
        $this->_entity = $entity;
    }

    public function get_entity ()
    {
        return $this->_entity;
    }

    /**
     * set_heading function
     *
     * @param string $heading
     * @return void
     */
    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    /**
     * get_heading function
     *
     * @return string
     */
    public function get_heading ()
    {
        return $this->_heading;
    }


	public function source_id_mapping ()
	{
		return $this->_entity->source_id_mapping();

	}

	public function type_mapping ()
	{
		return $this->_entity->type_mapping();

	}

	public function status_mapping ()
	{
		return $this->_entity->status_mapping();

	}

	public function circunscripcion_mapping ()
	{
		return $this->_entity->circunscripcion_mapping();

	}

	public function gender_mapping ()
	{
		return $this->_entity->gender_mapping();

	}

	public function married_status_mapping ()
	{
		return $this->_entity->married_status_mapping();

	}

	public function age_mapping ()
	{
		return $this->_entity->age_mapping();

	}

}