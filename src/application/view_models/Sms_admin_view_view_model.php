<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * View Sms View Model
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Sms_admin_view_view_model
{
    protected $_entity;
    protected $_model;
	protected $_id;
	protected $_slug;
	protected $_tag;
	protected $_content;


    public function __construct($entity)
    {
        $this->_entity = $entity;
    }

    public function get_entity ()
    {
        return $this->_entity;
    }

    /**
     * set_heading function
     *
     * @param string $heading
     * @return void
     */
    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    /**
     * get_heading function
     *
     * @return string
     */
    public function get_heading ()
    {
        return $this->_heading;
    }

    public function set_model ($model)
    {
        $this->_model = $model;
		$this->_id = $model->id;
		$this->_slug = $model->slug;
		$this->_tag = $model->tag;
		$this->_content = $model->content;

    }

	public function get_id ()
	{
		return $this->_id;
	}

	public function set_id ($id)
	{
		$this->_id = $id;
	}

	public function get_slug ()
	{
		return $this->_slug;
	}

	public function set_slug ($slug)
	{
		$this->_slug = $slug;
	}

	public function get_tag ()
	{
		return $this->_tag;
	}

	public function set_tag ($tag)
	{
		$this->_tag = $tag;
	}

	public function get_content ()
	{
		return $this->_content;
	}

	public function set_content ($content)
	{
		$this->_content = $content;
	}

	public function to_json ()
	{
		return [
		'id' => $this->get_id(),
		'slug' => $this->get_slug(),
		'tag' => $this->get_tag(),
		'content' => $this->get_content(),
		];
	}

}