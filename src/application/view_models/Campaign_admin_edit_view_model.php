<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Edit Campaign View Model
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Campaign_admin_edit_view_model
{
    protected $_entity;
	protected $_id;
	protected $_name;
	protected $_status;
	protected $_content;


    public function __construct($entity)
    {
        $this->_entity = $entity;
    }

    public function get_entity ()
    {
        return $this->_entity;
    }

    /**
     * set_heading function
     *
     * @param string $heading
     * @return void
     */
    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    /**
     * get_heading function
     *
     * @return string
     */
    public function get_heading ()
    {
        return $this->_heading;
    }

    public function set_model ($model)
    {
        $this->_model = $model;
		$this->_id = $model->id;
		$this->_name = $model->name;
		$this->_status = $model->status;
		$this->_content = $model->content;

    }

	public function source_id_mapping ()
	{
		return $this->_entity->source_id_mapping();

	}

	public function type_mapping ()
	{
		return $this->_entity->type_mapping();

	}

	public function status_mapping ()
	{
		return $this->_entity->status_mapping();

	}

	public function circunscripcion_mapping ()
	{
		return $this->_entity->circunscripcion_mapping();

	}

	public function gender_mapping ()
	{
		return $this->_entity->gender_mapping();

	}

	public function married_status_mapping ()
	{
		return $this->_entity->married_status_mapping();

	}

	public function age_mapping ()
	{
		return $this->_entity->age_mapping();

	}

	public function get_name ()
	{
		return $this->_name;
	}

	public function set_name ($name)
	{
		$this->_name = $name;
	}

	public function get_status ()
	{
		return $this->_status;
	}

	public function set_status ($status)
	{
		$this->_status = $status;
	}

	public function get_content ()
	{
		return $this->_content;
	}

	public function set_content ($content)
	{
		$this->_content = $content;
	}

	public function get_id ()
	{
		return $this->_id;
	}

	public function set_id ($id)
	{
		$this->_id = $id;
	}

}