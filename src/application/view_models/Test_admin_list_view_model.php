<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Test List View Model
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Test_admin_list_view_model
{
    protected $_list = [];
    protected $_column = ['ID','Archivo','Acción'];
    protected $_entity;
    protected $_heading = 'Test';

    public function __construct($entity)
    {
        $this->_entity = $entity;
    }

    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    public function get_heading ()
    {
        return $this->_heading;
    }

    public function set_column ($column)
    {
        $this->_column = $column;
    }

    public function get_column ()
    {
        return $this->_column;
    }

    public function image_or_file ($file)
    {
        $images = ['.jpg','.png', '.gif', '.jpeg', '.bmp'];
        $is_image = FALSE;
        if ($this->strposa($file, $images))
        {
            return "<div class='mkd-image-container'><img class='img-fluid' src='{$file}' onerror=\"if (this.src != '/uploads/placeholder.jpg') this.src = '/uploads/placeholder.jpg';\"/></div>";
        }

        return "<a href='{$file}' target='__blank'>{$file}</a>";
    }

    /**
     * Strpos for array
     *
     * @param string $haystack
     * @param array $needle
     * @return boolean
     */
    private function strposa($haystack, $needle)
    {
        foreach($needle as $query)
        {
            if(strpos($haystack, $query) !== FALSE)
            {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function set_list ($list)
    {
        $this->_list = $list;
    }

    public function get_list ()
    {
        return $this->_list;
    }

	public function to_json ()
	{
		$list = $this->get_list();

		$clean_list = [];

		foreach ($list as $key => $value)
		{
			$clean_list_entry = [];
			$clean_list_entry['id'] = $list[$key]->id;
			$clean_list_entry['file'] = $list[$key]->file;
			$clean_list[] = $clean_list_entry;
		}

		return [
			'page' => 1,
			'num_page' => 1,
			'num_item' => count($clean_list),
			'item' => $clean_list
		];
	}

}