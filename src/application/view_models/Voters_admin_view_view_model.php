<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * View Voters View Model
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Voters_admin_view_view_model
{
    protected $_entity;
    protected $_model;
	protected $_id;
	protected $_avanza_member_id;
	protected $_government_id;
	protected $_first_name;
	protected $_last_name;
	protected $_married_status;
	protected $_gender;
	protected $_date_of_birth;
	protected $_place_of_birth;
	protected $_member_pld;
	protected $_circunscripcion;
	protected $_age;
	protected $_address;
	protected $_address_number;
	protected $_recinto;
	protected $_recinto_name;
	protected $_recinto_address;
	protected $_colegio;
	protected $_sector;
	protected $_sector_name;
	protected $_voting_sub_sector_code;
	protected $_phone_1;
	protected $_phone_type_1;
	protected $_phone_2;
	protected $_phone_type_2;
	protected $_email;
	protected $_facebook;
	protected $_twitter;
	protected $_instagram;
	protected $_vote_in_primary;
	protected $_cordinator_id;
	protected $_is_cordinator;
	protected $_occupation_id;
	protected $_government_sponsor;
	protected $_is_vote_external;
	protected $_parent_voting_table;
	protected $_voter_turn;
	protected $_has_voted_municipal_2020;
	protected $_has_voted_general_2020;
	protected $_has_contact_call;
	protected $_has_contact_call_poll;
	protected $_has_contact_sms;
	protected $_has_contact_sms_poll;
	protected $_has_contact_email;


    public function __construct($entity)
    {
        $this->_entity = $entity;
    }

    public function get_entity ()
    {
        return $this->_entity;
    }

    /**
     * set_heading function
     *
     * @param string $heading
     * @return void
     */
    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    /**
     * get_heading function
     *
     * @return string
     */
    public function get_heading ()
    {
        return $this->_heading;
    }

    public function set_model ($model)
    {
        $this->_model = $model;
		$this->_id = $model->id;
		$this->_avanza_member_id = $model->avanza_member_id;
		$this->_government_id = $model->government_id;
		$this->_first_name = $model->first_name;
		$this->_last_name = $model->last_name;
		$this->_married_status = $model->married_status;
		$this->_gender = $model->gender;
		$this->_date_of_birth = $model->date_of_birth;
		$this->_place_of_birth = $model->place_of_birth;
		$this->_member_pld = $model->member_pld;
		$this->_circunscripcion = $model->circunscripcion;
		$this->_age = $model->age;
		$this->_address = $model->address;
		$this->_address_number = $model->address_number;
		$this->_recinto = $model->recinto;
		$this->_recinto_name = $model->recinto_name;
		$this->_recinto_address = $model->recinto_address;
		$this->_colegio = $model->colegio;
		$this->_sector = $model->sector;
		$this->_sector_name = $model->sector_name;
		$this->_voting_sub_sector_code = $model->voting_sub_sector_code;
		$this->_phone_1 = $model->phone_1;
		$this->_phone_type_1 = $model->phone_type_1;
		$this->_phone_2 = $model->phone_2;
		$this->_phone_type_2 = $model->phone_type_2;
		$this->_email = $model->email;
		$this->_facebook = $model->facebook;
		$this->_twitter = $model->twitter;
		$this->_instagram = $model->instagram;
		$this->_vote_in_primary = $model->vote_in_primary;
		$this->_cordinator_id = $model->cordinator_id;
		$this->_is_cordinator = $model->is_cordinator;
		$this->_occupation_id = $model->occupation_id;
		$this->_government_sponsor = $model->government_sponsor;
		$this->_is_vote_external = $model->is_vote_external;
		$this->_parent_voting_table = $model->parent_voting_table;
		$this->_voter_turn = $model->voter_turn;
		$this->_has_voted_municipal_2020 = $model->has_voted_municipal_2020;
		$this->_has_voted_general_2020 = $model->has_voted_general_2020;
		$this->_has_contact_call = $model->has_contact_call;
		$this->_has_contact_call_poll = $model->has_contact_call_poll;
		$this->_has_contact_sms = $model->has_contact_sms;
		$this->_has_contact_sms_poll = $model->has_contact_sms_poll;
		$this->_has_contact_email = $model->has_contact_email;

    }

	public function member_pld_mapping ()
	{
		return $this->_entity->member_pld_mapping();

	}

	public function is_vote_external_mapping ()
	{
		return $this->_entity->is_vote_external_mapping();

	}

	public function is_cordinator_mapping ()
	{
		return $this->_entity->is_cordinator_mapping();

	}

	public function gender_mapping ()
	{
		return $this->_entity->gender_mapping();

	}

	public function married_status_mapping ()
	{
		return $this->_entity->married_status_mapping();

	}

	public function phone_type_1_mapping ()
	{
		return $this->_entity->phone_type_1_mapping();

	}

	public function phone_type_2_mapping ()
	{
		return $this->_entity->phone_type_2_mapping();

	}

	public function get_id ()
	{
		return $this->_id;
	}

	public function set_id ($id)
	{
		$this->_id = $id;
	}

	public function get_avanza_member_id ()
	{
		return $this->_avanza_member_id;
	}

	public function set_avanza_member_id ($avanza_member_id)
	{
		$this->_avanza_member_id = $avanza_member_id;
	}

	public function get_government_id ()
	{
		return $this->_government_id;
	}

	public function set_government_id ($government_id)
	{
		$this->_government_id = $government_id;
	}

	public function get_first_name ()
	{
		return $this->_first_name;
	}

	public function set_first_name ($first_name)
	{
		$this->_first_name = $first_name;
	}

	public function get_last_name ()
	{
		return $this->_last_name;
	}

	public function set_last_name ($last_name)
	{
		$this->_last_name = $last_name;
	}

	public function get_married_status ()
	{
		return $this->_married_status;
	}

	public function set_married_status ($married_status)
	{
		$this->_married_status = $married_status;
	}

	public function get_gender ()
	{
		return $this->_gender;
	}

	public function set_gender ($gender)
	{
		$this->_gender = $gender;
	}

	public function get_date_of_birth ()
	{
		return $this->_date_of_birth;
	}

	public function set_date_of_birth ($date_of_birth)
	{
		$this->_date_of_birth = $date_of_birth;
	}

	public function get_place_of_birth ()
	{
		return $this->_place_of_birth;
	}

	public function set_place_of_birth ($place_of_birth)
	{
		$this->_place_of_birth = $place_of_birth;
	}

	public function get_member_pld ()
	{
		return $this->_member_pld;
	}

	public function set_member_pld ($member_pld)
	{
		$this->_member_pld = $member_pld;
	}

	public function get_circunscripcion ()
	{
		return $this->_circunscripcion;
	}

	public function set_circunscripcion ($circunscripcion)
	{
		$this->_circunscripcion = $circunscripcion;
	}

	public function get_age ()
	{
		return $this->_age;
	}

	public function set_age ($age)
	{
		$this->_age = $age;
	}

	public function get_address ()
	{
		return $this->_address;
	}

	public function set_address ($address)
	{
		$this->_address = $address;
	}

	public function get_address_number ()
	{
		return $this->_address_number;
	}

	public function set_address_number ($address_number)
	{
		$this->_address_number = $address_number;
	}

	public function get_recinto ()
	{
		return $this->_recinto;
	}

	public function set_recinto ($recinto)
	{
		$this->_recinto = $recinto;
	}

	public function get_recinto_name ()
	{
		return $this->_recinto_name;
	}

	public function set_recinto_name ($recinto_name)
	{
		$this->_recinto_name = $recinto_name;
	}

	public function get_recinto_address ()
	{
		return $this->_recinto_address;
	}

	public function set_recinto_address ($recinto_address)
	{
		$this->_recinto_address = $recinto_address;
	}

	public function get_colegio ()
	{
		return $this->_colegio;
	}

	public function set_colegio ($colegio)
	{
		$this->_colegio = $colegio;
	}

	public function get_sector ()
	{
		return $this->_sector;
	}

	public function set_sector ($sector)
	{
		$this->_sector = $sector;
	}

	public function get_sector_name ()
	{
		return $this->_sector_name;
	}

	public function set_sector_name ($sector_name)
	{
		$this->_sector_name = $sector_name;
	}

	public function get_voting_sub_sector_code ()
	{
		return $this->_voting_sub_sector_code;
	}

	public function set_voting_sub_sector_code ($voting_sub_sector_code)
	{
		$this->_voting_sub_sector_code = $voting_sub_sector_code;
	}

	public function get_phone_1 ()
	{
		return $this->_phone_1;
	}

	public function set_phone_1 ($phone_1)
	{
		$this->_phone_1 = $phone_1;
	}

	public function get_phone_type_1 ()
	{
		return $this->_phone_type_1;
	}

	public function set_phone_type_1 ($phone_type_1)
	{
		$this->_phone_type_1 = $phone_type_1;
	}

	public function get_phone_2 ()
	{
		return $this->_phone_2;
	}

	public function set_phone_2 ($phone_2)
	{
		$this->_phone_2 = $phone_2;
	}

	public function get_phone_type_2 ()
	{
		return $this->_phone_type_2;
	}

	public function set_phone_type_2 ($phone_type_2)
	{
		$this->_phone_type_2 = $phone_type_2;
	}

	public function get_email ()
	{
		return $this->_email;
	}

	public function set_email ($email)
	{
		$this->_email = $email;
	}

	public function get_facebook ()
	{
		return $this->_facebook;
	}

	public function set_facebook ($facebook)
	{
		$this->_facebook = $facebook;
	}

	public function get_twitter ()
	{
		return $this->_twitter;
	}

	public function set_twitter ($twitter)
	{
		$this->_twitter = $twitter;
	}

	public function get_instagram ()
	{
		return $this->_instagram;
	}

	public function set_instagram ($instagram)
	{
		$this->_instagram = $instagram;
	}

	public function get_vote_in_primary ()
	{
		return $this->_vote_in_primary;
	}

	public function set_vote_in_primary ($vote_in_primary)
	{
		$this->_vote_in_primary = $vote_in_primary;
	}

	public function get_cordinator_id ()
	{
		return $this->_cordinator_id;
	}

	public function set_cordinator_id ($cordinator_id)
	{
		$this->_cordinator_id = $cordinator_id;
	}

	public function get_is_cordinator ()
	{
		return $this->_is_cordinator;
	}

	public function set_is_cordinator ($is_cordinator)
	{
		$this->_is_cordinator = $is_cordinator;
	}

	public function get_occupation_id ()
	{
		return $this->_occupation_id;
	}

	public function set_occupation_id ($occupation_id)
	{
		$this->_occupation_id = $occupation_id;
	}

	public function get_government_sponsor ()
	{
		return $this->_government_sponsor;
	}

	public function set_government_sponsor ($government_sponsor)
	{
		$this->_government_sponsor = $government_sponsor;
	}

	public function get_is_vote_external ()
	{
		return $this->_is_vote_external;
	}

	public function set_is_vote_external ($is_vote_external)
	{
		$this->_is_vote_external = $is_vote_external;
	}

	public function get_parent_voting_table ()
	{
		return $this->_parent_voting_table;
	}

	public function set_parent_voting_table ($parent_voting_table)
	{
		$this->_parent_voting_table = $parent_voting_table;
	}

	public function get_voter_turn ()
	{
		return $this->_voter_turn;
	}

	public function set_voter_turn ($voter_turn)
	{
		$this->_voter_turn = $voter_turn;
	}

	public function get_has_voted_municipal_2020 ()
	{
		return $this->_has_voted_municipal_2020;
	}

	public function set_has_voted_municipal_2020 ($has_voted_municipal_2020)
	{
		$this->_has_voted_municipal_2020 = $has_voted_municipal_2020;
	}

	public function get_has_voted_general_2020 ()
	{
		return $this->_has_voted_general_2020;
	}

	public function set_has_voted_general_2020 ($has_voted_general_2020)
	{
		$this->_has_voted_general_2020 = $has_voted_general_2020;
	}

	public function get_has_contact_call ()
	{
		return $this->_has_contact_call;
	}

	public function set_has_contact_call ($has_contact_call)
	{
		$this->_has_contact_call = $has_contact_call;
	}

	public function get_has_contact_call_poll ()
	{
		return $this->_has_contact_call_poll;
	}

	public function set_has_contact_call_poll ($has_contact_call_poll)
	{
		$this->_has_contact_call_poll = $has_contact_call_poll;
	}

	public function get_has_contact_sms ()
	{
		return $this->_has_contact_sms;
	}

	public function set_has_contact_sms ($has_contact_sms)
	{
		$this->_has_contact_sms = $has_contact_sms;
	}

	public function get_has_contact_sms_poll ()
	{
		return $this->_has_contact_sms_poll;
	}

	public function set_has_contact_sms_poll ($has_contact_sms_poll)
	{
		$this->_has_contact_sms_poll = $has_contact_sms_poll;
	}

	public function get_has_contact_email ()
	{
		return $this->_has_contact_email;
	}

	public function set_has_contact_email ($has_contact_email)
	{
		$this->_has_contact_email = $has_contact_email;
	}

	public function to_json ()
	{
		return [
		'id' => $this->get_id(),
		'avanza_member_id' => $this->get_avanza_member_id(),
		'government_id' => $this->get_government_id(),
		'first_name' => $this->get_first_name(),
		'last_name' => $this->get_last_name(),
		'married_status' => $this->get_married_status(),
		'gender' => $this->get_gender(),
		'date_of_birth' => $this->get_date_of_birth(),
		'place_of_birth' => $this->get_place_of_birth(),
		'member_pld' => $this->get_member_pld(),
		'circunscripcion' => $this->get_circunscripcion(),
		'age' => $this->get_age(),
		'address' => $this->get_address(),
		'address_number' => $this->get_address_number(),
		'recinto' => $this->get_recinto(),
		'recinto_name' => $this->get_recinto_name(),
		'recinto_address' => $this->get_recinto_address(),
		'colegio' => $this->get_colegio(),
		'sector' => $this->get_sector(),
		'sector_name' => $this->get_sector_name(),
		'voting_sub_sector_code' => $this->get_voting_sub_sector_code(),
		'phone_1' => $this->get_phone_1(),
		'phone_type_1' => $this->get_phone_type_1(),
		'phone_2' => $this->get_phone_2(),
		'phone_type_2' => $this->get_phone_type_2(),
		'email' => $this->get_email(),
		'facebook' => $this->get_facebook(),
		'twitter' => $this->get_twitter(),
		'instagram' => $this->get_instagram(),
		'vote_in_primary' => $this->get_vote_in_primary(),
		'cordinator_id' => $this->get_cordinator_id(),
		'is_cordinator' => $this->get_is_cordinator(),
		'occupation_id' => $this->get_occupation_id(),
		'government_sponsor' => $this->get_government_sponsor(),
		'is_vote_external' => $this->get_is_vote_external(),
		'parent_voting_table' => $this->get_parent_voting_table(),
		'voter_turn' => $this->get_voter_turn(),
		'has_voted_municipal_2020' => $this->get_has_voted_municipal_2020(),
		'has_voted_general_2020' => $this->get_has_voted_general_2020(),
		'has_contact_call' => $this->get_has_contact_call(),
		'has_contact_call_poll' => $this->get_has_contact_call_poll(),
		'has_contact_sms' => $this->get_has_contact_sms(),
		'has_contact_sms_poll' => $this->get_has_contact_sms_poll(),
		'has_contact_email' => $this->get_has_contact_email(),
		];
	}

}