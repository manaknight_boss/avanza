<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Add Voters View Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Voters_admin_add_view_model
{
    protected $_entity;

    public function __construct($entity)
    {
        $this->_entity = $entity;
    }

    public function get_entity ()
    {
        return $this->_entity;
    }

    /**
     * set_heading function
     *
     * @param string $heading
     * @return void
     */
    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    /**
     * get_heading function
     *
     * @return string
     */
    public function get_heading ()
    {
        return $this->_heading;
    }


	public function member_pld_mapping ()
	{
		return $this->_entity->member_pld_mapping();

	}

	public function is_vote_external_mapping ()
	{
		return $this->_entity->is_vote_external_mapping();

	}

	public function is_cordinator_mapping ()
	{
		return $this->_entity->is_cordinator_mapping();

	}

	public function gender_mapping ()
	{
		return $this->_entity->gender_mapping();

	}

	public function married_status_mapping ()
	{
		return $this->_entity->married_status_mapping();

	}

	public function phone_type_1_mapping ()
	{
		return $this->_entity->phone_type_1_mapping();

	}

	public function phone_type_2_mapping ()
	{
		return $this->_entity->phone_type_2_mapping();

	}

}