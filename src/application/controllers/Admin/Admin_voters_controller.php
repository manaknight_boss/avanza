<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Voters Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_voters_controller extends Admin_controller
{
    protected $_model_file = 'voters_new_model';
    public $_page_name = 'Electores';

    public function __construct()
    {
        parent::__construct();
        

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Voters_admin_list_paginate_view_model.php';
        $session = $this->get_session();
        $format = $this->input->get('format', TRUE) ?? 'view';
        $order_by = $this->input->get('order_by', TRUE) ?? '';
        $direction = $this->input->get('direction', TRUE) ?? 'ASC';

        $this->_data['view_model'] = new Voters_admin_list_paginate_view_model(
            $this->voters_new_model,
            $this->pagination,
            '/admin/voters/0');
        $this->_data['view_model']->set_heading('Electores');
        $this->_data['view_model']->set_id(($this->input->get('id', TRUE) != NULL) ? $this->input->get('id', TRUE) : NULL);
		$this->_data['view_model']->set_avanza_member_id(($this->input->get('avanza_member_id', TRUE) != NULL) ? $this->input->get('avanza_member_id', TRUE) : NULL);
		$this->_data['view_model']->set_government_id(($this->input->get('government_id', TRUE) != NULL) ? $this->input->get('government_id', TRUE) : NULL);
		$this->_data['view_model']->set_first_name(($this->input->get('first_name', TRUE) != NULL) ? $this->input->get('first_name', TRUE) : NULL);
		$this->_data['view_model']->set_last_name(($this->input->get('last_name', TRUE) != NULL) ? $this->input->get('last_name', TRUE) : NULL);
		$this->_data['view_model']->set_married_status(($this->input->get('married_status', TRUE) != NULL) ? $this->input->get('married_status', TRUE) : NULL);
		$this->_data['view_model']->set_gender(($this->input->get('gender', TRUE) != NULL) ? $this->input->get('gender', TRUE) : NULL);
		$this->_data['view_model']->set_date_of_birth(($this->input->get('date_of_birth', TRUE) != NULL) ? $this->input->get('date_of_birth', TRUE) : NULL);
		$this->_data['view_model']->set_place_of_birth(($this->input->get('place_of_birth', TRUE) != NULL) ? $this->input->get('place_of_birth', TRUE) : NULL);
		$this->_data['view_model']->set_member_pld(($this->input->get('member_pld', TRUE) != NULL) ? $this->input->get('member_pld', TRUE) : NULL);
		$this->_data['view_model']->set_circunscripcion(($this->input->get('circunscripcion', TRUE) != NULL) ? $this->input->get('circunscripcion', TRUE) : NULL);
		$this->_data['view_model']->set_age(($this->input->get('age', TRUE) != NULL) ? $this->input->get('age', TRUE) : NULL);
		$this->_data['view_model']->set_address(($this->input->get('address', TRUE) != NULL) ? $this->input->get('address', TRUE) : NULL);
		$this->_data['view_model']->set_address_number(($this->input->get('address_number', TRUE) != NULL) ? $this->input->get('address_number', TRUE) : NULL);
		$this->_data['view_model']->set_recinto(($this->input->get('recinto', TRUE) != NULL) ? $this->input->get('recinto', TRUE) : NULL);
		$this->_data['view_model']->set_recinto_name(($this->input->get('recinto_name', TRUE) != NULL) ? $this->input->get('recinto_name', TRUE) : NULL);
		$this->_data['view_model']->set_recinto_address(($this->input->get('recinto_address', TRUE) != NULL) ? $this->input->get('recinto_address', TRUE) : NULL);
		$this->_data['view_model']->set_colegio(($this->input->get('colegio', TRUE) != NULL) ? $this->input->get('colegio', TRUE) : NULL);
		$this->_data['view_model']->set_sector(($this->input->get('sector', TRUE) != NULL) ? $this->input->get('sector', TRUE) : NULL);
		$this->_data['view_model']->set_sector_name(($this->input->get('sector_name', TRUE) != NULL) ? $this->input->get('sector_name', TRUE) : NULL);
		$this->_data['view_model']->set_voting_sub_sector_code(($this->input->get('voting_sub_sector_code', TRUE) != NULL) ? $this->input->get('voting_sub_sector_code', TRUE) : NULL);
		$this->_data['view_model']->set_phone_1(($this->input->get('phone_1', TRUE) != NULL) ? $this->input->get('phone_1', TRUE) : NULL);
		$this->_data['view_model']->set_phone_type_1(($this->input->get('phone_type_1', TRUE) != NULL) ? $this->input->get('phone_type_1', TRUE) : NULL);
		$this->_data['view_model']->set_phone_2(($this->input->get('phone_2', TRUE) != NULL) ? $this->input->get('phone_2', TRUE) : NULL);
		$this->_data['view_model']->set_phone_type_2(($this->input->get('phone_type_2', TRUE) != NULL) ? $this->input->get('phone_type_2', TRUE) : NULL);
		$this->_data['view_model']->set_email(($this->input->get('email', TRUE) != NULL) ? $this->input->get('email', TRUE) : NULL);
		$this->_data['view_model']->set_facebook(($this->input->get('facebook', TRUE) != NULL) ? $this->input->get('facebook', TRUE) : NULL);
		$this->_data['view_model']->set_twitter(($this->input->get('twitter', TRUE) != NULL) ? $this->input->get('twitter', TRUE) : NULL);
		$this->_data['view_model']->set_instagram(($this->input->get('instagram', TRUE) != NULL) ? $this->input->get('instagram', TRUE) : NULL);
		$this->_data['view_model']->set_vote_in_primary(($this->input->get('vote_in_primary', TRUE) != NULL) ? $this->input->get('vote_in_primary', TRUE) : NULL);
		$this->_data['view_model']->set_cordinator_id(($this->input->get('cordinator_id', TRUE) != NULL) ? $this->input->get('cordinator_id', TRUE) : NULL);
		$this->_data['view_model']->set_is_cordinator(($this->input->get('is_cordinator', TRUE) != NULL) ? $this->input->get('is_cordinator', TRUE) : NULL);
		$this->_data['view_model']->set_occupation_id(($this->input->get('occupation_id', TRUE) != NULL) ? $this->input->get('occupation_id', TRUE) : NULL);
		$this->_data['view_model']->set_government_sponsor(($this->input->get('government_sponsor', TRUE) != NULL) ? $this->input->get('government_sponsor', TRUE) : NULL);
		$this->_data['view_model']->set_is_vote_external(($this->input->get('is_vote_external', TRUE) != NULL) ? $this->input->get('is_vote_external', TRUE) : NULL);
		$this->_data['view_model']->set_parent_voting_table(($this->input->get('parent_voting_table', TRUE) != NULL) ? $this->input->get('parent_voting_table', TRUE) : NULL);
		$this->_data['view_model']->set_voter_turn(($this->input->get('voter_turn', TRUE) != NULL) ? $this->input->get('voter_turn', TRUE) : NULL);
		$this->_data['view_model']->set_has_voted_municipal_2020(($this->input->get('has_voted_municipal_2020', TRUE) != NULL) ? $this->input->get('has_voted_municipal_2020', TRUE) : NULL);
		$this->_data['view_model']->set_has_voted_general_2020(($this->input->get('has_voted_general_2020', TRUE) != NULL) ? $this->input->get('has_voted_general_2020', TRUE) : NULL);
		$this->_data['view_model']->set_has_contact_call(($this->input->get('has_contact_call', TRUE) != NULL) ? $this->input->get('has_contact_call', TRUE) : NULL);
		$this->_data['view_model']->set_has_contact_call_poll(($this->input->get('has_contact_call_poll', TRUE) != NULL) ? $this->input->get('has_contact_call_poll', TRUE) : NULL);
		$this->_data['view_model']->set_has_contact_sms(($this->input->get('has_contact_sms', TRUE) != NULL) ? $this->input->get('has_contact_sms', TRUE) : NULL);
		$this->_data['view_model']->set_has_contact_sms_poll(($this->input->get('has_contact_sms_poll', TRUE) != NULL) ? $this->input->get('has_contact_sms_poll', TRUE) : NULL);
		$this->_data['view_model']->set_has_contact_email(($this->input->get('has_contact_email', TRUE) != NULL) ? $this->input->get('has_contact_email', TRUE) : NULL);
		
        $where = [
            'id' => $this->_data['view_model']->get_id(),
			'avanza_member_id' => $this->_data['view_model']->get_avanza_member_id(),
			'government_id' => $this->_data['view_model']->get_government_id(),
			'first_name' => $this->_data['view_model']->get_first_name(),
			'last_name' => $this->_data['view_model']->get_last_name(),
			'married_status' => $this->_data['view_model']->get_married_status(),
			'gender' => $this->_data['view_model']->get_gender(),
			'date_of_birth' => $this->_data['view_model']->get_date_of_birth(),
			'place_of_birth' => $this->_data['view_model']->get_place_of_birth(),
			'member_pld' => $this->_data['view_model']->get_member_pld(),
			'circunscripcion' => $this->_data['view_model']->get_circunscripcion(),
			'age' => $this->_data['view_model']->get_age(),
			'address' => $this->_data['view_model']->get_address(),
			'address_number' => $this->_data['view_model']->get_address_number(),
			'recinto' => $this->_data['view_model']->get_recinto(),
			'recinto_name' => $this->_data['view_model']->get_recinto_name(),
			'recinto_address' => $this->_data['view_model']->get_recinto_address(),
			'colegio' => $this->_data['view_model']->get_colegio(),
			'sector' => $this->_data['view_model']->get_sector(),
			'sector_name' => $this->_data['view_model']->get_sector_name(),
			'voting_sub_sector_code' => $this->_data['view_model']->get_voting_sub_sector_code(),
			'phone_1' => $this->_data['view_model']->get_phone_1(),
			'phone_type_1' => $this->_data['view_model']->get_phone_type_1(),
			'phone_2' => $this->_data['view_model']->get_phone_2(),
			'phone_type_2' => $this->_data['view_model']->get_phone_type_2(),
			'email' => $this->_data['view_model']->get_email(),
			'facebook' => $this->_data['view_model']->get_facebook(),
			'twitter' => $this->_data['view_model']->get_twitter(),
			'instagram' => $this->_data['view_model']->get_instagram(),
			'vote_in_primary' => $this->_data['view_model']->get_vote_in_primary(),
			'cordinator_id' => $this->_data['view_model']->get_cordinator_id(),
			'is_cordinator' => $this->_data['view_model']->get_is_cordinator(),
			'occupation_id' => $this->_data['view_model']->get_occupation_id(),
			'government_sponsor' => $this->_data['view_model']->get_government_sponsor(),
			'is_vote_external' => $this->_data['view_model']->get_is_vote_external(),
			'parent_voting_table' => $this->_data['view_model']->get_parent_voting_table(),
			'voter_turn' => $this->_data['view_model']->get_voter_turn(),
			'has_voted_municipal_2020' => $this->_data['view_model']->get_has_voted_municipal_2020(),
			'has_voted_general_2020' => $this->_data['view_model']->get_has_voted_general_2020(),
			'has_contact_call' => $this->_data['view_model']->get_has_contact_call(),
			'has_contact_call_poll' => $this->_data['view_model']->get_has_contact_call_poll(),
			'has_contact_sms' => $this->_data['view_model']->get_has_contact_sms(),
			'has_contact_sms_poll' => $this->_data['view_model']->get_has_contact_sms_poll(),
			'has_contact_email' => $this->_data['view_model']->get_has_contact_email(),
			
        ];

        $this->_data['view_model']->set_total_rows($this->voters_new_model->count($where));

        $this->_data['view_model']->set_format_layout($this->_data['layout_clean_mode']);
        $this->_data['view_model']->set_per_page(25);
        $this->_data['view_model']->set_order_by($order_by);
        $this->_data['view_model']->set_sort($direction);
        $this->_data['view_model']->set_sort_base_url('/admin/voters/0');
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->voters_new_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where,
            $order_by,
            $direction));

        if ($format != 'view')
        {
            return $this->output->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($this->_data['view_model']->to_json()));
        }

        return $this->render('Admin/Voters', $this->_data);
	}

	public function add()
	{
        include_once __DIR__ . '/../../view_models/Voters_admin_add_view_model.php';
        $this->form_validation = $this->voters_new_model->set_form_validation(
        $this->form_validation, $this->voters_new_model->get_all_validation_rule());
        $this->_data['view_model'] = new Voters_admin_add_view_model($this->voters_new_model);
        $this->_data['view_model']->set_heading('Electores');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/VotersAdd', $this->_data);
        }

        $avanza_member_id = $this->input->post('avanza_member_id');
		$government_id = $this->input->post('government_id');
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$married_status = $this->input->post('married_status');
		$gender = $this->input->post('gender');
		$date_of_birth = $this->input->post('date_of_birth');
		$place_of_birth = $this->input->post('place_of_birth');
		$member_pld = $this->input->post('member_pld');
		$circunscripcion = $this->input->post('circunscripcion');
		$age = $this->input->post('age');
		$address = $this->input->post('address');
		$address_number = $this->input->post('address_number');
		$recinto = $this->input->post('recinto');
		$recinto_name = $this->input->post('recinto_name');
		$recinto_address = $this->input->post('recinto_address');
		$colegio = $this->input->post('colegio');
		$sector = $this->input->post('sector');
		$sector_name = $this->input->post('sector_name');
		$voting_sub_sector_code = $this->input->post('voting_sub_sector_code');
		$phone_1 = $this->input->post('phone_1');
		$phone_type_1 = $this->input->post('phone_type_1');
		$phone_2 = $this->input->post('phone_2');
		$phone_type_2 = $this->input->post('phone_type_2');
		$email = $this->input->post('email');
		$facebook = $this->input->post('facebook');
		$twitter = $this->input->post('twitter');
		$instagram = $this->input->post('instagram');
		$vote_in_primary = $this->input->post('vote_in_primary');
		$cordinator_id = $this->input->post('cordinator_id');
		$is_cordinator = $this->input->post('is_cordinator');
		$occupation_id = $this->input->post('occupation_id');
		$government_sponsor = $this->input->post('government_sponsor');
		$is_vote_external = $this->input->post('is_vote_external');
		$parent_voting_table = $this->input->post('parent_voting_table');
		$voter_turn = $this->input->post('voter_turn');
		$has_voted_municipal_2020 = $this->input->post('has_voted_municipal_2020');
		$has_voted_general_2020 = $this->input->post('has_voted_general_2020');
		$has_contact_call = $this->input->post('has_contact_call');
		$has_contact_call_poll = $this->input->post('has_contact_call_poll');
		$has_contact_sms = $this->input->post('has_contact_sms');
		$has_contact_sms_poll = $this->input->post('has_contact_sms_poll');
		$has_contact_email = $this->input->post('has_contact_email');
		
        $result = $this->voters_new_model->create([
            'avanza_member_id' => $avanza_member_id,
			'government_id' => $government_id,
			'first_name' => $first_name,
			'last_name' => $last_name,
			'married_status' => $married_status,
			'gender' => $gender,
			'date_of_birth' => $date_of_birth,
			'place_of_birth' => $place_of_birth,
			'member_pld' => $member_pld,
			'circunscripcion' => $circunscripcion,
			'age' => $age,
			'address' => $address,
			'address_number' => $address_number,
			'recinto' => $recinto,
			'recinto_name' => $recinto_name,
			'recinto_address' => $recinto_address,
			'colegio' => $colegio,
			'sector' => $sector,
			'sector_name' => $sector_name,
			'voting_sub_sector_code' => $voting_sub_sector_code,
			'phone_1' => $phone_1,
			'phone_type_1' => $phone_type_1,
			'phone_2' => $phone_2,
			'phone_type_2' => $phone_type_2,
			'email' => $email,
			'facebook' => $facebook,
			'twitter' => $twitter,
			'instagram' => $instagram,
			'vote_in_primary' => $vote_in_primary,
			'cordinator_id' => $cordinator_id,
			'is_cordinator' => $is_cordinator,
			'occupation_id' => $occupation_id,
			'government_sponsor' => $government_sponsor,
			'is_vote_external' => $is_vote_external,
			'parent_voting_table' => $parent_voting_table,
			'voter_turn' => $voter_turn,
			'has_voted_municipal_2020' => $has_voted_municipal_2020,
			'has_voted_general_2020' => $has_voted_general_2020,
			'has_contact_call' => $has_contact_call,
			'has_contact_call_poll' => $has_contact_call_poll,
			'has_contact_sms' => $has_contact_sms,
			'has_contact_sms_poll' => $has_contact_sms_poll,
			'has_contact_email' => $has_contact_email,
			
        ]);

        if ($result)
        {
            
            return $this->redirect('/admin/voters/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/VotersAdd', $this->_data);
	}

	public function edit($id)
	{
        $model = $this->voters_new_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/voters/0');
        }

        include_once __DIR__ . '/../../view_models/Voters_admin_edit_view_model.php';
        $this->form_validation = $this->voters_new_model->set_form_validation(
        $this->form_validation, $this->voters_new_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Voters_admin_edit_view_model($this->voters_new_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Electores');
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/VotersEdit', $this->_data);
        }

        $avanza_member_id = $this->input->post('avanza_member_id');
		$government_id = $this->input->post('government_id');
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$married_status = $this->input->post('married_status');
		$gender = $this->input->post('gender');
		$date_of_birth = $this->input->post('date_of_birth');
		$place_of_birth = $this->input->post('place_of_birth');
		$member_pld = $this->input->post('member_pld');
		$circunscripcion = $this->input->post('circunscripcion');
		$age = $this->input->post('age');
		$address = $this->input->post('address');
		$address_number = $this->input->post('address_number');
		$recinto = $this->input->post('recinto');
		$recinto_name = $this->input->post('recinto_name');
		$recinto_address = $this->input->post('recinto_address');
		$colegio = $this->input->post('colegio');
		$sector = $this->input->post('sector');
		$sector_name = $this->input->post('sector_name');
		$voting_sub_sector_code = $this->input->post('voting_sub_sector_code');
		$phone_1 = $this->input->post('phone_1');
		$phone_type_1 = $this->input->post('phone_type_1');
		$phone_2 = $this->input->post('phone_2');
		$phone_type_2 = $this->input->post('phone_type_2');
		$email = $this->input->post('email');
		$facebook = $this->input->post('facebook');
		$twitter = $this->input->post('twitter');
		$instagram = $this->input->post('instagram');
		$vote_in_primary = $this->input->post('vote_in_primary');
		$cordinator_id = $this->input->post('cordinator_id');
		$is_cordinator = $this->input->post('is_cordinator');
		$occupation_id = $this->input->post('occupation_id');
		$government_sponsor = $this->input->post('government_sponsor');
		$is_vote_external = $this->input->post('is_vote_external');
		$parent_voting_table = $this->input->post('parent_voting_table');
		$voter_turn = $this->input->post('voter_turn');
		$has_voted_municipal_2020 = $this->input->post('has_voted_municipal_2020');
		$has_voted_general_2020 = $this->input->post('has_voted_general_2020');
		$has_contact_call = $this->input->post('has_contact_call');
		$has_contact_call_poll = $this->input->post('has_contact_call_poll');
		$has_contact_sms = $this->input->post('has_contact_sms');
		$has_contact_sms_poll = $this->input->post('has_contact_sms_poll');
		$has_contact_email = $this->input->post('has_contact_email');
		
        $result = $this->voters_new_model->edit([
            'avanza_member_id' => $avanza_member_id,
			'government_id' => $government_id,
			'first_name' => $first_name,
			'last_name' => $last_name,
			'married_status' => $married_status,
			'gender' => $gender,
			'date_of_birth' => $date_of_birth,
			'place_of_birth' => $place_of_birth,
			'member_pld' => $member_pld,
			'circunscripcion' => $circunscripcion,
			'age' => $age,
			'address' => $address,
			'address_number' => $address_number,
			'recinto' => $recinto,
			'recinto_name' => $recinto_name,
			'recinto_address' => $recinto_address,
			'colegio' => $colegio,
			'sector' => $sector,
			'sector_name' => $sector_name,
			'voting_sub_sector_code' => $voting_sub_sector_code,
			'phone_1' => $phone_1,
			'phone_type_1' => $phone_type_1,
			'phone_2' => $phone_2,
			'phone_type_2' => $phone_type_2,
			'email' => $email,
			'facebook' => $facebook,
			'twitter' => $twitter,
			'instagram' => $instagram,
			'vote_in_primary' => $vote_in_primary,
			'cordinator_id' => $cordinator_id,
			'is_cordinator' => $is_cordinator,
			'occupation_id' => $occupation_id,
			'government_sponsor' => $government_sponsor,
			'is_vote_external' => $is_vote_external,
			'parent_voting_table' => $parent_voting_table,
			'voter_turn' => $voter_turn,
			'has_voted_municipal_2020' => $has_voted_municipal_2020,
			'has_voted_general_2020' => $has_voted_general_2020,
			'has_contact_call' => $has_contact_call,
			'has_contact_call_poll' => $has_contact_call_poll,
			'has_contact_sms' => $has_contact_sms,
			'has_contact_sms_poll' => $has_contact_sms_poll,
			'has_contact_email' => $has_contact_email,
			
        ], $id);

        if ($result)
        {
            
            return $this->redirect('/admin/voters/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/VotersEdit', $this->_data);
	}

	public function view($id)
	{
        $model = $this->voters_new_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/voters/0');
		}


        include_once __DIR__ . '/../../view_models/Voters_admin_view_view_model.php';
		$this->_data['view_model'] = new Voters_admin_view_view_model($this->voters_new_model);
		$this->_data['view_model']->set_heading('Electores');
        $this->_data['view_model']->set_model($model);
        return $this->render('Admin/VotersView', $this->_data);
	}




	public function autocomplete($text)
	{
		if (strlen($text) < 2)
		{
				echo '[]';
				exit;
		}
	
		$where = [
			'government_id LIKE "%' . $text . '%"'
		];
	
		$result = $this->voters_new_model->get_paginated(0, 25, $where);
	
		echo json_encode($result);
		exit;
	}

}