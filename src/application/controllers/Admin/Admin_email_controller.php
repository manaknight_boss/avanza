<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Email Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_email_controller extends Admin_controller
{
    protected $_model_file = 'email_model';
    public $_page_name = 'Correos';

    public function __construct()
    {
        parent::__construct();
        

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Email_admin_list_paginate_view_model.php';
        $format = $this->input->get('format', TRUE) ?? 'view';
        $order_by = $this->input->get('order_by', TRUE) ?? '';
        $direction = $this->input->get('direction', TRUE) ?? 'ASC';
        $session = $this->get_session();
        $where = [];
        $this->_data['view_model'] = new Email_admin_list_paginate_view_model(
            $this->email_model,
            $this->pagination,
            '/admin/emails/0');
        $this->_data['view_model']->set_heading('Correos');
        $this->_data['view_model']->set_total_rows($this->email_model->count($where));

        $this->_data['view_model']->set_per_page(25);
        $this->_data['view_model']->set_page($page);
        $this->_data['view_model']->set_order_by($order_by);
        $this->_data['view_model']->set_sort($direction);
        $this->_data['view_model']->set_sort_base_url('/admin/emails/0');
		$this->_data['view_model']->set_list($this->email_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where,
            $order_by,
            $direction));

        if ($format != 'view')
        {
            return $this->output->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($this->_data['view_model']->to_json()));
        }

        return $this->render('Admin/Email', $this->_data);
	}

	public function add()
	{
        include_once __DIR__ . '/../../view_models/Email_admin_add_view_model.php';
        $this->form_validation = $this->email_model->set_form_validation(
        $this->form_validation, $this->email_model->get_all_validation_rule());
        $this->_data['view_model'] = new Email_admin_add_view_model($this->email_model);
        $this->_data['view_model']->set_heading('Correos');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/EmailAdd', $this->_data);
        }

        $slug = $this->input->post('slug');
		$subject = $this->input->post('subject');
		$tag = $this->input->post('tag');
		$html = $this->input->post('html');
		
        $result = $this->email_model->create([
            'slug' => $slug,
			'subject' => $subject,
			'tag' => $tag,
			'html' => $html,
			
        ]);

        if ($result)
        {
            
            return $this->redirect('/admin/emails/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/EmailAdd', $this->_data);
	}

	public function edit($id)
	{
        $model = $this->email_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/emails/0');
        }

        include_once __DIR__ . '/../../view_models/Email_admin_edit_view_model.php';
        $this->form_validation = $this->email_model->set_form_validation(
        $this->form_validation, $this->email_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Email_admin_edit_view_model($this->email_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Correos');
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/EmailEdit', $this->_data);
        }

        $subject = $this->input->post('subject');
		$html = $this->input->post('html');
		
        $result = $this->email_model->edit([
            'subject' => $subject,
			'html' => $html,
			
        ], $id);

        if ($result)
        {
            
            return $this->redirect('/admin/emails/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/EmailEdit', $this->_data);
	}

	public function view($id)
	{
        $model = $this->email_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/emails/0');
		}


        include_once __DIR__ . '/../../view_models/Email_admin_view_view_model.php';
		$this->_data['view_model'] = new Email_admin_view_view_model($this->email_model);
		$this->_data['view_model']->set_heading('Correos');
        $this->_data['view_model']->set_model($model);
        return $this->render('Admin/EmailView', $this->_data);
	}





}