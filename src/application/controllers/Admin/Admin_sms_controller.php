<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Sms Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_sms_controller extends Admin_controller
{
    protected $_model_file = 'sms_model';
    public $_page_name = 'SMS';

    public function __construct()
    {
        parent::__construct();
        

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Sms_admin_list_paginate_view_model.php';
        $format = $this->input->get('format', TRUE) ?? 'view';
        $order_by = $this->input->get('order_by', TRUE) ?? '';
        $direction = $this->input->get('direction', TRUE) ?? 'ASC';
        $session = $this->get_session();
        $where = [];
        $this->_data['view_model'] = new Sms_admin_list_paginate_view_model(
            $this->sms_model,
            $this->pagination,
            '/admin/sms/0');
        $this->_data['view_model']->set_heading('SMS');
        $this->_data['view_model']->set_total_rows($this->sms_model->count($where));

        $this->_data['view_model']->set_per_page(25);
        $this->_data['view_model']->set_page($page);
        $this->_data['view_model']->set_order_by($order_by);
        $this->_data['view_model']->set_sort($direction);
        $this->_data['view_model']->set_sort_base_url('/admin/sms/0');
		$this->_data['view_model']->set_list($this->sms_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where,
            $order_by,
            $direction));

        if ($format != 'view')
        {
            return $this->output->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($this->_data['view_model']->to_json()));
        }

        return $this->render('Admin/Sms', $this->_data);
	}

	public function add()
	{
        include_once __DIR__ . '/../../view_models/Sms_admin_add_view_model.php';
        $this->form_validation = $this->sms_model->set_form_validation(
        $this->form_validation, $this->sms_model->get_all_validation_rule());
        $this->_data['view_model'] = new Sms_admin_add_view_model($this->sms_model);
        $this->_data['view_model']->set_heading('SMS');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/SmsAdd', $this->_data);
        }

        $slug = $this->input->post('slug');
		$tag = $this->input->post('tag');
		$content = $this->input->post('content');
		
        $result = $this->sms_model->create([
            'slug' => $slug,
			'tag' => $tag,
			'content' => $content,
			
        ]);

        if ($result)
        {
            
            return $this->redirect('/admin/sms/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/SmsAdd', $this->_data);
	}

	public function edit($id)
	{
        $model = $this->sms_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/sms/0');
        }

        include_once __DIR__ . '/../../view_models/Sms_admin_edit_view_model.php';
        $this->form_validation = $this->sms_model->set_form_validation(
        $this->form_validation, $this->sms_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Sms_admin_edit_view_model($this->sms_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('SMS');
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/SmsEdit', $this->_data);
        }

        $content = $this->input->post('content');
		
        $result = $this->sms_model->edit([
            'content' => $content,
			
        ], $id);

        if ($result)
        {
            
            return $this->redirect('/admin/sms/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/SmsEdit', $this->_data);
	}

	public function view($id)
	{
        $model = $this->sms_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/sms/0');
		}


        include_once __DIR__ . '/../../view_models/Sms_admin_view_view_model.php';
		$this->_data['view_model'] = new Sms_admin_view_view_model($this->sms_model);
		$this->_data['view_model']->set_heading('SMS');
        $this->_data['view_model']->set_model($model);
        return $this->render('Admin/SmsView', $this->_data);
	}





}