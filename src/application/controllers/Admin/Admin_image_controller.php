<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Image Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_image_controller extends Admin_controller
{
    protected $_model_file = 'image_model';
    public $_page_name = 'Imagenes';

    public function __construct()
    {
        parent::__construct();
        

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Image_admin_list_paginate_view_model.php';
        $format = $this->input->get('format', TRUE) ?? 'view';
        $order_by = $this->input->get('order_by', TRUE) ?? '';
        $direction = $this->input->get('direction', TRUE) ?? 'ASC';
        $session = $this->get_session();
        $where = [];
        $this->_data['view_model'] = new Image_admin_list_paginate_view_model(
            $this->image_model,
            $this->pagination,
            '/admin/image/0');
        $this->_data['view_model']->set_heading('Imagenes');
        $this->_data['view_model']->set_total_rows($this->image_model->count($where));

        $this->_data['view_model']->set_per_page(25);
        $this->_data['view_model']->set_page($page);
        $this->_data['view_model']->set_order_by($order_by);
        $this->_data['view_model']->set_sort($direction);
        $this->_data['view_model']->set_sort_base_url('/admin/image/0');
		$this->_data['view_model']->set_list($this->image_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where,
            $order_by,
            $direction));

        if ($format != 'view')
        {
            return $this->output->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($this->_data['view_model']->to_json()));
        }

        return $this->render('Admin/Image', $this->_data);
	}





	public function view($id)
	{
        $model = $this->image_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/image/0');
		}


        include_once __DIR__ . '/../../view_models/Image_admin_view_view_model.php';
		$this->_data['view_model'] = new Image_admin_view_view_model($this->image_model);
		$this->_data['view_model']->set_heading('Imagenes');
        $this->_data['view_model']->set_model($model);
        return $this->render('Admin/ImageView', $this->_data);
	}

	public function delete($id)
	{
        $model = $this->image_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/image/0');
        }

        $result = $this->image_model->real_delete($id);

        if ($result)
        {
            
            return $this->redirect('/admin/image/0', 'refresh');
        }

        $this->error('Error');
        return redirect('/admin/image/0');
	}



}