<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Test Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_test_controller extends Admin_controller
{
    protected $_model_file = 'test_model';
    public $_page_name = 'Pruebas';

    public function __construct()
    {
        parent::__construct();
        

    }

	public function index()
	{
		$this->load->library('pagination');
		$session = $this->get_session();
        include_once __DIR__ . '/../../view_models/Test_admin_list_view_model.php';
        $this->_data['view_model'] = new Test_admin_list_view_model($this->test_model);
		$this->_data['view_model']->set_list($this->test_model->get_all());
		$this->_data['view_model']->set_heading('Pruebas');
        return $this->render('Admin/Test', $this->_data);
	}

	public function add()
	{
        include_once __DIR__ . '/../../view_models/Test_admin_add_view_model.php';
        $this->form_validation = $this->test_model->set_form_validation(
        $this->form_validation, $this->test_model->get_all_validation_rule());
        $this->_data['view_model'] = new Test_admin_add_view_model($this->test_model);
        $this->_data['view_model']->set_heading('Pruebas');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/TestAdd', $this->_data);
        }

        $file = $this->input->post('file');
		$file_id = $this->input->post('file_id');
		
        $result = $this->test_model->create([
            'file' => $file,
			'file_id' => $file_id,
			
        ]);

        if ($result)
        {
            
            return $this->redirect('/admin/test', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/TestAdd', $this->_data);
	}

	public function edit($id)
	{
        $model = $this->test_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/test');
        }

        include_once __DIR__ . '/../../view_models/Test_admin_edit_view_model.php';
        $this->form_validation = $this->test_model->set_form_validation(
        $this->form_validation, $this->test_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Test_admin_edit_view_model($this->test_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Pruebas');
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/TestEdit', $this->_data);
        }

        $file = $this->input->post('file');
		$file_id = $this->input->post('file_id');
		
        $result = $this->test_model->edit([
            'file' => $file,
			'file_id' => $file_id,
			
        ], $id);

        if ($result)
        {
            
            return $this->redirect('/admin/test', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/TestEdit', $this->_data);
	}

	public function view($id)
	{
        $model = $this->test_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/test');
		}


        include_once __DIR__ . '/../../view_models/Test_admin_view_view_model.php';
		$this->_data['view_model'] = new Test_admin_view_view_model($this->test_model);
		$this->_data['view_model']->set_heading('Pruebas');
        $this->_data['view_model']->set_model($model);
        return $this->render('Admin/TestView', $this->_data);
	}





}