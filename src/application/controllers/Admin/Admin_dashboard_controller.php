<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';

/**
 * Admin Dashboard Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_dashboard_controller extends Admin_controller
{
    public $_page_name = 'Dashboard';

    public function __construct()
    {
        parent::__construct();
    }

    public function index ()
    {
        $this->load->model('voters_new_model');
        $this->load->model('sms_history_model');
        $this->load->model('call_history_model');
        $this->load->model('email_history_model');
        $this->load->library('cache_service');
        $this->load->library('dashboards_service');
        $this->cache_service->set_adapter('file', 3600);
        $data = $this->cache_service->get('dashboard');

        if ($data)
        {
            $this->_data = $data;
        }
        else
        {
            $this->_data['total_voters'] = $this->voters_new_model->count([]);
            $this->_data['total_sms_reach'] = $this->voters_new_model->count(['has_contact_sms' => 1]);
            $this->_data['total_sms_no_response'] = $this->sms_history_model->raw_query('SELECT count(DISTINCT(government_id)) as n FROM sms_history WHERE sms_result=1')->result()[0]->n;
            $this->_data['total_sms_sent'] = $this->sms_history_model->raw_query('SELECT count(*) as n FROM sms_history')->result()[0]->n;
            $this->_data['total_sms_poll'] = $this->voters_new_model->count(['has_contact_sms_poll' => 1]);
            $this->_data['total_voice_reach'] = $this->voters_new_model->count(['has_contact_call' => 1]);;
            $this->_data['total_voice_no_response'] = $this->call_history_model->raw_query('SELECT count(DISTINCT(government_id)) as n FROM call_history WHERE call_result=1')->result()[0]->n;
            $this->_data['total_voice_sent'] = $this->call_history_model->raw_query('SELECT count(*) as n FROM call_history')->result()[0]->n;
            $this->_data['total_voice_poll'] = $this->voters_new_model->count(['has_contact_call_poll' => 1]);
            $this->_data['total_email_reach'] = $this->voters_new_model->count(['has_contact_email' => 1]);
            $this->_data['total_email_sent'] = $this->email_history_model->raw_query('SELECT count(*) as n FROM email_history')->result()[0]->n;
            $this->_data['total_poll_voice'] = $this->voters_new_model->count(['has_contact_call_poll' => 1]);
            $this->_data['total_poll_sms'] = $this->voters_new_model->count(['has_contact_sms_poll' => 1]);
            $this->_data['total_poll_response_1'] = $this->call_history_model->raw_query('SELECT count(DISTINCT(government_id)) as n FROM call_history WHERE poll_result=1')->result()[0]->n +
            $this->sms_history_model->raw_query('SELECT count(DISTINCT(government_id)) as n FROM sms_history WHERE poll_result=1')->result()[0]->n;
            $this->_data['total_poll_response_2'] = $this->call_history_model->raw_query('SELECT count(DISTINCT(government_id)) as n FROM call_history WHERE poll_result=2')->result()[0]->n +
            $this->sms_history_model->raw_query('SELECT count(DISTINCT(government_id)) as n FROM sms_history WHERE poll_result=2')->result()[0]->n;
            $this->_data['total_poll_response_3'] = $this->call_history_model->raw_query('SELECT count(DISTINCT(government_id)) as n FROM call_history WHERE poll_result=3')->result()[0]->n +
            $this->sms_history_model->raw_query('SELECT count(DISTINCT(government_id)) as n FROM sms_history WHERE poll_result=3')->result()[0]->n;
            $this->_data['voice_headers'] = ['Circ.', 'Votantes', 'Enviado', 'Contactados', 'Avanza %', 'Tactica %', 'Progreso'];
            $this->_data['voice_data'] = $this->dashboards_service->get_voice_data($this->voters_new_model);
            $this->_data['sms_headers'] = ['Circ.', 'Votantes', 'Enviado', 'Contactados', 'Avanza %', 'Tactica %', 'Progreso'];
            $this->_data['sms_data'] = $this->dashboards_service->get_sms_data($this->voters_new_model);
            $this->cache_service->set('dashboard', $this->_data);
        }

        return $this->render('Admin/Dashboard', $this->_data);
    }

    public function clear_cache()
    {
        $this->load->library('cache_service');
        $this->cache_service->set_adapter('file', 3600);
        $this->cache_service->remove('dashboard');
        return $this->redirect('/admin/dashboard');
    }


    public function custom_call ($id)
    {
        $this->load->model('custom_call_list_model');
        $model = $this->custom_call_list_model->get($id);
        $this->load->model('call_history_model');
        $this->_data['name'] = $model->name;
        $this->_data['voice_file'] = $model->voice_file;
        $this->_data['list'] = $this->call_history_model->get_all([
            'campaign_id' => $id,
            'call_type' => 5
        ]);
        return $this->render('Admin/CampaignCustomCallView', $this->_data);
    }
}