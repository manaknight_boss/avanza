<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Campaign Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_campaign_controller extends Admin_controller
{
    protected $_model_file = 'campaign_model';
    public $_page_name = 'Campañas';

    public function __construct()
    {
        parent::__construct();
		$this->_data['sector'] = $this->get_sectors();
		$this->_data['recinto'] = $this->get_recintos();
		$this->_data['college'] = $this->get_colleges();

    }

	public function index($page)
	{
		$this->load->library('pagination');
		include_once __DIR__ . '/../../view_models/Campaign_admin_list_paginate_view_model.php';
		$session = $this->get_session();
		$format = $this->input->get('format', TRUE) ?? 'view';
		$order_by = $this->input->get('order_by', TRUE) ?? '';
		$direction = $this->input->get('direction', TRUE) ?? 'ASC';

		$this->_data['view_model'] = new Campaign_admin_list_paginate_view_model(
				$this->campaign_model,
				$this->pagination,
				'/admin/campaigns/0');
		$this->_data['view_model']->set_heading('Campañas');
		$this->_data['view_model']->set_id(($this->input->get('id', TRUE) != NULL) ? $this->input->get('id', TRUE) : NULL);
		$this->_data['view_model']->set_name(($this->input->get('name', TRUE) != NULL) ? $this->input->get('name', TRUE) : NULL);
		$this->_data['view_model']->set_type(($this->input->get('type', TRUE) != NULL) ? $this->input->get('type', TRUE) : NULL);
		$this->_data['view_model']->set_status(($this->input->get('status', TRUE) != NULL) ? $this->input->get('status', TRUE) : NULL);
		$this->_data['view_model']->set_circunscripcion(($this->input->get('circunscripcion', TRUE) != NULL) ? $this->input->get('circunscripcion', TRUE) : NULL);
		$this->_data['view_model']->set_age(($this->input->get('age', TRUE) != NULL) ? $this->input->get('age', TRUE) : NULL);
		$this->_data['view_model']->set_recinto(($this->input->get('recinto', TRUE) != NULL) ? $this->input->get('recinto', TRUE) : NULL);
		$this->_data['view_model']->set_college(($this->input->get('college', TRUE) != NULL) ? $this->input->get('college', TRUE) : NULL);
		$this->_data['view_model']->set_gender(($this->input->get('gender', TRUE) != NULL) ? $this->input->get('gender', TRUE) : NULL);
		$this->_data['view_model']->set_married_status(($this->input->get('married_status', TRUE) != NULL) ? $this->input->get('married_status', TRUE) : NULL);

        $where = [
					'id' => $this->_data['view_model']->get_id(),
					'name' => $this->_data['view_model']->get_name(),
					'type' => $this->_data['view_model']->get_type(),
					'status' => $this->_data['view_model']->get_status(),
					'circunscripcion' => $this->_data['view_model']->get_circunscripcion(),
					'age' => $this->_data['view_model']->get_age(),
					'recinto' => $this->_data['view_model']->get_recinto(),
					'college' => $this->_data['view_model']->get_college(),
					'gender' => $this->_data['view_model']->get_gender(),
					'married_status' => $this->_data['view_model']->get_married_status(),
        ];

        $this->_data['view_model']->set_total_rows($this->campaign_model->count($where));

        $this->_data['view_model']->set_per_page(25);
        $this->_data['view_model']->set_order_by($order_by);
        $this->_data['view_model']->set_sort($direction);
        $this->_data['view_model']->set_sort_base_url('/admin/campaigns/0');
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->campaign_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
						$where,
						$order_by,
            $direction));
        return $this->render('Admin/Campaign', $this->_data);
	}

	public function add_sms()
	{
		include_once __DIR__ . '/../../view_models/Campaign_admin_add_view_model.php';
		$this->form_validation = $this->campaign_model->set_form_validation(
		$this->form_validation, $this->campaign_model->get_all_validation_rule());
		$this->_data['view_model'] = new Campaign_admin_add_view_model($this->campaign_model);
		$this->_data['view_model']->set_heading('Campañas');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/CampaignAdd', $this->_data);
		}

		$name = $this->input->post('name');
		$type = $this->input->post('type');
		$circunscripcion = $this->input->post('circunscripcion');
		$age = $this->input->post('age');
		$recinto = $this->input->post('recinto');
		$source_id = $this->input->post('source_id');
		$content = $this->input->post('content');
		$college = $this->input->post('college');
		$gender = $this->input->post('gender');
		$married_status = $this->input->post('married_status');

		$result = $this->campaign_model->create([
			'name' => $name,
			'type' => $type,
			'circunscripcion' => $circunscripcion,
			'age' => $age,
			'recinto' => $recinto,
			'source_id' => $source_id,
			'content' => $content,
			'college' => $college,
			'gender' => $gender,
			'married_status' => $married_status
		]);

		if ($result)
		{
			$this->update_campaign_list_sms($circunscripcion, $age, $recinto, $source_id, $college, $gender, $married_status, $result);

			return $this->redirect('/admin/campaigns/0?order_by=id&direction=DESC', 'refresh');
		}

		$this->_data['error'] = 'Error';
		return $this->render('Admin/CampaignAdd', $this->_data);
	}

	public function add_voice()
	{
		include_once __DIR__ . '/../../view_models/Campaign_admin_add_view_model.php';
		$this->form_validation = $this->campaign_model->set_form_validation(
		$this->form_validation, $this->campaign_model->get_all_validation_rule());
		$this->_data['view_model'] = new Campaign_admin_add_view_model($this->campaign_model);
		$this->_data['view_model']->set_heading('Campañas');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/CampaignAddVoice', $this->_data);
		}

		$name = $this->input->post('name');
		$type = $this->input->post('type');
		$circunscripcion = $this->input->post('circunscripcion');
		$age = $this->input->post('age');
		$recinto = $this->input->post('recinto');
		$source_id = $this->input->post('source_id');
		$content = $this->input->post('voice_file');
		$college = $this->input->post('college');
		$gender = $this->input->post('gender');
		$married_status = $this->input->post('married_status');

		$result = $this->campaign_model->create([
				'name' => $name,
				'type' => $type,
				'circunscripcion' => $circunscripcion,
				'age' => $age,
				'recinto' => $recinto,
				'source_id' => $source_id,
				'content' => $content,
				'college' => $college,
				'gender' => $gender,
				'married_status' => $married_status
		]);

		if ($result)
		{
			$this->update_campaign_list_call($circunscripcion, $age, $recinto, $source_id, $college, $gender, $married_status, $result);
			return $this->redirect('/admin/campaigns/0?order_by=id&direction=DESC', 'refresh');
		}

		$this->_data['error'] = 'Error';
		return $this->render('Admin/CampaignAdd', $this->_data);
	}

	public function add_custom_sms()
	{
		include_once __DIR__ . '/../../view_models/Campaign_admin_add_view_model.php';
		$this->form_validation = $this->campaign_model->set_form_validation(
		$this->form_validation, [
			['name', 'Nombre', 'required|max_length[255]'],
			['type', 'Tipo', 'required|integer'],
			['content', 'Contenido', 'required'],
		]);
		$this->_data['view_model'] = new Campaign_admin_add_view_model($this->campaign_model);
		$this->_data['view_model']->set_heading('Campañas');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/CampaignCustomAdd', $this->_data);
		}

		$name = $this->input->post('name');
		$type = $this->input->post('type');
		$content = $this->input->post('content');

		$result = $this->campaign_model->create([
			'name' => $name,
			'type' => $type,
			'circunscripcion' => 0,
			'age' => 0,
			'recinto' => 0,
			'source_id' => 0,
			'content' => $content,
			'college' => 0,
			'gender' => 0,
			'married_status' => 0,
			'total' => 0,
			'count_query' => '',
			'query' => ''
		]);

		return $this->redirect('/admin/campaigns/0?order_by=id&direction=DESC', 'refresh');
	}

	public function add_custom_voice()
	{
		include_once __DIR__ . '/../../view_models/Campaign_admin_add_view_model.php';
		$this->form_validation = $this->campaign_model->set_form_validation(
		$this->form_validation, [
			['name', 'Nombre', 'required|max_length[255]'],
			['type', 'Tipo', 'required|integer']
		]);
		$this->_data['view_model'] = new Campaign_admin_add_view_model($this->campaign_model);
		$this->_data['view_model']->set_heading('Campañas');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/CampaignCustomAddVoice', $this->_data);
		}

		$name = $this->input->post('name');
		$type = $this->input->post('type');
		$content = $this->input->post('voice_file');

		$result = $this->campaign_model->create([
			'name' => $name,
			'type' => $type,
			'circunscripcion' => 0,
			'age' => 0,
			'recinto' => 0,
			'source_id' => 0,
			'content' => $content,
			'college' => 0,
			'gender' => 0,
			'married_status' => 0,
			'total' => 0,
			'count_query' => '',
			'query' => ''
		]);

		return $this->redirect('/admin/campaigns/0?order_by=id&direction=DESC', 'refresh');
	}

	public function edit($id)
	{
        $model = $this->campaign_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
        }

        include_once __DIR__ . '/../../view_models/Campaign_admin_edit_view_model.php';
        $this->form_validation = $this->campaign_model->set_form_validation(
        $this->form_validation, $this->campaign_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Campaign_admin_edit_view_model($this->campaign_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Campaign');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/CampaignEdit', $this->_data);
        }

        $name = $this->input->post('name');
				$status = $this->input->post('status');
				$content = $this->input->post('content');

        $result = $this->campaign_model->edit([
            'name' => $name,
						'status' => $status,
						'content' => $content,
        ], $id);

        if ($result)
        {
					return $this->redirect('/admin/campaigns/0?order_by=id&direction=DESC', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/CampaignEdit', $this->_data);
	}

	public function view($id, $page=0)
	{
    $model = $this->campaign_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
		}

		include_once __DIR__ . '/../../view_models/Campaign_admin_view_view_model.php';
		$this->_data['view_model'] = new Campaign_admin_view_view_model($this->campaign_model);
		$this->_data['view_model']->set_heading('Campañas');
		$this->_data['view_model']->set_model($model);
		$this->_data['campaign_id'] = $id;
		$this->load->model('voters_new_model');
		$this->load->model('call_history_model');
		$this->load->model('sms_history_model');
		if ($model->type == 2 || $model->type == 4)
		{
			$result = $this->call_history_model->get_all([
				'campaign_id' => $id
			]);
		}
		else
		{
			$result = $this->sms_history_model->get_all([
				'campaign_id' => $id
			]);

		}
		$result = $this->process_results($result);
		$this->_data['list'] = $result;
		if ($model->type == 4 || $model->type == 5)
		{
			return $this->render('Admin/CampaignView', $this->_data);
		}
		else
		{
			return $this->render('Admin/CampaignCustomView', $this->_data);
		}
	}

	// public function view($id, $page=0)
	// {
  //   $model = $this->campaign_model->get($id);

	// 	if (!$model)
	// 	{
	// 		$this->error('Error');
	// 		return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
	// 	}

	// 	include_once __DIR__ . '/../../view_models/Campaign_admin_view_view_model.php';
	// 	$this->_data['view_model'] = new Campaign_admin_view_view_model($this->campaign_model);
	// 	$this->_data['view_model']->set_heading('Campañas');
	// 	$this->_data['view_model']->set_model($model);

	// 	$this->load->model('voters_new_model');

	// 	if (strlen($model->query) < 1)
	// 	{
	// 		$this->load->model('call_history_model');
	// 		$num_query = $this->call_history_model->count([
	// 			'campaign_id' => $id
	// 		]);
	// 		$this->_data['total_rows'] = $num_query;

	// 		$this->_data['per_page'] = 100;
	// 		$this->_data['offset'] = $page;
	// 		$this->_data['campaign_id'] = $id;
	// 		$this->_data['num_links'] = ceil($this->_data['total_rows'] / $this->_data['per_page']);
	// 		$this->_data['base_url'] = '/admin/campaigns/view/' . $id;
	// 		$this->pagination->initialize($this->_data);
	// 		if ($model->type == 2 || $model->type == 4)
	// 		{
	// 			$query = 'SELECT v.id, v.government_id, v.first_name, v.last_name, v.phone_1, v.phone_2, c.call_date, c.call_result, c.call_type, c.poll_result, c.sid, c.id as call_id FROM call_history c LEFT JOIN voters_new v ON v.government_id = c.government_id WHERE campaign_id=' . $id;
	// 		}

	// 		if ($model->type == 3 || $model->type == 5)
	// 		{
	// 			$query = 'SELECT v.id, v.government_id,  v.first_name, v.last_name, v.phone_1, v.phone_2, c.sms_date, c.sms_result, c.sms_type, c.poll_result, c.sid, c.id as call_id FROM sms_history c LEFT JOIN voters_new v ON v.government_id = c.government_id WHERE campaign_id=' . $id;
	// 		}

	// 		$list = $this->voters_new_model->raw_query($query . " LIMIT {$this->_data['offset']}, {$this->_data['per_page']}");
	// 		$this->_data['list'] = $list->result();
	// 		$this->_data['links'] = $this->pagination->create_links();
	// 		return $this->render('Admin/CampaignCustomView', $this->_data);
	// 	}

	// 	$this->_data['total_rows'] = $model->total;
	// 	$this->_data['per_page'] = 100;
	// 	$this->_data['offset'] = $page;
	// 	$this->_data['campaign_id'] = $id;
	// 	$this->_data['num_links'] = ceil($this->_data['total_rows'] / $this->_data['per_page']);
	// 	$this->_data['base_url'] = '/admin/campaigns/view/' . $id;
	// 	$this->pagination->initialize($this->_data);
	// 	$list = $this->voters_new_model->raw_query($model->query . " LIMIT {$this->_data['offset']}, {$this->_data['per_page']}");
	// 	$this->_data['list'] = $list->result();
	// 	$this->_data['links'] = $this->pagination->create_links();

	// 	return $this->render('Admin/CampaignView', $this->_data);
	// }

	public function change_status($campaign_id, $status)
	{
    $model = $this->campaign_model->get($campaign_id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
		}

		$this->campaign_model->edit([
			'status' => $status
		], $campaign_id);

		if ($status == 2)
		{
			$this->load->model('campaign_result_model');
			if ($model->type == 2 || $model->type == 3)
			{
				$this->campaign_result_model->create([
					'campaign_id' => $model->id,
					'type' => $model->type,
					'circunscripcion' => $model->circunscripcion,
					'status' => 1,
					'recinto' => $model->recinto,
					'college' => $model->college,
					'gender' => $model->gender,
					'age' => $model->age,
					'martial_status' => $model->married_status,
					'reached' => 0,
					'total' => $model->total,
					'respond_1' => 0,
					'respond_2' => 0,
					'respond_3' => 0,
					'engaged' => 0,
					'dne' => 0
				]);
			}
		}
		return redirect('/admin/campaigns/view/' . $campaign_id);
	}

	public function call_manually($campaign_id, $government_id)
	{
		$this->load->model('voters_new_model');
		$this->load->model('call_history_model');
    $model = $this->campaign_model->get($campaign_id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
		}

		$voter = $this->voters_new_model->get_by_field('government_id', $government_id);

		if (!$voter)
		{
			$this->error('Error');
			return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
		}

		$call_history_exist = $this->call_history_model->get_by_fields([
			'government_id' => $government_id,
			'campaign_id' => $campaign_id
		]);

		if ($call_history_exist)
		{
			if ($call_history_exist->call_result == 1)
			{
				$this->error('Contactado');
				return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
			}
			else if ($call_history_exist->call_result == 2)
			{
				$this->error('Numero no existe');
				return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
			}
			else
			{
				$this->call_history_model->edit([
					'call_result' => 1,
					'call_date' => date('Y-m-d'),
				], $call_history_exist->id);
				$this->success('llamada exitosa');
				return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
			}
		}

		$this->call_history_model->create([
			'government_id' => $government_id,
			'phone_1' => $voter->phone_1,
			'phone_2' => $voter->phone_2,
			'campaign_id' => $campaign_id,
			'call_date' => date('Y-m-d'),
			'call_result' => 1,
			'call_type' => 2,
			'poll_result' => 0
		]);

		$this->success('llamada exitosa');
		return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
	}

	public function sms_manually($campaign_id, $government_id)
	{
		$this->load->model('voters_new_model');
		$this->load->model('sms_history_model');
    $model = $this->campaign_model->get($campaign_id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
		}

		$voter = $this->voters_new_model->get_by_field('government_id', $government_id);

		if (!$voter)
		{
			$this->error('Error');
			return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
		}

		$sms_history_exist = $this->sms_history_model->get_by_fields([
			'government_id' => $government_id,
			'campaign_id' => $campaign_id
		]);

		if ($sms_history_exist)
		{
			if ($sms_history_exist->sms_result == 1)
			{
				$this->error('Contactado SMS');
				return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
			}
			else if ($sms_history_exist->sms_result == 2)
			{
				$this->error('Numero no existe');
				return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
			}

			else
			{
				$this->sms_history_model->edit([
					'sms_result' => 1,
					'sms_date' => date('Y-m-d'),
				], $sms_history_exist->id);
				$this->success('SMS exitoso');
				return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
			}
		}

		$this->sms_history_model->create([
			'government_id' => $government_id,
			'phone_1' => $voter->phone_1,
			'phone_2' => $voter->phone_2,
			'campaign_id' => $campaign_id,
			'sms_date' => date('Y-m-d'),
			'sms_result' => 1,
			'sms_type' => 3,
			'poll_result' => 0
		]);

		$this->success('SMS exitoso');
		return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
	}

	public function callresponse($campaign_id, $government_id, $response)
	{
		$this->load->model('voters_new_model');
		$this->load->model('call_history_model');
    $model = $this->campaign_model->get($campaign_id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
		}

		$voter = $this->voters_new_model->get_by_field('government_id', $government_id);

		if (!$voter)
		{
			$this->error('Error');
			return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
		}

		$call_history_exist = $this->call_history_model->get_by_fields([
			'government_id' => $government_id,
			'campaign_id' => $campaign_id
		]);

		if ($call_history_exist)
		{
			if ($call_history_exist->poll_result > 0)
			{
				$this->error('Contactado');
				return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
			}
			else
			{
				$this->call_history_model->edit([
					'call_result' => 1,
					'poll_result' => $response,
					'call_date' => date('Y-m-d'),
				], $call_history_exist->id);
				$this->success('llamada exitosa');
				return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
			}
		}

		$this->call_history_model->create([
			'government_id' => $government_id,
			'phone_1' => $voter->phone_1,
			'phone_2' => $voter->phone_2,
			'campaign_id' => $campaign_id,
			'call_date' => date('Y-m-d'),
			'call_result' => 1,
			'call_type' => 4,
			'poll_result' => $response
		]);

		$this->success('llamada exitosa');
		return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
	}

	public function smsresponse($campaign_id, $government_id, $response)
	{
		$this->load->model('voters_new_model');
		$this->load->model('sms_history_model');
    $model = $this->campaign_model->get($campaign_id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
		}

		$voter = $this->voters_new_model->get_by_field('government_id', $government_id);

		if (!$voter)
		{
			$this->error('Error');
			return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
		}

		$sms_history_exist = $this->sms_history_model->get_by_fields([
			'government_id' => $government_id,
			'campaign_id' => $campaign_id
		]);

		if ($sms_history_exist)
		{
			if ($sms_history_exist->poll_result > 0)
			{
				$this->error('Contactado SMS');
				return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
			}
			else
			{
				$this->sms_history_model->edit([
					'sms_result' => 1,
					'poll_result' => $response,
					'sms_date' => date('Y-m-d'),
				], $sms_history_exist->id);
				$this->success('SMS exitoso');
				return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
			}
		}

		$this->sms_history_model->create([
			'government_id' => $government_id,
			'phone_1' => $voter->phone_1,
			'phone_2' => $voter->phone_2,
			'campaign_id' => $campaign_id,
			'sms_date' => date('Y-m-d'),
			'sms_result' => 1,
			'sms_type' => 4,
			'poll_result' => $response
		]);

		$this->success('SMS exitoso');
		return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
	}

	public function get_sectors ()
	{
		$this->load->model('voters_new_model');
		$result = $this->voters_new_model->raw_query('SELECT DISTINCT(sector) as sector FROM voters_new');
		$sectors = [];
		foreach ($result->result() as $key => $row) {
			if (strlen($row->sector) > 0)
			{
				$sectors[$row->sector] = $row->sector;
			}
		}
		return $sectors;
	}

	public function update_campaign_list_call ($circunscripcion, $age, $recinto, $source_id,  $college, $gender, $married_status, $campaign_id)
	{
		$this->load->model('voters_new_model');
		$this->load->model('call_history_model');
		$query = 'SELECT v.id, v.government_id, v.first_name, v.last_name, v.phone_1, v.phone_2, c.call_date, c.call_result, c.call_type, c.poll_result FROM voters_new v LEFT JOIN call_history c ON v.government_id = c.government_id';
		$count_query = 'SELECT COUNT(*) as num FROM voters_new v';
		$parameters = ["(v.phone_1 != '' OR v.phone_2 != '')"];
		if ($circunscripcion && $circunscripcion > 0)
		{
			$parameters[] = "v.circunscripcion={$circunscripcion}";
		}
		if ($source_id && $source_id > 0)
		{
			if ($source_id == 1)
			{
				$parameters[] = " v.avanza_member_id>0 ";
			}

			if ($source_id == 2)
			{
				$parameters[] = " v.avanza_member_id=0 ";
			}

			if($source_id == 3)
			{
				$parameters[] = " v.tactical_member_id ";
			}
		}
		if ($recinto && $recinto > 0)
		{
			$parameters[] = "v.recinto='{$recinto}'";
		}
		if ($college && $college > 0)
		{
			$parameters[] = "v.colegio='{$college}'";
		}
		if ($gender && $gender != '0')
		{
			$parameters[] = "v.gender='{$gender}'";
		}
		if ($married_status && $married_status != '0')
		{
			$parameters[] = "v.married_status='{$married_status}'";
		}
		if ($age && $age > 0)
		{
			$age_mapping = $this->voters_new_model->age_mapping();
			$age_range = $age_mapping[$age];
			$parameters[] = "(v.age >= {$age_range[0]} and v.age <={$age_range[1]})";
		}
		if (count($parameters) > 0)
		{
			$query = $query . ' WHERE ' . implode(' AND ', $parameters) . ' ORDER BY RAND() LIMIT 100';
			$count_query = $count_query . ' WHERE ' . implode(' AND ', $parameters);
		}
		else
		{
			$query = $query . ' WHERE 1 ORDER BY RAND() LIMIT 100';
			$count_query = $count_query . ' WHERE 1';
		}
		$result = $this->voters_new_model->raw_query($count_query);
		$count_row = $result->result();
		if (count($count_row) == 1)
		{
			$num = $count_row[0]->num;
			$this->campaign_model->edit([
				'total' => $num,
				'count_query' => $count_query,
				'query' => $query
			], $campaign_id);
			$result = $this->voters_new_model->raw_query($query);
			$full_result = $result->result();

			foreach ($full_result as $key => $value)
			{
				$payload = [
					'government_id' => $value->government_id,
					'campaign_id' => $campaign_id,
					'call_result' => 0,
					'poll_result' => 0,
					'call_type' => 4,
					'phone_1' => $value->phone_1,
					'phone_2' => $value->phone_2,
					'call_date' => date('Y-m-d')
				];
				$this->call_history_model->create($payload);
			}

			return TRUE;
		}
	}

	public function update_campaign_list_sms ($circunscripcion, $age, $recinto, $source_id,  $college, $gender, $married_status, $campaign_id)
	{
		$this->load->model('voters_new_model');
		$this->load->model('sms_history_model');
		$query = 'SELECT v.id, v.government_id, v.first_name, v.last_name, v.phone_1, v.phone_2, c.sms_date, c.sms_result, c.sms_type, c.poll_result FROM voters_new v LEFT JOIN sms_history c ON v.government_id = c.government_id';
		$count_query = 'SELECT COUNT(*) as num FROM voters_new v';
		$parameters = ["(v.phone_1 != '' OR v.phone_2 != '')"];
		if ($circunscripcion && $circunscripcion > 0)
		{
			$parameters[] = "v.circunscripcion={$circunscripcion}";
		}
		if ($source_id && $source_id > 0)
		{
			if ($source_id == 1)
			{
				$parameters[] = "v.avanza_member_id>0";
			}
			if ($source_id == 2)
			{
				$parameters[] = "v.avanza_member_id=0";
			}

			if($source_id == 3)
			{
				$parameters[] = "v.tactical_member_id";
			}

		}
		if ($recinto && $recinto > 0)
		{
			$parameters[] = "v.recinto='{$recinto}'";
		}
		if ($college && $college > 0)
		{
			$parameters[] = "v.colegio='{$college}'";
		}
		if ($gender && $gender != '0')
		{
			$parameters[] = "v.gender='{$gender}'";
		}
		if ($married_status && $married_status != '0')
		{
			$parameters[] = "v.married_status='{$married_status}'";
		}
		if ($age && $age > 0)
		{
			$age_mapping = $this->voters_new_model->age_mapping();
			$age_range = $age_mapping[$age];
			$parameters[] = "(v.age >= {$age_range[0]} and v.age <={$age_range[1]})";
		}
		if (count($parameters) > 0)
		{
			$query = $query . ' WHERE ' . implode(' AND ', $parameters) . ' ORDER BY RAND() LIMIT 100';
			$count_query = $count_query . ' WHERE ' . implode(' AND ', $parameters);
		}
		else
		{
			$query = $query . ' WHERE 1 ORDER BY RAND() LIMIT 100';
			$count_query = $count_query . ' WHERE 1 ';
		}
		$result = $this->voters_new_model->raw_query($count_query);
		$count_row = $result->result();

		if (count($count_row) == 1)
		{
			$num = $count_row[0]->num;
			$this->campaign_model->edit([
				'total' => $num,
				'count_query' => $count_query,
				'query' => $query
			], $campaign_id);

			$result = $this->voters_new_model->raw_query($query);
			$full_result = $result->result();

			foreach ($full_result as $key => $value)
			{
				$payload = [
					'government_id' => $value->government_id,
					'campaign_id' => $campaign_id,
					'poll_result' => 0,
					'phone_1' => $value->phone_1,
					'phone_2' => $value->phone_2,
					'sms_date' => date('Y-m-d'),
					'sms_result' => 0,
					'sms_type' => 5
				];
				$this->sms_history_model->create($payload);
			}

			return TRUE;
		}
	}

	public function update_campaign_list_email ($circunscripcion, $age, $recinto, $source_id,  $college, $gender, $married_status, $campaign_id)
	{
		$this->load->model('voters_new_model');
		$query = 'SELECT v.id, v.government_id, v.first_name, v.last_name, v.email, c.email_date, c.email_result FROM voters_new v LEFT JOIN email_history c ON v.government_id = c.government_id';
		$count_query = 'SELECT COUNT(*) as num FROM voters_new v';
		$parameters = ["(v.email != '' AND v.email != null)"];

		if ($circunscripcion && $circunscripcion > 0)
		{
			$parameters[] = "v.circunscripcion={$circunscripcion}";
		}
		if ($source_id && $source_id > 0)
		{
			if ($source_id == 1)
			{
				$parameters[] = "v.avanza_member_id>0";
			}
			if ($source_id == 2)
			{
				$parameters[] = "v.avanza_member_id=0";
			}
			if ($source_id == 3)
			{
				$parameters[] = "v.tactical_member_id=0";
			}
		}
		if ($recinto && $recinto > 0)
		{
			$parameters[] = "v.recinto='{$recinto}'";
		}
		if ($college && $college > 0)
		{
			$parameters[] = "v.colegio='{$college}'";
		}
		if ($gender && $gender != '0')
		{
			$parameters[] = "v.gender='{$gender}'";
		}
		if ($married_status && $married_status != '0')
		{
			$parameters[] = "v.married_status='{$married_status}'";
		}
		if ($age && $age > 0)
		{
			$age_mapping = $this->voters_new_model->age_mapping();
			$age_range = $age_mapping[$age];
			$parameters[] = "(v.age >= {$age_range[0]} and v.age <={$age_range[1]})";
		}
		if (count($parameters) > 0)
		{
			$query = $query . ' WHERE ' . implode(' AND ', $parameters);
			$count_query = $count_query . ' WHERE ' . implode(' AND ', $parameters);
		}
		else
		{
			$query = $query . ' WHERE 1 ';
			$count_query = $count_query . ' WHERE 1 ';
		}
		$result = $this->voters_new_model->raw_query($count_query);
		$count_row = $result->result();
		if (count($count_row) == 1)
		{
			$num = $count_row[0]->num;
			$this->campaign_model->edit([
				'total' => $num,
				'count_query' => $count_query,
				'query' => $query
			], $campaign_id);
			return TRUE;
		}
	}

	public function get_colleges ()
	{
		$this->load->model('voters_new_model');
		$result = $this->voters_new_model->raw_query('SELECT DISTINCT(colegio) as colegio FROM voters_new');
		$colegios = [];
		foreach ($result->result() as $key => $row)
		{
			if (strlen($row->colegio) > 0)
			{
				$colegios[$row->colegio] = $row->colegio;
			}
		}
		return $colegios;
	}

	public function results($campaign_id)
	{
    $model = $this->campaign_model->get($campaign_id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/campaigns/0?order_by=id&direction=DESC');
		}

		$this->load->model('campaign_result_model');
		$list = $this->campaign_result_model->get_all(['campaign_id' => $campaign_id]);
		$this->_data['title'] = 'Resultado de Campañas';
		$this->_data['list'] = $list;
		return $this->render('Admin/CampaignResultView', $this->_data);
	}

	public function get_recintos ()
	{
		$this->load->model('voters_new_model');
		$result = $this->voters_new_model->raw_query('SELECT DISTINCT(recinto) as recinto, recinto_name FROM voters_new');
		$recintos = [];
		foreach ($result->result() as $key => $row) {
			if (strlen($row->recinto) > 0)
			{
				$recintos[$row->recinto] = $row->recinto . '(' . $row->recinto_name . ')';
			}
		}
		return $recintos;
	}

	public function process_results ($list)
	{
		$this->load->model('voters_new_model');
		foreach ($list as $key => $value)
		{
			$list[$key]->first_name = '';
			$list[$key]->last_name = '';
			$exist = $this->voters_new_model->get_by_field('government_id', $value->government_id);
			if ($exist)
			{
				$list[$key]->first_name = $exist->first_name;
				$list[$key]->last_name = $exist->last_name;
			}
		}
		return $list;
	}
}