<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Custom_call_list Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Custom_call_list_controller extends Admin_controller
{
    protected $_model_file = 'custom_call_list_model';
    public $_page_name = 'Lista de Llamadas CSV';

    public function __construct()
    {
        parent::__construct();
        

    }

	public function index()
	{
		$this->load->library('pagination');
		$session = $this->get_session();
        include_once __DIR__ . '/../../view_models/Custom_call_list_admin_list_view_model.php';
        $this->_data['view_model'] = new Custom_call_list_admin_list_view_model($this->custom_call_list_model);
		$this->_data['view_model']->set_list($this->custom_call_list_model->get_all());
		$this->_data['view_model']->set_heading('Lista de Llamadas CSV');
        return $this->render('Admin/Custom_call_list', $this->_data);
	}

	public function add()
	{
        include_once __DIR__ . '/../../view_models/Custom_call_list_admin_add_view_model.php';
        $this->form_validation = $this->custom_call_list_model->set_form_validation(
        $this->form_validation, $this->custom_call_list_model->get_all_validation_rule());
        $this->_data['view_model'] = new Custom_call_list_admin_add_view_model($this->custom_call_list_model);
        $this->_data['view_model']->set_heading('Lista de Llamadas CSV');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/Custom_call_listAdd', $this->_data);
        }

        $name = $this->input->post('name');
		$type = $this->input->post('type');
		$voice_file = $this->input->post('voice_file');
		$voice_file_id = $this->input->post('voice_file_id');
		$file = $this->input->post('file');
		$file_id = $this->input->post('file_id');
		
        $result = $this->custom_call_list_model->create([
            'name' => $name,
			'type' => $type,
			'voice_file' => $voice_file,
			'voice_file_id' => $voice_file_id,
			'file' => $file,
			'file_id' => $file_id,
			
        ]);

        if ($result)
        {
            				$this->load->library('csv_import_service');
				$process = $this->csv_import_service->get_csv_data(__DIR__  . '/../../../..' . $file);
				if ($process['status'])
				{
				    $this->load->model('call_history_model');
				    $count = 0;
				    foreach ($process['data'] as $key => $value)
				    {
				        $row = explode(',', $value[0]);
				$semi_row = explode(';', $value[0]);$condition = ($row && count($row) > 1);if (!$condition){$row = $semi_row;}
				        if ($row && count($row) > 1)
				        {
				            $name = $row[0] ?? '';
				            $phone = $this->clean_phone($row[1]);
				
				            if (strlen($phone) < 1)
				            {
				                continue;
				            }
				
				            $this->call_history_model->create([
				                'government_id' => $name,
				                'phone_1' => $phone,
				                'phone_2' => '',
				                'campaign_id' => $result,
				                'call_date' => date('Y-m-d'),
				                'call_result' => 0,
				                'call_type' => 5,
				                'poll_result' => 0
				            ]);
				            $count++;
				        }
				    }
				$this->load->model('campaign_result_model');
				$this->campaign_result_model->create([
				'campaign_id' => $result,
				'type' => 6,
				'circunscripcion' => 0,
				'status' => 1,
				'recinto' => 0,
				'college' => 0,
				'gender' => 0,
				'age' => 0,
				'martial_status' => 'S',
				'reached' => 0,
				'total' => $count,
				'respond_1' => 0,
				'respond_2' => 0,
				'respond_3' => 0,
				'engaged' => 0,
				'dne' => 0
				]);
				}

            return $this->redirect('/admin/custom_call_list', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/Custom_call_listAdd', $this->_data);
	}








private function clean_phone ($phone){if (!$phone){return '';}$cleanPhones = trim(str_replace(['(', ')', '-', ' '], ['','','', ''], $phone));$cleanPhone = '';$parts = explode(',', $cleanPhones);if (count($parts) > 0){foreach ($parts as $singlePhone) {$cleanSinglePhone = trim($singlePhone);if (strlen($cleanSinglePhone) === 10){return $cleanSinglePhone;}}}else{if (strlen($cleanPhones) === 10){$cleanPhone = $cleanPhones;}}return $cleanPhone;}
}