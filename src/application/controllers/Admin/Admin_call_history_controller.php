<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Call_history Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_call_history_controller extends Admin_controller
{
    protected $_model_file = 'call_history_model';
    public $_page_name = 'Campañas';

    public function __construct()
    {
        parent::__construct();

    }

    public function add_custom($id)
	{
        include_once __DIR__ . '/../../view_models/Call_history_admin_add_view_model.php';
        $this->load->model('voters_new_model');
        $this->load->model('campaign_model');
        $this->form_validation = $this->call_history_model->set_form_validation(
        $this->form_validation, [
            ['government_id', 'Cédula', 'required']
        ]);
        $this->_data['view_model'] = new Call_history_admin_add_view_model($this->call_history_model);
        $this->_data['view_model']->set_heading('Campañas');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/Call_historyAdd', $this->_data);
        }

        $government_id = $this->input->post('government_id');
        $voter_id = $this->input->post('voter_id');
        $campaign = $this->campaign_model->get($id);
        $payload = [
            'government_id' => $government_id,
            'campaign_id' => $id,
            'call_result' => 0,
            'poll_result' => 0,
            'call_type' => $campaign->type
        ];
        if ($voter_id)
        {
            $voter = $this->voters_new_model->get($voter_id);
            $payload['phone_1'] = $voter->phone_1;
            $payload['phone_2'] = $voter->phone_2;
        }
        $result = $this->call_history_model->create($payload);

        if ($result)
        {
            return $this->redirect('/admin/campaigns/view/' . $id, 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/Call_historyAdd', $this->_data);
	}

	public function delete($id)
	{
        $model = $this->call_history_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/call_history/0');
        }

        $result = $this->call_history_model->real_delete($id);

        if ($result)
        {

            return $this->redirect('/admin/call_history/0', 'refresh');
        }

        $this->error('Error');
        return redirect('/admin/call_history/0');
	}

    public function delete_custom($campaign_id, $id)
	{
        $model = $this->call_history_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/campaigns/view/' . $campaign_id);
        }

        $result = $this->call_history_model->real_delete($id);

        if ($result)
        {
            $this->error('Borrada');
            return $this->redirect('/admin/campaigns/view/' . $campaign_id, 'refresh');
        }

        $this->error('Error');
        return $this->redirect('/admin/campaigns/view/' . $campaign_id, 'refresh');
	}

}