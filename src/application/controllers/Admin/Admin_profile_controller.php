<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';

/**
 * Admin Profile Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_profile_controller extends Admin_controller
{
    protected $_model_file = 'user_model';
    public $_page_name = 'Perfil';

    public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
        $session = $this->get_session();
        $model = $this->user_model->get($session['user_id']);
        $id = $session['user_id'];

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/dashboard');
        }

        include_once __DIR__ . '/../../view_models/Admin_profile_view_model.php';
        $email_validation_rules = 'required|valid_email';

		if ($this->input->post('email') != $session['email'])
		{
			$email_validation_rules .= '|is_unique[user.email]';
		}

		$this->form_validation->set_rules('email', 'Correo', $email_validation_rules);
		$this->form_validation->set_rules('password', 'Contraseña', '');
		$this->form_validation->set_rules('first_name', 'Nombre', 'required');
        $this->form_validation->set_rules('last_name', 'Apellidos', 'required');

        $this->_data['view_model'] = new Admin_profile_view_model($this->user_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Admin');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/Profile', $this->_data);
        }

        $email = $this->input->post('email');
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
        $password = $this->input->post('password');

        $payload = [
            'email' => $email,
			'first_name' => $first_name,
			'last_name' => $last_name
        ];

        if (strlen($password) > 0)
        {
            $payload['password'] = password_hash($password, PASSWORD_BCRYPT);
        }

        $result = $this->user_model->edit_raw($payload, $id);

        if ($result)
        {
            $this->success('Almacenado');
            return $this->redirect('/admin/dashboard', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/Profile', $this->_data);
	}
}