<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
include_once __DIR__ . '/../../services/Num_users_report_service.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Num_users Report Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_num_users_report_controller extends Admin_controller
{
    protected $_model_file = 'user_model';
    public $_page_name = 'Reporte de Usuarios';

    public function __construct()
    {
        parent::__construct();
    }

    public function index ()
    {
        $export = ($this->input->get('export', TRUE)) ? $this->input->get('export', TRUE) : FALSE;
		$this->set_fields('start_date');
		$this->set_fields('end_date');
		$this->_data['list'] = [];

		$start_date = $this->input->get('start_date', TRUE);
		$end_date = $this->input->get('end_date', TRUE);


        $service = new Num_users_report_service($this->user_model, $this->_data['start_date'], $this->_data['end_date']);
        $result = $service->process([]);
		if ($export)
		{
			$service->generate_csv(['Date', 'Number of Users'], $result, 'report_users_' . $start_date . '_' . $end_date . '.csv');

		}
		else
		{
			$this->_data['list'] = $result;
			return $this->render('Admin/Num_usersReport', $this->_data);
		}
    }

    private function set_fields ($field_name)
    {
        $this->_data[$field_name] = ($this->input->get($field_name, TRUE)) ? $this->input->get($field_name, TRUE) : NULL;
    }
}