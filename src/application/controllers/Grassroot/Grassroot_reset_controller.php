<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once __DIR__ . '/../../services/User_service.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Reset Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Grassroot_reset_controller extends Manaknight_Controller
{
	public function index ($reset_token)
	{
        $this->load->model('grassroot_user_model');
        $this->load->model('token_model');

        $service = new User_service($this->grassroot_user_model);
        $service->set_token_model($this->token_model);

        $valid_user = $service->valid_reset_token($reset_token);

        if (!$valid_user)
        {
            $this->error('Su correo no esta registrado en nuestros sistemas.');
            return $this->redirect('/grassroot/login');
        }

        $this->_data['reset_token'] = $reset_token;

        $this->form_validation->set_rules('password', 'Contraseña', 'trim|required');
        $this->form_validation->set_rules('confirm_password', 'xyzConfirm Password', 'trim|required|matches[password]');

        if ($this->form_validation->run() === FALSE)
        {
            echo $this->load->view('Grassroot/Reset', $this->_data, TRUE);
            exit;
        }

        $password = $this->input->post('password');
        $password_reseted = $service->reset_password($valid_user->id, $password);

        if ($password_reseted)
        {
            $service->invalidate_token($reset_token, $valid_user->id);
            $this->success('Contraseña actualizada. Ya puede acceder');
            return $this->redirect('/grassroot/login');
        }

        $this->error('Token de actualizacion es invalido.');
        return $this->redirect('/grassroot/login');
	}
}