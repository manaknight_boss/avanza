<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Grassroot_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Recintos Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Recinto_coordinator_controller extends Grassroot_controller
{
    protected $_model_file = 'recinto_coordinator_model';
    public $_page_name = 'Recintos';

    public function __construct()
    {
        parent::__construct();
        

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Recintos_grassroot_list_paginate_view_model.php';
        $session = $this->get_session();
        $format = $this->input->get('format', TRUE) ?? 'view';
        $order_by = $this->input->get('order_by', TRUE) ?? '';
        $direction = $this->input->get('direction', TRUE) ?? 'ASC';

        $this->_data['view_model'] = new Recintos_grassroot_list_paginate_view_model(
            $this->recinto_coordinator_model,
            $this->pagination,
            '/grassroot/recintos/0');
        $this->_data['view_model']->set_heading('Recintos');
        $this->_data['view_model']->set_recinto(($this->input->get('recinto', TRUE) != NULL) ? $this->input->get('recinto', TRUE) : NULL);
		$this->_data['view_model']->set_recinto_name(($this->input->get('recinto_name', TRUE) != NULL) ? $this->input->get('recinto_name', TRUE) : NULL);
		$this->_data['view_model']->set_collegio(($this->input->get('collegio', TRUE) != NULL) ? $this->input->get('collegio', TRUE) : NULL);
		$this->_data['view_model']->set_coordinator_name(($this->input->get('coordinator_name', TRUE) != NULL) ? $this->input->get('coordinator_name', TRUE) : NULL);
		$this->_data['view_model']->set_coordinator_government_id(($this->input->get('coordinator_government_id', TRUE) != NULL) ? $this->input->get('coordinator_government_id', TRUE) : NULL);
		$this->_data['view_model']->set_assigned(($this->input->get('assigned', TRUE) != NULL) ? $this->input->get('assigned', TRUE) : NULL);
		$this->_data['view_model']->set_personel(($this->input->get('personel', TRUE) != NULL) ? $this->input->get('personel', TRUE) : NULL);
		$this->_data['view_model']->set_present(($this->input->get('present', TRUE) != NULL) ? $this->input->get('present', TRUE) : NULL);
		
        $where = [
            'recinto' => $this->_data['view_model']->get_recinto(),
			'recinto_name' => $this->_data['view_model']->get_recinto_name(),
			'collegio' => $this->_data['view_model']->get_collegio(),
			'coordinator_name' => $this->_data['view_model']->get_coordinator_name(),
			'coordinator_government_id' => $this->_data['view_model']->get_coordinator_government_id(),
			'assigned' => $this->_data['view_model']->get_assigned(),
			'personel' => $this->_data['view_model']->get_personel(),
			'present' => $this->_data['view_model']->get_present(),
			
        ];

        $this->_data['view_model']->set_total_rows($this->recinto_coordinator_model->count($where));

        $this->_data['view_model']->set_format_layout($this->_data['layout_clean_mode']);
        $this->_data['view_model']->set_per_page(25);
        $this->_data['view_model']->set_order_by($order_by);
        $this->_data['view_model']->set_sort($direction);
        $this->_data['view_model']->set_sort_base_url('/grassroot/recintos/0');
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->recinto_coordinator_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where,
            $order_by,
            $direction));

        if ($format != 'view')
        {
            return $this->output->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($this->_data['view_model']->to_json()));
        }

        return $this->render('Grassroot/Recintos', $this->_data);
	}



	public function edit($id)
	{
        $model = $this->recinto_coordinator_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/grassroot/recintos/0');
        }

        include_once __DIR__ . '/../../view_models/Recintos_grassroot_edit_view_model.php';
        $this->form_validation = $this->recinto_coordinator_model->set_form_validation(
        $this->form_validation, $this->recinto_coordinator_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Recintos_grassroot_edit_view_model($this->recinto_coordinator_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Recintos');
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Grassroot/RecintosEdit', $this->_data);
        }

        $recinto = $this->input->post('recinto');
		$recinto_name = $this->input->post('recinto_name');
		$collegio = $this->input->post('collegio');
		$coordinator_name = $this->input->post('coordinator_name');
		$coordinator_government_id = $this->input->post('coordinator_government_id');
		$password = $this->input->post('password');
		
        $result = $this->recinto_coordinator_model->edit([
            'recinto' => $recinto,
			'recinto_name' => $recinto_name,
			'collegio' => $collegio,
			'coordinator_name' => $coordinator_name,
			'coordinator_government_id' => $coordinator_government_id,
			'password' => $password,
			
        ], $id);

        if ($result)
        {
            
            return $this->redirect('/grassroot/recintos/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Grassroot/RecintosEdit', $this->_data);
	}







}