<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once __DIR__ . '/../../services/User_service.php';
include_once 'Grassroot_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Login Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Grassroot_login_controller extends Manaknight_Controller
{
	protected $_redirect = '/grassroot/voters/0';

    public function __construct()
    {
        parent::__construct();
    }

	public function index ()
	{
        $this->load->model('grassroot_user_model');
        $this->load->helper('cookie');

        $service = new User_service($this->grassroot_user_model);

        $this->form_validation->set_rules('email', 'Cédula', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            echo $this->load->view('Grassroot/Login', $this->_data, TRUE);
            exit;
        }

        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $redirect = $service->get_redirect($this->input->cookie('redirect', TRUE), $this->_redirect);

        $authenticated_user = $service->login($email, $password);

        if ($authenticated_user)
        {
            delete_cookie('redirect');
            $this->set_session('user_id', (int) $authenticated_user->id);
            $this->set_session('email', (string) $authenticated_user->email);
            $this->set_session('role', (string) $authenticated_user->role_id);
            return $this->redirect($redirect);
        }

        $this->error('Correo o Contraseña invalido.');
        return $this->redirect('grassroot/login');
    }

    public function logout ()
    {
        $this->destroy_session();
		return $this->redirect('grassroot/login');
    }
}