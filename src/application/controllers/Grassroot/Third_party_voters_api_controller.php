<?php

class Third_party_voters_api_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function index ($government_id)
    {
        $this->load->model('voters_new_model');
        $exist = $this->voters_new_model->get_by_field('government_id', $government_id);
        if ($exist)
        {
          $this->form_validation->set_rules('has_voted', 'Voted', 'required|integer');

          if ($this->form_validation->run() === FALSE)
          {
            $data = [];
            $data['code'] = 403;
            $data['success'] = FALSE;
            $data['error'] = [
              'table' => 'El campo has_voted debe contener un número entero(0, 1).'
            ];
            return $this->output->set_content_type($this->_supported_formats[$this->_format])
                ->set_status_header(403)
                ->set_output(json_encode($data));

          }

          $has_voted = $this->input->post('has_voted');
          if ($has_voted > 0)
          {
            $this->voters_new_model->edit([
              'has_voted_municipal_2020' => 1
            ], $exist->id);
          }
          else
          {
            $this->voters_new_model->edit([
              'has_voted_municipal_2020' => 0
            ], $exist->id);
          }

          return $this->output->set_content_type('application/json')
          ->set_status_header(200)
          ->set_output(json_encode(array(
            'code' => 200,
            'success' => TRUE
          )));
        }
        else
        {
          $data = [];
          $data['code'] = 404;
          $data['success'] = FALSE;
          $data['error'] = [
            'government_id' => 'Cedula no existe'
          ];
          return $this->output->set_content_type('application/json')
          ->set_status_header(404)
          ->set_output(json_encode($data));
        }
    }
}