<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Grassroot_api_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Voters API Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Grassroot_voters_no_auth_api_controller extends Grassroot_api_controller
{
    protected $_model_file = 'voters_new_model';

    public function __construct()
    {
        parent::__construct();
    }

		protected function _middleware()
    {
        return [
        ];
		}

		public function voted($table, $flag=1)
		{
			$voter_turns = $this->input->post('voter_turns');
			if (!isset($voter_turns) || empty($voter_turns))
			{
				return $this->_render_custom_error('Invalidos');
			}

			$clean_list = [];
			foreach ($voter_turns as $key => $value)
			{
				$voter = $this->voters_new_model->get_by_fields([
					'voter_turn' => (string)$value,
					'colegio' => (string)$table
				]);

				if ($voter)
				{
					$payload = [
						'has_voted_municipal_2020' => 1
					];

					if ($flag == 2)
					{
						$payload = [
							'has_voted_general_2020' => 1
						];

					}
					$this->voters_new_model->edit($payload, $voter->id);
					$clean_list[] = $value;
					$this->curl('http://avanzaplataforms.azurewebsites.net/api/v1/avanzapi/votos/id/' . $voter->government_id, [
					// $this->curl('http://avanzaplataforms.azurewebsites.net/api/v1/avanzapi/poll/' . $voter->recinto, [
						'HasVoted' => true,
						'WasTexted' => false,
						'WasEmailed' => false,
						'WasPolled' => false,
						'Cedula' => $voter->government_id
					]);
				}

			}

			return $this->success([
					'voters' => $clean_list
			], 200);
		}

		private function curl($url, $payload) {
			//create a new cURL resource
			$ch = curl_init($url);

			//setup request to send json via POST
			$payload = json_encode($payload);

			//attach encoded JSON string to the POST fields
			curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

			//set the content type to application/json
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

			//return response instead of outputting
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			//execute the POST request
			$result = curl_exec($ch);
			error_log(print_r($result, true));
			//close cURL resource
			curl_close($ch);
		}


		public function government($id)
		{
			$model = $this->voters_new_model->get_by_field('government_id', (string)$id);

			if (!$model)
			{
				return $this->_render_custom_error([
					'error' => 'Error'
				]);
			}

			include_once __DIR__ . '/../../view_models/Voters_grassroot_view_view_model.php';
			$this->_data['view_model'] = new Voters_grassroot_view_view_model($this->voters_new_model);
			$this->_data['view_model']->set_model($model);
			return $this->success(['data' => $this->_data['view_model']->to_json()], 200);
		}

		public function profile()
		{
			$this->load->model('grassroot_user_model');
			$this->load->model('raw_recintos_model');
			$model = $this->grassroot_user_model->get($this->get_user_id());

			if (!$model)
			{
				return $this->_render_custom_error([
					'error' => 'Error'
				]);
			}
			$recinto_query_1 = $this->grassroot_user_model->raw_query("Select * FROM raw_colegios_f WHERE Colegio=\"{$model->stripe_id}\"");
			$recinto_result_1 = $recinto_query_1->result();
			$recinto_query_2 = $this->grassroot_user_model->raw_query("Select * FROM raw_colegios_f WHERE Colegio=\"{$model->refer}\"");
			$recinto_result_2 = $recinto_query_2->result();
			$recinto_query_3 = $this->grassroot_user_model->raw_query("Select * FROM raw_colegios_f WHERE Colegio=\"{$model->phone}\"");
			$recinto_result_3 = $recinto_query_3->result();
			$recinto1_id = $recinto_result_1 ? $recinto_result_1[0]->ID_Recinto: 0;
			$recinto2_id = $recinto_result_2 ? $recinto_result_2[0]->ID_Recinto: 0;
			$recinto3_id = $recinto_result_3 ? $recinto_result_3[0]->ID_Recinto: 0;
			$recinto1 = $this->raw_recintos_model->get_by_field('RecintoId', $recinto1_id);
			$recinto2 = $this->raw_recintos_model->get_by_field('RecintoId', $recinto2_id);
			$recinto3 = $this->raw_recintos_model->get_by_field('RecintoId', $recinto3_id);

			return $this->success(['data' => [
				'id' => $model->id,
				'email' => $model->email,
				'first_name' => $model->first_name,
				'last_name' => $model->last_name,
				'table1' => (string)$model->stripe_id,
				'table2' => (string)$model->refer,
				'table3' => (string)$model->phone,
				'recinto1' => $recinto1 ? trim($recinto1->Recinto) : '',
				'recinto2' => $recinto2 ? trim($recinto2->Recinto) : '',
				'recinto3' => $recinto3 ? trim($recinto3->Recinto) : ''
			]], 200);
		}

		public function final($id)
		{
			$this->load->model('final_count_model');
			$model = $this->final_count_model->get_by_field('table', $id);

			if (!$model)
			{
				return $this->_render_custom_error([
					'error' => 'Error'
				]);
			}

			include_once __DIR__ . '/../../view_models/Final_grassroot_view_view_model.php';
			$this->_data['view_model'] = new Final_grassroot_view_view_model($this->final_count_model);
			$this->_data['view_model']->set_model($model);
			return $this->success(['data' => $this->_data['view_model']->to_json()], 200);
		}

		public function table($id, $flag=1)
		{
			$this->load->model('voters_new_model');
			$count = $this->voters_new_model->count([
				'colegio' => (string)$id,
				'(avanza_member_id > 0 OR tactical_member_id > 0)'
			]);

			/**
			 * Steps:
			 * 1.Count Total Voters
			 * 2.Count Total Voted
			 * 3.Do math to see how many didnt vote
			 * 4.Get cordinator list
			 * 5.Count how many voters per cordinator
			 * 6.Count how many voters not voted per cordinator
			 * 7.List out all voters
			 */

			 /**
				* Queries:
				* 1.Count WHERE table
				* 2.Count WHERE table and voted_general/municipal = 1
				* 3.Do subtraction to know difference
				* 4.Get distinct cordinator list with counts, how many not voted
				*/
			$voted_payload = ['colegio' => (string)$id, 'avanza_member_id > 0'];
			if ($flag == 1)
			{
				$voted_payload['has_voted_general_2020'] = 1;
			}

			if ($flag == 2)
			{
				$voted_payload['has_voted_municipal_2020'] = 1;
			}

			$data = [
				'total_voters' => $count,
				'total_voted' => $this->voters_new_model->count($voted_payload),
			];

			$data['total_not_voted'] = $data['total_voters'] - $data['total_voted'];

			if ($data['total_not_voted'] < 0)
			{
				$data['total_not_voted'] = 0;
			}

			$cordinator_query = $this->voters_new_model->raw_query("SELECT cordinator_id, COUNT(*) as num FROM voters_new WHERE colegio='{$id}' and (avanza_member_id > 0 OR tactical_member_id > 0) GROUP BY cordinator_id;");
			$cordinators = [];

			foreach ($cordinator_query->result() as $key => $row)
			{
				$payload = [
					'id' => $row->cordinator_id,
					'government_id' => '',
					'first_name' => '',
					'last_name' => '',
					'total' => (int)$row->num,
					'left' => $row->num
				];
				if ($payload['id'] == "0")
				{
					$payload['government_id'] = '';
					$payload['first_name'] = 'CFD';
					$payload['last_name'] = '';
				}
				$cordinator_payload = [
					'cordinator_id' => $row->cordinator_id,
					'(avanza_member_id > 0 OR tactical_member_id > 0)'
				];

				if ($flag == 1)
				{
					$cordinator_payload['has_voted_general_2020'] = 1;
				}

				if ($flag == 2)
				{
					$cordinator_payload['has_voted_municipal_2020'] = 1;
				}

				$payload['left'] = $payload['total'] - $this->voters_new_model->count($cordinator_payload);

				if ($payload['left'] < 0)
				{
					$payload['left'] = 0;
				}

				$cordinator = $this->voters_new_model->get((int)$row->cordinator_id);
				if ($cordinator)
				{
					$payload['government_id'] = $cordinator->government_id;
					$payload['first_name'] = $cordinator->first_name;
					$payload['last_name'] = $cordinator->last_name;
					$payload['phone_1'] = $cordinator->phone_1;
					$payload['phone_2'] = $cordinator->phone_2;
				}

				$cordinators[] = $payload;
			}

			$data['cordinators'] = $cordinators;
			$this->_data['data'] = $data;
			return $this->success(['data' => $this->_data['data']], 200);
		}

		public function cordinator_all($id)
		{
			$this->load->model('voters_new_model');
			if ($id > 0)
			{
				$model = $this->voters_new_model->get($id);

				if (!$model)
				{
					return $this->_render_custom_error([
						'error' => 'Error'
					]);
				}
			}
			else
			{
				$id = 0;
			}

			$cordinator_query = $this->voters_new_model->raw_query("SELECT * FROM voters_new WHERE (is_cordinator=1 OR is_tactical_cordinator=1) AND cordinator_id={$id} AND (avanza_member_id > 0 OR tactical_member_id > 0)");

			$payload = [
				'total' => 0,
				'left' => 0,
				'first_name' => '',
				'last_name' => '',
				'government_id' => '',
				'phone_1' => '',
				'phone_2' => '',
				'email' => ''
			];
			$people = [];
			$result = $cordinator_query->result();

			foreach ($result as $key => $row)
			{
				$payload['total'] = $payload['total'] + 1;
				$condition = ($row->has_voted_municipal_2020 == 0);

				if ($condition)
				{
					$payload['left'] = $payload['left'] + 1;
					$people[] = [
						'first_name' => $row->first_name,
						'last_name' => $row->last_name,
						'phone_1' => $row->phone_1,
						'phone_2' => $row->phone_2,
						'government_id' => $row->government_id,
						'married_status' => $row->married_status,
						'gender' => $row->gender,
						'email' => $row->email,
						'left' => 0
					];
				}

				$cordinator_payload = [
					'cordinator_id' => $row->cordinator_id,
					'(avanza_member_id > 0 OR tactical_member_id > 0)'
				];

				$cordinator_payload['has_voted_municipal_2020'] = 1;

				$payload['voted'] = $this->voters_new_model->count($cordinator_payload);
				$payload['left'] = $payload['total'] - $payload['voted'];

				if ($payload['left'] < 0)
				{
					$payload['left'] = 0;
				}
			}

			$data['people'] = $people;

			if ($id > 0)
			{
				$cordinator = $this->voters_new_model->get($id);
				$payload['first_name'] = $cordinator->first_name;
				$payload['last_name'] = $cordinator->last_name;
				$payload['government_id'] = $cordinator->government_id;
				$payload['phone_1'] = $cordinator->phone_1;
				$payload['phone_2'] = $cordinator->phone_2;
				$payload['email'] = $cordinator->email;
			}
			$data['summary'] = $payload;
			$this->_data['data'] = $data;

			return $this->success(['data' => $this->_data['data']], 200);
		}

		public function cordinator($id, $table, $flag=1)
		{
			$this->load->model('voters_new_model');
			if ($id > 0)
			{
				$model = $this->voters_new_model->get($id);

				if (!$model)
				{
					return $this->_render_custom_error([
						'error' => 'Error'
					]);
				}
			}
			else
			{
				$id = 0;
			}

			$cordinator_query = $this->voters_new_model->raw_query("SELECT * FROM voters_new WHERE cordinator_id={$id} AND colegio='{$table}' AND (avanza_member_id > 0 OR tactical_member_id > 0)");

			$payload = [
				'total' => 0,
				'left' => 0,
				'first_name' => '',
				'last_name' => '',
				'government_id' => '',
				'phone_1' => '',
				'phone_2' => '',
				'email' => ''
			];
			$people = [];
			$result = $cordinator_query->result();

			foreach ($result as $key => $row)
			{
				$payload['total'] = $payload['total'] + 1;
				$condition = ($flag == 1 && $row->has_voted_general_2020 == 0) ||
				($flag == 2 && $row->has_voted_municipal_2020 == 0);

				if ($condition)
				{
					$payload['left'] = $payload['left'] + 1;
					$people[] = [
						'first_name' => $row->first_name,
						'last_name' => $row->last_name,
						'phone_1' => $row->phone_1,
						'phone_2' => $row->phone_2,
						'government_id' => $row->government_id,
						'married_status' => $row->married_status,
						'gender' => $row->gender,
						'email' => $row->email
					];
				}

				$cordinator_payload = [
					'cordinator_id' => $row->cordinator_id,
					'(avanza_member_id > 0 OR tactical_member_id > 0)'
				];

				if ($flag == 1)
				{
					$cordinator_payload['has_voted_general_2020'] = 1;
				}

				if ($flag == 2)
				{
					$cordinator_payload['has_voted_municipal_2020'] = 1;
				}

				$payload['voted'] = $this->voters_new_model->count($cordinator_payload);
				$payload['left'] = $payload['total'] - $payload['voted'];

				if ($payload['left'] < 0)
				{
					$payload['left'] = 0;
				}
			}

			$data['people'] = $people;

			if ($id > 0)
			{
				$cordinator = $this->voters_new_model->get($id);
				$payload['first_name'] = $cordinator->first_name;
				$payload['last_name'] = $cordinator->last_name;
				$payload['government_id'] = $cordinator->government_id;
				$payload['phone_1'] = $cordinator->phone_1;
				$payload['phone_2'] = $cordinator->phone_2;
				$payload['email'] = $cordinator->email;
			}
			$data['summary'] = $payload;
			$this->_data['data'] = $data;

			return $this->success(['data' => $this->_data['data']], 200);
		}

		public function add_final_count ()
    {
				$this->load->model('image_model');
				$this->load->model('final_count_model');
				$this->form_validation->set_rules('table', 'Table', 'required|is_unique[final_count.table]');

        if ($this->form_validation->run() === FALSE)
        {
					$data = [];
					$data['code'] = 403;
					$data['success'] = FALSE;
					$data['error'] = [
						'table' => 'Ya existe una imagen de acta para este colegio. No se permiten duplicados.'
					];
					return $this->output->set_content_type($this->_supported_formats[$this->_format])
							->set_status_header(403)
							->set_output(json_encode($data));

				}

				$table = $this->input->post('table');
				$this->load->library('mime_service');

				if  (!(isset($_FILES) && count($_FILES) > 0 && isset($_FILES['file'])))
        {
					return $this->output->set_content_type('application/json')
							->set_status_header(403)
							->set_output(json_encode([
									'message' => 'El archivo no existe'
							]));
        }

        $file = $_FILES['file'];
        $size = $file['size'];
        $path = $file['tmp_name'];
        $type = $file['type'];
        $extension = $this->mime_service->get_extension($type);

        if ($size > $this->config->item('upload_byte_size_limit'))
        {
            return $this->output->set_content_type('application/json')
            ->set_status_header(403)
            ->set_output(json_encode([
                'message' => 'Imgen es demasiado pesada'
            ]));
        }

        $filename = md5(uniqid() . time()) . $extension;
        $width = 0;
        $height = 0;
        $user_id = 0;
        $image_path = __DIR__ . '/../../../../uploads/';

        if (!move_uploaded_file($path, $image_path . $filename))
        {
            return $this->output->set_content_type('application/json')
            ->set_status_header(403)
            ->set_output(json_encode([
                'message' => 'No se pudo cargar el archivo'
            ]));
        }

        $image_id = $this->image_model->create([
            'url' => '/uploads/' . $filename,
            'type' => 4,
            'user_id' => $user_id,
            'width' => $width,
            'caption' => '',
            'height' => $height
				]);

				$this->final_count_model->create([
					'table' => $table,
					'file' => '/uploads/' . $filename,
					'file_id' => $image_id
				]);

        return $this->success([], 200);
		}
}