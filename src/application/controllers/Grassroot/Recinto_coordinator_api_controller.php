<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Grassroot_api_auth_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Recinto Cordinator API Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Recinto_coordinator_api_controller extends Grassroot_api_auth_controller
{
    protected $_model_file = 'recinto_coordinator_model';

    public function __construct()
    {
        parent::__construct();
    }

		public function login ()
		{
			$this->load->model('recinto_coordinator_model');
			$this->form_validation->set_rules('government_id', 'Cédula', 'trim|required');
			$this->form_validation->set_rules('password', 'Contraseña', 'trim|required');

			if ($this->form_validation->run() === FALSE)
			{
					return $this->_render_validation_error();
			}

			$government_id = $this->input->post('government_id');
			$password = $this->input->post('password');
			$exist = $this->recinto_coordinator_model->get_by_field('coordinator_government_id', $government_id);

			if (!$exist)
			{
				return $this->_render_custom_error([
					'government_id' => 'Cédula invalido.'
				]);
			}

			if ($password !== $exist->password)
			{
				return $this->_render_custom_error([
					'government_id' => 'Cédula invalido.'
				]);
			}

			return $this->success([
				'success' => TRUE
			], 200);
		}

		public function profile ($government_id)
		{
			$this->load->model('recinto_coordinator_model');

			$exist = $this->recinto_coordinator_model->get_by_field('coordinator_government_id', $government_id);

			if (!$exist)
			{
				return $this->_render_custom_error([
					'government_id' => 'Cédula invalido.'
				]);
			}

			unset($exist->password);

			return $this->success([
				'data' => $exist
			], 200);
		}

		public function recinto ($recinto)
		{
			$this->load->model('recinto_collegio_personnel_model');
			$this->load->model('raw_recintos_model');

			$results = $this->recinto_collegio_personnel_model->get_all([
				'recinto' => $recinto
			]);
			$recinto_found = $this->raw_recintos_model->get_by_field('RecintoId', $recinto);
			$recinto_name = $recinto_found->Recinto;
			$num = count($results);
			return $this->success([
				'data' => $results,
				'college' => $num,
				'personal' => 6 * $num,
				'present' => $this->count_present($results),
				'recinto' => $recinto,
				'recinto_name' => $recinto_name,
			], 200);
		}

		public function dashboard ($recinto)
		{
			$this->load->model('recinto_collegio_personnel_model');
			$this->load->model('voters_model');
			$this->load->model('raw_recintos_model');
			$data = [];
			$results = $this->recinto_collegio_personnel_model->get_all([
				'recinto' => $recinto
			]);
			$recinto_found = $this->raw_recintos_model->get_by_field('RecintoId', $recinto);
			$recinto_name = $recinto_found->Recinto;
			$full_total = 0;
			$full_voted = 0;
			foreach ($results as $key => $value)
			{
				$payload = [
					'college' => $value->collegio,
					'recinto' => $recinto,
					'staff_lead_name' => $value->staff_lead_name,
					'phone_1' => '',
					'phone_2' => '',
					'percentage' => 0,
				];
				if (strlen($value->staff_lead_government_id) > 0)
				{
					$exist = $this->voters_model->get_by_field('government_id', $value->staff_lead_government_id);
					if ($exist)
					{
						$payload['phone_1'] = $this->clean_phone($exist->phone_1);
						$payload['phone_2'] = $this->clean_phone($exist->phone_2);
					}

					$total = $this->voters_model->count(['colegio' => $value->collegio, 'recinto' => $value->recinto]);
					$avanza = $this->voters_model->count(['colegio' => $value->collegio, 'recinto' => $value->recinto, 'avanza_member_id > 0 OR tactical_member_id > 0', 'has_voted_municipal_2020' => 1]);
					if ($total !== NULL && $avanza !== NULL && $total !== 0)
					{
						$payload['percentage'] = number_format($avanza / $total * 100, 2);
						$full_total = $full_total + (int)$total;
						$full_voted = $full_voted + (int)$avanza;
					}
				}
				$data[] = $payload;
			}

			$full_not_voted = $full_total - $full_voted;

			if ($full_total - $full_voted < 0)
			{
				$full_not_voted = 0;
			}

			return $this->success([
				'recinto' => $recinto,
				'recinto_name' => $recinto_name,
				'data' => $data,
				'total' => $full_total,
				'voted' => $full_voted,
				'not_voted' => $full_not_voted
			], 200);
		}

		public function update ($id, $field, $value)
		{
			$this->load->model('recinto_collegio_personnel_model');

			$this->recinto_collegio_personnel_model->edit([
				$field => $value
			], $id);
			return $this->success([], 200);
		}

		private function count_present ($result)
		{
			$count = 0;
			foreach ($result as $key => $value)
			{
				if ($value->staff_lead_present == 1)
				{
					$count += 1;
				}
				if ($value->transmitter_present == 1)
				{
					$count += 1;
				}
				if ($value->delegate_1_present == 1)
				{
					$count += 1;
				}
				if ($value->delegate_2_present == 1)
				{
					$count += 1;
				}
				if ($value->runner_1_present == 1)
				{
					$count += 1;
				}
				if ($value->runner_2_present == 1)
				{
					$count += 1;
				}
			}
			return $count;
		}

		public function personnel ($recinto, $college)
		{
			$this->load->model('recinto_collegio_personnel_model');
			$this->load->model('voters_model');

			$results = $this->recinto_collegio_personnel_model->get_all([
				'recinto' => $recinto,
				'collegio' => $college,
			]);
			$row = $results[0];
			$present_count = 0;
			$phone = '';
			if ($row && strlen($row->staff_lead_government_id) > 0)
			{
				$staff = $this->voters_model->get_by_field('government_id', $row->staff_lead_government_id);
				if ($staff)
				{
					$phone_1 = $this->clean_phone(($staff->phone_1));
					$phone_2 = $this->clean_phone(($staff->phone_2));
					if ($phone_1)
					{
						$phone = $phone_1;
					}
					else
					{
						$phone = $phone_2;
					}
				}
			}

			if ($row->staff_lead_present == 1) {
				$present_count++;
			}

			if ($row->transmitter_present == 1) {
				$present_count++;
			}

			if ($row->delegate_1_present == 1) {
				$present_count++;
			}

			if ($row->delegate_2_present == 1) {
				$present_count++;
			}

			if ($row->runner_1_present == 1) {
				$present_count++;
			}

			if ($row->runner_2_present == 1) {
				$present_count++;
			}
			return $this->success([
				'data' => $results,
				'personnel' => 6,
				'present' => $present_count,
				'phone' => $phone
			], 200);
		}

		private function clean_phone ($phone)
    {
        if (!$phone)
        {
            return '';
        }

        $cleanPhones = trim(str_replace(['(', ')', '-', ' '], ['','','', ''], $phone));
        $cleanPhone = '';
        $parts = explode(',', $cleanPhones);
        if (count($parts) > 0)
        {
            foreach ($parts as $singlePhone) {
                $cleanSinglePhone = trim($singlePhone);
                if (strlen($cleanSinglePhone) === 10)
                {
                    return $cleanSinglePhone;
                }
            }
        }
        else
        {
            if (strlen($cleanPhones) === 10)
            {
                $cleanPhone = $cleanPhones;
            }
        }
        return $cleanPhone;
		}


}