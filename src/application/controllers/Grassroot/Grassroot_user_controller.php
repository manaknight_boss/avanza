<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Grassroot_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * User Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Grassroot_user_controller extends Grassroot_controller
{
    protected $_model_file = 'grassroot_user_model';
    public $_page_name = 'Usuarios';

    public function __construct()
    {
        parent::__construct();
        

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/User_grassroot_list_paginate_view_model.php';
        $session = $this->get_session();
        $format = $this->input->get('format', TRUE) ?? 'view';
        $order_by = $this->input->get('order_by', TRUE) ?? '';
        $direction = $this->input->get('direction', TRUE) ?? 'ASC';

        $this->_data['view_model'] = new User_grassroot_list_paginate_view_model(
            $this->grassroot_user_model,
            $this->pagination,
            '/grassroot/users/0');
        $this->_data['view_model']->set_heading('Usuarios');
        $this->_data['view_model']->set_id(($this->input->get('id', TRUE) != NULL) ? $this->input->get('id', TRUE) : NULL);
		$this->_data['view_model']->set_email(($this->input->get('email', TRUE) != NULL) ? $this->input->get('email', TRUE) : NULL);
		$this->_data['view_model']->set_first_name(($this->input->get('first_name', TRUE) != NULL) ? $this->input->get('first_name', TRUE) : NULL);
		$this->_data['view_model']->set_last_name(($this->input->get('last_name', TRUE) != NULL) ? $this->input->get('last_name', TRUE) : NULL);
		$this->_data['view_model']->set_role_id(($this->input->get('role_id', TRUE) != NULL) ? $this->input->get('role_id', TRUE) : NULL);
		$this->_data['view_model']->set_status(($this->input->get('status', TRUE) != NULL) ? $this->input->get('status', TRUE) : NULL);
		
        $where = [
            'id' => $this->_data['view_model']->get_id(),
			'email' => $this->_data['view_model']->get_email(),
			'first_name' => $this->_data['view_model']->get_first_name(),
			'last_name' => $this->_data['view_model']->get_last_name(),
			'role_id' => $this->_data['view_model']->get_role_id(),
			'status' => $this->_data['view_model']->get_status(),
			
        ];

        $this->_data['view_model']->set_total_rows($this->grassroot_user_model->count($where));

        $this->_data['view_model']->set_format_layout($this->_data['layout_clean_mode']);
        $this->_data['view_model']->set_per_page(25);
        $this->_data['view_model']->set_order_by($order_by);
        $this->_data['view_model']->set_sort($direction);
        $this->_data['view_model']->set_sort_base_url('/grassroot/users/0');
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->grassroot_user_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where,
            $order_by,
            $direction));

        if ($format != 'view')
        {
            return $this->output->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($this->_data['view_model']->to_json()));
        }

        return $this->render('Grassroot/User', $this->_data);
	}

	public function add()
	{
        include_once __DIR__ . '/../../view_models/User_grassroot_add_view_model.php';
        $this->form_validation = $this->grassroot_user_model->set_form_validation(
        $this->form_validation, $this->grassroot_user_model->get_all_validation_rule());
        $this->_data['view_model'] = new User_grassroot_add_view_model($this->grassroot_user_model);
        $this->_data['view_model']->set_heading('Usuarios');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Grassroot/UserAdd', $this->_data);
        }

        $email = $this->input->post('email');
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$password = $this->input->post('password');
		$role_id = $this->input->post('role_id');
		$stripe_id = $this->input->post('stripe_id');
		$refer = $this->input->post('refer');
		$phone = $this->input->post('phone');
		
        $result = $this->grassroot_user_model->create([
            'email' => $email,
			'first_name' => $first_name,
			'last_name' => $last_name,
			'password' => $password,
			'role_id' => $role_id,
			'stripe_id' => $stripe_id,
			'refer' => $refer,
			'phone' => $phone,
			"password" => str_replace('$2y$', '$2b$', password_hash($password, PASSWORD_BCRYPT)),
        ]);

        if ($result)
        {
            
            return $this->redirect('/grassroot/users/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Grassroot/UserAdd', $this->_data);
	}

	public function edit($id)
	{
        $model = $this->grassroot_user_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/grassroot/users/0');
        }

        include_once __DIR__ . '/../../view_models/User_grassroot_edit_view_model.php';
        $this->form_validation = $this->grassroot_user_model->set_form_validation(
        $this->form_validation, $this->grassroot_user_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new User_grassroot_edit_view_model($this->grassroot_user_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Usuarios');
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Grassroot/UserEdit', $this->_data);
        }

        $email = $this->input->post('email');
		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$password = $this->input->post('password');
		$role_id = $this->input->post('role_id');
		$stripe_id = $this->input->post('stripe_id');
		$refer = $this->input->post('refer');
		$phone = $this->input->post('phone');
		$status = $this->input->post('status');
		
        $result = $this->grassroot_user_model->edit([
            'email' => $email,
			'first_name' => $first_name,
			'last_name' => $last_name,
			'password' => $password,
			'role_id' => $role_id,
			'stripe_id' => $stripe_id,
			'refer' => $refer,
			'phone' => $phone,
			'status' => $status,
			"password" => str_replace('$2y$', '$2b$', password_hash($password, PASSWORD_BCRYPT)),
        ], $id);

        if ($result)
        {
            
            return $this->redirect('/grassroot/users/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Grassroot/UserEdit', $this->_data);
	}

	public function view($id)
	{
        $model = $this->grassroot_user_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/grassroot/users/0');
		}


        include_once __DIR__ . '/../../view_models/User_grassroot_view_view_model.php';
		$this->_data['view_model'] = new User_grassroot_view_view_model($this->grassroot_user_model);
		$this->_data['view_model']->set_heading('Usuarios');
        $this->_data['view_model']->set_model($model);
        return $this->render('Grassroot/UserView', $this->_data);
	}

	public function delete($id)
	{
        $model = $this->grassroot_user_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/grassroot/users/0');
        }

        $result = $this->grassroot_user_model->real_delete($id);

        if ($result)
        {
            
            return $this->redirect('/grassroot/users/0', 'refresh');
        }

        $this->error('Error');
        return redirect('/grassroot/users/0');
	}



}