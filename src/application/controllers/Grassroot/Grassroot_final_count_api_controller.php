<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Grassroot_api_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Final API Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Grassroot_final_count_api_controller extends Grassroot_api_controller
{
    protected $_model_file = 'final_count_model';

    public function __construct()
    {
        parent::__construct();
    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Final_grassroot_list_paginate_view_model.php';

        $where = [];
        $this->_data['view_model'] = new Final_grassroot_list_paginate_view_model(
            $this->final_count_model,
            $this->pagination,
            '/grassroot/final/0');
        $this->_data['view_model']->set_heading('{{{page_name}}}');
        $this->_data['view_model']->set_total_rows($this->final_count_model->count($where));

        $this->_data['view_model']->set_per_page(10);
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->final_count_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where));
        return $this->success($this->_data['view_model']->to_json(), 200);
	}

	public function add()
	{
        $this->form_validation = $this->final_count_model->set_form_validation(
        $this->form_validation, $this->final_count_model->get_all_validation_rule());

		if ($this->form_validation->run() === FALSE)
		{
			return $this->_render_validation_error();
        }

        $table = $this->input->post('table');
		$file = $this->input->post('file');
		$file_id = $this->input->post('file_id');
		$mayor_1 = $this->input->post('mayor_1');
		$mayor_count_1 = $this->input->post('mayor_count_1');
		$mayor_2 = $this->input->post('mayor_2');
		$mayor_count_2 = $this->input->post('mayor_count_2');
		$mayor_3 = $this->input->post('mayor_3');
		$mayor_count_3 = $this->input->post('mayor_count_3');
		$mayor_4 = $this->input->post('mayor_4');
		$mayor_count_4 = $this->input->post('mayor_count_4');
		$mayor_5 = $this->input->post('mayor_5');
		$mayor_count_5 = $this->input->post('mayor_count_5');
		$mayor_6 = $this->input->post('mayor_6');
		$mayor_count_6 = $this->input->post('mayor_count_6');
		$mayor_7 = $this->input->post('mayor_7');
		$mayor_count_7 = $this->input->post('mayor_count_7');
		$mayor_8 = $this->input->post('mayor_8');
		$mayor_count_8 = $this->input->post('mayor_count_8');
		$mayor_9 = $this->input->post('mayor_9');
		$mayor_count_9 = $this->input->post('mayor_count_9');
		$mayor_10 = $this->input->post('mayor_10');
		$mayor_count_10 = $this->input->post('mayor_count_10');
		$senator_1 = $this->input->post('senator_1');
		$senator_count_1 = $this->input->post('senator_count_1');
		$senator_2 = $this->input->post('senator_2');
		$senator_count_2 = $this->input->post('senator_count_2');
		$senator_3 = $this->input->post('senator_3');
		$senator_count_3 = $this->input->post('senator_count_3');
		$senator_4 = $this->input->post('senator_4');
		$senator_count_4 = $this->input->post('senator_count_4');
		$senator_5 = $this->input->post('senator_5');
		$senator_count_5 = $this->input->post('senator_count_5');
		$senator_6 = $this->input->post('senator_6');
		$senator_count_6 = $this->input->post('senator_count_6');
		$senator_7 = $this->input->post('senator_7');
		$senator_count_7 = $this->input->post('senator_count_7');
		$senator_8 = $this->input->post('senator_8');
		$senator_count_8 = $this->input->post('senator_count_8');
		$senator_9 = $this->input->post('senator_9');
		$senator_count_9 = $this->input->post('senator_count_9');
		$senator_10 = $this->input->post('senator_10');
		$senator_count_10 = $this->input->post('senator_count_10');
		$regional_1 = $this->input->post('regional_1');
		$regional_count_1 = $this->input->post('regional_count_1');
		$regional_2 = $this->input->post('regional_2');
		$regional_count_2 = $this->input->post('regional_count_2');
		$regional_3 = $this->input->post('regional_3');
		$regional_count_3 = $this->input->post('regional_count_3');
		$regional_4 = $this->input->post('regional_4');
		$regional_count_4 = $this->input->post('regional_count_4');
		$regional_5 = $this->input->post('regional_5');
		$regional_count_5 = $this->input->post('regional_count_5');
		$regional_6 = $this->input->post('regional_6');
		$regional_count_6 = $this->input->post('regional_count_6');
		$regional_7 = $this->input->post('regional_7');
		$regional_count_7 = $this->input->post('regional_count_7');
		$regional_8 = $this->input->post('regional_8');
		$regional_count_8 = $this->input->post('regional_count_8');
		$regional_9 = $this->input->post('regional_9');
		$regional_count_9 = $this->input->post('regional_count_9');
		$regional_10 = $this->input->post('regional_10');
		$regional_count_10 = $this->input->post('regional_count_10');
		$regional_11 = $this->input->post('regional_11');
		$regional_count_11 = $this->input->post('regional_count_11');
		$regional_12 = $this->input->post('regional_12');
		$regional_count_12 = $this->input->post('regional_count_12');
		$regional_13 = $this->input->post('regional_13');
		$regional_count_13 = $this->input->post('regional_count_13');
		$regional_14 = $this->input->post('regional_14');
		$regional_count_14 = $this->input->post('regional_count_14');
		$regional_15 = $this->input->post('regional_15');
		$regional_count_15 = $this->input->post('regional_count_15');
		
        $result = $this->final_count_model->create([
            'table' => $table,
			'file' => $file,
			'file_id' => $file_id,
			'mayor_1' => $mayor_1,
			'mayor_count_1' => $mayor_count_1,
			'mayor_2' => $mayor_2,
			'mayor_count_2' => $mayor_count_2,
			'mayor_3' => $mayor_3,
			'mayor_count_3' => $mayor_count_3,
			'mayor_4' => $mayor_4,
			'mayor_count_4' => $mayor_count_4,
			'mayor_5' => $mayor_5,
			'mayor_count_5' => $mayor_count_5,
			'mayor_6' => $mayor_6,
			'mayor_count_6' => $mayor_count_6,
			'mayor_7' => $mayor_7,
			'mayor_count_7' => $mayor_count_7,
			'mayor_8' => $mayor_8,
			'mayor_count_8' => $mayor_count_8,
			'mayor_9' => $mayor_9,
			'mayor_count_9' => $mayor_count_9,
			'mayor_10' => $mayor_10,
			'mayor_count_10' => $mayor_count_10,
			'senator_1' => $senator_1,
			'senator_count_1' => $senator_count_1,
			'senator_2' => $senator_2,
			'senator_count_2' => $senator_count_2,
			'senator_3' => $senator_3,
			'senator_count_3' => $senator_count_3,
			'senator_4' => $senator_4,
			'senator_count_4' => $senator_count_4,
			'senator_5' => $senator_5,
			'senator_count_5' => $senator_count_5,
			'senator_6' => $senator_6,
			'senator_count_6' => $senator_count_6,
			'senator_7' => $senator_7,
			'senator_count_7' => $senator_count_7,
			'senator_8' => $senator_8,
			'senator_count_8' => $senator_count_8,
			'senator_9' => $senator_9,
			'senator_count_9' => $senator_count_9,
			'senator_10' => $senator_10,
			'senator_count_10' => $senator_count_10,
			'regional_1' => $regional_1,
			'regional_count_1' => $regional_count_1,
			'regional_2' => $regional_2,
			'regional_count_2' => $regional_count_2,
			'regional_3' => $regional_3,
			'regional_count_3' => $regional_count_3,
			'regional_4' => $regional_4,
			'regional_count_4' => $regional_count_4,
			'regional_5' => $regional_5,
			'regional_count_5' => $regional_count_5,
			'regional_6' => $regional_6,
			'regional_count_6' => $regional_count_6,
			'regional_7' => $regional_7,
			'regional_count_7' => $regional_count_7,
			'regional_8' => $regional_8,
			'regional_count_8' => $regional_count_8,
			'regional_9' => $regional_9,
			'regional_count_9' => $regional_count_9,
			'regional_10' => $regional_10,
			'regional_count_10' => $regional_count_10,
			'regional_11' => $regional_11,
			'regional_count_11' => $regional_count_11,
			'regional_12' => $regional_12,
			'regional_count_12' => $regional_count_12,
			'regional_13' => $regional_13,
			'regional_count_13' => $regional_count_13,
			'regional_14' => $regional_14,
			'regional_count_14' => $regional_count_14,
			'regional_15' => $regional_15,
			'regional_count_15' => $regional_count_15,
			
        ]);

        if ($result)
        {
            
            return $this->success([], 200);
        }

        return $this->_render_custom_error([
            'error' => 'Error'
        ]);
	}

	public function edit($id)
	{
        $model = $this->final_count_model->get($id);

		if (!$model)
		{
            return $this->_render_custom_error([
                'error' => 'Error'
            ]);
        }

        $this->form_validation = $this->final_count_model->set_form_validation(
        $this->form_validation, $this->final_count_model->get_all_edit_validation_rule());

		if ($this->form_validation->run() === FALSE)
		{
			return $this->_render_validation_error();
        }

        $table = $this->input->post('table');
		$file = $this->input->post('file');
		$file_id = $this->input->post('file_id');
		$mayor_1 = $this->input->post('mayor_1');
		$mayor_count_1 = $this->input->post('mayor_count_1');
		$mayor_2 = $this->input->post('mayor_2');
		$mayor_count_2 = $this->input->post('mayor_count_2');
		$mayor_3 = $this->input->post('mayor_3');
		$mayor_count_3 = $this->input->post('mayor_count_3');
		$mayor_4 = $this->input->post('mayor_4');
		$mayor_count_4 = $this->input->post('mayor_count_4');
		$mayor_5 = $this->input->post('mayor_5');
		$mayor_count_5 = $this->input->post('mayor_count_5');
		$mayor_6 = $this->input->post('mayor_6');
		$mayor_count_6 = $this->input->post('mayor_count_6');
		$mayor_7 = $this->input->post('mayor_7');
		$mayor_count_7 = $this->input->post('mayor_count_7');
		$mayor_8 = $this->input->post('mayor_8');
		$mayor_count_8 = $this->input->post('mayor_count_8');
		$mayor_9 = $this->input->post('mayor_9');
		$mayor_count_9 = $this->input->post('mayor_count_9');
		$mayor_10 = $this->input->post('mayor_10');
		$mayor_count_10 = $this->input->post('mayor_count_10');
		$senator_1 = $this->input->post('senator_1');
		$senator_count_1 = $this->input->post('senator_count_1');
		$senator_2 = $this->input->post('senator_2');
		$senator_count_2 = $this->input->post('senator_count_2');
		$senator_3 = $this->input->post('senator_3');
		$senator_count_3 = $this->input->post('senator_count_3');
		$senator_4 = $this->input->post('senator_4');
		$senator_count_4 = $this->input->post('senator_count_4');
		$senator_5 = $this->input->post('senator_5');
		$senator_count_5 = $this->input->post('senator_count_5');
		$senator_6 = $this->input->post('senator_6');
		$senator_count_6 = $this->input->post('senator_count_6');
		$senator_7 = $this->input->post('senator_7');
		$senator_count_7 = $this->input->post('senator_count_7');
		$senator_8 = $this->input->post('senator_8');
		$senator_count_8 = $this->input->post('senator_count_8');
		$senator_9 = $this->input->post('senator_9');
		$senator_count_9 = $this->input->post('senator_count_9');
		$senator_10 = $this->input->post('senator_10');
		$senator_count_10 = $this->input->post('senator_count_10');
		$regional_1 = $this->input->post('regional_1');
		$regional_count_1 = $this->input->post('regional_count_1');
		$regional_2 = $this->input->post('regional_2');
		$regional_count_2 = $this->input->post('regional_count_2');
		$regional_3 = $this->input->post('regional_3');
		$regional_count_3 = $this->input->post('regional_count_3');
		$regional_4 = $this->input->post('regional_4');
		$regional_count_4 = $this->input->post('regional_count_4');
		$regional_5 = $this->input->post('regional_5');
		$regional_count_5 = $this->input->post('regional_count_5');
		$regional_6 = $this->input->post('regional_6');
		$regional_count_6 = $this->input->post('regional_count_6');
		$regional_7 = $this->input->post('regional_7');
		$regional_count_7 = $this->input->post('regional_count_7');
		$regional_8 = $this->input->post('regional_8');
		$regional_count_8 = $this->input->post('regional_count_8');
		$regional_9 = $this->input->post('regional_9');
		$regional_count_9 = $this->input->post('regional_count_9');
		$regional_10 = $this->input->post('regional_10');
		$regional_count_10 = $this->input->post('regional_count_10');
		$regional_11 = $this->input->post('regional_11');
		$regional_count_11 = $this->input->post('regional_count_11');
		$regional_12 = $this->input->post('regional_12');
		$regional_count_12 = $this->input->post('regional_count_12');
		$regional_13 = $this->input->post('regional_13');
		$regional_count_13 = $this->input->post('regional_count_13');
		$regional_14 = $this->input->post('regional_14');
		$regional_count_14 = $this->input->post('regional_count_14');
		$regional_15 = $this->input->post('regional_15');
		$regional_count_15 = $this->input->post('regional_count_15');
		
        $result = $this->final_count_model->edit([
            'table' => $table,
			'file' => $file,
			'file_id' => $file_id,
			'mayor_1' => $mayor_1,
			'mayor_count_1' => $mayor_count_1,
			'mayor_2' => $mayor_2,
			'mayor_count_2' => $mayor_count_2,
			'mayor_3' => $mayor_3,
			'mayor_count_3' => $mayor_count_3,
			'mayor_4' => $mayor_4,
			'mayor_count_4' => $mayor_count_4,
			'mayor_5' => $mayor_5,
			'mayor_count_5' => $mayor_count_5,
			'mayor_6' => $mayor_6,
			'mayor_count_6' => $mayor_count_6,
			'mayor_7' => $mayor_7,
			'mayor_count_7' => $mayor_count_7,
			'mayor_8' => $mayor_8,
			'mayor_count_8' => $mayor_count_8,
			'mayor_9' => $mayor_9,
			'mayor_count_9' => $mayor_count_9,
			'mayor_10' => $mayor_10,
			'mayor_count_10' => $mayor_count_10,
			'senator_1' => $senator_1,
			'senator_count_1' => $senator_count_1,
			'senator_2' => $senator_2,
			'senator_count_2' => $senator_count_2,
			'senator_3' => $senator_3,
			'senator_count_3' => $senator_count_3,
			'senator_4' => $senator_4,
			'senator_count_4' => $senator_count_4,
			'senator_5' => $senator_5,
			'senator_count_5' => $senator_count_5,
			'senator_6' => $senator_6,
			'senator_count_6' => $senator_count_6,
			'senator_7' => $senator_7,
			'senator_count_7' => $senator_count_7,
			'senator_8' => $senator_8,
			'senator_count_8' => $senator_count_8,
			'senator_9' => $senator_9,
			'senator_count_9' => $senator_count_9,
			'senator_10' => $senator_10,
			'senator_count_10' => $senator_count_10,
			'regional_1' => $regional_1,
			'regional_count_1' => $regional_count_1,
			'regional_2' => $regional_2,
			'regional_count_2' => $regional_count_2,
			'regional_3' => $regional_3,
			'regional_count_3' => $regional_count_3,
			'regional_4' => $regional_4,
			'regional_count_4' => $regional_count_4,
			'regional_5' => $regional_5,
			'regional_count_5' => $regional_count_5,
			'regional_6' => $regional_6,
			'regional_count_6' => $regional_count_6,
			'regional_7' => $regional_7,
			'regional_count_7' => $regional_count_7,
			'regional_8' => $regional_8,
			'regional_count_8' => $regional_count_8,
			'regional_9' => $regional_9,
			'regional_count_9' => $regional_count_9,
			'regional_10' => $regional_10,
			'regional_count_10' => $regional_count_10,
			'regional_11' => $regional_11,
			'regional_count_11' => $regional_count_11,
			'regional_12' => $regional_12,
			'regional_count_12' => $regional_count_12,
			'regional_13' => $regional_13,
			'regional_count_13' => $regional_count_13,
			'regional_14' => $regional_14,
			'regional_count_14' => $regional_count_14,
			'regional_15' => $regional_15,
			'regional_count_15' => $regional_count_15,
			
        ], $id);

        if ($result)
        {
            
            return $this->success([], 200);
        }

        return $this->_render_custom_error([
            'error' => 'Error'
        ]);
	}

	public function view($id)
	{
        $model = $this->final_count_model->get($id);

		if (!$model)
		{
			return $this->_render_custom_error([
				'error' => 'Error'
			]);
		}


        include_once __DIR__ . '/../../view_models/Final_grassroot_view_view_model.php';
        $this->_data['view_model'] = new Final_grassroot_view_view_model($this->final_count_model);
        $this->_data['view_model']->set_model($model);
        return $this->success(['data' => $this->_data['view_model']->to_json()], 200);
	}

	public function delete($id)
	{
        $model = $this->final_count_model->get($id);

		if (!$model)
		{
            return $this->_render_custom_error([
                'error' => 'Error'
            ]);
        }

        $result = $this->final_count_model->real_delete($id);

        if ($result)
        {
            
            return $this->success([], 200);
        }

        return $this->_render_custom_error([
            'error' => 'Error'
        ]);
	}
}