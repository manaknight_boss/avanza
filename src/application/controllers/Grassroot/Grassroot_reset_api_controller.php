<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once __DIR__ . '/../../services/User_service.php';
include_once 'Grassroot_api_auth_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Reset API Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Grassroot_reset_api_controller extends Grassroot_api_auth_controller
{
    public function __construct()
    {
        parent::__construct();
    }

	public function index ($reset_token)
	{
        $this->load->model('grassroot_user_model');
        $this->load->model('token_model');

        $service = new User_service($this->grassroot_user_model);
        $service->set_token_model($this->token_model);

        $valid_user = $service->valid_reset_token($reset_token);

        if (!$valid_user)
        {
            return $this->_render_custom_error([
                'token' => 'Token de actualizacion es invalido.'
            ]);
        }

        $this->_data['reset_token'] = $reset_token;

        $this->form_validation->set_rules('password', 'Contraseña', 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            return $this->_render_validation_error();
        }

        $password = $this->input->post('password');
        $password_reseted = $service->reset_password($valid_user->id, $password);

        if ($password_reseted)
        {
            $service->invalidate_token($reset_token, $valid_user->id);
            return $this->success([
                'message' => 'Contraseña actualizada. Ya puede acceder'
            ]);
        }

        return $this->_render_custom_error([
            'token' => 'Token de actualizacion es invalido.'
        ]);
    }
}