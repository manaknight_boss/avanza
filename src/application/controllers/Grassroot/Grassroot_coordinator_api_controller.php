<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Grassroot_api_auth_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Voters Cordinator API Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Grassroot_coordinator_api_controller extends Grassroot_api_auth_controller
{
    protected $_model_file = 'raw_avanza_members2_model';

    public function __construct()
    {
        parent::__construct();
    }

		public function login ()
		{
			$this->load->model('raw_avanza_members2_model');
			$this->form_validation->set_rules('government_id', 'Cédula', 'trim|required');

			if ($this->form_validation->run() === FALSE)
			{
					return $this->_render_validation_error();
			}

			$government_id = $this->input->post('government_id');
			$exist = $this->raw_avanza_members2_model->get_by_field('cedulaCoodinador', $government_id);

			if (!$exist)
			{
				return $this->_render_custom_error([
					'government_id' => 'Cédula invalido.'
				]);
			}
			return $this->success([
				'success' => TRUE
			], 200);
		}

		public function coordinator ($id)
		{
			$this->load->model('voters_new_model');
			$this->load->model('raw_avanza_members2_model');
			$this->load->model('raw_tactical_members_model');
			$this->load->model('raw_recintos_model');
			$recintos = $this->raw_recintos_model->get_all([]);
			$cordinator_query = $this->raw_avanza_members2_model->raw_query("SELECT * FROM raw_avanza_members2 WHERE cedulaCoodinador={$id}");
			$cordinator = $this->raw_avanza_members2_model->get_by_field('cedulaMiembro', $id);

			if (!$cordinator)
			{

				$cordinator = $this->raw_tactical_members_model->get_by_field('cedulaMiembro', $id);

				if(!$cordinator)
				{
					return $this->success([
						'success' => FALSE,
						'message' => 'Error'
					], 200);
				}
			}

			$cordinator_in_voter_table = $this->voters_new_model->get_by_field('government_id', $id);
			$payload = [
				'first_name' => $cordinator->nombreMiembro,
				'last_name' => $cordinator->apellidoMiembro,
				'government_id' => $cordinator->cedulaMiembro,
				'phone_1' => $cordinator->telefono_Residencial,
				'phone_2' => $cordinator->telefono_Celular,
				'id' => (int)$cordinator_in_voter_table->id,
				'total' => 0,
				'voted' => 0,
				'not_voted' => 0
			];
			$people = [];
			$result = $cordinator_query->result();

			foreach ($result as $key => $row)
			{
				$payload['total'] = $payload['total'] + 1;
				$voter = $this->voters_new_model->get_by_field('government_id', $row->cedulaMiembro);

				if ($voter && $voter->has_voted_municipal_2020 == 1)
				{
					$payload['voted'] = $payload['voted'] + 1;
				}

				$people[] = [
					'first_name' => $row->nombreMiembro . ' ' . $row->apellidoMiembro,
					'last_name' => $this->find_recinto_info($recintos, (int)$row->codigoRecintoMiembro),
					'recinto' => (int)$row->codigoRecintoMiembro,
					'phone_1' => $this->clean_phone($row->telefono_Residencial),
					'phone_2' => $this->clean_phone($row->telefono_Celular),
					'government_id' => $row->cedulaMiembro
				];

				$payload['not_voted'] = $payload['total'] - $payload['voted'];

				if ($payload['not_voted'] < 0)
				{
					$payload['not_voted'] = 0;
				}
			}

			$data['people'] = $people;

			$data['summary'] = $payload;
			$this->_data['data'] = $data;

			return $this->success([
				'success' => FALSE,
				'data' => $this->_data['data']
			], 200);
		}

		private function find_recinto_info ($data, $recinto)
    {
        if (is_numeric($recinto))
        {
            $intRecinto = (int)$recinto;
            foreach ($data as $row)
            {
                if ($intRecinto == $row->RecintoId)
                {
									return $row->Recinto;
                }
            }
        }
        return '';
		}

		private function clean_phone ($phone)
    {
        if (!$phone)
        {
            return '';
        }

        $cleanPhones = trim(str_replace(['(', ')', '-', ' '], ['','','', ''], $phone));
        $cleanPhone = '';
        $parts = explode(',', $cleanPhones);
        if (count($parts) > 0)
        {
            foreach ($parts as $singlePhone) {
                $cleanSinglePhone = trim($singlePhone);
                if (strlen($cleanSinglePhone) === 10)
                {
                    return $cleanSinglePhone;
                }
            }
        }
        else
        {
            if (strlen($cleanPhones) === 10)
            {
                $cleanPhone = $cleanPhones;
            }
        }
        return $cleanPhone;
    }
}