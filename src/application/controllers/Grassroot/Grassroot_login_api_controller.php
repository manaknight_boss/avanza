<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once __DIR__ . '/../../services/User_service.php';
include_once __DIR__ . '/../../services/Token_service.php';
include_once 'Grassroot_api_auth_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Login API Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Grassroot_login_api_controller extends Grassroot_api_auth_controller
{
    public function __construct()
    {
        parent::__construct();
    }

	public function index ()
	{
        $this->load->model('grassroot_user_model');
        $this->load->model('token_model');

        $service = new User_service($this->grassroot_user_model);
        $token_service = new Token_service();
        $token_service->set_model($this->token_model);

        $this->form_validation->set_rules('email', 'Cédula', 'trim|required');
        $this->form_validation->set_rules('password', 'Contraseña', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            return $this->_render_validation_error();
        }

        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $authenticated_user = $service->login($email, $password);

        if ($authenticated_user)
        {
            $key = $this->config->item('jwt_key');
            $base_url = $this->config->item('base_url');
            $jwt_expire_at = $this->config->item('jwt_expire_at');
            $jwt_refresh_expire_at = $this->config->item('jwt_refresh_expire_at');
            $access_token = $token_service->generate_access_token($key, $base_url, $jwt_expire_at, [
                'user_id' => $authenticated_user->id,
                'role_id' => $authenticated_user->role_id
            ]);
            $refresh_token = $token_service->generate_refresh_token($authenticated_user->id, $authenticated_user->role_id, $jwt_refresh_expire_at);

            return $this->success([
                'access_token' => $access_token,
                'refresh_token' => $refresh_token,
                'expire_in' => 14400
            ], 200);
        }

        return $this->_render_custom_error([
            'email' => 'Correo o Contraseña invalido.'
        ]);
    }

    public function token ()
	{
        $this->load->model('grassroot_user_model');
        $this->load->model('token_model');

        $service = new User_service($this->grassroot_user_model);
        $token_service = new Token_service();
        $token_service->set_model($this->token_model);

        $this->form_validation->set_rules('token', 'xyzToken', 'trim|required');

        if ($this->form_validation->run() === FALSE)
        {
            return $this->_render_validation_error();
        }

        $token = $this->input->post('token');

        $exist = $this->token_model->get_by_fields([
            'token' => $token,
            'type' => 2
        ]);

        if ($exist)
        {
            $expired_time = strtotime($exist->expire_at);
            $now = time();
            if ($now < $expired_time)
            {
                $key = $this->config->item('jwt_key');
                $base_url = $this->config->item('base_url');
                $jwt_expire_at = $this->config->item('jwt_expire_at');
                $data = json_decode($exist->data, TRUE);

                $this->token_model->real_delete_by_fields([
                    'user_id' => $data['user_id'],
                    'type' => 1
                ]);

                $access_token = $token_service->generate_access_token($key, $base_url, $jwt_expire_at, [
                    'user_id' => $data['user_id'],
                    'role_id' => $data['role_id']
                ]);

                return $this->success([
                    'access_token' => $access_token['token'],
                    'expire_in' => $jwt_expire_at
                ], 200);
            }
            else
            {
                $this->token_model->real_delete_by_fields([
                    'user_id' => $exist->user_id,
                    'type' => 1
                ]);
                $this->token_model->real_delete_by_fields([
                    'user_id' => $exist->user_id,
                    'type' => 2
                ]);
                return $this->expire_token_error_message();
            }
        }
        else
        {
            return $this->unauthorize_error_message();
        }
    }
}