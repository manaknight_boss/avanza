<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Grassroot_controller.php';

/**
 * Grassroot Dashboard Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Grassroot_tactical_dashboard_controller extends Grassroot_controller
{
    public $_page_name = 'Tablero Tactical';

    public function __construct()
    {
        parent::__construct();
    }

    public function index ()
    {
        $this->load->model('voters_new_model');
        $this->load->library('cache_service');
        $this->cache_service->set_adapter('file', 300);
        $data = $this->cache_service->get('grassroot_dashboard_global_tactical');

        if ($data)
        {
            $this->_data = $data;
        }
        else
        {
            $this->_data['total_circunscripcion_1'] = $this->voters_new_model->count([
                'circunscripcion' => 1,
                'tactical_member_id > 0'
            ]);
            $this->_data['voted_circunscripcion_1'] = $this->voters_new_model->count([
                'circunscripcion' => 1,
                'has_voted_municipal_2020' => 1,
                'tactical_member_id > 0'
            ]);
            $this->_data['not_voted_circunscripcion_1'] = $this->voters_new_model->count([
                'circunscripcion' => 1,
                'has_voted_municipal_2020' => 0,
                'tactical_member_id > 0'
            ]);
            $this->_data['circunscripcion_1_turnout'] = ($this->_data['voted_circunscripcion_1'] > 1) ? number_format($this->_data['voted_circunscripcion_1'] / $this->_data['total_circunscripcion_1'] * 100, 4) . '%': '0%';
            $this->_data['total_circunscripcion_2'] = $this->voters_new_model->count([
                'circunscripcion' => 2,
                'tactical_member_id > 0'
            ]);
            $this->_data['voted_circunscripcion_2'] = $this->voters_new_model->count([
                'circunscripcion' => 2,
                'has_voted_municipal_2020' => 1,
                'tactical_member_id > 0'
            ]);
            $this->_data['not_voted_circunscripcion_2'] = $this->voters_new_model->count([
                'circunscripcion' => 2,
                'has_voted_municipal_2020' => 0,
                'tactical_member_id > 0'
            ]);
            $this->_data['circunscripcion_2_turnout'] = ($this->_data['voted_circunscripcion_2'] > 1) ? number_format($this->_data['voted_circunscripcion_2'] / $this->_data['total_circunscripcion_2'] * 100, 4) . '%': '0%';
            $this->_data['total_circunscripcion_3'] = $this->voters_new_model->count([
                'circunscripcion' => 3,
                'tactical_member_id > 0'
            ]);
            $this->_data['voted_circunscripcion_3'] = $this->voters_new_model->count([
                'circunscripcion' => 3,
                'has_voted_municipal_2020' => 1,
                'tactical_member_id > 0'
            ]);
            $this->_data['not_voted_circunscripcion_3'] = $this->voters_new_model->count([
                'circunscripcion' => 3,
                'has_voted_municipal_2020' => 0,
                'tactical_member_id > 0'
            ]);
            $this->_data['circunscripcion_3_turnout'] = ($this->_data['voted_circunscripcion_3'] > 1) ? number_format($this->_data['voted_circunscripcion_3'] / $this->_data['total_circunscripcion_3'] * 100, 4) . '%': '0%';
            $this->_data['circunscripcion_headers'] = ['Circ.', 'Recinto', 'Total Electores', 'Total Votaron', 'Total por Votar', '% Participación', 'Acción'];
            $this->_data['circunscripcion'] = $this->get_circunscripcion();
            $this->cache_service->set('grassroot_dashboard_global_tactical', $this->_data);
        }

        return $this->render('Grassroot/Dashboard', $this->_data);
    }

    public function global_index ()
    {
        $this->load->model('voters_new_model');
        $this->load->library('cache_service');
        $this->cache_service->set_adapter('file', 300);
        $data = $this->cache_service->get('grassroot_dashboard_global_tactical');

        if ($data)
        {
            $this->_data = $data;
        }
        else
        {
            $this->_data['total'] = $this->voters_new_model->count([]);
            $this->_data['total_circunscripcion_1'] = $this->voters_new_model->count([
                'circunscripcion' => 1
            ]);
            $this->_data['voted_circunscripcion_1'] = $this->voters_new_model->count([
                'circunscripcion' => 1,
                'has_voted_municipal_2020' => 1
            ]);
            $this->_data['not_voted_circunscripcion_1'] = $this->voters_new_model->count([
                'circunscripcion' => 1,
                'has_voted_municipal_2020' => 0
            ]);
            $this->_data['avanza_total_circunscripcion_1'] = $this->voters_new_model->count([
                'circunscripcion' => 1,
                'tactical_member_id > 0'
            ]);
            $this->_data['avanza_voted_circunscripcion_1'] = $this->voters_new_model->count([
                'circunscripcion' => 1,
                'has_voted_municipal_2020' => 1,
                'tactical_member_id > 0'
            ]);
            $this->_data['avanza_not_voted_circunscripcion_1'] = $this->voters_new_model->count([
                'circunscripcion' => 1,
                'has_voted_municipal_2020' => 0,
                'tactical_member_id > 0'
            ]);

            if ($this->_data['voted_circunscripcion_1'] > 0)
            {
                $this->_data['avanza_circunscripcion_1_turnout'] = number_format($this->_data['avanza_voted_circunscripcion_1'] / $this->_data['voted_circunscripcion_1'] * 100, 4);
                $this->_data['other_circunscripcion_1_turnout'] =  number_format(($this->_data['voted_circunscripcion_1'] - $this->_data['avanza_voted_circunscripcion_1']) / $this->_data['voted_circunscripcion_1'] * 100, 4);
                $this->_data['circunscripcion_1_turnout'] = number_format($this->_data['voted_circunscripcion_1'] / $this->_data['total_circunscripcion_1'] * 100, 4);
            }
            else
            {
                $this->_data['avanza_circunscripcion_1_turnout'] = '0.0000';
                $this->_data['other_circunscripcion_1_turnout'] =  '0.0000';
                $this->_data['circunscripcion_1_turnout'] = '0.0000';
            }

            $this->_data['total_circunscripcion_2'] = $this->voters_new_model->count([
                'circunscripcion' => 2
            ]);
            $this->_data['voted_circunscripcion_2'] = $this->voters_new_model->count([
                'circunscripcion' => 2,
                'has_voted_municipal_2020' => 1
            ]);
            $this->_data['not_voted_circunscripcion_2'] = $this->voters_new_model->count([
                'circunscripcion' => 2,
                'has_voted_municipal_2020' => 0
            ]);
            $this->_data['avanza_total_circunscripcion_2'] = $this->voters_new_model->count([
                'circunscripcion' => 2,
                'tactical_member_id > 0'
            ]);
            $this->_data['avanza_voted_circunscripcion_2'] = $this->voters_new_model->count([
                'circunscripcion' => 2,
                'has_voted_municipal_2020' => 1,
                'tactical_member_id > 0'
            ]);
            $this->_data['avanza_not_voted_circunscripcion_2'] = $this->voters_new_model->count([
                'circunscripcion' => 2,
                'has_voted_municipal_2020' => 0,
                'tactical_member_id > 0'
            ]);

            if ($this->_data['voted_circunscripcion_2'] > 0)
            {
                $this->_data['avanza_circunscripcion_2_turnout'] = number_format($this->_data['avanza_voted_circunscripcion_2'] / $this->_data['voted_circunscripcion_2'] * 100, 4);
                $this->_data['other_circunscripcion_2_turnout'] =  number_format(($this->_data['voted_circunscripcion_2'] - $this->_data['avanza_voted_circunscripcion_2']) / $this->_data['voted_circunscripcion_2'] * 100, 4);
                $this->_data['circunscripcion_2_turnout'] = number_format($this->_data['voted_circunscripcion_2'] / $this->_data['total_circunscripcion_2'] * 100, 4);
            }
            else
            {
                $this->_data['avanza_circunscripcion_2_turnout'] = '0.0000';
                $this->_data['other_circunscripcion_2_turnout'] =  '0.0000';
                $this->_data['circunscripcion_2_turnout'] = '0.0000';
            }

            $this->_data['total_circunscripcion_3'] = $this->voters_new_model->count([
                'circunscripcion' => 3
            ]);
            $this->_data['voted_circunscripcion_3'] = $this->voters_new_model->count([
                'circunscripcion' => 3,
                'has_voted_municipal_2020' => 1
            ]);
            $this->_data['not_voted_circunscripcion_3'] = $this->voters_new_model->count([
                'circunscripcion' => 3,
                'has_voted_municipal_2020' => 0
            ]);
            $this->_data['avanza_total_circunscripcion_3'] = $this->voters_new_model->count([
                'circunscripcion' => 3,
                'tactical_member_id > 0'
            ]);
            $this->_data['avanza_voted_circunscripcion_3'] = $this->voters_new_model->count([
                'circunscripcion' => 3,
                'has_voted_municipal_2020' => 1,
                'tactical_member_id > 0'
            ]);
            $this->_data['avanza_not_voted_circunscripcion_3'] = $this->voters_new_model->count([
                'circunscripcion' => 3,
                'has_voted_municipal_2020' => 0,
                'tactical_member_id > 0'
            ]);

            if ($this->_data['voted_circunscripcion_3'] > 0)
            {
                $this->_data['avanza_circunscripcion_3_turnout'] = number_format($this->_data['avanza_voted_circunscripcion_3'] / $this->_data['voted_circunscripcion_3'] * 100, 4);
                $this->_data['other_circunscripcion_3_turnout'] =  number_format(($this->_data['voted_circunscripcion_3'] - $this->_data['avanza_voted_circunscripcion_3']) / $this->_data['voted_circunscripcion_3'] * 100, 4);
                $this->_data['circunscripcion_3_turnout'] = number_format($this->_data['voted_circunscripcion_3'] / $this->_data['total_circunscripcion_3'] * 100, 4);
            }
            else
            {
                $this->_data['avanza_circunscripcion_3_turnout'] = '0.0000';
                $this->_data['other_circunscripcion_3_turnout'] =  '0.0000';
                $this->_data['circunscripcion_3_turnout'] = '0.0000';
            }

            $this->_data['total_all'] = $this->_data['total_circunscripcion_3'] + $this->_data['total_circunscripcion_2'] + $this->_data['total_circunscripcion_1'];
            $this->_data['voted_all'] = $this->_data['voted_circunscripcion_3'] + $this->_data['voted_circunscripcion_2'] + $this->_data['voted_circunscripcion_1'];
            $this->_data['not_voted_all'] = $this->_data['not_voted_circunscripcion_3'] + $this->_data['not_voted_circunscripcion_2'] + $this->_data['not_voted_circunscripcion_1'];
            $this->_data['all_turnout'] = number_format($this->_data['voted_all'] / $this->_data['total'] * 100, 4);

            $this->_data['total_avanza'] = $this->_data['avanza_total_circunscripcion_3'] + $this->_data['avanza_total_circunscripcion_2'] + $this->_data['avanza_total_circunscripcion_1'];
            $this->_data['voted_avanza'] = $this->_data['avanza_voted_circunscripcion_3'] + $this->_data['avanza_voted_circunscripcion_2'] + $this->_data['avanza_voted_circunscripcion_1'];
            $this->_data['not_voted_avanza'] = $this->_data['avanza_not_voted_circunscripcion_3'] + $this->_data['avanza_not_voted_circunscripcion_2'] + $this->_data['avanza_not_voted_circunscripcion_1'];

            $this->_data['turnout_percentage_other'] = number_format(($this->_data['voted_all'] - $this->_data['voted_avanza'])/ $this->_data['voted_all'] * 100, 4);
            $this->_data['turnout_percentage_avanza'] = number_format($this->_data['voted_avanza'] / $this->_data['voted_all'] * 100, 4);

            $this->_data['circunscripcion_headers'] = ['Circ.', 'Recinto', 'Total Electores', 'Avanza Total Electores', 'Total Votaron', 'Total por Votar', '% Participación', 'Avanza % Participación', 'Acción'];
            $this->_data['circunscripcion'] = $this->get_circunscripcion_global();
            $this->cache_service->set('grassroot_dashboard_global_tactical', $this->_data);
        }
        $this->_data['page_name'] = 'Tablero DN';
        return $this->render('Grassroot/GlobalTacticalDashboard', $this->_data);
    }

    public function clear_cache()
    {
        $this->load->library('cache_service');
        $this->cache_service->set_adapter('file', 300);
        $this->cache_service->remove('grassroot_dashboard');
        $this->cache_service->remove('grassroot_dashboard_region1');
        $this->cache_service->remove('grassroot_dashboard_region2');
        $this->cache_service->remove('grassroot_dashboard_region3');
        $this->cache_service->remove('grassroot_dashboard_global_tactical');
        return $this->redirect('/grassroot/dashboard');
    }

    public function get_circunscripcion()
    {
        $result = [];
        $result[] = $this->build_circunscripcion(1);
        $result[] = $this->build_circunscripcion(2);
        $result[] = $this->build_circunscripcion(3);
        return $result;
    }

    public function build_circunscripcion($region)
    {
        $recinto_query = $this->voters_new_model->raw_query("SELECT recinto, count(*) as num_voters, sum(has_voted_municipal_2020) as voted FROM voters_new WHERE circunscripcion = {$region} and avanza_member_id > 0 GROUP BY recinto;")->result();
        $result = [];

        foreach ($recinto_query as $key => $value)
        {
            $payload = [
                'region' => $region,
                'total' => $value->num_voters,
                'voted' => $value->voted,
                'not_voted' =>  (int)$value->num_voters - (int)$value->voted,
                'percentage' => $value->voted > 0 ? number_format((int)$value->voted / (int) $value->num_voters * 100, 2) : 0,
                'recinto' => $value->recinto
            ];

            if ($payload['percentage'] < 15)
            {
                $result[] = $payload;
            }
        }
        return $result;
    }

    public function get_circunscripcion_global()
    {
        $result = [];
        $result[] = $this->build_circunscripcion_global(1);
        $result[] = $this->build_circunscripcion_global(2);
        $result[] = $this->build_circunscripcion_global(3);
        return $result;
    }

    public function build_circunscripcion_global($region)
    {
        $recinto_query = $this->voters_new_model->raw_query("SELECT recinto, count(*) as num_voters, sum(has_voted_municipal_2020) as voted, SUM(avanza_member_id > 0) as avanza, SUM(avanza_member_id > 0 and has_voted_municipal_2020=1) as avanza_voted  FROM voters_new WHERE circunscripcion = {$region} GROUP BY recinto;")->result();
        $result = [];

        foreach ($recinto_query as $key => $value)
        {
            $payload = [
                'region' => $region,
                'total' => $value->num_voters,
                'total_avanza' => $value->avanza,
                'voted' => $value->voted,
                'not_voted' =>  (int)$value->num_voters - (int)$value->voted,
                'percentage' => $value->voted > 0 ? number_format((int)$value->voted / (int) $value->num_voters * 100, 2) : 0,
                'percentage_rest' => $value->voted > 0 ? number_format(((int)$value->voted  - (int)$value->avanza_voted)/ (int) $value->num_voters * 100, 2) : 0,
                'percentage_avanza' => $value->avanza_voted > 0 ? number_format((int)$value->avanza_voted / (int) $value->num_voters * 100, 2) : 0,
                'recinto' => $value->recinto
            ];

            if ($payload['percentage'] < 15)
            {
                $result[] = $payload;
            }
        }
        return $result;
    }

    public function section ($region)
    {
        $this->load->model('voters_new_model');
        $this->load->model('raw_recintos_model');
        $this->load->library('cache_service');
        $this->cache_service->set_adapter('file', 300);
        $data = null;//$this->cache_service->get('grassroot_dashboard_global_tactical' . $region);
        if ($data)
        {
            $this->_data = $data;
        }
        else
        {
            $this->_data['recintos'] = $this->raw_recintos_model->get_all([]);
            $this->_data['total_circunscripcion'] = $this->voters_new_model->count([
            'circunscripcion' => $region,
            'avanza_member_id > 0'
            ]);
            $this->_data['voted_circunscripcion'] = $this->voters_new_model->count([
                'circunscripcion' => $region,
                'has_voted_municipal_2020' => 1,
                'avanza_member_id > 0'
            ]);
            $this->_data['not_voted_circunscripcion'] = $this->voters_new_model->count([
                'circunscripcion' => $region,
                'has_voted_municipal_2020' => 0,
                'avanza_member_id > 0'
            ]);

            $this->_data['circunscripcion_turnout'] = ($this->_data['voted_circunscripcion'] > 1) ? number_format($this->_data['voted_circunscripcion'] / $this->_data['total_circunscripcion'], 2) . '%': '0%';
            $this->_data['circunscripcion_headers'] = ['Circ.', 'Recinto', 'Total Electores', 'Total Votaron', 'Total por Votar', '% Participación', 'Acción'];
            $this->_data['circunscripcion'] = $this->build_circunscripcion($region);
            foreach ($this->_data['circunscripcion'] as $key => $value)
            {
                $this->_data['circunscripcion'][$key]['recinto_name'] = '';
                foreach ($this->_data['recintos'] as $k => $v)
                {
                    if ($v->RecintoId == $value['recinto'])
                    {
                        $this->_data['circunscripcion'][$key]['recinto_name'] = $v->Recinto;
                    }
                }

            }
            $this->cache_service->set('grassroot_dashboard_global_tactical' . $region, $this->_data);
        }
        $this->_data['region'] = $region;
        $this->_data['page_name'] = 'Circunscripcion ' . $region;
        return $this->render('Grassroot/Circunscripcion', $this->_data);
    }

    public function recinto ($region)
    {
        $this->load->model('voters_new_model');
        $this->load->model('raw_recintos_model');
        $recinto = $this->raw_recintos_model->get_by_field('RecintoId', $region);
        $this->_data['recinto_name'] = $recinto->Recinto . ' ' . $recinto->Direccion;
        $query = $this->voters_new_model->raw_query("SELECT cordinator_id, count(*) as num_voters, sum(has_voted_municipal_2020) as voted FROM voters_new WHERE recinto = {$region} and avanza_member_id > 0 GROUP BY cordinator_id;");
        $result = $query->result();

        $cordinators = [];

        foreach ($result as $key => $value)
        {
            if ($value->cordinator_id < 1)
            {
                continue;
            }

            $payload = [
                'num_voters' => $value->num_voters,
                'voted' => $value->voted,
                'not_voted' => (int)$value->num_voters - (int)$value->voted,
            ];

            $cordinator = $this->voters_new_model->get($value->cordinator_id);

            $payload['name'] = $cordinator->first_name . ' ' . $cordinator->last_name;
            $payload['government_id'] = $cordinator->government_id;
            $payload['phone_1'] = $cordinator->phone_1;
            $payload['phone_2'] = $cordinator->phone_2;
            $payload['id'] = $cordinator->id;
            $cordinators[] = $payload;
         }


        $this->_data['cordinators'] = $cordinators;
        $this->_data['headers'] = ['ID', 'Nombre', 'Cédula', 'Telefono', 'Acción'];
        $this->_data['recinto'] = $region;
        return $this->render('Grassroot/Recinto', $this->_data);
    }

    public function cordinator ($id, $region, $flag=1)
    {
        $this->load->model('voters_new_model');
        $cordinator_query = $this->voters_new_model->raw_query("SELECT * FROM voters_new WHERE cordinator_id={$id} AND recinto='{$region}' AND avanza_member_id > 0");
        $payload = [
            'total' => 0,
            'left' => 0,
            'first_name' => '',
            'last_name' => '',
            'government_id' => '',
            'phone_1' => '',
            'phone_2' => '',
            'email' => ''
        ];
        $people = [];
        $result = $cordinator_query->result();

        foreach ($result as $key => $row)
        {
            $payload['total'] = $payload['total'] + 1;
            $condition = ($flag == 1 && $row->has_voted_general_2020 == 0) ||
            ($flag == 2 && $row->has_voted_municipal_2020 == 0);

            if ($condition)
            {
                $payload['left'] = $payload['left'] + 1;
                $people[] = [
                    'id' => $row->id,
                    'first_name' => $row->first_name,
                    'last_name' => $row->last_name,
                    'phone_1' => $row->phone_1,
                    'phone_2' => $row->phone_2,
                    'email' => $row->email,
                    'government_id' => $row->government_id,
                    'married_status' => $row->married_status,
                    'gender' => $row->gender,
                    'email' => $row->email,
                    'table' => $row->colegio,
                    'recinto' => $row->recinto,
                ];
                $this->_data['page_name'] = 'Circunscripcion ' . $row->circunscripcion;
            }

            $cordinator_payload = [
                'cordinator_id' => $row->cordinator_id
            ];

            if ($flag == 1)
            {
                $cordinator_payload['has_voted_general_2020'] = 1;
            }

            if ($flag == 2)
            {
                $cordinator_payload['has_voted_municipal_2020'] = 1;
            }

            $payload['voted'] = $this->voters_new_model->count($cordinator_payload);
            $payload['left'] = $payload['total'] - $payload['voted'];
        }

        $this->_data['people'] = $people;

        if ($id > 0)
        {
            $cordinator = $this->voters_new_model->get($id);
            $payload['first_name'] = $cordinator->first_name;
            $payload['last_name'] = $cordinator->last_name;
            $payload['government_id'] = $cordinator->government_id;
            $payload['phone_1'] = $cordinator->phone_1;
            $payload['phone_2'] = $cordinator->phone_2;
            $payload['email'] = $cordinator->email;
        }
        $this->_data['summary'] = $payload;
        $this->_data['headers'] = ['ID', 'Nombre', 'Cédula', 'Telefono 1', 'Telefono 2', 'Correo', 'Colegio', 'Recinto'];
        $this->_data['recinto'] = $region;
        return $this->render('Grassroot/Cordinator', $this->_data);
    }

    public function global_index_old ()
    {
        $this->load->model('voters_new_model');
        $this->load->library('cache_service');
        $this->cache_service->set_adapter('file', 300);
        $data = $this->cache_service->get('grassroot_dashboard_global_tactical');

        if ($data)
        {
            $this->_data = $data;
        }
        else
        {
            $this->_data['total'] = $this->voters_new_model->count([]);
            $this->_data['total_circunscripcion_1'] = $this->voters_new_model->count([
                'circunscripcion' => 1,
                'avanza_member_id = 0'
            ]);
            $this->_data['voted_circunscripcion_1'] = $this->voters_new_model->count([
                'circunscripcion' => 1,
                'has_voted_municipal_2020' => 1,
                'avanza_member_id = 0'
            ]);
            $this->_data['not_voted_circunscripcion_1'] = $this->voters_new_model->count([
                'circunscripcion' => 1,
                'has_voted_municipal_2020' => 0,
                'avanza_member_id = 0'
            ]);
            $this->_data['avanza_total_circunscripcion_1'] = $this->voters_new_model->count([
                'circunscripcion' => 1,
                'avanza_member_id > 0'
            ]);
            $this->_data['avanza_voted_circunscripcion_1'] = $this->voters_new_model->count([
                'circunscripcion' => 1,
                'has_voted_municipal_2020' => 1,
                'avanza_member_id > 0'
            ]);
            $this->_data['avanza_not_voted_circunscripcion_1'] = $this->voters_new_model->count([
                'circunscripcion' => 1,
                'has_voted_municipal_2020' => 0,
                'avanza_member_id > 0'
            ]);
            $this->_data['avanza_circunscripcion_1_turnout'] = ($this->_data['avanza_voted_circunscripcion_1'] > 1) ? number_format($this->_data['avanza_voted_circunscripcion_1'] / $this->_data['avanza_total_circunscripcion_1'] * 100, 4) . '%': '0.0000%';
            $this->_data['circunscripcion_1_turnout'] = ($this->_data['voted_circunscripcion_1'] > 1) ? number_format($this->_data['voted_circunscripcion_1'] / $this->_data['total_circunscripcion_1'] * 100, 4) . '%': '0.0000%';
            $this->_data['total_circunscripcion_2'] = $this->voters_new_model->count([
                'circunscripcion' => 2,
                'avanza_member_id = 0'
            ]);
            $this->_data['voted_circunscripcion_2'] = $this->voters_new_model->count([
                'circunscripcion' => 2,
                'has_voted_municipal_2020' => 1,
                'avanza_member_id = 0'
            ]);
            $this->_data['not_voted_circunscripcion_2'] = $this->voters_new_model->count([
                'circunscripcion' => 2,
                'has_voted_municipal_2020' => 0,
                'avanza_member_id = 0'
            ]);
            $this->_data['avanza_total_circunscripcion_2'] = $this->voters_new_model->count([
                'circunscripcion' => 2,
                'avanza_member_id > 0'
            ]);
            $this->_data['avanza_voted_circunscripcion_2'] = $this->voters_new_model->count([
                'circunscripcion' => 2,
                'has_voted_municipal_2020' => 1,
                'avanza_member_id > 0'
            ]);
            $this->_data['avanza_not_voted_circunscripcion_2'] = $this->voters_new_model->count([
                'circunscripcion' => 2,
                'has_voted_municipal_2020' => 0,
                'avanza_member_id > 0'
            ]);
            $this->_data['avanza_circunscripcion_2_turnout'] = ($this->_data['avanza_voted_circunscripcion_2'] > 1) ? number_format($this->_data['avanza_voted_circunscripcion_2'] / $this->_data['avanza_total_circunscripcion_2'] * 100, 4) . '%': '0.0000%';
            $this->_data['circunscripcion_2_turnout'] = ($this->_data['voted_circunscripcion_2'] > 1) ? number_format($this->_data['voted_circunscripcion_2'] / $this->_data['total_circunscripcion_2'] * 100, 4) . '%': '0%';
            $this->_data['total_circunscripcion_3'] = $this->voters_new_model->count([
                'circunscripcion' => 3,
                'avanza_member_id = 0'
            ]);
            $this->_data['voted_circunscripcion_3'] = $this->voters_new_model->count([
                'circunscripcion' => 3,
                'has_voted_municipal_2020' => 1,
                'avanza_member_id = 0'
            ]);
            $this->_data['not_voted_circunscripcion_3'] = $this->voters_new_model->count([
                'circunscripcion' => 3,
                'has_voted_municipal_2020' => 0,
                'avanza_member_id = 0'
            ]);
            $this->_data['avanza_total_circunscripcion_3'] = $this->voters_new_model->count([
                'circunscripcion' => 3,
                'avanza_member_id > 0'
            ]);
            $this->_data['avanza_voted_circunscripcion_3'] = $this->voters_new_model->count([
                'circunscripcion' => 3,
                'has_voted_municipal_2020' => 1,
                'avanza_member_id > 0'
            ]);
            $this->_data['avanza_not_voted_circunscripcion_3'] = $this->voters_new_model->count([
                'circunscripcion' => 3,
                'has_voted_municipal_2020' => 0,
                'avanza_member_id > 0'
            ]);
            $this->_data['avanza_circunscripcion_3_turnout'] = ($this->_data['avanza_voted_circunscripcion_3'] > 1) ? number_format($this->_data['avanza_voted_circunscripcion_3'] / $this->_data['avanza_total_circunscripcion_3'] * 100, 4) . '%': '0.0000%';
            $this->_data['circunscripcion_3_turnout'] = ($this->_data['voted_circunscripcion_3'] > 1) ? number_format($this->_data['voted_circunscripcion_3'] / $this->_data['total_circunscripcion_3'] * 100, 4) . '%': '0%';

            $this->_data['total_all'] = $this->_data['total_circunscripcion_3'] + $this->_data['total_circunscripcion_2'] + $this->_data['total_circunscripcion_1'];
            $this->_data['voted_all'] = $this->_data['voted_circunscripcion_3'] + $this->_data['voted_circunscripcion_2'] + $this->_data['voted_circunscripcion_1'];
            $this->_data['not_voted_all'] = $this->_data['not_voted_circunscripcion_3'] + $this->_data['not_voted_circunscripcion_2'] + $this->_data['not_voted_circunscripcion_1'];
            $this->_data['all_turnout'] = number_format($this->_data['voted_all'] / $this->_data['total'] * 100, 4) . '%';

            $this->_data['total_avanza'] = $this->_data['avanza_total_circunscripcion_3'] + $this->_data['avanza_total_circunscripcion_2'] + $this->_data['avanza_total_circunscripcion_1'];
            $this->_data['voted_avanza'] = $this->_data['avanza_voted_circunscripcion_3'] + $this->_data['avanza_voted_circunscripcion_2'] + $this->_data['avanza_voted_circunscripcion_1'];
            $this->_data['not_voted_avanza'] = $this->_data['avanza_not_voted_circunscripcion_3'] + $this->_data['avanza_not_voted_circunscripcion_2'] + $this->_data['avanza_not_voted_circunscripcion_1'];
            $this->_data['avanza_turnout'] = number_format($this->_data['voted_avanza'] / $this->_data['total'] * 100, 4) . '%';
            $this->_data['turnout_percentage_other'] = $this->_data['avanza_turnout'];
            $this->_data['turnout_percentage_avanza'] = $this->_data['all_turnout'];
            $this->_data['turnout_percentage_rest'] = 100 - (float)$this->_data['turnout_percentage_other'] - (float)$this->_data['turnout_percentage_avanza'];

            $this->_data['circunscripcion_headers'] = ['Circ.', 'Recinto', 'Total Electores', 'Avanza Total Electores', 'Total Votaron', 'Total por Votar', '% Participación', 'Avanza % Participación', 'Acción'];
            $this->_data['circunscripcion'] = $this->get_circunscripcion_global();
            $this->cache_service->set('grassroot_dashboard_global_tactical', $this->_data);
        }
        $this->_data['page_name'] = 'Tablero DN';
        return $this->render('Grassroot/GlobalDashboard', $this->_data);
    }
}