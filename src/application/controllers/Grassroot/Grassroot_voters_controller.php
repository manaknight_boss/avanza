<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Grassroot_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Voters Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Grassroot_voters_controller extends Grassroot_controller
{
    protected $_model_file = 'voters_new_model';
    public $_page_name = 'Votantes';

    public function __construct()
    {
        parent::__construct();
        $this->load->library('cache_service');
        $this->cache_service->set_adapter('file', 432000);
        $recintos = $this->cache_service->get('recinto');
        $colegios = $this->cache_service->get('colegio');

        if ($recintos)
        {
            $this->_data['recinto'] = $recintos;
        }
        else
        {
            $recintos = $this->get_recintos();
            $this->cache_service->set('recinto', $recintos);
            $this->_data['recinto'] = $recintos;
        }

        if ($colegios)
        {
            $this->_data['colegio'] = $colegios;
        }
        else
        {
            $colegios = $this->get_colleges();
            $this->cache_service->set('colegio', $colegios);
            $this->_data['colegio'] = $colegios;
        }
    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Voters_grassroot_list_paginate_view_model.php';
        $session = $this->get_session();
        $this->_data['view_model'] = new Voters_grassroot_list_paginate_view_model(
            $this->voters_new_model,
            $this->pagination,
            '/grassroot/voters/0');
        $this->_data['view_model']->set_heading('Votantes');
        $this->_data['view_model']->set_circunscripcion(($this->input->get('circunscripcion', TRUE) != NULL) ? $this->input->get('circunscripcion', TRUE) : NULL);
		$this->_data['view_model']->set_government_id(($this->input->get('government_id', TRUE) != NULL) ? (string)$this->input->get('government_id', TRUE) : NULL);
		$this->_data['view_model']->set_recinto(($this->input->get('recinto', TRUE) != NULL) ? $this->input->get('recinto', TRUE) : NULL);
		$this->_data['view_model']->set_colegio(($this->input->get('colegio', TRUE) != NULL) ? (string)$this->input->get('colegio', TRUE) : NULL);
		$this->_data['view_model']->set_has_voted_municipal_2020(($this->input->get('has_voted_municipal_2020', TRUE) != NULL) ? $this->input->get('has_voted_municipal_2020', TRUE) : NULL);
		$this->_data['view_model']->set_source(($this->input->get('source', TRUE) != NULL) ? $this->input->get('source', TRUE) : NULL);

        $where = [
            'circunscripcion' => $this->_data['view_model']->get_circunscripcion(),
			'government_id' => $this->_data['view_model']->get_government_id(),
			'recinto' => $this->_data['view_model']->get_recinto(),
			'colegio' => $this->_data['view_model']->get_colegio(),
			'has_voted_municipal_2020' => $this->_data['view_model']->get_has_voted_municipal_2020(),
        ];

        if ($this->_data['view_model']->get_source() == 1)
        {
            $where[] = 'avanza_member_id > 0';
        }

        if ($this->_data['view_model']->get_source() == 3)
        {
            $where[] = ' tactical_member_id > 0';
        }

        $this->_data['view_model']->set_total_rows($this->voters_new_model->count($where));

        $this->_data['view_model']->set_per_page(20);
        $this->_data['view_model']->set_page($page);
        $list = $this->voters_new_model->get_paginated(
        $this->_data['view_model']->get_page(),
        $this->_data['view_model']->get_per_page(),
        $where);
		$this->_data['view_model']->set_list($this->process_cordinator($list));
        return $this->render('Grassroot/Voters', $this->_data);
	}

	public function get_recintos ()
	{
		$this->load->model('voters_new_model');
		$result = $this->voters_new_model->raw_query('SELECT DISTINCT(recinto) as recinto, recinto_name FROM voters_new');
		$recintos = [];
		foreach ($result->result() as $key => $row) {
			if (strlen($row->recinto) > 0)
			{
				$recintos[$row->recinto] = $row->recinto . '(' . $row->recinto_name . ')';
			}
		}
		return $recintos;
	}

	public function get_colleges ()
	{
		$this->load->model('voters_new_model');
		$result = $this->voters_new_model->raw_query('SELECT DISTINCT(colegio) as colegio FROM voters_new');
		$colegios = [];
		foreach ($result->result() as $key => $row)
		{
			if (strlen($row->colegio) > 0)
			{
				$colegios[$row->colegio] = $row->colegio;
			}
		}
		return $colegios;
	}

    public function voteNow ($id)
	{
        $this->load->model('voters_new_model');
        $model = $this->voters_new_model->get($id);
        $this->voters_new_model->edit([
            'has_voted_municipal_2020' => 1
        ], $id);
		return $this->redirect('/grassroot/voters/0');
	}

    public function process_cordinator ($list)
    {
        foreach ($list as $key => $value)
        {
            if ($value->cordinator_id > 0)
            {
                $cordinator = $this->voters_new_model->get($value->cordinator_id);
                $phone_1 = $cordinator->phone_1 ? $cordinator->phone_1 : '';
                $phone_2 = $cordinator->phone_2 ? $cordinator->phone_2 : '';
                $list[$key]->cordinator = $cordinator->first_name . ' ' . $cordinator->last_name . '<br/>' . $cordinator->government_id . '<br/><br/>' . $phone_1 . '<br/>' . $phone_2;
            }
            else
            {
                if ($value->avanza_member_id > 0 || $value->tactical_member_id > 0)
                {
                    $list[$key]->cordinator = 'CFD';
                }
                else
                {
                    $list[$key]->cordinator = 'No';
                }
            }
        }
        return $list;
    }
}