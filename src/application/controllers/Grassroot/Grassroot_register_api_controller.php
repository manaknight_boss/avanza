<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once __DIR__ . '/../../services/User_service.php';
include_once __DIR__ . '/../../services/Token_service.php';
include_once 'Grassroot_api_auth_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Register API Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Grassroot_register_api_controller extends Grassroot_api_auth_controller
{
    public function __construct()
    {
        parent::__construct();
    }

	public function index ()
	{
        $this->load->model('grassroot_user_model');
        $this->load->model('token_model');

        $service = new User_service($this->grassroot_user_model);
        $token_service = new Token_service();
        $token_service->set_model($this->token_model);

        $this->form_validation->set_rules('email', 'Cédula', 'trim|required|is_unique[user.email]');
        $this->form_validation->set_rules('password', 'Contraseña', 'trim|required');
        $this->form_validation->set_rules('first_name', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('last_name', 'Apellidos', 'trim|required');
        $this->form_validation->set_rules('table1', 'Colegio 1', 'required|integer');
        $this->form_validation->set_rules('table2', 'Colegio 2', 'required|integer');
        $this->form_validation->set_rules('table3', 'Colegio 3', 'required|integer');

        if ($this->form_validation->run() === FALSE)
        {
            return $this->_render_validation_error();
        }

        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $table = $this->input->post('table');
        $table2 = $this->input->post('table2');
        $table3 = $this->input->post('table3');

        $created_user = $service->create($email, $password, $first_name, $last_name, 1);

        if (!$created_user)
        {
            return $this->render([
                'email' => 'No se pudo crear el usuario. Intentelo nuevamente'
            ]);
        }

        $this->grassroot_user_model->edit([
            'stripe_id' => $table,
            'refer' => $table2,
            'phone' => $table3,

        ], $created_user->id);

        $key = $this->config->item('jwt_key');
        $base_url = $this->config->item('base_url');
        $jwt_expire_at = $this->config->item('jwt_expire_at');
        $jwt_refresh_expire_at = $this->config->item('jwt_refresh_expire_at');
        $access_token = $token_service->generate_access_token($key, $base_url, $jwt_expire_at, [
            'user_id' => $created_user->id,
            'role_id' => $created_user->role_id
        ]);
        $refresh_token = $token_service->generate_refresh_token($created_user->id, $jwt_refresh_expire_at);

        return $this->success([
            'access_token' => $access_token,
            'refresh_token' => $refresh_token,
            'expire_in' => 14400
        ], 200);
    }
}