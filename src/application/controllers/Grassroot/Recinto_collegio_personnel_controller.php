<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Grassroot_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Personnel Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Recinto_collegio_personnel_controller extends Grassroot_controller
{
    protected $_model_file = 'recinto_collegio_personnel_model';
    public $_page_name = 'Personal';

    public function __construct()
    {
        parent::__construct();
        

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Personnel_grassroot_list_paginate_view_model.php';
        $session = $this->get_session();
        $format = $this->input->get('format', TRUE) ?? 'view';
        $order_by = $this->input->get('order_by', TRUE) ?? '';
        $direction = $this->input->get('direction', TRUE) ?? 'ASC';

        $this->_data['view_model'] = new Personnel_grassroot_list_paginate_view_model(
            $this->recinto_collegio_personnel_model,
            $this->pagination,
            '/grassroot/personnel/0');
        $this->_data['view_model']->set_heading('Personal');
        $this->_data['view_model']->set_recinto(($this->input->get('recinto', TRUE) != NULL) ? $this->input->get('recinto', TRUE) : NULL);
		$this->_data['view_model']->set_collegio(($this->input->get('collegio', TRUE) != NULL) ? $this->input->get('collegio', TRUE) : NULL);
		$this->_data['view_model']->set_staff_lead_name(($this->input->get('staff_lead_name', TRUE) != NULL) ? $this->input->get('staff_lead_name', TRUE) : NULL);
		$this->_data['view_model']->set_staff_lead_government_id(($this->input->get('staff_lead_government_id', TRUE) != NULL) ? $this->input->get('staff_lead_government_id', TRUE) : NULL);
		$this->_data['view_model']->set_transmitter_name(($this->input->get('transmitter_name', TRUE) != NULL) ? $this->input->get('transmitter_name', TRUE) : NULL);
		$this->_data['view_model']->set_transmitter_government_id(($this->input->get('transmitter_government_id', TRUE) != NULL) ? $this->input->get('transmitter_government_id', TRUE) : NULL);
		$this->_data['view_model']->set_delegate_1_name(($this->input->get('delegate_1_name', TRUE) != NULL) ? $this->input->get('delegate_1_name', TRUE) : NULL);
		$this->_data['view_model']->set_delegate_1_government_id(($this->input->get('delegate_1_government_id', TRUE) != NULL) ? $this->input->get('delegate_1_government_id', TRUE) : NULL);
		$this->_data['view_model']->set_delegate_2_name(($this->input->get('delegate_2_name', TRUE) != NULL) ? $this->input->get('delegate_2_name', TRUE) : NULL);
		$this->_data['view_model']->set_delegate_2_government_id(($this->input->get('delegate_2_government_id', TRUE) != NULL) ? $this->input->get('delegate_2_government_id', TRUE) : NULL);
		$this->_data['view_model']->set_runner_1_name(($this->input->get('runner_1_name', TRUE) != NULL) ? $this->input->get('runner_1_name', TRUE) : NULL);
		$this->_data['view_model']->set_runner_1_government_id(($this->input->get('runner_1_government_id', TRUE) != NULL) ? $this->input->get('runner_1_government_id', TRUE) : NULL);
		$this->_data['view_model']->set_runner_2_name(($this->input->get('runner_2_name', TRUE) != NULL) ? $this->input->get('runner_2_name', TRUE) : NULL);
		$this->_data['view_model']->set_runner_2_government_id(($this->input->get('runner_2_government_id', TRUE) != NULL) ? $this->input->get('runner_2_government_id', TRUE) : NULL);
		$this->_data['view_model']->set_staff_lead_present(($this->input->get('staff_lead_present', TRUE) != NULL) ? $this->input->get('staff_lead_present', TRUE) : NULL);
		$this->_data['view_model']->set_transmitter_present(($this->input->get('transmitter_present', TRUE) != NULL) ? $this->input->get('transmitter_present', TRUE) : NULL);
		$this->_data['view_model']->set_delegate_1_present(($this->input->get('delegate_1_present', TRUE) != NULL) ? $this->input->get('delegate_1_present', TRUE) : NULL);
		$this->_data['view_model']->set_delegate_2_present(($this->input->get('delegate_2_present', TRUE) != NULL) ? $this->input->get('delegate_2_present', TRUE) : NULL);
		$this->_data['view_model']->set_runner_1_present(($this->input->get('runner_1_present', TRUE) != NULL) ? $this->input->get('runner_1_present', TRUE) : NULL);
		$this->_data['view_model']->set_runner_2_present(($this->input->get('runner_2_present', TRUE) != NULL) ? $this->input->get('runner_2_present', TRUE) : NULL);
		
        $where = [
            'recinto' => $this->_data['view_model']->get_recinto(),
			'collegio' => $this->_data['view_model']->get_collegio(),
			'staff_lead_name' => $this->_data['view_model']->get_staff_lead_name(),
			'staff_lead_government_id' => $this->_data['view_model']->get_staff_lead_government_id(),
			'transmitter_name' => $this->_data['view_model']->get_transmitter_name(),
			'transmitter_government_id' => $this->_data['view_model']->get_transmitter_government_id(),
			'delegate_1_name' => $this->_data['view_model']->get_delegate_1_name(),
			'delegate_1_government_id' => $this->_data['view_model']->get_delegate_1_government_id(),
			'delegate_2_name' => $this->_data['view_model']->get_delegate_2_name(),
			'delegate_2_government_id' => $this->_data['view_model']->get_delegate_2_government_id(),
			'runner_1_name' => $this->_data['view_model']->get_runner_1_name(),
			'runner_1_government_id' => $this->_data['view_model']->get_runner_1_government_id(),
			'runner_2_name' => $this->_data['view_model']->get_runner_2_name(),
			'runner_2_government_id' => $this->_data['view_model']->get_runner_2_government_id(),
			'staff_lead_present' => $this->_data['view_model']->get_staff_lead_present(),
			'transmitter_present' => $this->_data['view_model']->get_transmitter_present(),
			'delegate_1_present' => $this->_data['view_model']->get_delegate_1_present(),
			'delegate_2_present' => $this->_data['view_model']->get_delegate_2_present(),
			'runner_1_present' => $this->_data['view_model']->get_runner_1_present(),
			'runner_2_present' => $this->_data['view_model']->get_runner_2_present(),
			
        ];

        $this->_data['view_model']->set_total_rows($this->recinto_collegio_personnel_model->count($where));

        $this->_data['view_model']->set_format_layout($this->_data['layout_clean_mode']);
        $this->_data['view_model']->set_per_page(25);
        $this->_data['view_model']->set_order_by($order_by);
        $this->_data['view_model']->set_sort($direction);
        $this->_data['view_model']->set_sort_base_url('/grassroot/personnel/0');
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->recinto_collegio_personnel_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where,
            $order_by,
            $direction));

        if ($format != 'view')
        {
            return $this->output->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($this->_data['view_model']->to_json()));
        }

        return $this->render('Grassroot/Personnel', $this->_data);
	}



	public function edit($id)
	{
        $model = $this->recinto_collegio_personnel_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/grassroot/personnel/0');
        }

        include_once __DIR__ . '/../../view_models/Personnel_grassroot_edit_view_model.php';
        $this->form_validation = $this->recinto_collegio_personnel_model->set_form_validation(
        $this->form_validation, $this->recinto_collegio_personnel_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Personnel_grassroot_edit_view_model($this->recinto_collegio_personnel_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Personal');
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Grassroot/PersonnelEdit', $this->_data);
        }

        $staff_lead_name = $this->input->post('staff_lead_name');
		$staff_lead_government_id = $this->input->post('staff_lead_government_id');
		$transmitter_name = $this->input->post('transmitter_name');
		$transmitter_government_id = $this->input->post('transmitter_government_id');
		$delegate_1_name = $this->input->post('delegate_1_name');
		$delegate_1_government_id = $this->input->post('delegate_1_government_id');
		$delegate_2_name = $this->input->post('delegate_2_name');
		$delegate_2_government_id = $this->input->post('delegate_2_government_id');
		$runner_1_name = $this->input->post('runner_1_name');
		$runner_1_government_id = $this->input->post('runner_1_government_id');
		$runner_2_name = $this->input->post('runner_2_name');
		$runner_2_government_id = $this->input->post('runner_2_government_id');
		$staff_lead_present = $this->input->post('staff_lead_present');
		$transmitter_present = $this->input->post('transmitter_present');
		$delegate_1_present = $this->input->post('delegate_1_present');
		$delegate_2_present = $this->input->post('delegate_2_present');
		$runner_1_present = $this->input->post('runner_1_present');
		$runner_2_present = $this->input->post('runner_2_present');
		
        $result = $this->recinto_collegio_personnel_model->edit([
            'staff_lead_name' => $staff_lead_name,
			'staff_lead_government_id' => $staff_lead_government_id,
			'transmitter_name' => $transmitter_name,
			'transmitter_government_id' => $transmitter_government_id,
			'delegate_1_name' => $delegate_1_name,
			'delegate_1_government_id' => $delegate_1_government_id,
			'delegate_2_name' => $delegate_2_name,
			'delegate_2_government_id' => $delegate_2_government_id,
			'runner_1_name' => $runner_1_name,
			'runner_1_government_id' => $runner_1_government_id,
			'runner_2_name' => $runner_2_name,
			'runner_2_government_id' => $runner_2_government_id,
			'staff_lead_present' => $staff_lead_present,
			'transmitter_present' => $transmitter_present,
			'delegate_1_present' => $delegate_1_present,
			'delegate_2_present' => $delegate_2_present,
			'runner_1_present' => $runner_1_present,
			'runner_2_present' => $runner_2_present,
			
        ], $id);

        if ($result)
        {
            return $this->redirect('/grassroot/personnel/0?recinto=' . $model->recinto, 'refresh');
            return $this->redirect('/grassroot/personnel/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Grassroot/PersonnelEdit', $this->_data);
	}

	public function view($id)
	{
        $model = $this->recinto_collegio_personnel_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/grassroot/personnel/0');
		}


        include_once __DIR__ . '/../../view_models/Personnel_grassroot_view_view_model.php';
		$this->_data['view_model'] = new Personnel_grassroot_view_view_model($this->recinto_collegio_personnel_model);
		$this->_data['view_model']->set_heading('Personal');
        $this->_data['view_model']->set_model($model);
        return $this->render('Grassroot/PersonnelView', $this->_data);
	}





}