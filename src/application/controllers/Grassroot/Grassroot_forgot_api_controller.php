<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once __DIR__ . '/../../services/User_service.php';
include_once 'Grassroot_api_auth_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Forgot API Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Grassroot_forgot_api_controller extends Grassroot_api_auth_controller
{
    public function __construct()
    {
        parent::__construct();
    }

	public function index ()
	{
        $this->load->model('grassroot_user_model');
        $this->load->model('email_model');
        $this->load->model('token_model');
        $this->load->library('mail_service');

        $service = new User_service($this->grassroot_user_model);

        $this->form_validation->set_rules('email', 'Correo', 'trim|required|valid_email');

        if ($this->form_validation->run() === FALSE)
        {
            return $this->_render_validation_error();
        }

        $email = $this->input->post('email');
        $from_email = $this->config->item('from_email');
        $base_url = $this->config->item('base_url');

        $this->mail_service->set_adapter('smtp');
        $service->set_email_model($this->email_model);
        $service->set_token_model($this->token_model);
        $service->set_email_service($this->mail_service);

        $reset = $service->forgot_password($email, $from_email, $base_url . '/grassroot/reset', 1);

        if ($reset)
        {
            return $this->success([
                'message' => 'Hemos enviado un correo de actualizacion. Revise su correo.'
            ]);
        }

        return $this->_render_custom_error([
            'email' => 'Su correo no esta registrado en nuestros sistemas.'
        ]);
	}
}