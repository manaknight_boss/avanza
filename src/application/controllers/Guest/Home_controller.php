<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Guest_controller.php';

class Home_controller extends Guest_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('MarketingPage');
    }

}
