<?php defined('BASEPATH') OR exit('No direct script access allowed');

include_once __DIR__ . '/../../middlewares/Token_middleware.php';
include_once __DIR__ . '/../../middlewares/Token_acl_middleware.php';
include_once __DIR__ . '/../../middlewares/Maintenance_middleware.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * SMS Custom API  Controller
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Sms_custom_api_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * Render api output
     *
     * @param mixed $data
     * @param number $code
     * @return string
     */
    public function render($data, $code)
    {
        http_response_code($code);
        header('Content-Type: ' . $this->_supported_formats[$this->_format]);
        switch ($this->_format)
        {
            case 'json':
                return $this->output->set_content_type($this->_supported_formats[$this->_format])
                ->set_status_header($code)
                ->set_output(json_encode($data));
                break;
            case 'test':
                return $data;
                break;
            default:
                return '<pre>' . print_r($data, TRUE) . '</pre>';
                break;
        }
    }

    /**
     * User token invalid
     *
     * @return string
     */
    public function unauthorize_error_message()
    {
        return $this->output->set_content_type($this->_supported_formats[$this->_format])
        ->set_status_header(401)
        ->set_output(json_encode([
            'code' => 401,
            'success' => FALSE,
            'message' => 'Credenciales invalidos'
        ]));
    }

    /**
     * User Role invalid
     *
     * @return string
     */
    public function unauthorize_resource_error_message()
    {
        return $this->output->set_content_type($this->_supported_formats[$this->_format])
            ->set_status_header(406)
            ->set_output(json_encode([
                'code' => 406,
                'success' => FALSE,
                'message' => 'cannot access resource'
            ]));
    }

    /**
     * Success API Call
     *
     * @return string
     */
    public function success($success)
    {
        $success['code'] = 200;
        $success['success'] = TRUE;
        return $this->output->set_content_type($this->_supported_formats[$this->_format])
            ->set_status_header(200)
            ->set_output(json_encode($success));
    }

    /**
     * Invalid form input
     *
     * @return string
     */
    protected function _render_validation_error ()
	{
        $data = [];
        $data['code'] = 403;
        $data['success'] = FALSE;
        $data['error'] = $this->form_validation->error_array();
        return $this->output->set_content_type($this->_supported_formats[$this->_format])
            ->set_status_header(403)
            ->set_output(json_encode($data));
    }

    /**
     * Render Custom Error
     *
     * @return string
     */
    protected function _render_custom_error ($errors)
	{
        $data = [];
        $data['code'] = 403;
        $data['success'] = FALSE;
        $data['error'] = $errors;
        return $this->output->set_content_type($this->_supported_formats[$this->_format])
            ->set_status_header(403)
            ->set_output(json_encode($data));
    }

    /**
     * Debug Controller to error_log and turn off in production
     *
     * @param mixed $data
     * @return void
     */
    public function dl($key, $data)
    {
        if (ENVIRONMENT == 'development')
        {
            error_log($key . ' CONTROLLER : <pre>' . print_r($data, TRUE) . '</pre>');
        }
    }

    /**
     * Debug json Controller to error_log and turn off in production
     *
     * @param mixed $data
     * @return void
     */
    public function dj($key, $data)
    {
        if (ENVIRONMENT == 'development')
        {
            error_log($key . ' CONTROLLER : ' . json_encode($data));
        }
    }

    public function custom ()
    {
        $this->load->model('call_history_model');
        $this->load->model('sms_history_model');
        $this->load->model('campaign_model');
        $this->load->library('sms_service');
        $this->sms_service->set_adapter('sms');

        // $calls = $this->call_history_model->_join_paginate('campaign', 'campaign_id',  [
        //     'a.call_result' => 0,
        //     'a.call_type' => 2,
        //     'b.status' => 3
        // ], 0, 10);
        $sms = $this->sms_history_model->_join_paginate('campaign', 'campaign_id',  [
            'a.sms_result' => 0,
            'a.sms_type' => 3,
            'b.status' => 2
        ], 0, 10);
        $result = [];
        $campaigns = [];
        // foreach ($calls as $key => $value)
        // {
        //     $message = '';
        //     if (array_key_exists($value->campaign_id, $campaigns))
        //     {
        //         $message = $campaigns[$value->campaign_id]->content;
        //     }
        //     else
        //     {
        //         $campaign = $this->campaign_model->get($value->campaign_id);
        //         if ($campaign)
        //         {
        //             $campaigns[$value->campaign_id] = $campaign;
        //             $message = $campaign->content;
        //         }
        //     }

        //     $call_number = $value->phone_1;

        //     if (!$value->phone_1 && $value->phone_2)
        //     {
        //         $call_number = $value->phone_2;
        //     }

        //     $result[] = [
        //         'phone' => $this->sms_service->add_country_code(1, $call_number),
        //         'type' => 'call',
        //         'timeout' => 3,
        //         'message' => $message,
        //         'save_api' => base_url() . 'v1/api/smscall/call/save/' . $value->a_id,
        //         'save_api_no_answer' => base_url() . 'v1/api/smscall/call/no_answer/' . $value->a_id,
        //         'save_api_fail' => base_url() . 'v1/api/smscall/call/fail/' . $value->a_id
        //     ];
        // }

        foreach ($sms as $key => $value)
        {
            $message = '';
            if (array_key_exists($value->campaign_id, $campaigns))
            {
                $message = $campaigns[$value->campaign_id]->content;
            }
            else
            {
                $campaign = $this->campaign_model->get($value->campaign_id);
                if ($campaign)
                {
                    $campaigns[$value->campaign_id] = $campaign;
                    $message = $campaign->content;
                }
            }

            $call_number = $value->phone_1;

            if (!$value->phone_1 && $value->phone_2)
            {
                $call_number = $value->phone_2;
            }

            $result[] = [
                'phone' => $this->sms_service->add_country_code(1, $call_number),
                'type' => 'sms',
                'timeout' => 3,
                'message' => $message,
                'save_api' => base_url() . 'v1/api/smscall/sms/save/' . $value->a_id,
                'save_api_no_answer' => base_url() . 'v1/api/smscall/sms/no_answer/' . $value->a_id,
                'save_api_fail' => base_url() . 'v1/api/smscall/sms/fail/' . $value->a_id
            ];
        }
        echo json_encode($result, JSON_UNESCAPED_SLASHES);
        exit;
    }

    public function customs ($num)
    {
        $this->load->model('call_history_model');
        $this->load->model('sms_history_model');
        $this->load->model('campaign_model');
        $this->load->library('sms_service');
        $this->sms_service->set_adapter('sms');

        // $calls = $this->call_history_model->_join_paginate('campaign', 'campaign_id',  [
        //     'a.call_result' => 0,
        //     'a.call_type' => 2,
        //     'b.status' => 3
        // ], 0, 10);
        $sms = $this->sms_history_model->_join_paginate('campaign', 'campaign_id',  [
            'a.sms_result' => 0,
            'a.sms_type' => 3,
            'b.status' => 2,
            'number' => $num
        ], 0, 10);
        $result = [];
        $campaigns = [];

        foreach ($sms as $key => $value)
        {
            $message = '';
            if (array_key_exists($value->campaign_id, $campaigns))
            {
                $message = $campaigns[$value->campaign_id]->content;
            }
            else
            {
                $campaign = $this->campaign_model->get($value->campaign_id);
                if ($campaign)
                {
                    $campaigns[$value->campaign_id] = $campaign;
                    $message = $campaign->content;
                }
            }

            $call_number = $value->phone_1;

            if (!$value->phone_1 && $value->phone_2)
            {
                $call_number = $value->phone_2;
            }

            $result[] = [
                'phone' => $this->sms_service->add_country_code(1, $call_number),
                'type' => 'sms',
                'timeout' => 3,
                'message' => $message,
                'save_api' => base_url() . 'v1/api/smscall/sms/save/' . $value->a_id,
                'save_api_no_answer' => base_url() . 'v1/api/smscall/sms/no_answer/' . $value->a_id,
                'save_api_fail' => base_url() . 'v1/api/smscall/sms/fail/' . $value->a_id
            ];
        }
        echo json_encode($result, JSON_UNESCAPED_SLASHES);
        exit;
    }

    public function smssave($id)
    {
        $this->load->model('sms_history_model');
        $this->load->model('campaign_model');
        $model = $this->sms_history_model->get($id);
        if ($model)
        {
            $this->sms_history_model->edit([
                'sms_result' => 1
            ], $id);
            $left = $this->sms_history_model->count([
                'campaign_id' => $model->campaign_id,
                'sms_result' => 0
            ]);
            if ($left < 1)
            {
                $this->campaign_model->edit([
                    'status' => 3
                ], $model->campaign_id);
            }
        }
        echo json_encode(['success' => TRUE, 'code' => 200]);
        exit;
    }
    public function callsave($id)
    {
        $this->load->model('call_history_model');
        $model = $this->call_history_model->get($id);
        if ($model)
        {
            $this->call_history_model->edit([
                'call_result' => 1
            ], $id);
        }
        echo json_encode(['success' => TRUE, 'code' => 200]);
        exit;
    }

    public function smsno_answer($id)
    {
        $this->load->model('sms_history_model');
        $model = $this->sms_history_model->get($id);
        if ($model)
        {
            $this->sms_history_model->edit([
                'sms_result' => 3
            ], $id);
        }
        echo json_encode(['success' => TRUE, 'code' => 200]);
        exit;
    }

    public function callno_answer($id)
    {
        $this->load->model('call_history_model');
        $model = $this->call_history_model->get($id);
        if ($model)
        {
            $this->call_history_model->edit([
                'call_result' => 3
            ], $id);
        }
        echo json_encode(['success' => TRUE, 'code' => 200]);
        exit;
    }

    public function smsfail($id)
    {
        $this->load->model('sms_history_model');
        $model = $this->sms_history_model->get($id);
        if ($model)
        {
            $this->sms_history_model->edit([
                'sms_result' => 2
            ], $id);
        }
        echo json_encode(['success' => TRUE, 'code' => 200]);
        exit;
    }

    public function callfail($id)
    {
        $this->load->model('call_history_model');
        $model = $this->call_history_model->get($id);
        if ($model)
        {
            $this->call_history_model->edit([
                'call_result' => 2
            ], $id);
        }
        echo json_encode(['success' => TRUE, 'code' => 200]);
        exit;
    }
}