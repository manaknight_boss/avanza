<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once __DIR__ . '/Cronjob_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Email Cronjob Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Email_cronjob_controller extends Cronjob_controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('email_history_model');
        $this->load->library('mail_service');
        ini_set('memory_limit','1024M');
        $this->load->model('worker_model');
        $this->load->model('campaign_model');
    }

    /**
     * Steps:
     * 1.Check if other job is running by checking worker
     * 2.If no worker, get campaign
     * 3.If campaign is ready, then set worker. Send email to owners
     * 4.Set campaign to running
     * 5.Start calling people
     * 6.When calling is done, set campaign to done
     * 7.Email owner its done
     * @return void
     */
    public function index ()
    {
        if ($this->worker_model->count([]) == 0)
        {
          $open_campaigns = $this->campaign_model->get_all([
            'status' => 2
          ]);

          error_log("Open Campaigns Found\n");

          if (count($open_campaigns) < 1)
          {
            return TRUE;
          }

          $campaign = null;

          foreach ($open_campaigns as $key => $value)
          {
            if ($value->type == 1)
            {
              $campaign = $open_campaigns[$key];
              break;
            }
          }

          error_log("Email Campaigns Found\n");

          if ($campaign == null)
          {
            return True;
          }
          //If campaign is ready, then set worker. Send email to owners
          $this->worker_model->create([
            'id' => 1,
            'job_name' => 'email_cron_job'
          ]);
          $this->campaign_model->edit([
            'status' => 1
          ], $campaign->id);
          $this->_send_email_notification('campaign-start', [
            'id' => $campaign->id,
            'total' => $campaign->total
          ], 'ryan@manaknight.com');
          $this->_send_email_notification('campaign-start', [
            'id' => $campaign->id,
            'total' => $campaign->total
          ], 'avanza360app@gmail.com');

          if (strlen($campaign->query) > 0)
          {
            $this->campaign_all($campaign);
          }
          else
          {
            $this->campaign_custom($campaign);
          }

          //When calling is done, set delete worker
          $this->worker_model->delete(1);
          //When calling is done, set campaign to done
          $this->campaign_model->edit([
            'status' => 3
          ], $campaign->id);
          //Email owner its done
          $this->_send_email_notification('campaign-end', [
            'id' => $campaign->id,
            'total' => $campaign->total
          ], 'ryan@manaknight.com');
          $this->_send_email_notification('campaign-end', [
            'id' => $campaign->id,
            'total' => $campaign->total
          ], 'avanza360app@gmail.com');
        }
    }

    public function campaign_all($campaign)
    {
      error_log("Campaign All\n");
      $this->mail_service->set_adapter('smtp');

      $count = $campaign->total;
      $per_page = 100;
      $num_pages = ceil($count / $per_page);
      error_log("Pagination Start $num_pages\n");

      for ($i=0; $i < $num_pages; $i++)
      {
        $offset = $i * $per_page;
        $query_results = $this->campaign_model->raw_query($campaign->query . " LIMIT {$offset}, {$per_page}");
        $result = $query_results->result();
        error_log("Page $i\n");

        foreach ($result as $key => $value)
        {
          $payload = [
            'government_id' => $value->government_id,
            'email' => $value->email,
            'email_date' => null,
            'campaign_id' => $campaign->id,
            'email_result' => 0
          ];

          $id = 0;

          if (!$value->email)
          {
            $payload['email_date'] = date('Y-m-d');
            $payload['email_result'] = 2;
            $this->email_history_model->create($payload);
            continue;
          }
          else
          {
            $id = $this->email_history_model->create($payload);
          }

          $from = $this->config->item('from_email');
          $this->mail_service->send($from, $value->email, 'Campaña', $campaign->content);
        }
      }
    }

    public function campaign_custom($campaign)
    {
      error_log("Campaign Custom\n");
      $this->mail_service->set_adapter('smtp');

      $count_query = $this->email_history_model->raw_query("SELECT COUNT(*) as num FROM email_history WHERE campaign_id={$campaign->id}");
      $count_result = $count_query->result();
      $count = $count_result[0]->num;
      $per_page = 100;
      $num_pages = ceil($count / $per_page);

      if ($num_pages < $per_page)
      {
        $num_pages = 1;
      }

      error_log("Pagination Start $count $num_pages\n");

      for ($i=0; $i < $num_pages; $i++)
      {
        $offset = $i * $per_page;
        $query_results = $this->email_history_model->raw_query("SELECT * FROM email_history WHERE campaign_id={$campaign->id} " . " LIMIT {$offset}, {$per_page}");
        $result = $query_results->result();
        error_log("Page $i\n");

        foreach ($result as $key => $value)
        {
          if ($value->email_result == 0)
          {
            $from = $this->config->item('from_email');
            $this->mail_service->send($from, $value->email, 'Campaña', $campaign->content);
          }
        }
      }
    }
}