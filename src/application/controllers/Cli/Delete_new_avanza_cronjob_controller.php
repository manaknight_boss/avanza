<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Cronjob_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Ingest New Avanza Cronjob Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Delete_new_avanza_cronjob_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index ()
    {
        $this->load->database();
        ini_set('memory_limit','1024M');
        $this->load->model('raw_avanza_members2_model');
        $file = file_get_contents('avanza3');
        $json = json_decode($file, TRUE);
        echo count($json);
        foreach ($json as $key => $row)
        {
            $id = $row['id'];
            $exist = $this->raw_avanza_members2_model->get_by_field('cordMiembroId', $id);
            if ($exist)
            {
                $this->raw_avanza_members2_model->real_delete($id);
            }
        }
        echo 'Complete Ingestion';

    }

    private function clean_phone ($phone)
    {
        if (!$phone)
        {
            return '';
        }

        $cleanPhones = trim(str_replace(['(', ')', '-', ' '], ['','','', ''], $phone));
        $cleanPhone = '';
        $parts = explode('/', $cleanPhones);
        if (count($parts) > 0)
        {
            foreach ($parts as $singlePhone) {
                $cleanSinglePhone = trim($singlePhone);
                if (strlen($cleanSinglePhone) === 10)
                {
                    return $cleanSinglePhone;
                }
            }
        }
        else
        {
            if (strlen($cleanPhones) === 10)
            {
                $cleanPhone = $cleanPhones;
            }
        }
        return $cleanPhone;
    }
}