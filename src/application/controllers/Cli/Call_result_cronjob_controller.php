<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once __DIR__ . '/Cronjob_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Call result Cronjob Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Call_result_cronjob_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index ()
    {
        ini_set('memory_limit','1024M');
        $this->load->database();
        $this->load->model('call_history_model');
        $this->load->model('voters_new_model');
        $this->load->library('voice_service');
        $this->voice_service->set_adapter();
        $per_page = 100;
        $results = $this->call_history_model->get_paginated(
        0,
        $per_page,
        [
          'call_result' => 0,
          'sid!=""'
        ]);

        foreach ($results as $call)
        {
          $log = $this->voice_service->retrieve_single_call_log($call->sid);

          if ($log)
          {
            $this->call_history_model->edit([
              'call_result' => $this->call_history_model->twillio_call_mapping($log->status)
            ], $call->id);
          }
          else
          {
            $this->call_history_model->edit([
              'call_result' => 2
            ], $call->id);
          }

          $voter = $this->voters_new_model->get_by_field('government_id', $call->government_id);

          if ($voter)
          {
            if ($call->call_type == 1)
            {
              $payload = [
                'has_contact_call_poll' => 1
              ];
            }
            else
            {
              $payload = [
                'has_contact_call' => 1
              ];
            }

            $this->voters_new_model->edit($payload, $voter->id);
          }
        }
        echo 'Complete Call Checks';
    }
}