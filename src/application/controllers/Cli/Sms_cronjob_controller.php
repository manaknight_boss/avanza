<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once __DIR__ . '/Cronjob_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Sms Cronjob Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Sms_cronjob_controller extends Cronjob_controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('sms_history_model');
        $this->load->library('sms_service');
        ini_set('memory_limit','1024M');
        $this->load->model('worker_model');
        $this->load->model('campaign_model');
    }

    /**
     * Steps:
     * 1.Loop through all voters
     * 2.Create call_history
     * 3.Call them
     * 4.Have webhook listen to response and update it
     * 5.Email owner that job is done
     *
     * Multiple paths:
     * 1.Have a worker lock so no other cron job can run
     * 2.Only do 1 campaign and change status so another script can't manage this
     * 3.Loop through pages, create on the fly, and update current page
     * @return void
     */
    public function index ()
    {
        if ($this->worker_model->count([]) == 0)
        {
          $open_campaigns = $this->campaign_model->get_all([
            'status' => 2,
            'type' => 5
          ]);
          $num = count($open_campaigns);
          echo("Open SMS PollingCampaigns Found {$num}\n");

          if ($num < 1)
          {
            exit;
          }

          $campaign = $open_campaigns[0];
          //If campaign is ready, then set worker. Send email to owners
          $this->worker_model->create([
            'id' => 1,
            'job_name' => 'sms_cron_job'
          ]);

          $this->campaign_model->edit([
            'status' => 1
          ], $campaign->id);

          $this->_send_email_notification('campaign-sms-start', [
            'id' => $campaign->id,
            'total' => $campaign->total
          ], 'ryan@manaknight.com');

          $this->_send_email_notification('campaign-sms-start', [
            'id' => $campaign->id,
            'total' => $campaign->total
          ], 'avanza360app@gmail.com');

          $this->send_campaign($campaign);

          //When calling is done, set delete worker
          $this->worker_model->real_delete_all();

          //When calling is done, set campaign to done
          $this->campaign_model->edit([
            'status' => 3
          ], $campaign->id);

          //Email owner its done
          $this->_send_email_notification('campaign-end', [
            'id' => $campaign->id,
            'total' => $campaign->total
          ], 'ryan@manaknight.com');
          $this->_send_email_notification('campaign-end', [
            'id' => $campaign->id,
            'total' => $campaign->total
          ], 'avanza360app@gmail.com');
        }
    }

    public function send_campaign($campaign)
    {
      echo("Campaign Send\n");
      $this->sms_service->set_adapter('sms');

      $query_results = $this->sms_history_model->get_all(['campaign_id' => $campaign->id]);
      foreach ($query_results as $key => $value)
      {
        $call_number = $value->phone_1;

        if (!$value->phone_1 && $value->phone_2)
        {
          $call_number = $value->phone_2;
        }

        if ($value->sms_result == 0)
        {
          //TODO
          $url = 'https://avanza360.app' . '/v1/api/sms/callback/' . $value->id;
          $sid = $this->sms_service->send_callback($this->sms_service->add_country_code(1, $call_number), $campaign->content, $url);

          if (!$sid)
          {
            $this->sms_history_model->edit([
              'sms_result' => 2,
              'sms_date' => date('Y-m-d')
            ], $value->id);
          }
          else
          {
            $this->sms_history_model->edit([
                'sid' => $sid,
                'sms_date' => date('Y-m-d')
              ], $value->id);
          }
        }
      }
    }
}