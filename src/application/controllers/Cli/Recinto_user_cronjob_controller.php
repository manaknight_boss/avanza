<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Cronjob_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Transform Voters Missing with Avanza Cronjob Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Recinto_user_cronjob_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index ()
    {
        ini_set('memory_limit','1024M');
        $this->load->database();
        $this->load->model('recinto_coordinator_model');
        $this->load->model('recinto_collegio_personnel_model');
        $this->load->model('voters_new_model');
        $this->load->model('raw_recintos_model');
        $recintos = $this->raw_recintos_model->get_all([]);
        foreach ($recintos as $key => $value)
        {
            $query_results = $this->voters_new_model->raw_query("SELECT DISTINCT(colegio) FROM voters_new WHERE recinto={$value->RecintoId};");
            $result = $query_results->result();
            $first = TRUE;
            foreach ($result as $key2 => $value2)
            {
                if ($first)
                {
                    $payload = [
                        'recinto' => $value->RecintoId,
                        'recinto_name' => $value->Recinto,
                        'collegio' => count($result)
                    ];
                    $this->recinto_coordinator_model->create($payload);
                    $first = FALSE;
                }
                $this->recinto_collegio_personnel_model->create([
                    'recinto' => $value->RecintoId,
                    'collegio' => (string)$value2->colegio
                ]);
            }
        }

        echo 'Complete Recinto Transition';
    }
}