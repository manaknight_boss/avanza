<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Cronjob_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Ingest Cronjob Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Ingest_avanza_cronjob_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index ()
    {
        $this->load->database();
        $this->load->model('raw_avanza_members_model');
        $file = file_get_contents('2');
        $json = json_decode($file, TRUE);
        echo count($json);
        foreach ($json as $key => $row)
        {
            $payload = [
                "miembroId" => $row['miembroId'],
                "cedulaMiembro" => $row['cedulaMiembro'],
                "cordMiembroId" => $row['cordMiembroId'],
                "nombreMiembro" => $row['nombreMiembro'],
                "apellidoMiembro" => $row['apellidoMiembro'],
                "estatusMiembro" => $row['estatusMiembro'],
                "sexoMiembro" => $row['sexoMiembro'],
                "fecha_NacimientoMiembro" => $row['fecha_NacimientoMiembro'],
                "miembro_PLD" => $row['miembro_PLD'],
                "calle" => $row['calle'],
                "numero" => $row['numero'],
                "codigo_Sector" => $row['codigo_Sector'],
                "codigo_SubSector" => $row['codigo_SubSector'],
                "telefono_Residencial" => $row['telefono_Residencial'],
                "telefono_Celular" => $row['telefono_Celular'],
                "correoMiembro" => $row['correoMiembro'],
                "facebookMiembro" => $row['facebookMiembro'],
                "twitterMiembro" => $row['twitterMiembro'],
                "instagramMiembro" => $row['instagramMiembro'],
                "estadoCivilMiembro" => $row['estadoCivilMiembro'],
                "codigoColegioMiembro" => $row['codigoColegioMiembro'],
                "codigoRecintoMiembro" => $row['codigoRecintoMiembro'],
                "circunscripcionMiembro" => $row['circunscripcionMiembro'],
                "edadMiembro" => $row['edadMiembro'],
                "votoMiembro" => $row['votoMiembro'],
                "cedulaCoodinador" => $row['cedulaCoodinador'],
                "nombreCoodinador" => $row['nombreCoodinador'],
                "apellidoCoodinador" => $row['apellidoCoodinador'],
                "contactResidencialCoodinador" => $row['contactResidencialCoodinador']
            ];
            $this->raw_avanza_members_model->create($payload);
            if ($key % 50 === 0)
            {
                sleep(1);
            }
        }
        echo 'Complete Ingestion';
        // $reset_token_status = $this->token_model->raw_query("UPDATE `token` SET status=0 WHERE `expire_at` < NOW();");
        // $remove_expired_tokens = $this->token_model->raw_query("DELETE FROM `token` WHERE status=0");
    }
}