<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once __DIR__ . '/Cronjob_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Call Custom Voice Cronjob Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Call_custom_cronjob_controller2 extends Cronjob_controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('call_history_model');
        $this->load->library('voice_service');
        ini_set('memory_limit','1024M');
        $this->load->model('worker_model');
        $this->load->model('campaign_model');
    }

    /**
     * Steps:
     * 1.Check if other job is running by checking worker
     * 2.If no worker, get campaign
     * 3.If campaign is ready, then set worker. Send email to owners
     * 4.Set campaign to running
     * 5.Start calling people
     * 6.When calling is done, set campaign to done
     * 7.Email owner its done
     * @return void
     */
    public function index ()
    {
        // if ($this->worker_model->count([]) == 0)
        // {
          $count = $this->call_history_model->count([
            'call_type' => 5,
            'call_result' => 0
          ]);

          if ($count < 1)
          {
            exit;
          }

          $per_page = 500;
          $per_page = 1;
          $num_pages = ceil($count / $per_page);

          if ($num_pages < $per_page)
          {
            $num_pages = 1;
          }

          echo("Pagination Start $count $num_pages\n");

          //If campaign is ready, then set worker. Send email to owners
          // $this->worker_model->create([
          //   'id' => 1,
          //   'job_name' => 'call_custom_cron_job'
          // ]);

          $this->_send_email_notification('campaign-start', [
            'id' => 9999999,
            'total' => $count
          ], 'ryan@manaknight.com');
          $this->_send_email_notification('campaign-start', [
            'id' => 9999999,
            'total' => $count
          ], 'avanza360app@gmail.com');
          $num_pages = 1;
          for ($i=0; $i < $num_pages; $i++)
          {
            $offset = $i * $per_page;
            $results = $this->call_history_model->get_paginated(
              $offset,
              $per_page,
            [
              'call_type' => 5,
              'call_result' => 0
            ],
            'id',
            'DESC');
            $this->send_campaign($results);
          }

          //When calling is done, set delete worker
          // $this->worker_model->real_delete_all();
          //When calling is done, set campaign to done
          //Email owner its done
          $this->_send_email_notification('campaign-end', [
            'id' => 9999999,
            'total' => $count
          ], 'ryan@manaknight.com');
          $this->_send_email_notification('campaign-end', [
            'id' => 9999999,
            'total' => $count
          ], 'avanza360app@gmail.com');
        // }
    }

    public function send_campaign($campaigns)
    {
      echo("Campaign Custom\n");
      $this->voice_service->set_adapter();

      foreach ($campaigns as $key => $value)
      {
        $call_number = $value->phone_1;

        if (!$value->phone_1 && $value->phone_2)
        {
          $call_number = $value->phone_2;
        }

        $url = base_url() . '/v1/api/voice/call/custom/' . $value->campaign_id;
        $url = 'https://avanza360.app' . '/v1/api/voice/call/custom/' . $value->campaign_id;
        // $url = 'http://avanza.manaknightdigital.com' . '/v1/api/voice/call/custom/' . $value->campaign_id;
        try {
          $sid = $this->voice_service->play($this->voice_service->add_country_code(1, $call_number), $url, 15);
          sleep(2);

          if (!$sid)
          {
            $this->call_history_model->edit([
              'call_result' => 2,
              'call_date' => date('Y-m-d')
            ], $value->id);
          }
          else
          {
            $this->call_history_model->edit([
                'sid' => $sid,
                'call_date' => date('Y-m-d')
              ], $value->id);
          }
        } catch (Exception $e) {
          error_log('Error ' . $call_number);
          $this->call_history_model->edit([
            'call_result' => 2,
            'call_date' => date('Y-m-d')
          ], $value->id);
        }



      }
    }
}