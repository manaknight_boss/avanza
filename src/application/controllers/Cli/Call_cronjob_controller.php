<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once __DIR__ . '/Cronjob_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Call Voice Cronjob Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Call_cronjob_controller extends Cronjob_controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('call_history_model');
        $this->load->library('voice_service');
        ini_set('memory_limit','1024M');
        $this->load->model('worker_model');
        $this->load->model('campaign_model');
    }

    /**
     * Steps:
     * 1.Check if other job is running by checking worker
     * 2.If no worker, get campaign
     * 3.If campaign is ready, then set worker. Send email to owners
     * 4.Set campaign to running
     * 5.Start calling people
     * 6.When calling is done, set campaign to done
     * 7.Email owner its done
     * @return void
     */
    public function index ()
    {
        if ($this->worker_model->count([]) == 0)
        {
          $open_campaigns = $this->campaign_model->get_all([
            'status' => 2
          ]);

          echo("Open Campaigns Found\n");

          if (count($open_campaigns) < 1)
          {
            return TRUE;
          }

          $campaign = null;

          foreach ($open_campaigns as $key => $value)
          {
            if ($value->type == 4)
            {
              $campaign = $open_campaigns[$key];
              break;
            }
          }

          echo("Call Campaigns Found\n");

          if ($campaign == null)
          {
            return True;
          }
          //If campaign is ready, then set worker. Send email to owners
          $this->worker_model->create([
            'id' => 1,
            'job_name' => 'call_cron_job'
          ]);
          $this->campaign_model->edit([
            'status' => 1
          ], $campaign->id);
          $this->_send_email_notification('campaign-start', [
            'id' => $campaign->id,
            'total' => $campaign->total
          ], 'ryan@manaknight.com');
          $this->_send_email_notification('campaign-start', [
            'id' => $campaign->id,
            'total' => $campaign->total
          ], 'avanza360app@gmail.com');

          if (strlen($campaign->query) > 0)
          {
            $this->campaign_all($campaign);
          }
          else
          {
            $this->campaign_custom($campaign);
          }

          //When calling is done, set delete worker
          $this->worker_model->real_delete_all();
          //When calling is done, set campaign to done
          $this->campaign_model->edit([
            'status' => 3
          ], $campaign->id);
          //Email owner its done
          $this->_send_email_notification('campaign-end', [
            'id' => $campaign->id,
            'total' => $campaign->total
          ], 'ryan@manaknight.com');
          $this->_send_email_notification('campaign-end', [
            'id' => $campaign->id,
            'total' => $campaign->total
          ], 'avanza360app@gmail.com');
        }
    }

    public function campaign_all($campaign)
    {
      echo("Campaign All\n");
      $this->voice_service->set_adapter();

      $count = $campaign->total;
      $per_page = 100;
      $num_pages = ceil($count / $per_page);
      echo("Pagination Start $num_pages\n");

      for ($i=0; $i < $num_pages; $i++)
      {
        $offset = $i * $per_page;
        $query_results = $this->campaign_model->raw_query($campaign->query . " LIMIT {$offset}, {$per_page}");
        $result = $query_results->result();
        echo("Page $i\n");

        foreach ($result as $key => $value)
        {
          $payload = [
            'government_id' => $value->government_id,
            'phone_1' => $value->phone_1,
            'phone_2' => $value->phone_2,
            'call_date' => null,
            'campaign_id' => $campaign->id,
            'call_result' => 0,
            'call_type' => $campaign->type,
            'poll_result' => 0,
            'sid' => ''
          ];

          $id = 0;

          if (!$value->phone_1 && !$value->phone_2)
          {
            $payload['call_date'] = date('Y-m-d');
            $payload['call_result'] = 2;
            $this->call_history_model->create($payload);
            continue;
          }
          else
          {
            $id = $this->call_history_model->create($payload);
          }

          $call_number = $value->phone_1;

          if (!$value->phone_1 && $value->phone_2)
          {
            $call_number = $value->phone_2;
          }

          // $url = ' https://0529e6e9.ngrok.io' . '/v1/api/voice/call/message/' . $id;
          $url = base_url() . '/v1/api/voice/call/message/' . $id;
          $sid = $this->voice_service->send($this->voice_service->add_country_code(1, $call_number), $url, 15);

          if (!$sid)
          {
            $this->call_history_model->edit([
              'call_result' => 2
            ], $id);
          }
          else
          {
            $this->call_history_model->edit([
                'sid' => $sid
              ], $id);
          }
        }
      }
    }

    public function campaign_custom($campaign)
    {
      echo("Campaign Custom\n");
      $this->voice_service->set_adapter();

      $count_query = $this->call_history_model->raw_query("SELECT COUNT(*) as num FROM call_history WHERE campaign_id={$campaign->id}");
      $count_result = $count_query->result();
      $count = $count_result[0]->num;
      $per_page = 100;
      $num_pages = ceil($count / $per_page);

      if ($num_pages < $per_page)
      {
        $num_pages = 1;
      }

      echo("Pagination Start $count $num_pages\n");

      for ($i=0; $i < $num_pages; $i++)
      {
        $offset = $i * $per_page;
        $query_results = $this->call_history_model->raw_query("SELECT * FROM call_history WHERE campaign_id={$campaign->id} " . " LIMIT {$offset}, {$per_page}");
        $result = $query_results->result();
        echo("Page $i\n");

        foreach ($result as $key => $value)
        {
          $call_number = $value->phone_1;

          if (!$value->phone_1 && $value->phone_2)
          {
            $call_number = $value->phone_2;
          }

          if ($value->call_result == 0)
          {
            // $url = ' https://0529e6e9.ngrok.io' . '/v1/api/voice/call/message/' . $campaign->id;
            $url = base_url() . '/v1/api/voice/call/message/' . $campaign->id;
            $sid = $this->voice_service->send($this->voice_service->add_country_code(1, $call_number), $url, 15);
            sleep(1);
            if (!$sid)
            {
              $this->call_history_model->edit([
                'call_result' => 2,
                'call_date' => date('Y-m-d')
              ], $value->id);
            }
            else
            {
              $this->call_history_model->edit([
                  'sid' => $sid,
                  'call_date' => date('Y-m-d')
                ], $value->id);
            }
          }
        }
      }
    }
}