<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once __DIR__ . '/Cronjob_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Campaign result Cronjob Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Campaign_result_cronjob_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index ()
    {
        ini_set('memory_limit','1024M');
        $this->load->database();
        $this->load->model('campaign_model');
        $this->load->model('campaign_result_model');
        $this->load->model('call_history_model');
        $this->load->model('sms_history_model');
        $per_page = 100;
        $results = $this->campaign_model->get_paginated(
        0,
        $per_page,
        [
          'status' => 3
        ]);

        foreach ($results as $campaign)
        {
          $campaign_results = $this->campaign_result_model->get_all(['campaign_id' => (int)$campaign->id]);
          $total = $campaign->total;

          if ($campaign->total == 0)
          {
            if ($campaign->type == 2 || $campaign->type == 4)
            {
              $total = $this->call_history_model->count([
                'campaign_id' => $campaign->id
              ]);
            }
            else if ($campaign->type == 3 || $campaign->type == 5)
            {
              $total = $this->sms_history_model->count([
                'campaign_id' => $campaign->id
              ]);
            }
          }

          foreach ($campaign_results as $campaign_result)
          {
            if ($campaign->type == 2 || $campaign->type == 4)
            {
              $no_answer_total = $this->call_history_model->count([
                'campaign_id' => $campaign->id,
                'call_result' => 3
              ]);
              $dne_total = $this->call_history_model->count([
                'campaign_id' => $campaign->id,
                'call_result' => 2
              ]);
              $complete_total = $this->call_history_model->count([
                'campaign_id' => $campaign->id,
                'call_result' => 1
              ]);
            }
            if ($campaign->type == 3 || $campaign->type == 5)
            {
              $no_answer_total = $this->sms_history_model->count([
                'campaign_id' => $campaign->id,
                'sms_result' => 3
              ]);
              $dne_total = $this->sms_history_model->count([
                'campaign_id' => $campaign->id,
                'sms_result' => 2
              ]);
              $complete_total = $this->sms_history_model->count([
                'campaign_id' => $campaign->id,
                'sms_result' => 1
              ]);
            }

            $this->campaign_result_model->edit([
              'reached' => $no_answer_total + $dne_total + $complete_total,
              'total' => $total,
              'engaged' => $complete_total,
              'dne' => $dne_total
            ], $campaign_result->id);
          }
          $this->campaign_model->edit([
            'status' => 4
          ],$campaign->id);
        }
        echo 'Complete Campaign Results';
    }
}