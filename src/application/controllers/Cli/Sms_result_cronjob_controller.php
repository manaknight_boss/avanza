<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Cronjob_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Sms result Cronjob Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Sms_result_cronjob_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index ()
    {
        ini_set('memory_limit','1024M');
        $this->load->database();
        $this->load->model('sms_history_model');
        $this->load->model('voters_new_model');
        $this->load->library('sms_service');
        $this->sms_service->set_adapter('sms');
        $per_page = 100;
        $results = $this->sms_history_model->get_paginated(
        0,
        $per_page,
        [
          'sms_result' => 0,
          'sid!=""'
        ]);

        foreach ($results as $sms)
        {
          $log = $this->sms_service->retrieve_single_sms_log($sms->sid);

          if ($log)
          {
            $this->sms_history_model->edit([
              'sms_result' => $this->sms_history_model->twillio_sms_mapping($log->status)
            ], $sms->id);
          }
          else
          {
            $this->sms_history_model->edit([
              'sms_result' => 2
            ], $sms->id);
          }

          $voter = $this->voters_new_model->get_by_field('government_id', $sms->government_id);

          if ($voter)
          {
            if ($sms->sms_type == 5)
            {
              $payload = [
                'has_contact_sms_poll' => 1
              ];
            }
            else
            {
              $payload = [
                'has_contact_sms' => 1
              ];
            }

            $this->voters_new_model->edit($payload, $voter->id);
          }
        }
        echo 'Complete sms Checks';
    }
}