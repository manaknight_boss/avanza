<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Cronjob_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Transform Voters with Grassroot Cronjob Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Transform_grassroot_cronjob_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index ()
    {
        ini_set('memory_limit','1024M');
        $this->load->database();
        $this->load->model('grassroot_user_model');
        $count = $this->grassroot_user_model->count([]);
        $per_page = 100;
        $num_pages = ceil($count / $per_page);
        $cordinator_list = [];
        echo "$count $per_page $num_pages\n";
        for ($i=0; $i < $num_pages; $i++)
        {
            $results = $this->grassroot_user_model->get_paginated(
                $i * $per_page,
                $per_page,
                []);
            echo "Page: $i\n";
            foreach ($results as $key => $user)
            {
                $table_1 = (string)$user->phone;
                $table_2 = (string)$user->refer;
                $table_3 = (string)$user->stripe_id;
                if (strlen($table_1) < 4)
                {
                    $table_1 = '0' . $table_1;
                }
                if (strlen($table_2) < 4)
                {
                    $table_2 = '0' . $table_2;
                }
                if (strlen($table_3) < 4)
                {
                    $table_3 = '0' . $table_3;
                }
                $this->grassroot_user_model->edit([
                    'phone' => $table_1,
                    'refer' => $table_2,
                    'stripe_id' => $table_3,
                ], $user->id);
            }
        }
        echo 'Complete Transformation';
    }
}