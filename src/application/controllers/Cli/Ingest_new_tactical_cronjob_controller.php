<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Cronjob_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Ingest New Tactical Cronjob Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Ingest_new_tactical_cronjob_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index ()
    {
        $this->load->database();
        ini_set('memory_limit','1024M');
        $this->load->model('raw_tactical_members_model');
        $file = file_get_contents('tactical1');
        $json = json_decode($file, TRUE);
        echo count($json);
        foreach ($json as $key => $row)
        {
            $phone_text = $row['contactos'];
            $phones = explode('/', $phone_text);
            $phone_1 = '';
            $phone_2 = '';
            if (count($phones) > 1)
            {
                $phone_1 = $phones[0];
                $phone_2 = $phones[1];
            }
            else
            {
                $phone_1 = $phone_text;
            }

            $payload = [
                "miembroId" => $row['miembroId'],
                "cedulaMiembro" => $row['cedulaMiembro'],
                "cordMiembroId" => $row['id'],
                "nombreMiembro" => $row['nombresMiembro'],
                "apellidoMiembro" => '',
                "estatusMiembro" => '',
                "direccion" => $row['direccion'],
                "sexoMiembro" => $row['sexoMiembro'],
                "fecha_NacimientoMiembro" => $row['fecha_NacimientoMiembro'],
                "miembro_PLD" => 0,
                "calle" => '',
                "numero" => '',
                "codigo_Sector" => $row['codigo_Sector'],
                "codigo_SubSector" => $row['codigo_SubSector'],
                "telefono_Residencial" => $phone_1,
                "telefono_Celular" => $phone_2,
                "correoMiembro" => $row['correoMiembro'],
                "facebookMiembro" => '',
                "twitterMiembro" => '',
                "instagramMiembro" => '',
                "estadoCivilMiembro" => '',
                "codigoColegioMiembro" => $row['codigoColegioMiembro'],
                "codigoRecintoMiembro" => $row['codigoRecintoMiembro'],
                "circunscripcionMiembro" => $row['circunscripcionMiembro'],
                "edadMiembro" => $row['edadMiembro'],
                "votoMiembro" => $row['votoMiembro'] == 'SI' ? 1 : 0,
                "cedulaCoodinador" => $row['cedulaCoodinador'],
                "nombreCoodinador" => $row['nombreCoodinador'],
                "apellidoCoodinador" => '',
                "recintoNombre" => $row['recintoNombre'],
                "contactResidencialCoodinador" => $row['contactoCoodinador']
            ];

            $this->raw_tactical_members_model->create($payload);
            if ($key % 50 === 0)
            {
                sleep(1);
            }
        }
        echo 'Complete Ingestion';

    }

    private function clean_phone ($phone)
    {
        if (!$phone)
        {
            return '';
        }

        $cleanPhones = trim(str_replace(['(', ')', '-', ' '], ['','','', ''], $phone));
        $cleanPhone = '';
        $parts = explode('/', $cleanPhones);
        if (count($parts) > 0)
        {
            foreach ($parts as $singlePhone) {
                $cleanSinglePhone = trim($singlePhone);
                if (strlen($cleanSinglePhone) === 10)
                {
                    return $cleanSinglePhone;
                }
            }
        }
        else
        {
            if (strlen($cleanPhones) === 10)
            {
                $cleanPhone = $cleanPhones;
            }
        }
        return $cleanPhone;
    }
}