<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Cronjob_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Transform Cronjob2 Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Transform_avanza_cronjob2_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index ()
    {
        ini_set('memory_limit','1024M');
        $this->load->database();
        $this->load->model('new_raw_voters_new_model');
        $this->load->model('raw_recintos_model');
        $this->load->model('voters_new_model');
        $recintos = $this->raw_recintos_model->get_all([]);
        $count = $this->new_raw_voters_new_model->count([]);
        $per_page = 100;
        $num_pages = ceil($count / $per_page);
        echo "$count $per_page $num_pages";
        for ($i=4245; $i < $num_pages; $i++)
        {
            $results = $this->new_raw_voters_model->get_paginated(
                $i * $per_page,
                $per_page,
                []);
            echo "Page: $i\n";
            foreach ($results as $key => $voter)
            {
                $recintoFound = $this->find_recinto_info($recintos, $voter->CodigoRecinto);
                $payload = [
                    'avanza_member_id' => 0,
                    'tactical_member_id' => 0,
                    'government_id' => $voter->Cedula,
                    'first_name' => $voter->Nombres,
                    'last_name' => $voter->PApellido . ' ' . $voter->SApellido,
                    'married_status' => $voter->EstadoCivil,
                    'gender' => $voter->Sexo,
                    'date_of_birth' => str_replace(' 00:00:00', '', $voter->Nacimiento),
                    'place_of_birth' => $voter->LugarNacimiento,
                    'member_pld' => $voter->simpatia_pld == null ? 0 : 1,
                    'circunscripcion' => $voter->Circunscripcion,
                    'age' => $this->calculate_age($voter->Nacimiento),
                    'address' => '',
                    'address_number' => '',
                    'recinto' => strlen($recintoFound['recinto_id']) > 0 ? $recintoFound['recinto_id'] : $voter->CodigoRecinto,
                    'recinto_name' => $recintoFound['recinto_name'],
                    'recinto_address' => $recintoFound['recinto_address'],
                    'colegio' => $voter->CodigoColegio,
                    'sector' => $voter->CodigoSector,
                    'sector_name' => $recintoFound['sector_name'],
                    'voting_sub_sector_code' => '',
                    'phone_1' => $this->clean_phone($voter->Telefono),
                    'phone_type_1' => 'u',
                    'phone_2' => $this->clean_phone($voter->tel_varios2),
                    'phone_type_2' => 'u',
                    'email' => '',
                    'facebook' => '',
                    'twitter' => '',
                    'instagram' => '',
                    'vote_in_primary' => 0,
                    'cordinator_id' => 0,
                    'is_cordinator' => 0,
                    'occupation_id' => $voter->IDOcupacion,
                    'government_sponsor' => $voter->solidaridad !== null ? 1 : 0,
                    'is_vote_external' => $voter->votaExterior !== null ? 1 : 0,
                    'parent_voting_table' => $voter->Mesa,
                    'voter_turn' => $voter->PosPagina,
                    'has_voted_municipal_2020' => 0,
                    'has_voted_general_2020' => 0,
                    'has_contact_call' => 0,
                    'has_contact_call_poll' => 0,
                    'has_contact_sms' => 0,
                    'has_contact_sms_poll' => 0,
                    'has_contact_email' => 0
                ];
                // error_log(print_r($payload, TRUE));
                $this->voters_new_model->create($payload);
                $payload = [];
                $recintoFound = null;
            }
        }
        echo 'Complete Transformation';
    }

    private function find_recinto_info ($data, $recinto)
    {
        $response = [
            'recinto_id' => '',
            'recinto_name' => '',
            'recinto_address' => '',
            'sector' => '',
            'sector_name' => '',
        ];

        if (is_numeric($recinto))
        {
            $intRecinto = (int)$recinto;
            foreach ($data as $row)
            {
                if ($intRecinto == $row->RecintoId)
                {
                    $response['recinto_id'] = $intRecinto;
                    $response['recinto_name'] = $row->Recinto;
                    $response['recinto_address'] = $row->Direccion;
                    $response['sector'] = $row->sector_jce;
                    $response['sector_name'] = $row->Sector;
                    return $response;
                }
            }
        }
        return $response;
    }

    private function clean_phone ($phone)
    {
        $cleanPhones = trim(str_replace(['(', ')', '-'], ['','',''], $phone));
        $cleanPhone = '';
        $parts = explode(',', $cleanPhones);
        if (count($parts) > 0)
        {
            foreach ($parts as $singlePhone) {
                $cleanSinglePhone = trim($singlePhone);
                if (strlen($cleanSinglePhone) === 10)
                {
                    return $cleanSinglePhone;
                }
            }
        }
        else
        {
            if (strlen($cleanPhones) === 10)
            {
                $cleanPhone = $cleanPhones;
            }
        }
        return $cleanPhone;
    }

    private function calculate_age ($date)
    {
        $cleanDate = str_replace(' 00:00:00', '', $date);
        $cleanTime = strtotime($cleanDate);
        $nowTime = strtotime("now");
        if ($cleanTime < 0) {
            $diffTime = $nowTime + abs($cleanTime);
        } else {
            $diffTime = $nowTime - $cleanTime;
        }
        return round($diffTime / 60 / 60 / 24 / 365, 0);
    }
}