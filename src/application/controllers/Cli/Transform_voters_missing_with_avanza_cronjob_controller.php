<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Cronjob_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Transform Voters Missing with Avanza Cronjob Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Transform_voters_missing_with_avanza_cronjob_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index ()
    {
        ini_set('memory_limit','1024M');
        $this->load->database();
        $this->load->model('raw_avanza_members_model');
        $this->load->model('voters_new_model');
        $this->load->model('raw_recintos_model');
        $recintos = $this->raw_recintos_model->get_all([]);
        $count = $this->raw_avanza_members_model->count([]);
        $per_page = 100;
        $num_pages = ceil($count / $per_page);
        echo "$count $per_page $num_pages\n";
        for ($i=0; $i < $num_pages; $i++)
        {
            $results = $this->raw_avanza_members_model->get_paginated(
                $i * $per_page,
                $per_page,
                []);
            echo "Page: $i\n";
            foreach ($results as $key => $voter)
            {
                $real_voter_found = $this->voters_new_model->get_by_field('government_id', $voter->cedulaMiembro);
                if (!$real_voter_found)
                {
                    $recintoFound = $this->find_recinto_info($recintos, (int)$voter->codigoRecintoMiembro);
                    $payload = [
                        'avanza_member_id' => $voter->miembroId,
                        'government_id' => $voter->cedulaMiembro,
                        'first_name' => $voter->nombreMiembro,
                        'last_name' => $voter->apellidoMiembro,
                        'married_status' => $voter->estadoCivilMiembro,
                        'gender' => $voter->sexoMiembro,
                        'date_of_birth' => str_replace('T00:00:00', '', $voter->fecha_NacimientoMiembro),
                        'place_of_birth' => '',
                        'member_pld' => $voter->miembro_PLD,
                        'circunscripcion' => $voter->circunscripcionMiembro,
                        'age' => $this->calculate_age($voter->fecha_NacimientoMiembro),
                        'address' => $voter->calle,
                        'address_number' => $voter->numero,
                        'recinto' => (int)$voter->codigoRecintoMiembro,
                        'recinto_name' => $recintoFound['recinto_name'],
                        'recinto_address' => $recintoFound['recinto_address'],
                        'colegio' => $voter->codigoColegioMiembro,
                        'sector' => $voter->codigo_Sector,
                        'sector_name' => $recintoFound['sector_name'],
                        'voting_sub_sector_code' => $voter->codigo_SubSector,
                        'phone_1' => $this->clean_phone($voter->telefono_Residencial),
                        'phone_type_1' => 0,
                        'phone_2' => $this->clean_phone($voter->telefono_Celular),
                        'phone_type_2' => 0,
                        'email' => $voter->correoMiembro,
                        'facebook' => $voter->facebookMiembro,
                        'twitter' => $voter->twitterMiembro,
                        'instagram' => $voter->instagramMiembro,
                        'vote_in_primary' => $voter->votoMiembro,
                        'cordinator_id' => 0,
                        'is_cordinator' => 0,
                        'occupation_id' => 0,
                        'government_sponsor' => 0,
                        'is_vote_external' => 0,
                        'parent_voting_table' => 0,
                        'voter_turn' => 0,
                        'has_voted_municipal_2020' => 0,
                        'has_voted_general_2020' => 0,
                        'has_contact_call' => 0,
                        'has_contact_call_poll' => 0,
                        'has_contact_sms' => 0,
                        'has_contact_sms_poll' => 0,
                        'has_contact_email' => 0
                    ];

                    if (strlen($payload['phone_1']) > 0)
                    {
                        $payload['phone_type_1'] = 1;
                    }
                    if (strlen($payload['phone_2']) > 0)
                    {
                        $payload['phone_type_2'] = 1;
                    }

                    error_log(print_r($payload, TRUE));
                    // $this->voters_new_model->create($payload);
                    $payload = [];
                    $real_voter_found = null;
                }
            }
        }
        echo 'Complete Transformation';
    }

    private function find_recinto_info ($data, $recinto)
    {
        $response = [
            'recinto_id' => '',
            'recinto_name' => '',
            'recinto_address' => '',
            'sector' => '',
            'sector_name' => '',
        ];

        if (is_numeric($recinto))
        {
            $intRecinto = (int)$recinto;
            foreach ($data as $row)
            {
                if ($intRecinto == $row->RecintoId)
                {
                    $response['recinto_id'] = $intRecinto;
                    $response['recinto_name'] = $row->Recinto;
                    $response['recinto_address'] = $row->Direccion;
                    $response['sector'] = $row->sector_jce;
                    $response['sector_name'] = $row->Sector;
                    return $response;
                }
            }
        }
        return $response;
    }

    private function clean_phone ($phone)
    {
        if (!$phone)
        {
            return '';
        }

        $cleanPhones = trim(str_replace(['(', ')', '-'], ['','',''], $phone));
        $cleanPhone = '';
        $parts = explode(',', $cleanPhones);
        if (count($parts) > 0)
        {
            foreach ($parts as $singlePhone) {
                $cleanSinglePhone = trim($singlePhone);
                if (strlen($cleanSinglePhone) === 10)
                {
                    return $cleanSinglePhone;
                }
            }
        }
        else
        {
            if (strlen($cleanPhones) === 10)
            {
                $cleanPhone = $cleanPhones;
            }
        }
        return $cleanPhone;
    }

    private function calculate_age ($date)
    {
        $cleanDate = str_replace(' 00:00:00', '', $date);
        $cleanTime = strtotime($cleanDate);
        $nowTime = strtotime("now");
        if ($cleanTime < 0) {
            $diffTime = $nowTime + abs($cleanTime);
        } else {
            $diffTime = $nowTime - $cleanTime;
        }
        return round($diffTime / 60 / 60 / 24 / 365, 0);
    }
}