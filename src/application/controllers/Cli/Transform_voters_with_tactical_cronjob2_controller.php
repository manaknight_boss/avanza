<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Cronjob_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Transform Voters with Avanza Cronjob2 Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Transform_voters_with_tactical_cronjob2_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index ()
    {
        ini_set('memory_limit','1024M');
        $this->load->database();
        $this->load->model('raw_tactical_members_model');
        $this->load->model('voters_new_model');
        $count = $this->raw_tactical_members_model->count([]);
        $per_page = 100;
        $num_pages = ceil($count / $per_page);
        $cordinator_list = [];
        echo "$count $per_page $num_pages\n";
        for ($i=0; $i < $num_pages; $i++)
        {
            $results = $this->raw_tactical_members_model->get_paginated(
                $i * $per_page,
                $per_page,
                []);
            echo "Page: $i\n";
            foreach ($results as $key => $voter)
            {
                $real_voter_found = $this->voters_new_model->get_by_field('government_id', $voter->cedulaMiembro);
                if ($real_voter_found)
                {
                    $payload = [
                        'tactical_member_id' => $voter->miembroId,
                        'vote_in_primary' => $voter->votoMiembro,
                        'voting_sub_sector_code' => $voter->codigo_SubSector,
                    ];
                    $phone_1 = $this->clean_phone($voter->telefono_Residencial);
                    $phone_2 = $this->clean_phone($voter->telefono_Celular);

                    if (strlen($voter->correoMiembro) > 0 && $voter->correoMiembro !='no definido / no definido')
                    {
                        $payload['email'] = $voter->correoMiembro;
                    }

                    if (strlen($phone_1) > 0)
                    {
                        $payload['phone_1'] = $phone_1;
                        $payload['phone_type_1'] = 1;
                    }

                    if (strlen($phone_2) > 0)
                    {
                        $payload['phone_2'] = $phone_2;
                        $payload['phone_type_2'] = 2;
                    }

                    $cordinator_found = $this->voters_new_model->get_by_field('government_id', $voter->cedulaCoodinador);

                    if (in_array($real_voter_found->id, $cordinator_list))
                    {
                        // echo 'Cordinator found ' . $real_voter_found->government_id . "\n";
                        $payload['is_tactical_cordinator'] = 1;
                    }

                    if ($cordinator_found)
                    {
                        $payload['tactical_cordinator_id'] = $cordinator_found->id;

                        if (!in_array($cordinator_found->id, $cordinator_list))
                        {
                            $cordinator_list[] = $cordinator_found->id;
                        }
                    }
                    if (is_numeric($voter->cedulaCoodinador))
                    {
                        $this->voters_new_model->edit($payload, $real_voter_found->id);
                    }
                    $payload = [];
                    $cordinator_found = null;
                    $real_voter_found = null;
                }
            }
        }
        echo 'Complete Transformation';
    }

    private function clean_phone ($phone)
    {
        if (!$phone)
        {
            return '';
        }

        $cleanPhones = trim(str_replace(['(', ')', '-'], ['','',''], $phone));
        $cleanPhone = '';
        $parts = explode(',', $cleanPhones);
        if (count($parts) > 0)
        {
            foreach ($parts as $singlePhone) {
                $cleanSinglePhone = trim($singlePhone);
                if (strlen($cleanSinglePhone) === 10)
                {
                    return $cleanSinglePhone;
                }
            }
        }
        else
        {
            if (strlen($cleanPhones) === 10)
            {
                $cleanPhone = $cleanPhones;
            }
        }
        return $cleanPhone;
    }

    private function calculate_age ($date)
    {
        $cleanDate = str_replace(' 00:00:00', '', $date);
        $cleanTime = strtotime($cleanDate);
        $nowTime = strtotime("now");
        if ($cleanTime < 0) {
            $diffTime = $nowTime + abs($cleanTime);
        } else {
            $diffTime = $nowTime - $cleanTime;
        }
        return round($diffTime / 60 / 60 / 24 / 365, 0);
    }
}