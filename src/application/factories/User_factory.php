<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * User Factory
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class User_factory
{
    /**
     * Create User
     *
     * @param mixed $user_model
     * @param string $email
     * @param string $password
     * @param integer $role
     * @param string $type
     * @return mixed
     */
    public function create($user_model, $email, $password, $role, $type='n')
    {
        $user_id = $user_model->create([
            'email' => $email,
            'username' => $email,
            'first_name' => '',
            'last_name' => '',
            'role_id' => $role,
			'refer' => uniqid(),
			'status' => 1,
			'verify' => 0,
			'profile_id' => 0,
			'type' => $type,
			'stripe_id' => '',
            'image' => 'https://i.imgur.com/AzJ7DRw.png',
			'image_id' => 1,
            'password' => str_replace('$2y$', '$2b$', password_hash($password, PASSWORD_BCRYPT))
        ]);

        return $user_id;
    }

    public function create_full_user($user_model, $email, $password, $first_name, $last_name, $role, $type='n')
    {
        $user_id = $user_model->create([
            'email' => $email,
            'username' => $email,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'phone' => '',
            'role_id' => $role,
			'refer' => uniqid(),
			'status' => 1,
            'verify' => 1,
            'profile_id' => 0,
			'type' => $type,
			'stripe_id' => '',
            'image' => 'https://i.imgur.com/AzJ7DRw.png',
            'image_id' => 1,
            'password' => str_replace('$2y$', '$2b$', password_hash($password, PASSWORD_BCRYPT))
        ]);

        return $user_id;
    }
}