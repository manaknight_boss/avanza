<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Call_history_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Call_history_model extends Manaknight_Model
{
	protected $_table = 'call_history';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'government_id',
		'phone_1',
		'phone_2',
		'sid',
		'call_date',
		'campaign_id',
		'call_result',
		'call_type',
		'poll_result',
		
    ];
	protected $_label_fields = [
    'ID','Cédula','Tel. 1','Tel. 2','Sid','Fecha llamada','Id campaña','Res. llamada','Tipo llamada','Res. Encuesta',
    ];
	protected $_use_timestamps = TRUE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['government_id', 'Cédula', 'required|string'],
		['phone_1', 'Tel. 1', 'required|max_length[255]'],
		['phone_2', 'Tel. 2', 'required|max_length[255]'],
		['sid', 'Sid', 'required|max_length[255]'],
		['call_date', 'Fecha llamada', 'required|date'],
		['campaign_id', 'Id campaña', 'required|integer'],
		['call_result', 'Res. llamada', 'required|integer'],
		['call_type', 'Tipo llamada', 'required|integer'],
		['poll_result', 'Res. Encuesta', 'required|integer'],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['government_id', 'Cédula', 'required|string'],
		['phone_1', 'Tel. 1', 'required|max_length[255]'],
		['phone_2', 'Tel. 2', 'required|max_length[255]'],
		['sid', 'Sid', 'required|max_length[255]'],
		['call_date', 'Fecha llamada', 'required|date'],
		['campaign_id', 'Id campaña', 'required|integer'],
		['call_result', 'Res. llamada', 'required|integer'],
		['call_type', 'Tipo llamada', 'required|integer'],
		['poll_result', 'Res. Encuesta', 'required|integer'],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        
        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }


	public function poll_result_mapping ()
	{
		return [
			0 => 'Nueva',
			5 => 'No respondió',
			4 => 'No existe',
			1 => 'Opcion 1',
			2 => 'Opcion 2',
			3 => 'Opcion 3',
		];
	}

	public function call_result_mapping ()
	{
		return [
			0 => 'Nueva',
			3 => 'No respondió',
			2 => 'No existe',
			1 => 'Respondió',
		];
	}

	public function call_type_mapping ()
	{
		return [
			2 => 'Voz',
			4 => 'Encuesta',
			5 => 'Custom',
		];
	}

	public function twillio_call_mapping($status)
	{
		$mapping = [
			'queued' => 0,
			'ringing' => 0,
			'canceled' => 3,
			'completed' => 1,
			'busy' => 1,
			'failed' => 3,
			'in-progress' => 0,
			'no-answer' => 3,
		];
	
			$keys = array_keys($mapping);
	
			if (!in_array($status, $keys))
			{
					return 0;
			}
	
			return $mapping[$status];
	}


}