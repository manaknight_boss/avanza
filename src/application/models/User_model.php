<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * User_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class User_model extends Manaknight_Model
{
	protected $_table = 'user';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'email',
		'password',
		'type',
		'first_name',
		'last_name',
		'phone',
		'image',
		'image_id',
		'refer',
		'profile_id',
		'verify',
		'role_id',
		'stripe_id',
		'status',
		
    ];
	protected $_label_fields = [
    'ID','Correo','Contraseña','Tipo','Nombre','Apellido','# Telefono','Imagen','Imagen ID','Codigo Referente','Colegio #','Verificado','Papel','ID de Stripe','Estado',
    ];
	protected $_use_timestamps = TRUE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['email', 'Correo', 'trim|required|valid_email|is_unique[user.email]'],
		['password', 'Contraseña', 'required'],
		['type', 'Tipo', ''],
		['first_name', 'Nombre', 'required'],
		['last_name', 'Apellido', 'required'],
		['phone', '# Telefono', 'required'],
		['image', 'Imagen', ''],
		['image_id', 'Imagen ID', ''],
		['refer', 'Codigo Referente', ''],
		['profile_id', 'Colegio #', ''],
		['verify', 'Verificado', ''],
		['role_id', 'Papel', 'required|in_list[1,2,3]'],
		['stripe_id', 'ID de Stripe', ''],
		['status', 'Estado', ''],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['email', 'Correo', 'trim|required|valid_email'],
		['password', 'Contraseña', ''],
		['type', 'Tipo', ''],
		['first_name', 'Nombre', 'required'],
		['last_name', 'Apellido', 'required'],
		['phone', '# Telefono', 'required'],
		['image', 'Imagen', ''],
		['image_id', 'Imagen ID', ''],
		['refer', 'Codigo Referente', ''],
		['profile_id', 'Colegio #', ''],
		['verify', 'Verificado', ''],
		['role_id', 'Papel', 'required|in_list[1,2,3]'],
		['stripe_id', 'ID de Stripe', ''],
		['status', 'Estado', 'required|in_list[0,1,2]'],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        $data['image'] = 'https://i.imgur.com/AzJ7DRw.png';
		$data['refer'] = uniqid();
		$data['status'] = 1;
		$data['verify'] = 0;
		$data['stripe_id'] = '';

		if(!isset($data['profile_id']))
		{
			$data['profile_id'] = 0;
		}

		if(!isset($data['type']))
		{
			$data['type'] = 'n';
		}
		if (strpos($data['phone'], '1') != 0)
		{
			$data['phone'] = '1' + $data['phone'];
		}

        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        if(isset($data['password']) && strlen($data['password']) < 1)
		{
			unset($data['password']);
		}

		if(isset($data['image']) && strlen($data['image']) < 1)
		{
			unset($data['image']);
		}

        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }


	public function verify_mapping ()
	{
		return [
			0 => 'Sin verificar',
			1 => 'Verificado',
		];
	}

	public function status_mapping ()
	{
		return [
			0 => 'Inactivo',
			1 => 'Activo',
			2 => 'Suspendido',
		];
	}

	public function role_id_mapping ()
	{
		return [
			1 => 'Grassroot',
			2 => 'Admin',
		];
	}

	public function autocomplete_email($email)
	{
		$this->db->like('email', $email);
		$query = $this->db->get($this->_table);
		$result = [];
		foreach ($query->result() as $row)
		{
			$result[] = [
				'id' => $row->id,
				'email' => $row->email
			];
		}
		return $result;
	}


}