<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Final_count_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Final_count_model extends Manaknight_Model
{
	protected $_table = 'final_count';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'table',
		'file',
		'file_id',
		'mayor_1',
		'mayor_count_1',
		'mayor_2',
		'mayor_count_2',
		'mayor_3',
		'mayor_count_3',
		'mayor_4',
		'mayor_count_4',
		'mayor_5',
		'mayor_count_5',
		'mayor_6',
		'mayor_count_6',
		'mayor_7',
		'mayor_count_7',
		'mayor_8',
		'mayor_count_8',
		'mayor_9',
		'mayor_count_9',
		'mayor_10',
		'mayor_count_10',
		'senator_1',
		'senator_count_1',
		'senator_2',
		'senator_count_2',
		'senator_3',
		'senator_count_3',
		'senator_4',
		'senator_count_4',
		'senator_5',
		'senator_count_5',
		'senator_6',
		'senator_count_6',
		'senator_7',
		'senator_count_7',
		'senator_8',
		'senator_count_8',
		'senator_9',
		'senator_count_9',
		'senator_10',
		'senator_count_10',
		'regional_1',
		'regional_count_1',
		'regional_2',
		'regional_count_2',
		'regional_3',
		'regional_count_3',
		'regional_4',
		'regional_count_4',
		'regional_5',
		'regional_count_5',
		'regional_6',
		'regional_count_6',
		'regional_7',
		'regional_count_7',
		'regional_8',
		'regional_count_8',
		'regional_9',
		'regional_count_9',
		'regional_10',
		'regional_count_10',
		'regional_11',
		'regional_count_11',
		'regional_12',
		'regional_count_12',
		'regional_13',
		'regional_count_13',
		'regional_14',
		'regional_count_14',
		'regional_15',
		'regional_count_15',
		
    ];
	protected $_label_fields = [
    'ID','Colegio','Imagen','Imagen ID','Alcalde 1','Votos Alcalde 1','Alcalde 2','Votos Alcalde 2','Alcalde 3','Votos Alcalde 3','Alcalde 4','Votos Alcalde 4','Alcalde 5','Votos Alcalde 5','Alcalde 6','Votos Alcalde 6','Alcalde7 ','Votos Alcalde 7','Alcalde8 ','Votos Alcalde 8','Alcalde 9','Votos Alcalde 9','Alcalde 10','Votos Alcalde 10',' Senador1','Votos Senador1',' Senador2','Votos Senador2',' Senador3','Votos Senador3',' Senador4','Votos Senador4',' Senador5','Votos Senador5',' Senador6','Votos Senador6',' Senador7','Votos Senador7',' Senador8','Votos Senador8',' Senador9','Votos Senador9',' Senador10','Votos Senador10','Regidor1','Votos regidor 1','Regidor2','Votos regidor 2','Regidor3','Votos regidor 3','Regidor4','Votos regidor 4','Regidor5','Votos regidor 5','Regidor6','Votos regidor 6','Regidor7','Votos regidor 7','Regidor8','Votos regidor 8','Regidor9','Votos regidor 9','Regidor10','Votos regidor 10','Regidor11','Votos regidor 11','Regidor12','Votos regidor 12','Regidor13','Votos regidor 13','Regidor14','Votos regidor 14','Regidor15','Votos regidor 15',
    ];
	protected $_use_timestamps = TRUE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['table', 'Colegio', 'required|is_unique[final_count.table]'],
		['file', 'Imagen', ''],
		['file_id', 'Imagen ID', ''],
		['mayor_1', 'Alcalde 1', ''],
		['mayor_count_1', 'Votos Alcalde 1', ''],
		['mayor_2', 'Alcalde 2', ''],
		['mayor_count_2', 'Votos Alcalde 2', ''],
		['mayor_3', 'Alcalde 3', ''],
		['mayor_count_3', 'Votos Alcalde 3', ''],
		['mayor_4', 'Alcalde 4', ''],
		['mayor_count_4', 'Votos Alcalde 4', ''],
		['mayor_5', 'Alcalde 5', ''],
		['mayor_count_5', 'Votos Alcalde 5', ''],
		['mayor_6', 'Alcalde 6', ''],
		['mayor_count_6', 'Votos Alcalde 6', ''],
		['mayor_7', 'Alcalde7 ', ''],
		['mayor_count_7', 'Votos Alcalde 7', ''],
		['mayor_8', 'Alcalde8 ', ''],
		['mayor_count_8', 'Votos Alcalde 8', ''],
		['mayor_9', 'Alcalde 9', ''],
		['mayor_count_9', 'Votos Alcalde 9', ''],
		['mayor_10', 'Alcalde 10', ''],
		['mayor_count_10', 'Votos Alcalde 10', ''],
		['senator_1', ' Senador1', ''],
		['senator_count_1', 'Votos Senador1', ''],
		['senator_2', ' Senador2', ''],
		['senator_count_2', 'Votos Senador2', ''],
		['senator_3', ' Senador3', ''],
		['senator_count_3', 'Votos Senador3', ''],
		['senator_4', ' Senador4', ''],
		['senator_count_4', 'Votos Senador4', ''],
		['senator_5', ' Senador5', ''],
		['senator_count_5', 'Votos Senador5', ''],
		['senator_6', ' Senador6', ''],
		['senator_count_6', 'Votos Senador6', ''],
		['senator_7', ' Senador7', ''],
		['senator_count_7', 'Votos Senador7', ''],
		['senator_8', ' Senador8', ''],
		['senator_count_8', 'Votos Senador8', ''],
		['senator_9', ' Senador9', ''],
		['senator_count_9', 'Votos Senador9', ''],
		['senator_10', ' Senador10', ''],
		['senator_count_10', 'Votos Senador10', ''],
		['regional_1', 'Regidor1', ''],
		['regional_count_1', 'Votos regidor 1', ''],
		['regional_2', 'Regidor2', ''],
		['regional_count_2', 'Votos regidor 2', ''],
		['regional_3', 'Regidor3', ''],
		['regional_count_3', 'Votos regidor 3', ''],
		['regional_4', 'Regidor4', ''],
		['regional_count_4', 'Votos regidor 4', ''],
		['regional_5', 'Regidor5', ''],
		['regional_count_5', 'Votos regidor 5', ''],
		['regional_6', 'Regidor6', ''],
		['regional_count_6', 'Votos regidor 6', ''],
		['regional_7', 'Regidor7', ''],
		['regional_count_7', 'Votos regidor 7', ''],
		['regional_8', 'Regidor8', ''],
		['regional_count_8', 'Votos regidor 8', ''],
		['regional_9', 'Regidor9', ''],
		['regional_count_9', 'Votos regidor 9', ''],
		['regional_10', 'Regidor10', ''],
		['regional_count_10', 'Votos regidor 10', ''],
		['regional_11', 'Regidor11', ''],
		['regional_count_11', 'Votos regidor 11', ''],
		['regional_12', 'Regidor12', ''],
		['regional_count_12', 'Votos regidor 12', ''],
		['regional_13', 'Regidor13', ''],
		['regional_count_13', 'Votos regidor 13', ''],
		['regional_14', 'Regidor14', ''],
		['regional_count_14', 'Votos regidor 14', ''],
		['regional_15', 'Regidor15', ''],
		['regional_count_15', 'Votos regidor 15', ''],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['table', 'Colegio', 'required'],
		['file', 'Imagen', ''],
		['file_id', 'Imagen ID', ''],
		['mayor_1', 'Alcalde 1', ''],
		['mayor_count_1', 'Votos Alcalde 1', ''],
		['mayor_2', 'Alcalde 2', ''],
		['mayor_count_2', 'Votos Alcalde 2', ''],
		['mayor_3', 'Alcalde 3', ''],
		['mayor_count_3', 'Votos Alcalde 3', ''],
		['mayor_4', 'Alcalde 4', ''],
		['mayor_count_4', 'Votos Alcalde 4', ''],
		['mayor_5', 'Alcalde 5', ''],
		['mayor_count_5', 'Votos Alcalde 5', ''],
		['mayor_6', 'Alcalde 6', ''],
		['mayor_count_6', 'Votos Alcalde 6', ''],
		['mayor_7', 'Alcalde7 ', ''],
		['mayor_count_7', 'Votos Alcalde 7', ''],
		['mayor_8', 'Alcalde8 ', ''],
		['mayor_count_8', 'Votos Alcalde 8', ''],
		['mayor_9', 'Alcalde 9', ''],
		['mayor_count_9', 'Votos Alcalde 9', ''],
		['mayor_10', 'Alcalde 10', ''],
		['mayor_count_10', 'Votos Alcalde 10', ''],
		['senator_1', ' Senador1', ''],
		['senator_count_1', 'Votos Senador1', ''],
		['senator_2', ' Senador2', ''],
		['senator_count_2', 'Votos Senador2', ''],
		['senator_3', ' Senador3', ''],
		['senator_count_3', 'Votos Senador3', ''],
		['senator_4', ' Senador4', ''],
		['senator_count_4', 'Votos Senador4', ''],
		['senator_5', ' Senador5', ''],
		['senator_count_5', 'Votos Senador5', ''],
		['senator_6', ' Senador6', ''],
		['senator_count_6', 'Votos Senador6', ''],
		['senator_7', ' Senador7', ''],
		['senator_count_7', 'Votos Senador7', ''],
		['senator_8', ' Senador8', ''],
		['senator_count_8', 'Votos Senador8', ''],
		['senator_9', ' Senador9', ''],
		['senator_count_9', 'Votos Senador9', ''],
		['senator_10', ' Senador10', ''],
		['senator_count_10', 'Votos Senador10', ''],
		['regional_1', 'Regidor1', ''],
		['regional_count_1', 'Votos regidor 1', ''],
		['regional_2', 'Regidor2', ''],
		['regional_count_2', 'Votos regidor 2', ''],
		['regional_3', 'Regidor3', ''],
		['regional_count_3', 'Votos regidor 3', ''],
		['regional_4', 'Regidor4', ''],
		['regional_count_4', 'Votos regidor 4', ''],
		['regional_5', 'Regidor5', ''],
		['regional_count_5', 'Votos regidor 5', ''],
		['regional_6', 'Regidor6', ''],
		['regional_count_6', 'Votos regidor 6', ''],
		['regional_7', 'Regidor7', ''],
		['regional_count_7', 'Votos regidor 7', ''],
		['regional_8', 'Regidor8', ''],
		['regional_count_8', 'Votos regidor 8', ''],
		['regional_9', 'Regidor9', ''],
		['regional_count_9', 'Votos regidor 9', ''],
		['regional_10', 'Regidor10', ''],
		['regional_count_10', 'Votos regidor 10', ''],
		['regional_11', 'Regidor11', ''],
		['regional_count_11', 'Votos regidor 11', ''],
		['regional_12', 'Regidor12', ''],
		['regional_count_12', 'Votos regidor 12', ''],
		['regional_13', 'Regidor13', ''],
		['regional_count_13', 'Votos regidor 13', ''],
		['regional_14', 'Regidor14', ''],
		['regional_count_14', 'Votos regidor 14', ''],
		['regional_15', 'Regidor15', ''],
		['regional_count_15', 'Votos regidor 15', ''],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        $data["mayor_1"] = '';
$data['mayor_count_1'] = 0;
$data["mayor_2"] = '';
$data['mayor_count_2'] = 0;
$data["mayor_3"] = '';
$data['mayor_count_3'] = 0;
$data["mayor_4"] = '';
$data['mayor_count_4'] = 0;
$data["mayor_5"] = '';
$data['mayor_count_5'] = 0;
$data["mayor_6"] = '';
$data['mayor_count_6'] = 0;
$data["mayor_7"] = '';
$data['mayor_count_7'] = 0;
$data["mayor_8"] = '';
$data['mayor_count_8'] = 0;
$data["mayor_9"] = '';
$data['mayor_count_9'] = 0;
$data["mayor_10"] = '';
$data['mayor_count_10'] = 0;
$data["senator_1"] = '';
$data['senator_count_1'] = 0;
$data["senator_2"] = '';
$data['senator_count_2'] = 0;
$data["senator_3"] = '';
$data['senator_count_3'] = 0;
$data["senator_4"] = '';
$data['senator_count_4'] = 0;
$data["senator_5"] = '';
$data['senator_count_5'] = 0;
$data["senator_6"] = '';
$data['senator_count_6'] = 0;
$data["senator_7"] = '';
$data['senator_count_7'] = 0;
$data["senator_8"] = '';
$data['senator_count_8'] = 0;
$data["senator_9"] = '';
$data['senator_count_9'] = 0;
$data["senator_10"] = '';
$data['senator_count_10'] = 0;
$data["regional_1"] = '';
$data['regional_count_1'] = 0;
$data["regional_2"] = '';
$data['regional_count_2'] = 0;
$data["regional_3"] = '';
$data['regional_count_3'] = 0;
$data["regional_4"] = '';
$data['regional_count_4'] = 0;
$data["regional_5"] = '';
$data['regional_count_5'] = 0;
$data["regional_6"] = '';
$data['regional_count_6'] = 0;
$data["regional_7"] = '';
$data['regional_count_7'] = 0;
$data["regional_8"] = '';
$data['regional_count_8'] = 0;
$data["regional_9"] = '';
$data['regional_count_9'] = 0;
$data["regional_10"] = '';
$data['regional_count_10'] = 0;
$data["regional_11"] = '';
$data['regional_count_11'] = 0;
$data["regional_12"] = '';
$data['regional_count_12'] = 0;
$data["regional_13"] = '';
$data['regional_count_13'] = 0;
$data["regional_14"] = '';
$data['regional_count_14'] = 0;
$data["regional_15"] = '';
$data['regional_count_15'] = 0;

        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }




}