<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Refer_log_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Refer_log_model extends Manaknight_Model
{
	protected $_table = 'refer_log';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'user_id',
		'referrer_user_id',
		'type',
		'status',
		
    ];
	protected $_label_fields = [
    'ID','Usuario Referente','Usuario Referente','Tipo','Estado',
    ];
	protected $_use_timestamps = TRUE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['user_id', 'Usuario Referente', 'required|integer'],
		['referrer_user_id', 'Usuario Referente', 'required|integer'],
		['type', 'Tipo', 'required|integer'],
		['status', 'Estado', 'required|integer'],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['user_id', 'Usuario Referente', ''],
		['referrer_user_id', 'Usuario Referente', ''],
		['type', 'Tipo', 'required|integer'],
		['status', 'Estado', ''],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        $data['status'] = 0;

        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }


	public function status_mapping ()
	{
		return [
			0 => 'pendiente',
			1 => 'confirmado',
			2 => 'pago',
		];
	}

	public function type_mapping ()
	{
		return [
			0 => 'user',
		];
	}


	public function get_user ($where)
	{
		return $this->_join ('user', 'user_id', $where);
	}

	public function get_user_paginated ($page, $limit, $where)
	{
		return $this->_join_paginate ('user', 'user_id', $where, $page, $limit);
	}

	public function get_referrer ($where)
	{
		return $this->_join ('referrer', 'referrer_user_id', $where);
	}

	public function get_referrer_paginated ($page, $limit, $where)
	{
		return $this->_join_paginate ('referrer', 'referrer_user_id', $where, $page, $limit);
	}


}