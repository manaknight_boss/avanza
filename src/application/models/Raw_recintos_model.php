<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Raw_recintos_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Raw_recintos_model extends Manaknight_Model
{
	protected $_table = 'raw_recintos';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'RecintoId',
		'BloqueID',
		'ID_bloque',
		'Circunscripcion',
		'Recinto',
		'Direccion',
		'Sector',
		'Barrioid',
		'SubBarrioid',
		'sector_jce',
		'Enlace',
		'latitud',
		'longitud',
		'fecharegistro',
		'fechamodificacion',
		'estado_id',
		'usuarioregistro',
		'identificador',
		'verificado',
		'actualizado_jce_2020',
		
    ];
	protected $_label_fields = [
    'ID','ID Recinto','ID Bloque','ID bloque','Circunscripcion','Recinto','Direccion','Sector','ID Barrio','ID Sub Barrio','Sector en jce','Enlace','latitud','longitud','fecharegistro','fechamodificacion','estado_id','usuarioregistro','identificador','verificado','actualizado_jce_2020',
    ];
	protected $_use_timestamps = FALSE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['RecintoId', 'ID Recinto', ''],
		['BloqueID', 'ID Bloque', ''],
		['ID_bloque', 'ID bloque', ''],
		['Circunscripcion', 'Circunscripcion', ''],
		['Recinto', 'Recinto', 'required'],
		['Direccion', 'Direccion', 'required'],
		['Sector', 'Sector', 'required'],
		['Barrioid', 'ID Barrio', ''],
		['SubBarrioid', 'ID Sub Barrio', ''],
		['sector_jce', 'Sector en jce', ''],
		['Enlace', 'Enlace', ''],
		['latitud', 'latitud', 'required'],
		['longitud', 'longitud', 'required'],
		['fecharegistro', 'fecharegistro', 'required'],
		['fechamodificacion', 'fechamodificacion', 'required'],
		['estado_id', 'estado_id', ''],
		['usuarioregistro', 'usuarioregistro', 'required'],
		['identificador', 'identificador', 'required'],
		['verificado', 'verificado', ''],
		['actualizado_jce_2020', 'actualizado_jce_2020', 'required'],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['RecintoId', 'ID Recinto', ''],
		['BloqueID', 'ID Bloque', ''],
		['ID_bloque', 'ID bloque', ''],
		['Circunscripcion', 'Circunscripcion', ''],
		['Recinto', 'Recinto', 'required'],
		['Direccion', 'Direccion', 'required'],
		['Sector', 'Sector', 'required'],
		['Barrioid', 'ID Barrio', ''],
		['SubBarrioid', 'ID Sub Barrio', ''],
		['sector_jce', 'Sector en jce', ''],
		['Enlace', 'Enlace', ''],
		['latitud', 'latitud', 'required'],
		['longitud', 'longitud', 'required'],
		['fecharegistro', 'fecharegistro', 'required'],
		['fechamodificacion', 'fechamodificacion', 'required'],
		['estado_id', 'estado_id', ''],
		['usuarioregistro', 'usuarioregistro', 'required'],
		['identificador', 'identificador', 'required'],
		['verificado', 'verificado', ''],
		['actualizado_jce_2020', 'actualizado_jce_2020', 'required'],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        
        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }




}