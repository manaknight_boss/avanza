<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Custom_call_list_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Custom_call_list_model extends Manaknight_Model
{
	protected $_table = 'custom_call_list';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'name',
		'type',
		'voice_file',
		'voice_file_id',
		'file',
		'file_id',
		
    ];
	protected $_label_fields = [
    'Nombre','Tipo','Archivo','ID Archivo','CSV','CSV ID',
    ];
	protected $_use_timestamps = TRUE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['name', 'Nombre', 'required|max_length[255]'],
		['type', 'Tipo', ''],
		['voice_file', 'Archivo', 'required'],
		['voice_file_id', 'ID Archivo', ''],
		['file', 'CSV', 'required'],
		['file_id', 'CSV ID', ''],
		
    ];
	protected $_validation_edit_rules = [
    ['name', 'Nombre', 'required|max_length[255]'],
		['type', 'Tipo', ''],
		['voice_file', 'Archivo', 'required'],
		['voice_file_id', 'ID Archivo', ''],
		['file', 'CSV', 'required'],
		['file_id', 'CSV ID', ''],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        
        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }


	public function type_mapping ()
	{
		return [
			1 => 'Voice',
		];
	}



}