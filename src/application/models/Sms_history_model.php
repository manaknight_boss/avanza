<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Sms_history_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Sms_history_model extends Manaknight_Model
{
	protected $_table = 'sms_history';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'government_id',
		'phone_1',
		'phone_2',
		'sms_date',
		'campaign_id',
		'sms_result',
		'sms_type',
		'poll_result',
		'sid',
		'number',
		
    ];
	protected $_label_fields = [
    'ID','Cédula','Tel. 1','Tel. 2','Fecha SMS','Id Campaña','Res. SMS','Tipo SMS','Res. Encuesta','Sid','number',
    ];
	protected $_use_timestamps = TRUE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['government_id', 'Cédula', 'required|integer'],
		['phone_1', 'Tel. 1', 'required|max_length[255]'],
		['phone_2', 'Tel. 2', 'required|max_length[255]'],
		['sms_date', 'Fecha SMS', 'required|date'],
		['campaign_id', 'Id Campaña', 'required|integer'],
		['sms_result', 'Res. SMS', 'required|integer'],
		['sms_type', 'Tipo SMS', 'required|integer'],
		['poll_result', 'Res. Encuesta', 'required'],
		['sid', 'Sid', 'required|max_length[255]'],
		['number', 'number', ''],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['government_id', 'Cédula', 'required|integer'],
		['phone_1', 'Tel. 1', 'required|max_length[255]'],
		['phone_2', 'Tel. 2', 'required|max_length[255]'],
		['sms_date', 'Fecha SMS', 'required|date'],
		['campaign_id', 'Id Campaña', 'required|integer'],
		['sms_result', 'Res. SMS', 'required|integer'],
		['sms_type', 'Tipo SMS', 'required|integer'],
		['poll_result', 'Res. Encuesta', 'required'],
		['sid', 'Sid', 'required|max_length[255]'],
		['number', 'number', ''],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        $data['number'] = rand(1,2);
        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }


	public function poll_result_mapping ()
	{
		return [
			0 => 'No respondió',
			4 => 'Numero no existe',
			1 => 'Opcion 1',
			2 => 'Opcion 2',
			3 => 'Opcion 3',
		];
	}

	public function sms_result_mapping ()
	{
		return [
			0 => 'Nueva',
			3 => 'No respondió',
			2 => 'No existe',
			1 => 'Respondió',
		];
	}

	public function sms_type_mapping ()
	{
		return [
			3 => 'SMS',
			5 => 'Encuesta',
		];
	}

	public function twillio_sms_mapping($status)
	{
		$mapping = [
			'accepted' => 0,
			'queued' => 0,
			'sending' => 0,
			'sent' => 1,
			'receiving' => 0,
			'received' => 0,
			'delivered' => 1,
			'undelivered' => 2,
			'failed' => 2,
			'read' => 1
		];
	
			$keys = array_keys($mapping);
	
				if (!in_array($status, $keys))
				{
					return 0;
				}
	
		return $mapping[$status];
		}


}