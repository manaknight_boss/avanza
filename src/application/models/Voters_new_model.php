<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Voters_new_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Voters_new_model extends Manaknight_Model
{
	protected $_table = 'voters_new';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'avanza_member_id',
		'tactical_member_id',
		'government_id',
		'first_name',
		'last_name',
		'married_status',
		'gender',
		'date_of_birth',
		'place_of_birth',
		'member_pld',
		'circunscripcion',
		'age',
		'address',
		'address_number',
		'recinto',
		'recinto_name',
		'recinto_address',
		'colegio',
		'sector',
		'sector_name',
		'voting_sub_sector_code',
		'phone_1',
		'phone_type_1',
		'phone_2',
		'phone_type_2',
		'email',
		'facebook',
		'twitter',
		'instagram',
		'vote_in_primary',
		'cordinator_id',
		'is_cordinator',
		'tactical_cordinator_id',
		'is_tactical_cordinator',
		'occupation_id',
		'government_sponsor',
		'is_vote_external',
		'parent_voting_table',
		'voter_turn',
		'has_voted_municipal_2020',
		'has_voted_general_2020',
		'has_contact_call',
		'has_contact_call_poll',
		'has_contact_sms',
		'has_contact_sms_poll',
		'has_contact_email',
		
    ];
	protected $_label_fields = [
    'ID','ID Avanza','ID Tactical','Cédula','Nombre','Apellidos','Estado Civil','Genero','Fecha nacimiento','Lugar Nacimiento','Miembro PLD','Circ.','Edad','Direccion','Numero','recinto','Nombre del Recinto','Direccion Recinto','colegio','sector','Nombre de sector','codigo sub sector','Telefono 1','Tipo telefono 1','Telefono 2','Tipo telefono 2','Correo','facebook','twitter','instagram','Voto en primarias','Id coordinador','Es coordinador','Id coordinador','Es coordinador','ID ocupacion','Aistencia de gobierno','Vota en el exterior','Mesa madre','Turno Votacion','Votó en Municipales2020','Votó en Generales2020','Recibió llamado','Recibió encuesta','Recibió SMS','Recibió encuesta SMS','Recibió correo',
    ];
	protected $_use_timestamps = FALSE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['avanza_member_id', 'ID Avanza', 'required|integer'],
		['tactical_member_id', 'ID Tactical', 'required|integer'],
		['government_id', 'Cédula', 'required|max_length[255]'],
		['first_name', 'Nombre', 'required|max_length[255]'],
		['last_name', 'Apellidos', 'required|max_length[255]'],
		['married_status', 'Estado Civil', 'required|max_length[1]'],
		['gender', 'Genero', 'required|max_length[1]'],
		['date_of_birth', 'Fecha nacimiento', 'required|date'],
		['place_of_birth', 'Lugar Nacimiento', 'required|max_length[255]'],
		['member_pld', 'Miembro PLD', 'required|integer'],
		['circunscripcion', 'Circ.', 'required|integer'],
		['age', 'Edad', 'required|integer'],
		['address', 'Direccion', 'required|max_length[255]'],
		['address_number', 'Numero', 'required|integer'],
		['recinto', 'recinto', 'required|max_length[255]'],
		['recinto_name', 'Nombre del Recinto', 'required|max_length[255]'],
		['recinto_address', 'Direccion Recinto', 'required|max_length[255]'],
		['colegio', 'colegio', 'required|max_length[255]'],
		['sector', 'sector', 'required|max_length[255]'],
		['sector_name', 'Nombre de sector', 'required|max_length[255]'],
		['voting_sub_sector_code', 'codigo sub sector', 'required|max_length[255]'],
		['phone_1', 'Telefono 1', 'required|max_length[255]'],
		['phone_type_1', 'Tipo telefono 1', 'required|integer'],
		['phone_2', 'Telefono 2', 'required|max_length[255]'],
		['phone_type_2', 'Tipo telefono 2', 'required|integer'],
		['email', 'Correo', 'required|max_length[255]'],
		['facebook', 'facebook', 'required|max_length[255]'],
		['twitter', 'twitter', 'required|max_length[255]'],
		['instagram', 'instagram', 'required|max_length[255]'],
		['vote_in_primary', 'Voto en primarias', 'required|integer'],
		['cordinator_id', 'Id coordinador', 'required|integer'],
		['is_cordinator', 'Es coordinador', 'required|integer'],
		['tactical_cordinator_id', 'Id coordinador', 'required|integer'],
		['is_tactical_cordinator', 'Es coordinador', 'required|integer'],
		['occupation_id', 'ID ocupacion', 'required|integer'],
		['government_sponsor', 'Aistencia de gobierno', 'required|integer'],
		['is_vote_external', 'Vota en el exterior', 'required|integer'],
		['parent_voting_table', 'Mesa madre', 'required|max_length[255]'],
		['voter_turn', 'Turno Votacion', 'required|max_length[255]'],
		['has_voted_municipal_2020', 'Votó en Municipales2020', 'required|integer'],
		['has_voted_general_2020', 'Votó en Generales2020', 'required|integer'],
		['has_contact_call', 'Recibió llamado', 'required|integer'],
		['has_contact_call_poll', 'Recibió encuesta', 'required|integer'],
		['has_contact_sms', 'Recibió SMS', 'required|integer'],
		['has_contact_sms_poll', 'Recibió encuesta SMS', 'required|integer'],
		['has_contact_email', 'Recibió correo', 'required|integer'],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['avanza_member_id', 'ID Avanza', 'required|integer'],
		['tactical_member_id', 'ID Tactical', 'required|integer'],
		['government_id', 'Cédula', 'required|max_length[255]'],
		['first_name', 'Nombre', 'required|max_length[255]'],
		['last_name', 'Apellidos', 'required|max_length[255]'],
		['married_status', 'Estado Civil', 'required|max_length[1]'],
		['gender', 'Genero', 'required|max_length[1]'],
		['date_of_birth', 'Fecha nacimiento', 'required|date'],
		['place_of_birth', 'Lugar Nacimiento', 'required|max_length[255]'],
		['member_pld', 'Miembro PLD', 'required|integer'],
		['circunscripcion', 'Circ.', 'required|integer'],
		['age', 'Edad', 'required|integer'],
		['address', 'Direccion', 'required|max_length[255]'],
		['address_number', 'Numero', 'required|integer'],
		['recinto', 'recinto', 'required|max_length[255]'],
		['recinto_name', 'Nombre del Recinto', 'required|max_length[255]'],
		['recinto_address', 'Direccion Recinto', 'required|max_length[255]'],
		['colegio', 'colegio', 'required|max_length[255]'],
		['sector', 'sector', 'required|max_length[255]'],
		['sector_name', 'Nombre de sector', 'required|max_length[255]'],
		['voting_sub_sector_code', 'codigo sub sector', 'required|max_length[255]'],
		['phone_1', 'Telefono 1', 'required|max_length[255]'],
		['phone_type_1', 'Tipo telefono 1', 'required|integer'],
		['phone_2', 'Telefono 2', 'required|max_length[255]'],
		['phone_type_2', 'Tipo telefono 2', 'required|integer'],
		['email', 'Correo', 'required|max_length[255]'],
		['facebook', 'facebook', 'required|max_length[255]'],
		['twitter', 'twitter', 'required|max_length[255]'],
		['instagram', 'instagram', 'required|max_length[255]'],
		['vote_in_primary', 'Voto en primarias', 'required|integer'],
		['cordinator_id', 'Id coordinador', 'required|integer'],
		['is_cordinator', 'Es coordinador', 'required|integer'],
		['tactical_cordinator_id', 'Id coordinador', 'required|integer'],
		['is_tactical_cordinator', 'Es coordinador', 'required|integer'],
		['occupation_id', 'ID ocupacion', 'required|integer'],
		['government_sponsor', 'Aistencia de gobierno', 'required|integer'],
		['is_vote_external', 'Vota en el exterior', 'required|integer'],
		['parent_voting_table', 'Mesa madre', 'required|max_length[255]'],
		['voter_turn', 'Turno Votacion', 'required|max_length[255]'],
		['has_voted_municipal_2020', 'Votó en Municipales2020', 'required|integer'],
		['has_voted_general_2020', 'Votó en Generales2020', 'required|integer'],
		['has_contact_call', 'Recibió llamado', 'required|integer'],
		['has_contact_call_poll', 'Recibió encuesta', 'required|integer'],
		['has_contact_sms', 'Recibió SMS', 'required|integer'],
		['has_contact_sms_poll', 'Recibió encuesta SMS', 'required|integer'],
		['has_contact_email', 'Recibió correo', 'required|integer'],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        
        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }


	public function member_pld_mapping ()
	{
		return [
			0 => 'No',
			1 => 'Si',
		];
	}

	public function is_vote_external_mapping ()
	{
		return [
			0 => 'No',
			1 => 'Si',
		];
	}

	public function is_cordinator_mapping ()
	{
		return [
			0 => 'No',
			1 => 'Si',
		];
	}

	public function gender_mapping ()
	{
		return [
			0 => 'desconocido',
			2 => 'F',
			1 => 'M',
		];
	}

	public function married_status_mapping ()
	{
		return [
			'S' => 'S',
			'M' => 'C',
		];
	}

	public function phone_type_1_mapping ()
	{
		return [
			0 => 'Desconocido',
			1 => 'Residencial',
			2 => 'Mobil',
		];
	}

	public function phone_type_2_mapping ()
	{
		return [
			0 => 'Desconocido',
			1 => 'Residencial',
			2 => 'Mobil',
		];
	}

	public function age_mapping ()
	{
		return [
			1 => [0, 10],
			2 => [11, 20],
			3 => [21, 30],
			4 => [31, 40],
			5 => [41, 50],
			6 => [51, 60],
			7 => [61, 70],
			8 => [71, 80],
			9 => [81, 90],
			10 => [91, 100],
			11 => [101, 1000]
		];
	}


}