<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Campaign_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Campaign_model extends Manaknight_Model
{
	protected $_table = 'campaign';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'name',
		'type',
		'status',
		'circunscripcion',
		'age',
		'recinto',
		'source_id',
		'gender',
		'married_status',
		'college',
		'content',
		'current_page',
		'total',
		'count_query',
		'query',
		
    ];
	protected $_label_fields = [
    'ID','Nombre','Tipo','Estado','Circ.','Edad','Recinto','Fuente','Genero','Estado Civil','Colegio','Contenido','Pagina actual','Total','Total en lista','Busqueda',
    ];
	protected $_use_timestamps = TRUE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['name', 'Nombre', 'required|max_length[255]'],
		['type', 'Tipo', 'required|integer'],
		['status', 'Estado', ''],
		['circunscripcion', 'Circ.', ''],
		['age', 'Edad', 'required|integer'],
		['recinto', 'Recinto', ''],
		['source_id', 'Fuente', ''],
		['gender', 'Genero', ''],
		['married_status', 'Estado Civil', ''],
		['college', 'Colegio', ''],
		['content', 'Contenido', 'required'],
		['current_page', 'Pagina actual', ''],
		['total', 'Total', ''],
		['count_query', 'Total en lista', ''],
		['query', 'Busqueda', ''],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['name', 'Nombre', 'required|max_length[255]'],
		['type', 'Tipo', ''],
		['status', 'Estado', 'required|integer'],
		['circunscripcion', 'Circ.', ''],
		['age', 'Edad', ''],
		['recinto', 'Recinto', ''],
		['source_id', 'Fuente', ''],
		['gender', 'Genero', ''],
		['married_status', 'Estado Civil', ''],
		['college', 'Colegio', ''],
		['content', 'Contenido', 'required'],
		['current_page', 'Pagina actual', ''],
		['total', 'Total', ''],
		['count_query', 'Total en lista', ''],
		['query', 'Busqueda', ''],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        $data['status'] = 0;
				$data['current_page'] = 0;
				$data['total'] = 0;
				$data['count_query'] = '';
				$data['query'] = '';

        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }


	public function source_id_mapping ()
	{
		return [
			0 => 'Todos',
			1 => 'Avanza',
			2 => 'Padron',
			3 => 'Tactica',
		];
	}

	public function type_mapping ()
	{
		return [
			0 => 'Todos',
			2 => 'Voz',
			3 => 'SMS',
			4 => 'Encuesta Voz',
			5 => 'Encuesta SMS',
		];
	}

	public function status_mapping ()
	{
		return [
			0 => 'Inactivo',
			1 => 'En progreso',
			2 => 'En turno',
			3 => 'Completa',
			4 => 'Finalizada',
		];
	}

	public function circunscripcion_mapping ()
	{
		return [
			0 => 'Todos',
			1 => 'Circ. 1',
			2 => 'Circ. 2',
			3 => 'Circ. 3',
		];
	}

	public function gender_mapping ()
	{
		return [
			0 => 'Todos',
			'F' => 'F',
			'M' => 'M',
		];
	}

	public function married_status_mapping ()
	{
		return [
			0 => 'Todos',
			'S' => 'S',
			'M' => 'C',
		];
	}

	public function age_mapping ()
	{
		return [
			0 => 'Todos',
			1 => '0 - 10',
			2 => '11 - 20',
			3 => '21 - 30',
			4 => '31 - 40',
			5 => '41 - 50',
			6 => '51 - 60',
			7 => '61 - 70',
			8 => '71 - 80',
			9 => '81 - 90',
			10 => '91 - 100',
			11 => '100+',
		];
	}



}