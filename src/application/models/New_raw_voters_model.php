<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * New_raw_voters_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class New_raw_voters_model extends Manaknight_Model
{
	protected $_table = 'new_raw_voters';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'Cedula',
		'CedulaFMT',
		'Nombres',
		'PApellido',
		'SApellido',
		'Nacimiento',
		'Sexo',
		'IDOcupacion',
		'EstadoCivil',
		'CodigoColegio',
		'CodigoRecinto',
		'Circunscripcion',
		'CodigoSector',
		'LugarNacimiento',
		'voto10',
		'voto12',
		'voto16',
		'sucidiado_salud',
		'solidaridad',
		'simpatia_pld',
		'registrado',
		'relac_pld',
		'votaExterior',
		'nuevoElector2020',
		'Voto',
		'Fecha_voto',
		'Mesa',
		'PosPagina',
		'IdNacionalidad',
		'Telefono',
		'tel_varios2',
		
    ];
	protected $_label_fields = [
    'ID','Cédula','CedulaFMT','Nombres','PApellido','SApellido','Nacimiento','Sexo','IDOcupacion','Estado Civil','Codigo Colegio','Codigo Recinto','Circunscripcion','Codigo Sector','Lugar Nacimiento','voto10','voto12','voto16','sucidiado_salud','solidaridad','simpatia_pld','registrado','relac_pld','votaExterior','nuevoElector2020','Voto','Fecha_voto','Mesa','Pos. Votante','ID nacionalidad','Telefono','Tel. Variado',
    ];
	protected $_use_timestamps = FALSE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['Cedula', 'Cédula', 'required'],
		['CedulaFMT', 'CedulaFMT', 'required'],
		['Nombres', 'Nombres', 'required'],
		['PApellido', 'PApellido', 'required'],
		['SApellido', 'SApellido', 'required'],
		['Nacimiento', 'Nacimiento', 'required'],
		['Sexo', 'Sexo', 'required'],
		['IDOcupacion', 'IDOcupacion', ''],
		['EstadoCivil', 'Estado Civil', 'required'],
		['CodigoColegio', 'Codigo Colegio', 'required'],
		['CodigoRecinto', 'Codigo Recinto', ''],
		['Circunscripcion', 'Circunscripcion', ''],
		['CodigoSector', 'Codigo Sector', ''],
		['LugarNacimiento', 'Lugar Nacimiento', 'required'],
		['voto10', 'voto10', 'required'],
		['voto12', 'voto12', 'required'],
		['voto16', 'voto16', 'required'],
		['sucidiado_salud', 'sucidiado_salud', 'required'],
		['solidaridad', 'solidaridad', 'required'],
		['simpatia_pld', 'simpatia_pld', 'required'],
		['registrado', 'registrado', 'required'],
		['relac_pld', 'relac_pld', 'required'],
		['votaExterior', 'votaExterior', 'required'],
		['nuevoElector2020', 'nuevoElector2020', ''],
		['Voto', 'Voto', ''],
		['Fecha_voto', 'Fecha_voto', ''],
		['Mesa', 'Mesa', 'required'],
		['PosPagina', 'Pos. Votante', ''],
		['IdNacionalidad', 'ID nacionalidad', ''],
		['Telefono', 'Telefono', 'required'],
		['tel_varios2', 'Tel. Variado', 'required'],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['Cedula', 'Cédula', 'required'],
		['CedulaFMT', 'CedulaFMT', 'required'],
		['Nombres', 'Nombres', 'required'],
		['PApellido', 'PApellido', 'required'],
		['SApellido', 'SApellido', 'required'],
		['Nacimiento', 'Nacimiento', 'required'],
		['Sexo', 'Sexo', 'required'],
		['IDOcupacion', 'IDOcupacion', ''],
		['EstadoCivil', 'Estado Civil', 'required'],
		['CodigoColegio', 'Codigo Colegio', 'required'],
		['CodigoRecinto', 'Codigo Recinto', ''],
		['Circunscripcion', 'Circunscripcion', ''],
		['CodigoSector', 'Codigo Sector', ''],
		['LugarNacimiento', 'Lugar Nacimiento', 'required'],
		['voto10', 'voto10', 'required'],
		['voto12', 'voto12', 'required'],
		['voto16', 'voto16', 'required'],
		['sucidiado_salud', 'sucidiado_salud', 'required'],
		['solidaridad', 'solidaridad', 'required'],
		['simpatia_pld', 'simpatia_pld', 'required'],
		['registrado', 'registrado', 'required'],
		['relac_pld', 'relac_pld', 'required'],
		['votaExterior', 'votaExterior', 'required'],
		['nuevoElector2020', 'nuevoElector2020', ''],
		['Voto', 'Voto', ''],
		['Fecha_voto', 'Fecha_voto', ''],
		['Mesa', 'Mesa', 'required'],
		['PosPagina', 'Pos. Votante', ''],
		['IdNacionalidad', 'ID nacionalidad', ''],
		['Telefono', 'Telefono', 'required'],
		['tel_varios2', 'Tel. Variado', 'required'],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        
        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }




}