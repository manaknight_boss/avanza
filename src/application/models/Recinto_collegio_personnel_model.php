<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Recinto_collegio_personnel_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Recinto_collegio_personnel_model extends Manaknight_Model
{
	protected $_table = 'recinto_collegio_personnel';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'recinto',
		'collegio',
		'staff_lead_name',
		'staff_lead_government_id',
		'transmitter_name',
		'transmitter_government_id',
		'delegate_1_name',
		'delegate_1_government_id',
		'delegate_2_name',
		'delegate_2_government_id',
		'runner_1_name',
		'runner_1_government_id',
		'runner_2_name',
		'runner_2_government_id',
		'staff_lead_present',
		'transmitter_present',
		'delegate_1_present',
		'delegate_2_present',
		'runner_1_present',
		'runner_2_present',
		
    ];
	protected $_label_fields = [
    'ID','Recinto','Colegio','Facilitador de Colegio','Cedula','Transmisor','Cedula','Delegado','Cedula','Facilitador Res.','Cedula','Operativo','Cedula','Transporte','Cedula','Encargado Presente','Transmisor Presente','Delegado 1 Presente','Delegado 2 Presente','Operativo 1 Presente','Operativo 2 Presente',
    ];
	protected $_use_timestamps = TRUE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['recinto', 'Recinto', ''],
		['collegio', 'Colegio', ''],
		['staff_lead_name', 'Facilitador de Colegio', 'required'],
		['staff_lead_government_id', 'Cedula', 'required'],
		['transmitter_name', 'Transmisor', 'required'],
		['transmitter_government_id', 'Cedula', 'required'],
		['delegate_1_name', 'Delegado', 'required'],
		['delegate_1_government_id', 'Cedula', 'required'],
		['delegate_2_name', 'Facilitador Res.', 'required'],
		['delegate_2_government_id', 'Cedula', 'required'],
		['runner_1_name', 'Operativo', 'required'],
		['runner_1_government_id', 'Cedula', 'required'],
		['runner_2_name', 'Transporte', 'required'],
		['runner_2_government_id', 'Cedula', 'required'],
		['staff_lead_present', 'Encargado Presente', 'required'],
		['transmitter_present', 'Transmisor Presente', 'required'],
		['delegate_1_present', 'Delegado 1 Presente', 'required'],
		['delegate_2_present', 'Delegado 2 Presente', 'required'],
		['runner_1_present', 'Operativo 1 Presente', 'required'],
		['runner_2_present', 'Operativo 2 Presente', 'required'],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['recinto', 'Recinto', ''],
		['collegio', 'Colegio', ''],
		['staff_lead_name', 'Facilitador de Colegio', 'required'],
		['staff_lead_government_id', 'Cedula', 'required'],
		['transmitter_name', 'Transmisor', 'required'],
		['transmitter_government_id', 'Cedula', 'required'],
		['delegate_1_name', 'Delegado', 'required'],
		['delegate_1_government_id', 'Cedula', 'required'],
		['delegate_2_name', 'Facilitador Res.', 'required'],
		['delegate_2_government_id', 'Cedula', 'required'],
		['runner_1_name', 'Operativo', 'required'],
		['runner_1_government_id', 'Cedula', 'required'],
		['runner_2_name', 'Transporte', 'required'],
		['runner_2_government_id', 'Cedula', 'required'],
		['staff_lead_present', 'Encargado Presente', 'required'],
		['transmitter_present', 'Transmisor Presente', 'required'],
		['delegate_1_present', 'Delegado 1 Presente', 'required'],
		['delegate_2_present', 'Delegado 2 Presente', 'required'],
		['runner_1_present', 'Operativo 1 Presente', 'required'],
		['runner_2_present', 'Operativo 2 Presente', 'required'],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        $data['staff_lead_name'] = '';$data['staff_lead_government_id'] = '';$data['transmitter_name'] = '';$data['transmitter_government_id'] = '';$data['delegate_1_name'] = '';$data['delegate_1_government_id'] = '';$data['delegate_2_name'] = '';$data['delegate_2_government_id'] = '';$data['runner_1_name'] = '';$data['runner_1_government_id'] = '';$data['runner_2_name'] = '';$data['runner_1_government_id'] = '';
        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }


	public function staff_lead_present_mapping ()
	{
		return [
			0 => 'No',
			1 => 'Si',
		];
	}

	public function transmitter_present_mapping ()
	{
		return [
			0 => 'No',
			1 => 'Si',
		];
	}

	public function delegate_1_present_mapping ()
	{
		return [
			0 => 'No',
			1 => 'Si',
		];
	}

	public function delegate_2_present_mapping ()
	{
		return [
			0 => 'No',
			1 => 'Si',
		];
	}

	public function runner_1_present_mapping ()
	{
		return [
			0 => 'No',
			1 => 'Si',
		];
	}

	public function runner_2_present_mapping ()
	{
		return [
			0 => 'No',
			1 => 'Si',
		];
	}



}