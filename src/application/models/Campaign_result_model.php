<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Campaign_result_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Campaign_result_model extends Manaknight_Model
{
	protected $_table = 'campaign_result';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'campaign_id',
		'type',
		'status',
		'circunscripcion',
		'age',
		'sector',
		'college',
		'gender',
		'martial_status',
		'total',
		'respond_1',
		'respond_2',
		'respond_3',
		'engaged',
		'reached',
		'dne',
		
    ];
	protected $_label_fields = [
    'ID','Id Campaña','Tipo','Estado','Circ.','Edad','Sector','Colegio','Genero','E. Civil','Total','Resp 1','Resp 2','Resp 3','Conectado','Contactado','No Existe',
    ];
	protected $_use_timestamps = TRUE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['campaign_id', 'Id Campaña', 'required'],
		['type', 'Tipo', 'required|integer'],
		['status', 'Estado', 'required|integer'],
		['circunscripcion', 'Circ.', 'required|integer'],
		['age', 'Edad', 'required|integer'],
		['sector', 'Sector', 'required|integer'],
		['college', 'Colegio', 'required|integer'],
		['gender', 'Genero', 'required|integer'],
		['martial_status', 'E. Civil', 'required|integer'],
		['total', 'Total', 'required|integer'],
		['respond_1', 'Resp 1', 'required|integer'],
		['respond_2', 'Resp 2', 'required|integer'],
		['respond_3', 'Resp 3', 'required|integer'],
		['engaged', 'Conectado', 'required|integer'],
		['reached', 'Contactado', 'required|integer'],
		['dne', 'No Existe', 'required|integer'],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['campaign_id', 'Id Campaña', 'required'],
		['type', 'Tipo', 'required|integer'],
		['status', 'Estado', 'required|integer'],
		['circunscripcion', 'Circ.', 'required|integer'],
		['age', 'Edad', 'required|integer'],
		['sector', 'Sector', 'required|integer'],
		['college', 'Colegio', 'required|integer'],
		['gender', 'Genero', 'required|integer'],
		['martial_status', 'E. Civil', 'required|integer'],
		['total', 'Total', 'required|integer'],
		['respond_1', 'Resp 1', 'required|integer'],
		['respond_2', 'Resp 2', 'required|integer'],
		['respond_3', 'Resp 3', 'required|integer'],
		['engaged', 'Conectado', 'required|integer'],
		['reached', 'Contactado', 'required|integer'],
		['dne', 'No Existe', 'required|integer'],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        
        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }


	public function type_mapping ()
	{
		return [
			0 => 'Todos',
			1 => 'Correo',
			2 => 'Voz',
			3 => 'SMS',
			4 => 'Encuesta voz',
			5 => 'Encuesta SMS',
			6 => 'Custom Call',
		];
	}

	public function status_mapping ()
	{
		return [
			0 => 'Inactivo',
			1 => 'Activo',
			2 => 'En Progreso',
			3 => 'En turno',
		];
	}

	public function gender_mapping ()
	{
		return [
			0 => 'Desconocido',
			2 => 'F',
			1 => 'M',
		];
	}

	public function martial_status_mapping ()
	{
		return [
			0 => 'S',
			1 => 'C',
		];
	}

	public function circunscripcion_mapping ()
	{
		return [
			0 => 'Todas',
			1 => 'Circ. 1',
			2 => 'Circ. 2',
			3 => 'Circ. 3',
		];
	}

	public function age_mapping ()
	{
		return [
			0 => 'Todos',
			1 => '0 - 10',
			2 => '11 - 20',
			3 => '21 - 30',
			4 => '31 - 40',
			5 => '41 - 50',
			6 => '51 - 60',
			7 => '61 - 70',
			8 => '71 - 80',
			9 => '81 - 90',
			10 => '91 - 100',
			11 => '100+',
		];
	}



}