<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Raw_tactical_members_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Raw_tactical_members_model extends Manaknight_Model
{
	protected $_table = 'raw_tactical_members';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'miembroId',
		'cedulaMiembro',
		'cordMiembroId',
		'nombreMiembro',
		'apellidoMiembro',
		'estatusMiembro',
		'sexoMiembro',
		'fecha_NacimientoMiembro',
		'miembro_PLD',
		'calle',
		'numero',
		'codigo_Sector',
		'codigo_SubSector',
		'telefono_Residencial',
		'telefono_Celular',
		'correoMiembro',
		'facebookMiembro',
		'twitterMiembro',
		'instagramMiembro',
		'estadoCivilMiembro',
		'codigoColegioMiembro',
		'codigoRecintoMiembro',
		'circunscripcionMiembro',
		'edadMiembro',
		'votoMiembro',
		'cedulaCoodinador',
		'nombreCoodinador',
		'apellidoCoodinador',
		'contactResidencialCoodinador',
		
    ];
	protected $_label_fields = [
    'ID','ID Miembro','Cédula','ID coordinador','Nombres','Apellidos','Estado','Género','Fecha Nacimiento','Miembro del PLD','calle','numero','Codigo sector','Codigo SubSector','Tel. Residencial','Tel. Celular','Correo','Fbook','Twitter','Instagram','Estado civil','Codigo Colegio','Codigo Recinto','circunscripcion','Edad','Voto','Cédula Coodinador','nombre Coodinador','apellido Coodinador','Contacto Res. Coodinador',
    ];
	protected $_use_timestamps = FALSE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['miembroId', 'ID Miembro', ''],
		['cedulaMiembro', 'Cédula', 'required'],
		['cordMiembroId', 'ID coordinador', 'required'],
		['nombreMiembro', 'Nombres', 'required'],
		['apellidoMiembro', 'Apellidos', 'required'],
		['estatusMiembro', 'Estado', 'required'],
		['sexoMiembro', 'Género', 'required'],
		['fecha_NacimientoMiembro', 'Fecha Nacimiento', 'required'],
		['miembro_PLD', 'Miembro del PLD', 'required'],
		['calle', 'calle', 'required'],
		['numero', 'numero', 'required'],
		['codigo_Sector', 'Codigo sector', 'required'],
		['codigo_SubSector', 'Codigo SubSector', 'required'],
		['telefono_Residencial', 'Tel. Residencial', 'required'],
		['telefono_Celular', 'Tel. Celular', 'required'],
		['correoMiembro', 'Correo', 'required'],
		['facebookMiembro', 'Fbook', 'required'],
		['twitterMiembro', 'Twitter', 'required'],
		['instagramMiembro', 'Instagram', 'required'],
		['estadoCivilMiembro', 'Estado civil', 'required'],
		['codigoColegioMiembro', 'Codigo Colegio', 'required'],
		['codigoRecintoMiembro', 'Codigo Recinto', 'required'],
		['circunscripcionMiembro', 'circunscripcion', ''],
		['edadMiembro', 'Edad', ''],
		['votoMiembro', 'Voto', ''],
		['cedulaCoodinador', 'Cédula Coodinador', 'required'],
		['nombreCoodinador', 'nombre Coodinador', 'required'],
		['apellidoCoodinador', 'apellido Coodinador', 'required'],
		['contactResidencialCoodinador', 'Contacto Res. Coodinador', 'required'],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['miembroId', 'ID Miembro', ''],
		['cedulaMiembro', 'Cédula', 'required'],
		['cordMiembroId', 'ID coordinador', 'required'],
		['nombreMiembro', 'Nombres', 'required'],
		['apellidoMiembro', 'Apellidos', 'required'],
		['estatusMiembro', 'Estado', 'required'],
		['sexoMiembro', 'Género', 'required'],
		['fecha_NacimientoMiembro', 'Fecha Nacimiento', 'required'],
		['miembro_PLD', 'Miembro del PLD', 'required'],
		['calle', 'calle', 'required'],
		['numero', 'numero', 'required'],
		['codigo_Sector', 'Codigo sector', 'required'],
		['codigo_SubSector', 'Codigo SubSector', 'required'],
		['telefono_Residencial', 'Tel. Residencial', 'required'],
		['telefono_Celular', 'Tel. Celular', 'required'],
		['correoMiembro', 'Correo', 'required'],
		['facebookMiembro', 'Fbook', 'required'],
		['twitterMiembro', 'Twitter', 'required'],
		['instagramMiembro', 'Instagram', 'required'],
		['estadoCivilMiembro', 'Estado civil', 'required'],
		['codigoColegioMiembro', 'Codigo Colegio', 'required'],
		['codigoRecintoMiembro', 'Codigo Recinto', 'required'],
		['circunscripcionMiembro', 'circunscripcion', ''],
		['edadMiembro', 'Edad', ''],
		['votoMiembro', 'Voto', ''],
		['cedulaCoodinador', 'Cédula Coodinador', 'required'],
		['nombreCoodinador', 'nombre Coodinador', 'required'],
		['apellidoCoodinador', 'apellido Coodinador', 'required'],
		['contactResidencialCoodinador', 'Contacto Res. Coodinador', 'required'],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        
        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }




}