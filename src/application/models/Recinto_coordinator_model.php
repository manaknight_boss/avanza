<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Recinto_coordinator_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Recinto_coordinator_model extends Manaknight_Model
{
	protected $_table = 'recinto_coordinator';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'recinto',
		'recinto_name',
		'collegio',
		'coordinator_name',
		'coordinator_government_id',
		'password',
		'personel',
		'assigned',
		'present',
		
    ];
	protected $_label_fields = [
    'ID','Numero','Nombre','Colegios','Encargado','Cédula','Contraseña','Personal','Asignados','Presente',
    ];
	protected $_use_timestamps = TRUE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['recinto', 'Numero', 'required'],
		['recinto_name', 'Nombre', 'required'],
		['collegio', 'Colegios', 'required'],
		['coordinator_name', 'Encargado', 'required'],
		['coordinator_government_id', 'Cédula', 'required'],
		['password', 'Contraseña', 'required'],
		['personel', 'Personal', ''],
		['assigned', 'Asignados', ''],
		['present', 'Presente', ''],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['recinto', 'Numero', 'required'],
		['recinto_name', 'Nombre', 'required'],
		['collegio', 'Colegios', 'required'],
		['coordinator_name', 'Encargado', 'required'],
		['coordinator_government_id', 'Cédula', 'required'],
		['password', 'Contraseña', 'required'],
		['personel', 'Personal', ''],
		['assigned', 'Asignados', ''],
		['present', 'Presente', ''],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        $data['coordinator_name'] = ''; $data['coordinator_government_id'] = ''; $data['personel'] = 6;$data['password'] = 'a123456';$data['assigned'] = 0;$data['present'] = 0;
        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }


	public function assigned_mapping ()
	{
		return [
			0 => 'No',
			1 => 'Si',
		];
	}

	public function present_mapping ()
	{
		return [
			0 => 'No',
			1 => 'Si',
		];
	}



}