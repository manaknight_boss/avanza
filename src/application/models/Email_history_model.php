<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Email_history_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Email_history_model extends Manaknight_Model
{
	protected $_table = 'email_history';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'government_id',
		'email',
		'email_date',
		'campaign_id',
		'email_result',
		
    ];
	protected $_label_fields = [
    'ID','Cédula','Correo','Fecha Correo','Id Campaña','Res. Correo',
    ];
	protected $_use_timestamps = TRUE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['government_id', 'Cédula', 'required|integer'],
		['email', 'Correo', 'required|max_length[255]'],
		['email_date', 'Fecha Correo', 'required|date'],
		['campaign_id', 'Id Campaña', 'required|integer'],
		['email_result', 'Res. Correo', 'required|integer'],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['government_id', 'Cédula', 'required|integer'],
		['email', 'Correo', 'required|max_length[255]'],
		['email_date', 'Fecha Correo', 'required|date'],
		['campaign_id', 'Id Campaña', 'required|integer'],
		['email_result', 'Res. Correo', 'required|integer'],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        
        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }


	public function email_result_mapping ()
	{
		return [
			0 => 'Enviado',
			1 => 'Abierto',
			2 => 'Error',
		];
	}



}