<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Avanza 360</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" crossorigin="anonymous">
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" crossorigin="anonymous"></script>
    <!-- Our Vendor CSS -->
    <!-- Our Custom CSS -->
    	<link rel="stylesheet" href="/assets/css/style.css"/>
	<link rel="stylesheet" href="/assets/css/portal.css"/>

</head>

<body>
<div class="wrapper">
        <!-- Sidebar  -->
        <?php if (!$layout_clean_mode) { ?>
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Avanza 360</h3>
            </div>

            <ul class="list-unstyled components">
			<li><a href='/grassroot/dashboard' class='<?php echo ($page_name == 'Tablero Avanza') ? 'active': '';?>'>Tablero Avanza</a></li>
			<li><a href='/grassroot/global' class='<?php echo ($page_name == 'Tablero DN') ? 'active': '';?>'>Tablero DN</a></li>
			<li><a href='/grassroot/tactical' class='<?php echo ($page_name == 'Tablero Tactical') ? 'active': '';?>'>Tablero Tactical</a></li>
			<li><a href='/grassroot/section/1' class='<?php echo ($page_name == 'Circ. 1') ? 'active': '';?>'>Circ. 1</a></li>
			<li><a href='/grassroot/section/2' class='<?php echo ($page_name == 'Circ. 2') ? 'active': '';?>'>Circ. 2</a></li>
			<li><a href='/grassroot/section/3' class='<?php echo ($page_name == 'Circ. 3') ? 'active': '';?>'>Circ. 3</a></li>
			<li><a href='/grassroot/voters/0' class='<?php echo ($page_name == 'Electores') ? 'active': '';?>'>Electores</a></li>
			<li><a href='/grassroot/recintos/0?order_by=recinto&direction=ASC' class='<?php echo ($page_name == 'Recintos') ? 'active': '';?>'>Recintos</a></li>
			<li><a href='/grassroot/final' class='<?php echo ($page_name == 'Actas') ? 'active': '';?>'>Actas</a></li>
			<li><a href='/grassroot/users/0' class='<?php echo ($page_name == 'Usuarios') ? 'active': '';?>'>Usuarios</a></li>
			<li><a href='/grassroot/profile' class='<?php echo ($page_name == 'Perfil') ? 'active': '';?>'>Perfil</a></li>
			<li><a href='/grassroot/logout' class='<?php echo ($page_name == 'Cerrar') ? 'active': '';?>'>Cerrar</a></li>

            </ul>
            <span class="copyright"><?php echo $setting['copyright'];?></span>
            <span class="copyright"></span>
        </nav>
        <?php } ?>
        <div id="content">
            <?php if (!$layout_clean_mode) { ?>
            <nav>
                <div class="row">
                    <div class="container-fluid">
                        <button type="button" id="sidebarCollapse" class="btn btn-dark">
                            <span>☰</span>
                        </button>
                    </div>
                </div>
            </nav>
            <?php } ?>