<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Avanza 360</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" crossorigin="anonymous">
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" crossorigin="anonymous"></script>
    <!-- Our Vendor CSS -->
    <!-- Our Custom CSS -->
    	<link rel="stylesheet" href="/assets/css/style.css"/>
	<link rel="stylesheet" href="/assets/css/portal.css"/>
	<link rel="stylesheet" href="/assets/css/easy-autocomplete.min.css"/>

</head>

<body>
<div class="wrapper">
        <!-- Sidebar  -->
        <?php if (!$layout_clean_mode) { ?>
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Avanza 360</h3>
            </div>

            <ul class="list-unstyled components">
			<li><a href='/admin/dashboard' class='<?php echo ($page_name == 'Tablero') ? 'active': '';?>'>Tablero</a></li>
			<li><a href='/admin/campaigns/0?order_by=id&direction=DESC' class='<?php echo ($page_name == 'Campañas') ? 'active': '';?>'>Campañas</a></li>
			<li><a href='/admin/custom_call_list' class='<?php echo ($page_name == 'Lista de Llamadas CSV') ? 'active': '';?>'>Lista de Llamadas CSV</a></li>
			<li><a href='/admin/campaigns/add/sms' class='<?php echo ($page_name == 'Generar Campaña SMS') ? 'active': '';?>'>Generar Campaña SMS</a></li>
			<li><a href='/admin/campaigns/add/voice' class='<?php echo ($page_name == 'Generar Campaña Voz') ? 'active': '';?>'>Generar Campaña Voz</a></li>
			<li><a href='/admin/campaigns/add_custom/sms' class='<?php echo ($page_name == 'Campaña SMS Personal') ? 'active': '';?>'>Campaña SMS Personal</a></li>
			<li><a href='/admin/campaigns/add_custom/voice' class='<?php echo ($page_name == 'Campaña Voz Personal') ? 'active': '';?>'>Campaña Voz Personal</a></li>
			<li><a href='/admin/users/0' class='<?php echo ($page_name == 'Usuarios') ? 'active': '';?>'>Usuarios</a></li>
			<li><a href='/admin/profile' class='<?php echo ($page_name == 'Perfil') ? 'active': '';?>'>Perfil</a></li>
			<li><a href='/admin/logout' class='<?php echo ($page_name == 'Cerrar') ? 'active': '';?>'>Cerrar</a></li>

            </ul>
            <span class="copyright"><?php echo $setting['copyright'];?></span>
            <span class="copyright"></span>
        </nav>
        <?php } ?>
        <div id="content">
            <?php if (!$layout_clean_mode) { ?>
            <nav>
                <div class="row">
                    <div class="container-fluid">
                        <button type="button" id="sidebarCollapse" class="btn btn-dark">
                            <span>☰</span>
                        </button>
                    </div>
                </div>
            </nav>
            <?php } ?>