<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/grassroot/dashboard" class="breadcrumb-link">Tablero</a></li>
						<li class="breadcrumb-item"><a href="/grassroot/personnel/0" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">Editar</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <?php if (validation_errors()) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?= validation_errors() ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($error) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($success) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Editar <?php echo $view_model->get_heading();?></h5>
                <div class="card-body">
                <?= form_open() ?>
				<div class="form-group">
					<label for="Facilitador de Colegio">Facilitador de Colegio </label>
					<input type="text" class="form-control" id="form_staff_lead_name" name="staff_lead_name" value="<?php echo set_value('staff_lead_name', $this->_data['view_model']->get_staff_lead_name());?>"/>
				</div>
				<div class="form-group">
					<label for="Cedula">Cedula </label>
					<input type="text" class="form-control" id="form_staff_lead_government_id" name="staff_lead_government_id" value="<?php echo set_value('staff_lead_government_id', $this->_data['view_model']->get_staff_lead_government_id());?>"/>
				</div>
				<div class="form-group">
					<label for="Transmisor">Transmisor </label>
					<input type="text" class="form-control" id="form_transmitter_name" name="transmitter_name" value="<?php echo set_value('transmitter_name', $this->_data['view_model']->get_transmitter_name());?>"/>
				</div>
				<div class="form-group">
					<label for="Cedula">Cedula </label>
					<input type="text" class="form-control" id="form_transmitter_government_id" name="transmitter_government_id" value="<?php echo set_value('transmitter_government_id', $this->_data['view_model']->get_transmitter_government_id());?>"/>
				</div>
				<div class="form-group">
					<label for="Delegado">Delegado </label>
					<input type="text" class="form-control" id="form_delegate_1_name" name="delegate_1_name" value="<?php echo set_value('delegate_1_name', $this->_data['view_model']->get_delegate_1_name());?>"/>
				</div>
				<div class="form-group">
					<label for="Cedula">Cedula </label>
					<input type="text" class="form-control" id="form_delegate_1_government_id" name="delegate_1_government_id" value="<?php echo set_value('delegate_1_government_id', $this->_data['view_model']->get_delegate_1_government_id());?>"/>
				</div>
				<div class="form-group">
					<label for="Facilitador Res.">Facilitador Res. </label>
					<input type="text" class="form-control" id="form_delegate_2_name" name="delegate_2_name" value="<?php echo set_value('delegate_2_name', $this->_data['view_model']->get_delegate_2_name());?>"/>
				</div>
				<div class="form-group">
					<label for="Cedula">Cedula </label>
					<input type="text" class="form-control" id="form_delegate_2_government_id" name="delegate_2_government_id" value="<?php echo set_value('delegate_2_government_id', $this->_data['view_model']->get_delegate_2_government_id());?>"/>
				</div>
				<div class="form-group">
					<label for="Operativo">Operativo </label>
					<input type="text" class="form-control" id="form_runner_1_name" name="runner_1_name" value="<?php echo set_value('runner_1_name', $this->_data['view_model']->get_runner_1_name());?>"/>
				</div>
				<div class="form-group">
					<label for="Cedula">Cedula </label>
					<input type="text" class="form-control" id="form_runner_1_government_id" name="runner_1_government_id" value="<?php echo set_value('runner_1_government_id', $this->_data['view_model']->get_runner_1_government_id());?>"/>
				</div>
				<div class="form-group">
					<label for="Transporte">Transporte </label>
					<input type="text" class="form-control" id="form_runner_2_name" name="runner_2_name" value="<?php echo set_value('runner_2_name', $this->_data['view_model']->get_runner_2_name());?>"/>
				</div>
				<div class="form-group">
					<label for="Cedula">Cedula </label>
					<input type="text" class="form-control" id="form_runner_2_government_id" name="runner_2_government_id" value="<?php echo set_value('runner_2_government_id', $this->_data['view_model']->get_runner_2_government_id());?>"/>
				</div>
				<div class="form-group">
					<label for="Encargado Presente">Encargado Presente </label>
					<select id="form_staff_lead_present" name="staff_lead_present" class="form-control">
						<?php foreach ($view_model->staff_lead_present_mapping() as $key => $value) {
							echo "<option value='{$key}' " . (($view_model->get_staff_lead_present() == $key && $view_model->get_staff_lead_present() != '') ? 'selected' : '') . "> {$value} </option>";
						}?>
					</select>
				</div>
				<div class="form-group">
					<label for="Transmisor Presente">Transmisor Presente </label>
					<select id="form_transmitter_present" name="transmitter_present" class="form-control">
						<?php foreach ($view_model->transmitter_present_mapping() as $key => $value) {
							echo "<option value='{$key}' " . (($view_model->get_transmitter_present() == $key && $view_model->get_transmitter_present() != '') ? 'selected' : '') . "> {$value} </option>";
						}?>
					</select>
				</div>
				<div class="form-group">
					<label for="Delegado 1 Presente">Delegado 1 Presente </label>
					<select id="form_delegate_1_present" name="delegate_1_present" class="form-control">
						<?php foreach ($view_model->delegate_1_present_mapping() as $key => $value) {
							echo "<option value='{$key}' " . (($view_model->get_delegate_1_present() == $key && $view_model->get_delegate_1_present() != '') ? 'selected' : '') . "> {$value} </option>";
						}?>
					</select>
				</div>
				<div class="form-group">
					<label for="Delegado 2 Presente">Delegado 2 Presente </label>
					<select id="form_delegate_2_present" name="delegate_2_present" class="form-control">
						<?php foreach ($view_model->delegate_2_present_mapping() as $key => $value) {
							echo "<option value='{$key}' " . (($view_model->get_delegate_2_present() == $key && $view_model->get_delegate_2_present() != '') ? 'selected' : '') . "> {$value} </option>";
						}?>
					</select>
				</div>
				<div class="form-group">
					<label for="Operativo 1 Presente">Operativo 1 Presente </label>
					<select id="form_runner_1_present" name="runner_1_present" class="form-control">
						<?php foreach ($view_model->runner_1_present_mapping() as $key => $value) {
							echo "<option value='{$key}' " . (($view_model->get_runner_1_present() == $key && $view_model->get_runner_1_present() != '') ? 'selected' : '') . "> {$value} </option>";
						}?>
					</select>
				</div>
				<div class="form-group">
					<label for="Operativo 2 Presente">Operativo 2 Presente </label>
					<select id="form_runner_2_present" name="runner_2_present" class="form-control">
						<?php foreach ($view_model->runner_2_present_mapping() as $key => $value) {
							echo "<option value='{$key}' " . (($view_model->get_runner_2_present() == $key && $view_model->get_runner_2_present() != '') ? 'selected' : '') . "> {$value} </option>";
						}?>
					</select>
				</div>


                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Enviar">
                </div>
                </form>
            </div>
        </div>
    </div>
</div>