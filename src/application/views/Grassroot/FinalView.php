<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/grassroot/dashboard" class="breadcrumb-link">Tablero</a></li>
						<li class="breadcrumb-item"><a href="/grassroot/final/0" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">Ver</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header"><?php echo $view_model->get_heading();?> Detalles</h5>
                <div class="card-body">
						<h6>Colegio:&nbsp; <?php echo $view_model->get_table();?></h6>
						<h6>Fecha Inicio:&nbsp; <?php echo $view_model->get_created_at();?></h6>
						<h6>Imagen:</h6>
						<img class='img-fluid' style="width: 50%;" src='<?php echo $view_model->get_file();?>'/>
						<h6>Alcalde 1:&nbsp; <?php echo $view_model->get_mayor_1() . ' ' . $view_model->get_mayor_count_1();?></h6>
						<h6>Alcalde 2:&nbsp; <?php echo $view_model->get_mayor_2() . ' ' . $view_model->get_mayor_count_2();?></h6>
						<h6>Alcalde 3:&nbsp; <?php echo $view_model->get_mayor_3() . ' ' . $view_model->get_mayor_count_3();?></h6>
						<h6>Alcalde 4:&nbsp; <?php echo $view_model->get_mayor_4() . ' ' . $view_model->get_mayor_count_4();?></h6>
						<h6>Alcalde 5:&nbsp; <?php echo $view_model->get_mayor_5() . ' ' . $view_model->get_mayor_count_5();?></h6>
						<h6>Alcalde 6:&nbsp; <?php echo $view_model->get_mayor_6() . ' ' . $view_model->get_mayor_count_6();?></h6>
						<h6>Alcalde7 :&nbsp; <?php echo $view_model->get_mayor_7() . ' ' . $view_model->get_mayor_count_7();?></h6>
						<h6>Alcalde8 :&nbsp; <?php echo $view_model->get_mayor_8() . ' ' . $view_model->get_mayor_count_8();?></h6>
						<h6>Alcalde 9:&nbsp; <?php echo $view_model->get_mayor_9() . ' ' . $view_model->get_mayor_count_9();?></h6>
						<h6>Alcalde 10:&nbsp; <?php echo $view_model->get_mayor_10() . ' ' . $view_model->get_mayor_count_10();?></h6>
						<h6> Senador1:&nbsp; <?php echo $view_model->get_senator_1() . ' ' . $view_model->get_senator_count_1();?></h6>
						<h6> Senador2:&nbsp; <?php echo $view_model->get_senator_2() . ' ' . $view_model->get_senator_count_2();?></h6>
						<h6> Senador3:&nbsp; <?php echo $view_model->get_senator_3() . ' ' . $view_model->get_senator_count_3();?></h6>
						<h6> Senador4:&nbsp; <?php echo $view_model->get_senator_4() . ' ' . $view_model->get_senator_count_4();?></h6>
						<h6> Senador5:&nbsp; <?php echo $view_model->get_senator_5() . ' ' . $view_model->get_senator_count_5();?></h6>
						<h6> Senador6:&nbsp; <?php echo $view_model->get_senator_6() . ' ' . $view_model->get_senator_count_6();?></h6>
						<h6> Senador7:&nbsp; <?php echo $view_model->get_senator_7() . ' ' . $view_model->get_senator_count_7();?></h6>
						<h6> Senador8:&nbsp; <?php echo $view_model->get_senator_8() . ' ' . $view_model->get_senator_count_8();?></h6>
						<h6> Senador9:&nbsp; <?php echo $view_model->get_senator_9() . ' ' . $view_model->get_senator_count_9();?></h6>
						<h6> Senador10:&nbsp; <?php echo $view_model->get_senator_10() . ' ' . $view_model->get_senator_count_10();?></h6>
						<h6>Regidors 1:&nbsp; <?php echo $view_model->get_regional_1() . ' ' . $view_model->get_regional_count_1();?></h6>
						<h6>Regidors 2:&nbsp; <?php echo $view_model->get_regional_2() . ' ' . $view_model->get_regional_count_2();?></h6>
						<h6>Regidors 3:&nbsp; <?php echo $view_model->get_regional_3() . ' ' . $view_model->get_regional_count_3();?></h6>
						<h6>Regidors 4:&nbsp; <?php echo $view_model->get_regional_4() . ' ' . $view_model->get_regional_count_4();?></h6>
						<h6>Regidors 5:&nbsp; <?php echo $view_model->get_regional_5() . ' ' . $view_model->get_regional_count_5();?></h6>
						<h6>Regidors 6:&nbsp; <?php echo $view_model->get_regional_6() . ' ' . $view_model->get_regional_count_6();?></h6>
						<h6>Regidors 7:&nbsp; <?php echo $view_model->get_regional_7() . ' ' . $view_model->get_regional_count_7();?></h6>
						<h6>Regidors 8:&nbsp; <?php echo $view_model->get_regional_8() . ' ' . $view_model->get_regional_count_8();?></h6>
						<h6>Regidors 9:&nbsp; <?php echo $view_model->get_regional_9() . ' ' . $view_model->get_regional_count_9();?></h6>
						<h6>Regidors 10:&nbsp; <?php echo $view_model->get_regional_10() . ' ' . $view_model->get_regional_count_10();?></h6>
						<h6>Regidors 11:&nbsp; <?php echo $view_model->get_regional_11() . ' ' . $view_model->get_regional_count_11();?></h6>
						<h6>Regidors 12:&nbsp; <?php echo $view_model->get_regional_12() . ' ' . $view_model->get_regional_count_12();?></h6>
						<h6>Regidors 13:&nbsp; <?php echo $view_model->get_regional_13() . ' ' . $view_model->get_regional_count_13();?></h6>
						<h6>Regidors 14:&nbsp; <?php echo $view_model->get_regional_14() . ' ' . $view_model->get_regional_count_14();?></h6>
						<h6>Regidors 15:&nbsp; <?php echo $view_model->get_regional_15() . ' ' . $view_model->get_regional_count_15();?></h6>

                </div>
        </div>
    </div>
</div>