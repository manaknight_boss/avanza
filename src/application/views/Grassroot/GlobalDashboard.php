<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Tablero DN <a href="/grassroot/dashboard/real" class="btn btn-primary">Actualizar Tablero</a></h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="container">
    <div class="row">
    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 mt-4">
        <div class="card">
            <h5 class="card-header">Circunscripcion 1</h5>
            <div class="card-body" style="min-height:270px">
            <canvas id="circunscripcion_1_dashboard_chart"></canvas>
            <?php echo "<div id='total_circunscripcion_1' data-num='{$total_circunscripcion_1}'> Total Electores: " . $total_circunscripcion_1;?></div>
            <?php echo "<div id='voted_circunscripcion_1' data-num='{$voted_circunscripcion_1}'> Total Votaron: " . $voted_circunscripcion_1;?></div>
            <?php echo "<div id='not_voted_circunscripcion_1' data-num='{$not_voted_circunscripcion_1}'> Total por Votar: " . $not_voted_circunscripcion_1;?></div>
            <?php echo "<div id='circunscripcion_1_turnout' data-num='{$circunscripcion_1_turnout}'> % Participación: " . $circunscripcion_1_turnout;?></div>
            <br/>
            <?php echo "<div id='avanza_circunscripcion_1_turnout' data-num='{$avanza_circunscripcion_1_turnout}'> %Votos Avanza: " . $avanza_circunscripcion_1_turnout;?>%</div>
            <?php echo "<div id='other_circunscripcion_1_turnout' data-num='{$other_circunscripcion_1_turnout}'> %No Avanza: " . $other_circunscripcion_1_turnout;?>%</div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 mt-4">
        <div class="card">
            <h5 class="card-header">Circunscripcion 2</h5>
            <div class="card-body" style="min-height:270px">
            <canvas id="circunscripcion_2_dashboard_chart"></canvas>
            <?php echo "<div id='total_circunscripcion_2' data-num='{$total_circunscripcion_2}'> Total Electores: " . $total_circunscripcion_2;?></div>
            <?php echo "<div id='voted_circunscripcion_2' data-num='{$voted_circunscripcion_2}'> Total Votaron: " . $voted_circunscripcion_2;?></div>
            <?php echo "<div id='not_voted_circunscripcion_2' data-num='{$not_voted_circunscripcion_2}'> Total por Votar: " . $not_voted_circunscripcion_2;?></div>
            <?php echo "<div id='circunscripcion_2_turnout' data-num='{$circunscripcion_2_turnout}'> % Participación: " . $circunscripcion_2_turnout;?></div>
            <br/>
            <?php echo "<div id='avanza_circunscripcion_2_turnout' data-num='{$avanza_circunscripcion_2_turnout}'> %Votos Avanza: " . $avanza_circunscripcion_2_turnout;?>%</div>
            <?php echo "<div id='other_circunscripcion_2_turnout' data-num='{$other_circunscripcion_2_turnout}'> %No Avanza: " . $other_circunscripcion_2_turnout;?>%</div>

            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 mt-4">
        <div class="card">
            <h5 class="card-header">Circunscripcion 3</h5>
            <div class="card-body" style="min-height:270px">
            <canvas id="circunscripcion_3_dashboard_chart"></canvas>
            <?php echo "<div id='total_circunscripcion_3' data-num='{$total_circunscripcion_3}'> Total Electores: " . $total_circunscripcion_3;?></div>
            <?php echo "<div id='voted_circunscripcion_3' data-num='{$voted_circunscripcion_3}'> Total Votaron: " . $voted_circunscripcion_3;?></div>
            <?php echo "<div id='not_voted_circunscripcion_3' data-num='{$not_voted_circunscripcion_3}'> Total por Votar: " . $not_voted_circunscripcion_3;?></div>
            <?php echo "<div id='circunscripcion_3_turnout' data-num='{$circunscripcion_3_turnout}'> % Participación: " . $circunscripcion_3_turnout;?></div>
            <br/>
            <?php echo "<div id='avanza_circunscripcion_3_turnout' data-num='{$avanza_circunscripcion_3_turnout}'> %Votos Avanza: " . $avanza_circunscripcion_3_turnout;?>%</div>
            <?php echo "<div id='other_circunscripcion_3_turnout' data-num='{$other_circunscripcion_3_turnout}'> %No Avanza: " . $other_circunscripcion_3_turnout;?>%</div>

            </div>
        </div>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-12">
        <div class="card">
            <h5 class="card-header">Avanza vs Todos</h5>
            <div class="card-body" style="min-height:270px">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 mt-6">
                    <?php echo "<div id='total_all' data-num='{$total_all}'> Total Electores: " . $total_all;?></div>
                    <?php echo "<div id='voted_all' data-num='{$voted_all}'> Total Votaron: " . $voted_all;?></div>
                    <?php echo "<div id='not_voted_all' data-num='{$not_voted_all}'> Total por Votar: " . $not_voted_all;?></div>
                    <?php echo "<div id='all_turnout' data-num='{$all_turnout}'> % Participación: " . $all_turnout;?></div>
                    <br/>
                    <?php echo "<div id='turnout_percentage_other' data-num='{$turnout_percentage_other}'> % No avanza " . $turnout_percentage_other;?>%</div>
                    <?php echo "<div id='turnout_percentage_avanza' data-num='{$turnout_percentage_avanza}'> %Votos Avanza: " . $turnout_percentage_avanza;?>%</div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 mt-6">
                        <canvas id="all_dashboard_chart"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
        <div class="card">
            <h5 class="card-header">Recintos que requieren atencion inmediata</h5>
        <div class="card-body" style="min-height:270px">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-circunscripcion_1" role="tab" aria-controls="nav-circunscripcion_1" aria-selected="true">Circunscripcion 1</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-circunscripcion_2" role="tab" aria-controls="nav-circunscripcion_2" aria-selected="false">Circunscripcion 2</a>
                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-circunscripcion_3" role="tab" aria-controls="nav-circunscripcion_3" aria-selected="false">Circunscripcion 3</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-circunscripcion_1" role="tabpanel" aria-labelledby="nav-home-tab">
                <!-- voice start -->
                <br/>
                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-striped">
                        <thead>
                        <?php foreach ($circunscripcion_headers as $data) {
                            echo "<th>{$data}</th>";
                        } ?>
                        </thead>
                        <tbody>
                        <?php foreach ($circunscripcion[0] as $data) { ?>
                            <?php
                            echo '<tr>';
                            echo "<td>{$data['region']}</td>";
                            echo "<td>{$data['recinto']}</td>";
                            echo "<td>{$data['total']}</td>";
                            echo "<td>{$data['total_avanza']}</td>";
							echo "<td>{$data['voted']}</td>";
							echo "<td>{$data['not_voted']}</td>";
							echo "<td>{$data['percentage_rest']}%</td>";
							echo "<td>{$data['percentage_avanza']}%</td>";
							echo "<td><a class=\"btn btn-success\" href=\"/grassroot/recinto/{$data['recinto']}\">Ver</a></td>";
                            echo '</tr>';
                            ?>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- voice end -->
                </div>
                <div class="tab-pane fade" id="nav-circunscripcion_2" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <!-- sms start -->
                    <br/>
                    <div class="table-responsive">
                        <table class="table table-hover table-condensed table-striped">
                            <thead>
                            <?php foreach ($circunscripcion_headers as $data) {
                            echo "<th>{$data}</th>";
                            } ?>
                            </thead>
                            <tbody>
                            <?php foreach ($circunscripcion[1] as $data) { ?>
                                <?php
                                echo '<tr>';
                                echo "<td>{$data['region']}</td>";
                                echo "<td>{$data['recinto']}</td>";
                                echo "<td>{$data['total']}</td>";
                                echo "<td>{$data['total_avanza']}</td>";
                                echo "<td>{$data['voted']}</td>";
                                echo "<td>{$data['not_voted']}</td>";
                                echo "<td>{$data['percentage_rest']}%</td>";
                                echo "<td>{$data['percentage_avanza']}%</td>";
                                echo "<td><a class=\"btn btn-success\" href=\"/grassroot/recinto/{$data['recinto']}\">Ver</a></td>";
                                echo '</tr>';
                                ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- sms end -->
                </div>
                <div class="tab-pane fade" id="nav-circunscripcion_3" role="tabpanel" aria-labelledby="nav-contact-tab">
                    <!-- sms start -->
                    <br/>
                    <div class="table-responsive">
                        <table class="table table-hover table-condensed table-striped">
                            <thead>
                            <?php foreach ($circunscripcion_headers as $data) {
                            echo "<th>{$data}</th>";
                            } ?>
                            </thead>
                            <tbody>
                            <?php foreach ($circunscripcion[2] as $data) { ?>
                                <?php
                                echo '<tr>';
                                echo "<td>{$data['region']}</td>";
                                echo "<td>{$data['recinto']}</td>";
                                echo "<td>{$data['total']}</td>";
                                echo "<td>{$data['total_avanza']}</td>";
                                echo "<td>{$data['voted']}</td>";
                                echo "<td>{$data['not_voted']}</td>";
                                echo "<td>{$data['percentage_rest']}%</td>";
                                echo "<td>{$data['percentage_avanza']}%</td>";
                                echo "<td><a class=\"btn btn-warning btn-sm\" href=\"/grassroot/recinto/{$data['recinto']}\">Ver</a></td>";
                                echo '</tr>';
                                ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- sms end -->
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
</div>
<script>

</script>