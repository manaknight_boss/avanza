<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/grassroot/dashboard" class="breadcrumb-link">Tablero</a></li>
						<li class="breadcrumb-item"><a href="/grassroot/recintos/0" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">Editar</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <?php if (validation_errors()) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?= validation_errors() ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($error) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($success) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Editar <?php echo $view_model->get_heading();?></h5>
                <div class="card-body">
                <?= form_open() ?>
				<div class="form-group">
					<label for="Numero">Numero </label>
					<input type="text" class="form-control" id="form_recinto" name="recinto" value="<?php echo set_value('recinto', $this->_data['view_model']->get_recinto());?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Nombre">Nombre </label>
					<input type="text" class="form-control" id="form_recinto_name" name="recinto_name" value="<?php echo set_value('recinto_name', $this->_data['view_model']->get_recinto_name());?>"/>
				</div>
				<div class="form-group">
					<label for="Colegios">Colegios </label>
					<input type="text" class="form-control" id="form_collegio" name="collegio" value="<?php echo set_value('collegio', $this->_data['view_model']->get_collegio());?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Encargado">Encargado </label>
					<input type="text" class="form-control" id="form_coordinator_name" name="coordinator_name" value="<?php echo set_value('coordinator_name', $this->_data['view_model']->get_coordinator_name());?>"/>
				</div>
				<div class="form-group">
					<label for="Cédula">Cédula </label>
					<input type="text" class="form-control" id="form_coordinator_government_id" name="coordinator_government_id" value="<?php echo set_value('coordinator_government_id', $this->_data['view_model']->get_coordinator_government_id());?>"/>
				</div>
				<div class="form-group">
					<label for="Contraseña">Contraseña </label>
					<input type="text" class="form-control" id="form_password" name="password" value="<?php echo set_value('password', $this->_data['view_model']->get_password());?>"/>
				</div>


                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Enviar">
                </div>
                </form>
            </div>
        </div>
    </div>
</div>