<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Circunscripcion <?php echo $region; ?></h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="container">
    <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-12">
        <div class="card">
            <h5 class="card-header">Circunscripcion <?php echo $region; ?></h5>
            <div class="card-body" style="min-height:270px">
            <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 mt-6">
                <canvas id="circunscripcion_n_dashboard_chart"></canvas>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 mt-6">
                <br/>
                <br/>
                <br/>
                <?php echo "<div id='total_circunscripcion' data-num='{$total_circunscripcion}'> Total Electores: " . $total_circunscripcion;?></div>
                <?php echo "<div id='voted_circunscripcion' data-num='{$voted_circunscripcion}'> Total Votaron: " . $voted_circunscripcion;?></div>
                <?php echo "<div id='not_voted_circunscripcion' data-num='{$not_voted_circunscripcion}'> Total por Votar: " . $not_voted_circunscripcion;?></div>
                <?php echo "<div id='circunscripcion_turnout' data-num='{$circunscripcion_turnout}'> % Participación: " . $circunscripcion_turnout;?></div>
            </div>
            </div>
            </div>
        </div>
    </div>
    </div>


<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
        <div class="card">
            <h5 class="card-header">Recintos que requieren atencion inmediata</h5>
        <div class="card-body" style="min-height:270px">
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-circunscripcion_1" role="tabpanel" aria-labelledby="nav-home-tab">
                <!-- voice start -->
                <br/>
                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-striped">
                        <thead>
                        <?php foreach ($circunscripcion_headers as $data) {
                            echo "<th>{$data}</th>";
                        } ?>
                        </thead>
                        <tbody>
                        <?php foreach ($circunscripcion as $data) { ?>
                            <?php
                            echo '<tr>';
                            echo "<td>{$data['region']}</td>";
                            echo "<td>{$data['recinto']}<br/> {$data['recinto_name']}</td>";
							echo "<td>{$data['total']}</td>";
							echo "<td>{$data['voted']}</td>";
							echo "<td>{$data['not_voted']}</td>";
							echo "<td>{$data['percentage']}%</td>";
							echo "<td><a class=\"btn btn-warning btn-sm\" href=\"/grassroot/recinto/{$data['recinto']}\">Ver</a></td>";
                            echo '</tr>';
                            ?>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- voice end -->
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
</div>
<script>

</script>