<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<h2><?php echo $view_model->get_heading();?></h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header"><?php echo $view_model->get_heading();?> Buscar</h5>
            <div class="card-body">
                <?= form_open('/grassroot/voters/0', ['method' => 'get']) ?>
                    <div class="row">
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Circunscripcion">Circ. </label>
								<input type="text" class="form-control" id="circunscripcion" name="circunscripcion" value="<?php echo $this->_data['view_model']->get_circunscripcion();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
                        </div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Cédula">Cédula </label>
								<input type="text" class="form-control" id="government_id" name="government_id" value="<?php echo $this->_data['view_model']->get_government_id();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Recinto">Recinto </label>
								<select name="recinto" class="form-control">
									<option value="">Todos</option>
									<?php foreach ($recinto as $key => $value) {
										echo "<option value='{$key}' " . (($view_model->get_recinto() == $key && $view_model->get_recinto() != '') ? 'selected' : '') . "> {$value} </option>";
									}?>
								</select>
							</div>
						</div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Colegio">Colegio </label>
								<select name="colegio" class="form-control">
									<option value="">Todos</option>
									<?php foreach ($colegio as $key => $value) {
										echo "<option value='{$key}' " . (($view_model->get_colegio() == $key && $view_model->get_colegio() != '') ? 'selected' : '') . "> {$value} </option>";
									}?>
								</select>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="form-group">
								<label for="Votaron">Votaron </label>
								<select name="has_voted_municipal_2020" class="form-control">
									<option value="">Todos</option>
									<option value="1" <?php (($view_model->get_has_voted_municipal_2020() == 1 && $view_model->get_has_voted_municipal_2020() != '') ? 'selected' : '')?> >Si</option>
									<option value="0" <?php (($view_model->get_has_voted_municipal_2020() == 0 && $view_model->get_has_voted_municipal_2020() != '') ? 'selected' : '')?>>No</option>
								</select>
							</div>
                        </div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="form-group">
								<label for="Votaron">Fuente </label>
								<select name="source" class="form-control">
									<option value="">Todos</option>
                                    <option value="1" <?php echo ($view_model->get_source() == '1' && $view_model->get_source() != '') ? 'selected' : '';?> >Avanza</option>
                                    <option value="3" <?php echo ($view_model->get_source() == '3' && $view_model->get_source() != '') ? 'selected' : '';?>>Tactica</option>
								</select>
							</div>
						</div>

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-primary" value="Buscar">
                        </div>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
        <div class="card">
            <h5 class="card-header">
                <div class="float-left"><?php echo $view_model->get_heading();?></div>
                <div class="float-right"></div>
                <div class="float-right"></div>
                <div class="clearfix"></div>
            </h5>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-striped">
                        <thead>
                        <?php foreach ($view_model->get_column() as $data) {
                            echo "<th>{$data}</th>";
                        } ?>
                        </thead>
                        <tbody>
                        <?php foreach ($view_model->get_list() as $data) { ?>
                            <?php
                            echo '<tr>';
							echo "<td>{$data->id}</td>";
							echo "<td>{$data->circunscripcion}</td>";
							echo'<td>' . $data->first_name . "<br/>" . $data->last_name . "<br/>" . $data->government_id . "<br/>" .  $data->gender . "<br/>" . $data->married_status . "<br/>" . $data->age . "<br/>". $data->phone_1 . "<br/>" . $data->phone_2 . '</td>';
							echo'<td>' . $data->recinto . "<br/>" . $data->recinto_name . "<br/>" . $data->recinto_address . "<br/>" . '</td>';
							echo "<td>{$data->colegio}</td>";
							echo "<td>{$data->cordinator}</td>";
							echo "<td>" . ($data->has_voted_municipal_2020 ? 'Si' : 'No') . "</td>";
							echo '<td>';
							$condition = ($data->is_cordinator == 1)?TRUE:FALSE;
							echo ($condition) ? "&nbsp;<a class='btn btn-warning btn-sm' target='_blank' href='/grassroot/cordinator/{$data->cordinator_id}/$data->recinto'>Ver</a>" : '';
							echo "&nbsp;<a class='btn btn-warning btn-sm' target='_blank' href='/grassroot/voters/voteNow/{$data->id}'>Votaron</a>";
							echo '</td>';
                            echo '</tr>';
                            ?>
                        <?php } ?>
                        </tbody>
                    </table>
                    <p class="pagination_custom"><?php echo $view_model->get_links(); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>