<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Recinto <?php echo $recinto; ?></h2>
<h6><?php echo $recinto_name; ?></h6>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="container">
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
        <div class="card">
            <h5 class="card-header">Coordinadores en Recinto</h5>
        <div class="card-body" style="min-height:270px">
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-circunscripcion_1" role="tabpanel" aria-labelledby="nav-home-tab">
                <!-- voice start -->
                <br/>
                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-striped">
                        <thead>
                        <?php foreach ($headers as $data) {
                            echo "<th>{$data}</th>";
                        } ?>
                        </thead>
                        <tbody>
                        <?php foreach ($cordinators as $data) { ?>
                            <?php
                            echo '<tr>';
                            echo "<td>{$data['id']}</td>";
                            echo "<td>{$data['name']}</td>";
							echo "<td>{$data['government_id']}</td>";
							echo "<td>{$data['phone_1']}<br/>{$data['phone_2']}</td>";
							echo "<td><a class=\"btn btn-warning btn-sm\" href=\"/grassroot/cordinator/{$data['id']}/{$recinto}\">Ver</a></td>";
                            echo '</tr>';
                            ?>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- voice end -->
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
</div>
<script>

</script>