<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/grassroot/dashboard" class="breadcrumb-link">Tablero</a></li>
						<li class="breadcrumb-item"><a href="/grassroot/final/0" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">Añadir</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <?php if (validation_errors()) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?= validation_errors() ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($error) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($success) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Añadir <?php echo $view_model->get_heading();?></h5>
                <div class="card-body">
                <?= form_open() ?>
				<div class="form-group">
					<label for="Colegio">Colegio </label>
					<input type="text" class="form-control" id="form_table" name="table" value="<?php echo set_value('table'); ?>"/>
				</div>
				<div class="form-group">
					<div class="mkd-upload-form-btn-wrapper">
						<label for="Imagen">Imagen</label>
						<button class="mkd-upload-btn">Cargar un Archivo</button>
						<input type="file" name="file_upload" id="file_upload" onchange="onFileUploaded(event, 'file')" accept=".gif,.jpg,.jpeg,.png,.doc,.docx,.pdf,.md,.txt,.rtf,.xls,.xlsx,.xml,.json,.html,.mp3,.mp4,.csv,.bmp,.mpeg,.ppt,.pptx,.svg,.wav,.webm,.weba,.woff,.tiff"/>
					<input type="hidden" id="file" name="file"/>
					<input type="hidden" id="file_id" name="file_id"/>
					<span id="file_text" class="mkd-upload-filename"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="Alcalde 1">Alcalde 1 </label>
					<input type="text" class="form-control" id="form_mayor_1" name="mayor_1" value="<?php echo set_value('mayor_1'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos Alcalde 1">Votos Alcalde 1 </label>
					<input type="text" class="form-control" id="form_mayor_count_1" name="mayor_count_1" value="<?php echo set_value('mayor_count_1'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Alcalde 2">Alcalde 2 </label>
					<input type="text" class="form-control" id="form_mayor_2" name="mayor_2" value="<?php echo set_value('mayor_2'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos Alcalde 2">Votos Alcalde 2 </label>
					<input type="text" class="form-control" id="form_mayor_count_2" name="mayor_count_2" value="<?php echo set_value('mayor_count_2'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Alcalde 3">Alcalde 3 </label>
					<input type="text" class="form-control" id="form_mayor_3" name="mayor_3" value="<?php echo set_value('mayor_3'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos Alcalde 3">Votos Alcalde 3 </label>
					<input type="text" class="form-control" id="form_mayor_count_3" name="mayor_count_3" value="<?php echo set_value('mayor_count_3'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Alcalde 4">Alcalde 4 </label>
					<input type="text" class="form-control" id="form_mayor_4" name="mayor_4" value="<?php echo set_value('mayor_4'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos Alcalde 4">Votos Alcalde 4 </label>
					<input type="text" class="form-control" id="form_mayor_count_4" name="mayor_count_4" value="<?php echo set_value('mayor_count_4'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Alcalde 5">Alcalde 5 </label>
					<input type="text" class="form-control" id="form_mayor_5" name="mayor_5" value="<?php echo set_value('mayor_5'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos Alcalde 5">Votos Alcalde 5 </label>
					<input type="text" class="form-control" id="form_mayor_count_5" name="mayor_count_5" value="<?php echo set_value('mayor_count_5'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Alcalde 6">Alcalde 6 </label>
					<input type="text" class="form-control" id="form_mayor_6" name="mayor_6" value="<?php echo set_value('mayor_6'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos Alcalde 6">Votos Alcalde 6 </label>
					<input type="text" class="form-control" id="form_mayor_count_6" name="mayor_count_6" value="<?php echo set_value('mayor_count_6'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Alcalde7 ">Alcalde7  </label>
					<input type="text" class="form-control" id="form_mayor_7" name="mayor_7" value="<?php echo set_value('mayor_7'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos Alcalde 7">Votos Alcalde 7 </label>
					<input type="text" class="form-control" id="form_mayor_count_7" name="mayor_count_7" value="<?php echo set_value('mayor_count_7'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Alcalde8 ">Alcalde8  </label>
					<input type="text" class="form-control" id="form_mayor_8" name="mayor_8" value="<?php echo set_value('mayor_8'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos Alcalde 8">Votos Alcalde 8 </label>
					<input type="text" class="form-control" id="form_mayor_count_8" name="mayor_count_8" value="<?php echo set_value('mayor_count_8'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Alcalde 9">Alcalde 9 </label>
					<input type="text" class="form-control" id="form_mayor_9" name="mayor_9" value="<?php echo set_value('mayor_9'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos Alcalde 9">Votos Alcalde 9 </label>
					<input type="text" class="form-control" id="form_mayor_count_9" name="mayor_count_9" value="<?php echo set_value('mayor_count_9'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Alcalde 10">Alcalde 10 </label>
					<input type="text" class="form-control" id="form_mayor_10" name="mayor_10" value="<?php echo set_value('mayor_10'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos Alcalde 10">Votos Alcalde 10 </label>
					<input type="text" class="form-control" id="form_mayor_count_10" name="mayor_count_10" value="<?php echo set_value('mayor_count_10'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for=" Senador1"> Senador1 </label>
					<input type="text" class="form-control" id="form_senator_1" name="senator_1" value="<?php echo set_value('senator_1'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos Senador1">Votos Senador1 </label>
					<input type="text" class="form-control" id="form_senator_count_1" name="senator_count_1" value="<?php echo set_value('senator_count_1'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for=" Senador2"> Senador2 </label>
					<input type="text" class="form-control" id="form_senator_2" name="senator_2" value="<?php echo set_value('senator_2'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos Senador2">Votos Senador2 </label>
					<input type="text" class="form-control" id="form_senator_count_2" name="senator_count_2" value="<?php echo set_value('senator_count_2'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for=" Senador3"> Senador3 </label>
					<input type="text" class="form-control" id="form_senator_3" name="senator_3" value="<?php echo set_value('senator_3'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos Senador3">Votos Senador3 </label>
					<input type="text" class="form-control" id="form_senator_count_3" name="senator_count_3" value="<?php echo set_value('senator_count_3'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for=" Senador4"> Senador4 </label>
					<input type="text" class="form-control" id="form_senator_4" name="senator_4" value="<?php echo set_value('senator_4'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos Senador4">Votos Senador4 </label>
					<input type="text" class="form-control" id="form_senator_count_4" name="senator_count_4" value="<?php echo set_value('senator_count_4'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for=" Senador5"> Senador5 </label>
					<input type="text" class="form-control" id="form_senator_5" name="senator_5" value="<?php echo set_value('senator_5'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos Senador5">Votos Senador5 </label>
					<input type="text" class="form-control" id="form_senator_count_5" name="senator_count_5" value="<?php echo set_value('senator_count_5'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for=" Senador6"> Senador6 </label>
					<input type="text" class="form-control" id="form_senator_6" name="senator_6" value="<?php echo set_value('senator_6'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos Senador6">Votos Senador6 </label>
					<input type="text" class="form-control" id="form_senator_count_6" name="senator_count_6" value="<?php echo set_value('senator_count_6'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for=" Senador7"> Senador7 </label>
					<input type="text" class="form-control" id="form_senator_7" name="senator_7" value="<?php echo set_value('senator_7'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos Senador7">Votos Senador7 </label>
					<input type="text" class="form-control" id="form_senator_count_7" name="senator_count_7" value="<?php echo set_value('senator_count_7'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for=" Senador8"> Senador8 </label>
					<input type="text" class="form-control" id="form_senator_8" name="senator_8" value="<?php echo set_value('senator_8'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos Senador8">Votos Senador8 </label>
					<input type="text" class="form-control" id="form_senator_count_8" name="senator_count_8" value="<?php echo set_value('senator_count_8'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for=" Senador9"> Senador9 </label>
					<input type="text" class="form-control" id="form_senator_9" name="senator_9" value="<?php echo set_value('senator_9'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos Senador9">Votos Senador9 </label>
					<input type="text" class="form-control" id="form_senator_count_9" name="senator_count_9" value="<?php echo set_value('senator_count_9'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for=" Senador10"> Senador10 </label>
					<input type="text" class="form-control" id="form_senator_10" name="senator_10" value="<?php echo set_value('senator_10'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos Senador10">Votos Senador10 </label>
					<input type="text" class="form-control" id="form_senator_count_10" name="senator_count_10" value="<?php echo set_value('senator_count_10'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Regidor1">Regidor1 </label>
					<input type="text" class="form-control" id="form_regional_1" name="regional_1" value="<?php echo set_value('regional_1'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos regidor 1">Votos regidor 1 </label>
					<input type="text" class="form-control" id="form_regional_count_1" name="regional_count_1" value="<?php echo set_value('regional_count_1'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Regidor2">Regidor2 </label>
					<input type="text" class="form-control" id="form_regional_2" name="regional_2" value="<?php echo set_value('regional_2'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos regidor 2">Votos regidor 2 </label>
					<input type="text" class="form-control" id="form_regional_count_2" name="regional_count_2" value="<?php echo set_value('regional_count_2'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Regidor3">Regidor3 </label>
					<input type="text" class="form-control" id="form_regional_3" name="regional_3" value="<?php echo set_value('regional_3'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos regidor 3">Votos regidor 3 </label>
					<input type="text" class="form-control" id="form_regional_count_3" name="regional_count_3" value="<?php echo set_value('regional_count_3'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Regidor4">Regidor4 </label>
					<input type="text" class="form-control" id="form_regional_4" name="regional_4" value="<?php echo set_value('regional_4'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos regidor 4">Votos regidor 4 </label>
					<input type="text" class="form-control" id="form_regional_count_4" name="regional_count_4" value="<?php echo set_value('regional_count_4'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Regidor5">Regidor5 </label>
					<input type="text" class="form-control" id="form_regional_5" name="regional_5" value="<?php echo set_value('regional_5'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos regidor 5">Votos regidor 5 </label>
					<input type="text" class="form-control" id="form_regional_count_5" name="regional_count_5" value="<?php echo set_value('regional_count_5'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Regidor6">Regidor6 </label>
					<input type="text" class="form-control" id="form_regional_6" name="regional_6" value="<?php echo set_value('regional_6'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos regidor 6">Votos regidor 6 </label>
					<input type="text" class="form-control" id="form_regional_count_6" name="regional_count_6" value="<?php echo set_value('regional_count_6'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Regidor7">Regidor7 </label>
					<input type="text" class="form-control" id="form_regional_7" name="regional_7" value="<?php echo set_value('regional_7'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos regidor 7">Votos regidor 7 </label>
					<input type="text" class="form-control" id="form_regional_count_7" name="regional_count_7" value="<?php echo set_value('regional_count_7'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Regidor8">Regidor8 </label>
					<input type="text" class="form-control" id="form_regional_8" name="regional_8" value="<?php echo set_value('regional_8'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos regidor 8">Votos regidor 8 </label>
					<input type="text" class="form-control" id="form_regional_count_8" name="regional_count_8" value="<?php echo set_value('regional_count_8'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Regidor9">Regidor9 </label>
					<input type="text" class="form-control" id="form_regional_9" name="regional_9" value="<?php echo set_value('regional_9'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos regidor 9">Votos regidor 9 </label>
					<input type="text" class="form-control" id="form_regional_count_9" name="regional_count_9" value="<?php echo set_value('regional_count_9'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Regidor10">Regidor10 </label>
					<input type="text" class="form-control" id="form_regional_10" name="regional_10" value="<?php echo set_value('regional_10'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos regidor 10">Votos regidor 10 </label>
					<input type="text" class="form-control" id="form_regional_count_10" name="regional_count_10" value="<?php echo set_value('regional_count_10'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Regidor11">Regidor11 </label>
					<input type="text" class="form-control" id="form_regional_11" name="regional_11" value="<?php echo set_value('regional_11'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos regidor 11">Votos regidor 11 </label>
					<input type="text" class="form-control" id="form_regional_count_11" name="regional_count_11" value="<?php echo set_value('regional_count_11'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Regidor12">Regidor12 </label>
					<input type="text" class="form-control" id="form_regional_12" name="regional_12" value="<?php echo set_value('regional_12'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos regidor 12">Votos regidor 12 </label>
					<input type="text" class="form-control" id="form_regional_count_12" name="regional_count_12" value="<?php echo set_value('regional_count_12'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Regidor13">Regidor13 </label>
					<input type="text" class="form-control" id="form_regional_13" name="regional_13" value="<?php echo set_value('regional_13'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos regidor 13">Votos regidor 13 </label>
					<input type="text" class="form-control" id="form_regional_count_13" name="regional_count_13" value="<?php echo set_value('regional_count_13'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Regidor14">Regidor14 </label>
					<input type="text" class="form-control" id="form_regional_14" name="regional_14" value="<?php echo set_value('regional_14'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos regidor 14">Votos regidor 14 </label>
					<input type="text" class="form-control" id="form_regional_count_14" name="regional_count_14" value="<?php echo set_value('regional_count_14'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Regidor15">Regidor15 </label>
					<input type="text" class="form-control" id="form_regional_15" name="regional_15" value="<?php echo set_value('regional_15'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Votos regidor 15">Votos regidor 15 </label>
					<input type="text" class="form-control" id="form_regional_count_15" name="regional_count_15" value="<?php echo set_value('regional_count_15'); ?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>


                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Enviar">
                </div>
                </form>
            </div>
        </div>
    </div>
</div>