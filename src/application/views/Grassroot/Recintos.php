<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<h2><?php echo $view_model->get_heading();?></h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card" id="recintos_filter_listing">
            <h5 class="card-header"><?php echo $view_model->get_heading();?> Buscar</h5>
            <div class="card-body">
                <?= form_open('/grassroot/recintos/0', ['method' => 'get']) ?>
                    <div class="row">
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Numero">Numero </label>
								<input type="text" class="form-control" id="recinto" name="recinto" value="<?php echo $this->_data['view_model']->get_recinto();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Nombre">Nombre </label>
								<input type="text" class="form-control" id="recinto_name" name="recinto_name" value="<?php echo $this->_data['view_model']->get_recinto_name();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Colegios">Colegios </label>
								<input type="text" class="form-control" id="collegio" name="collegio" value="<?php echo $this->_data['view_model']->get_collegio();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Encargado">Encargado </label>
								<input type="text" class="form-control" id="coordinator_name" name="coordinator_name" value="<?php echo $this->_data['view_model']->get_coordinator_name();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Cédula">Cédula </label>
								<input type="text" class="form-control" id="coordinator_government_id" name="coordinator_government_id" value="<?php echo $this->_data['view_model']->get_coordinator_government_id();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Asignados">Asignados </label>
								<select name="assigned" class="form-control">
									<option value="">Todos</option>
									<?php foreach ($view_model->assigned_mapping() as $key => $value) {
										echo "<option value='{$key}' " . (($view_model->get_assigned() == $key && $view_model->get_assigned() != '') ? 'selected' : '') . "> {$value} </option>";
									}?>
								</select>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Personal">Personal </label>
								<input type="text" class="form-control" id="personel" name="personel" value="<?php echo $this->_data['view_model']->get_personel();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Presente">Presente </label>
								<select name="present" class="form-control">
									<option value="">Todos</option>
									<?php foreach ($view_model->present_mapping() as $key => $value) {
										echo "<option value='{$key}' " . (($view_model->get_present() == $key && $view_model->get_present() != '') ? 'selected' : '') . "> {$value} </option>";
									}?>
								</select>
							</div>
						</div>

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-primary" value="Buscar">
                        </div>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
        <div class="card"  id="recintos_listing">
            <h5 class="card-header">
                <div class="float-left"><?php echo $view_model->get_heading();?></div>
                <div class="float-right"></div>
                <div class="float-right"></div>
                <div class="clearfix"></div>
            </h5>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-striped">
                        <thead>
                        <?php
                        $order_by = $view_model->get_order_by();
                        $direction = $view_model->get_sort();
                        $model_base_url = $view_model->get_sort_base_url();
                        $field_column = $view_model->get_field_column();
                        $clean_mode = $view_model->get_format_layout();
                        $format_mode = '';
                        if ($clean_mode) {
                            $format_mode = '&layout_clean_mode=1';
                        }
                        foreach ($view_model->get_column() as $key => $data) {
                            $data_field = $field_column[$key];
                            if (strlen($order_by) < 1 || $data_field == '')
                            {
                                echo "<th>{$data}</th>";
                            }
                            else
                            {
                                if ($order_by === $data_field)
                                {
                                    if ($direction == 'ASC')
                                    {
                                        echo "<th><a href='{$model_base_url}?order_by={$data_field}{$format_mode}&direction=DESC'>{$data} <i class='fas fa-sort-up' style='vertical-align: -0.35em;'></i></a></th>";
                                    }
                                    else
                                    {
                                        echo "<th><a href='{$model_base_url}?order_by={$data_field}{$format_mode}&direction=ASC'>{$data} <i class='fas fa-sort-down' style='margin-bottom:3px;'></i></a></th>";
                                    }
                                }
                                else
                                {
                                    echo "<th><a href='{$model_base_url}?order_by={$data_field}{$format_mode}&direction=ASC'>{$data} <i class='fas fa-sort-down'  style='margin-bottom:3px;color:#e2e2e2;'></i></a></th>";
                                }
                            }
                        } ?>
                        </thead>
                        <tbody>
                        <?php foreach ($view_model->get_list() as $data) { ?>
                            <?php
                            echo '<tr>';
							echo "<td>{$data->recinto}</td>";
							echo "<td>{$data->recinto_name}</td>";
							echo "<td>{$data->collegio}</td>";
							echo "<td>{$data->coordinator_name}</td>";
							echo "<td>{$data->coordinator_government_id}</td>";
							echo "<td>{$view_model->assigned_mapping()[$data->assigned]}</td>";
							echo "<td>{$data->personel}</td>";
							echo "<td>{$view_model->present_mapping()[$data->present]}</td>";
							echo '<td>';
							echo '<a class="btn btn-primary btn-sm" target="__blank" href="/grassroot/recintos/edit/' . $data->id . '">Editar</a>';
							$condition = TRUE;
							echo ($condition) ? "<a class='btn btn-info btn-sm' target='_blank' href='/grassroot/personnel/0?recinto={$data->recinto}'>Ver</a>" : '';
							echo '</td>';
                            echo '</tr>';
                            ?>
                        <?php } ?>
                        </tbody>
                    </table>
                    <p class="pagination_custom"><?php echo $view_model->get_links(); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>