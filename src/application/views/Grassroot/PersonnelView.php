<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/grassroot/dashboard" class="breadcrumb-link">Tablero</a></li>
						<li class="breadcrumb-item"><a href="/grassroot/personnel/0" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">Ver</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header"><?php echo $view_model->get_heading();?> Detalles</h5>
                <div class="card-body">
						<h6>Recinto:&nbsp; <?php echo $view_model->get_recinto();?></h6>
						<h6>Colegio:&nbsp; <?php echo $view_model->get_collegio();?></h6>
						<h6>Facilitador de Colegio:&nbsp; <?php echo $view_model->get_staff_lead_name();?></h6>
						<h6>Cedula:&nbsp; <?php echo $view_model->get_staff_lead_government_id();?></h6>
						<h6>Transmisor:&nbsp; <?php echo $view_model->get_transmitter_name();?></h6>
						<h6>Cedula:&nbsp; <?php echo $view_model->get_transmitter_government_id();?></h6>
						<h6>Delegado:&nbsp; <?php echo $view_model->get_delegate_1_name();?></h6>
						<h6>Cedula:&nbsp; <?php echo $view_model->get_delegate_1_government_id();?></h6>
						<h6>Facilitador Res.:&nbsp; <?php echo $view_model->get_delegate_2_name();?></h6>
						<h6>Cedula:&nbsp; <?php echo $view_model->get_delegate_2_government_id();?></h6>
						<h6>Operativo:&nbsp; <?php echo $view_model->get_runner_1_name();?></h6>
						<h6>Cedula:&nbsp; <?php echo $view_model->get_runner_1_government_id();?></h6>
						<h6>Transporte:&nbsp; <?php echo $view_model->get_runner_2_name();?></h6>
						<h6>Cedula:&nbsp; <?php echo $view_model->get_runner_2_government_id();?></h6>
						<h6>Encargado Presente:&nbsp; <?php echo $view_model->staff_lead_present_mapping()[$view_model->get_staff_lead_present()];?></h6>
						<h6>Transmisor Presente:&nbsp; <?php echo $view_model->transmitter_present_mapping()[$view_model->get_transmitter_present()];?></h6>
						<h6>Delegado 1 Presente:&nbsp; <?php echo $view_model->delegate_1_present_mapping()[$view_model->get_delegate_1_present()];?></h6>
						<h6>Delegado 2 Presente:&nbsp; <?php echo $view_model->delegate_2_present_mapping()[$view_model->get_delegate_2_present()];?></h6>
						<h6>Operativo 1 Presente:&nbsp; <?php echo $view_model->runner_1_present_mapping()[$view_model->get_runner_1_present()];?></h6>
						<h6>Operativo 2 Presente:&nbsp; <?php echo $view_model->runner_2_present_mapping()[$view_model->get_runner_2_present()];?></h6>

                </div>
        </div>
    </div>
</div>