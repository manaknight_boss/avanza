<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<h2><?php echo $view_model->get_heading();?></h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card" id="personnel_filter_listing">
            <h5 class="card-header"><?php echo $view_model->get_heading();?> Buscar</h5>
            <div class="card-body">
                <?= form_open('/grassroot/personnel/0', ['method' => 'get']) ?>
                    <div class="row">
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="recinto">recinto </label>
								<input type="text" class="form-control" id="recinto" name="recinto" value="<?php echo $this->_data['view_model']->get_recinto();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="collegio">collegio </label>
								<input type="text" class="form-control" id="collegio" name="collegio" value="<?php echo $this->_data['view_model']->get_collegio();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="staff_lead_name">staff_lead_name </label>
								<input type="text" class="form-control" id="staff_lead_name" name="staff_lead_name" value="<?php echo $this->_data['view_model']->get_staff_lead_name();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="staff_lead_government_id">staff_lead_government_id </label>
								<input type="text" class="form-control" id="staff_lead_government_id" name="staff_lead_government_id" value="<?php echo $this->_data['view_model']->get_staff_lead_government_id();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="transmitter_name">transmitter_name </label>
								<input type="text" class="form-control" id="transmitter_name" name="transmitter_name" value="<?php echo $this->_data['view_model']->get_transmitter_name();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="transmitter_government_id">transmitter_government_id </label>
								<input type="text" class="form-control" id="transmitter_government_id" name="transmitter_government_id" value="<?php echo $this->_data['view_model']->get_transmitter_government_id();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="delegate_1_name">delegate_1_name </label>
								<input type="text" class="form-control" id="delegate_1_name" name="delegate_1_name" value="<?php echo $this->_data['view_model']->get_delegate_1_name();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="delegate_1_government_id">delegate_1_government_id </label>
								<input type="text" class="form-control" id="delegate_1_government_id" name="delegate_1_government_id" value="<?php echo $this->_data['view_model']->get_delegate_1_government_id();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="delegate_2_name">delegate_2_name </label>
								<input type="text" class="form-control" id="delegate_2_name" name="delegate_2_name" value="<?php echo $this->_data['view_model']->get_delegate_2_name();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="delegate_2_government_id">delegate_2_government_id </label>
								<input type="text" class="form-control" id="delegate_2_government_id" name="delegate_2_government_id" value="<?php echo $this->_data['view_model']->get_delegate_2_government_id();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="runner_1_name">runner_1_name </label>
								<input type="text" class="form-control" id="runner_1_name" name="runner_1_name" value="<?php echo $this->_data['view_model']->get_runner_1_name();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="runner_1_government_id">runner_1_government_id </label>
								<input type="text" class="form-control" id="runner_1_government_id" name="runner_1_government_id" value="<?php echo $this->_data['view_model']->get_runner_1_government_id();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="runner_2_name">runner_2_name </label>
								<input type="text" class="form-control" id="runner_2_name" name="runner_2_name" value="<?php echo $this->_data['view_model']->get_runner_2_name();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="runner_2_government_id">runner_2_government_id </label>
								<input type="text" class="form-control" id="runner_2_government_id" name="runner_2_government_id" value="<?php echo $this->_data['view_model']->get_runner_2_government_id();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="staff_lead_present">staff_lead_present </label>
								<select name="staff_lead_present" class="form-control">
									<option value="">Todos</option>
									<?php foreach ($view_model->staff_lead_present_mapping() as $key => $value) {
										echo "<option value='{$key}' " . (($view_model->get_staff_lead_present() == $key && $view_model->get_staff_lead_present() != '') ? 'selected' : '') . "> {$value} </option>";
									}?>
								</select>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="transmitter_present">transmitter_present </label>
								<select name="transmitter_present" class="form-control">
									<option value="">Todos</option>
									<?php foreach ($view_model->transmitter_present_mapping() as $key => $value) {
										echo "<option value='{$key}' " . (($view_model->get_transmitter_present() == $key && $view_model->get_transmitter_present() != '') ? 'selected' : '') . "> {$value} </option>";
									}?>
								</select>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="delegate_1_present">delegate_1_present </label>
								<select name="delegate_1_present" class="form-control">
									<option value="">Todos</option>
									<?php foreach ($view_model->delegate_1_present_mapping() as $key => $value) {
										echo "<option value='{$key}' " . (($view_model->get_delegate_1_present() == $key && $view_model->get_delegate_1_present() != '') ? 'selected' : '') . "> {$value} </option>";
									}?>
								</select>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="delegate_2_present">delegate_2_present </label>
								<select name="delegate_2_present" class="form-control">
									<option value="">Todos</option>
									<?php foreach ($view_model->delegate_2_present_mapping() as $key => $value) {
										echo "<option value='{$key}' " . (($view_model->get_delegate_2_present() == $key && $view_model->get_delegate_2_present() != '') ? 'selected' : '') . "> {$value} </option>";
									}?>
								</select>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="runner_1_present">runner_1_present </label>
								<select name="runner_1_present" class="form-control">
									<option value="">Todos</option>
									<?php foreach ($view_model->runner_1_present_mapping() as $key => $value) {
										echo "<option value='{$key}' " . (($view_model->get_runner_1_present() == $key && $view_model->get_runner_1_present() != '') ? 'selected' : '') . "> {$value} </option>";
									}?>
								</select>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="runner_2_present">runner_2_present </label>
								<select name="runner_2_present" class="form-control">
									<option value="">Todos</option>
									<?php foreach ($view_model->runner_2_present_mapping() as $key => $value) {
										echo "<option value='{$key}' " . (($view_model->get_runner_2_present() == $key && $view_model->get_runner_2_present() != '') ? 'selected' : '') . "> {$value} </option>";
									}?>
								</select>
							</div>
						</div>

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-primary" value="Buscar">
                        </div>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
        <div class="card"  id="personnel_listing">
            <h5 class="card-header">
                <div class="float-left"><?php echo $view_model->get_heading();?></div>
                <div class="float-right"></div>
                <div class="float-right"></div>
                <div class="clearfix"></div>
            </h5>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-striped">
                        <thead>
                        <?php
                        $order_by = $view_model->get_order_by();
                        $direction = $view_model->get_sort();
                        $model_base_url = $view_model->get_sort_base_url();
                        $field_column = $view_model->get_field_column();

                        foreach ($view_model->get_column() as $key => $data) {
                            $data_field = $field_column[$key];
                            if (strlen($order_by) < 1 || $data_field == '')
                            {
                                echo "<th>{$data}</th>";
                            }
                            else
                            {
                                if ($order_by === $data_field)
                                {
                                    if ($direction == 'ASC')
                                    {
                                        echo "<th><a href='{$model_base_url}?order_by={$data_field}&direction=DESC'>{$data} <i class='fas fa-sort-up' style='vertical-align: -0.35em;'></i></a></th>";
                                    }
                                    else
                                    {
                                        echo "<th><a href='{$model_base_url}?order_by={$data_field}&direction=ASC'>{$data} <i class='fas fa-sort-down' style='margin-bottom:3px;'></i></a></th>";
                                    }
                                }
                                else
                                {
                                    echo "<th><a href='{$model_base_url}?order_by={$data_field}&direction=ASC'>{$data} <i class='fas fa-sort-down'  style='margin-bottom:3px;color:#e2e2e2;'></i></a></th>";
                                }
                            }
                        } ?>
                        </thead>
                        <tbody>
                        <?php foreach ($view_model->get_list() as $data) { ?>
                            <?php
                            echo '<tr>';
							echo "<td>{$data->collegio}</td>";
							echo "<td>6</td>";
							$present_count = 0;

							if ($data->staff_lead_present == 1) {
								$present_count++;
							}

							if ($data->transmitter_present == 1) {
								$present_count++;
							}

							if ($data->delegate_1_present == 1) {
								$present_count++;
							}

							if ($data->delegate_2_present == 1) {
								$present_count++;
							}

							if ($data->runner_1_present == 1) {
								$present_count++;
							}

							if ($data->runner_2_present == 1) {
								$present_count++;
							}

							echo "<td>" . $present_count . "</td>";
							echo '<td>';
							echo '<a class="btn btn-primary btn-sm" target="__blank" href="/grassroot/personnel/edit/' . $data->id . '">Editar</a>';
							echo '&nbsp;<a class="btn btn-info btn-sm" target="__blank" href="/grassroot/personnel/view/' . $data->id . '">Ver</a>';
							echo '</td>';
                            echo '</tr>';
                            ?>
                        <?php } ?>
                        </tbody>
                    </table>
                    <p class="pagination_custom"><?php echo $view_model->get_links(); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>