<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Electores en Recinto <?php echo $recinto; ?></h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="container">
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
        <div class="card">
            <h5 class="card-header">Coordinador</h5>
        <div class="card-body" style="min-height:270px">
        <p>Nombre: <?php echo $summary['first_name'] . '&nbsp;' . $summary['last_name'];?></p>
        <p>Cédula: <?php echo $summary['government_id'];?></p>
        <p>Telefono 1: <?php echo $summary['phone_1']?></p>
        <p>Telefono 2: <?php echo $summary['phone_2'];?></p>
        <p>Correo: <?php echo $summary['email'];?></p>
        <p>Total Electores: <?php echo $summary['total'];?></p>
        <p>Votaron: <?php echo $summary['voted'];?></p>
        <p>Por votar: <?php echo $summary['left'];?></p>
        </div>
    </div>
</div>
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
        <div class="card">
            <h5 class="card-header">Votantes</h5>
        <div class="card-body" style="min-height:270px">
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-circunscripcion_1" role="tabpanel" aria-labelledby="nav-home-tab">
                <br/>
                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-striped">
                        <thead>
                        <?php foreach ($headers as $data) {
                            echo "<th>{$data}</th>";
                        } ?>
                        </thead>
                        <tbody>
                        <?php foreach ($people as $data) { ?>
                            <?php
                            echo '<tr>';
                            echo "<td>{$data['id']}</td>";
                            echo "<td>{$data['first_name']}&nbsp;{$data['last_name']}</td>";
							echo "<td>{$data['government_id']}</td>";
							echo "<td>{$data['phone_1']}</td>";
							echo "<td>{$data['phone_2']}</td>";
							echo "<td>{$data['email']}</td>";
							echo "<td>{$data['table']}</td>";
							echo "<td>{$data['recinto']}</td>";
                            echo '</tr>';
                            ?>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- voice end -->
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
</div>
<script>

</script>