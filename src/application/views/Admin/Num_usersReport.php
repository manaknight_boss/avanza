<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<h2>Reporte de Usuarios</h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (validation_errors()) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?= validation_errors() ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Filtrar</h5>
            <div class="card-body">
                <?= form_open('', ['method' => 'get']) ?>
                    <div class="row">
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Fecha Inicio">Fecha Inicio </label>
								<input type="date" class="form-control" id="start_date" name="start_date" value="<?php echo ($this->_data['start_date'] != NULL) ? $this->_data['start_date'] : '';?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Fecha Final">Fecha Final </label>
								<input type="date" class="form-control" id="end_date" name="end_date" value="<?php echo ($this->_data['end_date'] != NULL) ? $this->_data['end_date'] : '';?>"/>
							</div>
						</div>

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Buscar">
                        </div>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
        <div class="card">
            <h5 class="card-header">
                <div class="float-left">Resultados</div>
                <div class="float-right"><a class="btn btn-primary btn-sm" href="/admin/report/users?export=csv&start_date=<?php echo $this->_data['start_date'];?>&end_date=<?php echo $this->_data['end_date'];?>"><i class="fas fa-cloud-download-alt"></i></a></div>
            </h5>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-striped">
                        <thead>
								<td>Date</td>
								<td>Number of Users</td>

                        </thead>
                        <tbody><?php foreach ($list as $value) {
								echo '<tr>';
								echo "<td>" . $value['created_at'] . "</td>";
								echo "<td>" . $value['num'] . "</td>";
								echo '<tr>';
							}?></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>