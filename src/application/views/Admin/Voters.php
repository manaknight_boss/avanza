<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<h2><?php echo $view_model->get_heading();?></h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card" id="voters_filter_listing">
            <h5 class="card-header"><?php echo $view_model->get_heading();?> Buscar</h5>
            <div class="card-body">
                <?= form_open('/admin/voters/0', ['method' => 'get']) ?>
                    <div class="row">
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="ID">ID </label>
								<input type="text" class="form-control" id="id" name="id" value="<?php echo $this->_data['view_model']->get_id();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="ID Avanza">ID Avanza </label>
								<input type="text" class="form-control" id="avanza_member_id" name="avanza_member_id" value="<?php echo $this->_data['view_model']->get_avanza_member_id();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Cédula">Cédula </label>
								<input type="text" class="form-control" id="government_id" name="government_id" value="<?php echo $this->_data['view_model']->get_government_id();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Nombre">Nombre </label>
								<input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $this->_data['view_model']->get_first_name();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Apellidos">Apellidos </label>
								<input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $this->_data['view_model']->get_last_name();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Estado Civil">Estado Civil </label>
								<select name="married_status" class="form-control">
									<option value="">Todos</option>
									<?php foreach ($view_model->married_status_mapping() as $key => $value) {
										echo "<option value='{$key}' " . (($view_model->get_married_status() == $key && $view_model->get_married_status() != '') ? 'selected' : '') . "> {$value} </option>";
									}?>
								</select>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Genero">Genero </label>
								<select name="gender" class="form-control">
									<option value="">Todos</option>
									<?php foreach ($view_model->gender_mapping() as $key => $value) {
										echo "<option value='{$key}' " . (($view_model->get_gender() == $key && $view_model->get_gender() != '') ? 'selected' : '') . "> {$value} </option>";
									}?>
								</select>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Fecha nacimiento">Fecha nacimiento </label>
								<input type="date" class="form-control" id="date_of_birth" name="date_of_birth" value="<?php echo $this->_data['view_model']->get_date_of_birth();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Lugar Nacimiento">Lugar Nacimiento </label>
								<input type="text" class="form-control" id="place_of_birth" name="place_of_birth" value="<?php echo $this->_data['view_model']->get_place_of_birth();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Miembro PLD">Miembro PLD </label>
								<select name="member_pld" class="form-control">
									<option value="">Todos</option>
									<?php foreach ($view_model->member_pld_mapping() as $key => $value) {
										echo "<option value='{$key}' " . (($view_model->get_member_pld() == $key && $view_model->get_member_pld() != '') ? 'selected' : '') . "> {$value} </option>";
									}?>
								</select>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Circ.">Circ. </label>
								<input type="text" class="form-control" id="circunscripcion" name="circunscripcion" value="<?php echo $this->_data['view_model']->get_circunscripcion();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Edad">Edad </label>
								<input type="text" class="form-control" id="age" name="age" value="<?php echo $this->_data['view_model']->get_age();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Direccion">Direccion </label>
								<input type="text" class="form-control" id="address" name="address" value="<?php echo $this->_data['view_model']->get_address();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Numero">Numero </label>
								<input type="text" class="form-control" id="address_number" name="address_number" value="<?php echo $this->_data['view_model']->get_address_number();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="recinto">recinto </label>
								<input type="text" class="form-control" id="recinto" name="recinto" value="<?php echo $this->_data['view_model']->get_recinto();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Nombre del Recinto">Nombre del Recinto </label>
								<input type="text" class="form-control" id="recinto_name" name="recinto_name" value="<?php echo $this->_data['view_model']->get_recinto_name();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Direccion Recinto">Direccion Recinto </label>
								<input type="text" class="form-control" id="recinto_address" name="recinto_address" value="<?php echo $this->_data['view_model']->get_recinto_address();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="colegio">colegio </label>
								<input type="text" class="form-control" id="colegio" name="colegio" value="<?php echo $this->_data['view_model']->get_colegio();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="sector">sector </label>
								<input type="text" class="form-control" id="sector" name="sector" value="<?php echo $this->_data['view_model']->get_sector();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Nombre de sector">Nombre de sector </label>
								<input type="text" class="form-control" id="sector_name" name="sector_name" value="<?php echo $this->_data['view_model']->get_sector_name();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="codigo sub sector">codigo sub sector </label>
								<input type="text" class="form-control" id="voting_sub_sector_code" name="voting_sub_sector_code" value="<?php echo $this->_data['view_model']->get_voting_sub_sector_code();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Telefono 1">Telefono 1 </label>
								<input type="text" class="form-control" id="phone_1" name="phone_1" value="<?php echo $this->_data['view_model']->get_phone_1();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Tipo telefono 1">Tipo telefono 1 </label>
								<select name="phone_type_1" class="form-control">
									<option value="">Todos</option>
									<?php foreach ($view_model->phone_type_1_mapping() as $key => $value) {
										echo "<option value='{$key}' " . (($view_model->get_phone_type_1() == $key && $view_model->get_phone_type_1() != '') ? 'selected' : '') . "> {$value} </option>";
									}?>
								</select>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Telefono 2">Telefono 2 </label>
								<input type="text" class="form-control" id="phone_2" name="phone_2" value="<?php echo $this->_data['view_model']->get_phone_2();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Tipo telefono 2">Tipo telefono 2 </label>
								<select name="phone_type_2" class="form-control">
									<option value="">Todos</option>
									<?php foreach ($view_model->phone_type_2_mapping() as $key => $value) {
										echo "<option value='{$key}' " . (($view_model->get_phone_type_2() == $key && $view_model->get_phone_type_2() != '') ? 'selected' : '') . "> {$value} </option>";
									}?>
								</select>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Correo">Correo </label>
								<input type="text" class="form-control" id="email" name="email" value="<?php echo $this->_data['view_model']->get_email();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="facebook">facebook </label>
								<input type="text" class="form-control" id="facebook" name="facebook" value="<?php echo $this->_data['view_model']->get_facebook();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="twitter">twitter </label>
								<input type="text" class="form-control" id="twitter" name="twitter" value="<?php echo $this->_data['view_model']->get_twitter();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="instagram">instagram </label>
								<input type="text" class="form-control" id="instagram" name="instagram" value="<?php echo $this->_data['view_model']->get_instagram();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Voto en primarias">Voto en primarias </label>
								<input type="text" class="form-control" id="vote_in_primary" name="vote_in_primary" value="<?php echo $this->_data['view_model']->get_vote_in_primary();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Id coordinador">Id coordinador </label>
								<input type="text" class="form-control" id="cordinator_id" name="cordinator_id" value="<?php echo $this->_data['view_model']->get_cordinator_id();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Es coordinador">Es coordinador </label>
								<select name="is_cordinator" class="form-control">
									<option value="">Todos</option>
									<?php foreach ($view_model->is_cordinator_mapping() as $key => $value) {
										echo "<option value='{$key}' " . (($view_model->get_is_cordinator() == $key && $view_model->get_is_cordinator() != '') ? 'selected' : '') . "> {$value} </option>";
									}?>
								</select>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="ID ocupacion">ID ocupacion </label>
								<input type="text" class="form-control" id="occupation_id" name="occupation_id" value="<?php echo $this->_data['view_model']->get_occupation_id();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Aistencia de gobierno">Aistencia de gobierno </label>
								<input type="text" class="form-control" id="government_sponsor" name="government_sponsor" value="<?php echo $this->_data['view_model']->get_government_sponsor();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Vota en el exterior">Vota en el exterior </label>
								<select name="is_vote_external" class="form-control">
									<option value="">Todos</option>
									<?php foreach ($view_model->is_vote_external_mapping() as $key => $value) {
										echo "<option value='{$key}' " . (($view_model->get_is_vote_external() == $key && $view_model->get_is_vote_external() != '') ? 'selected' : '') . "> {$value} </option>";
									}?>
								</select>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Mesa madre">Mesa madre </label>
								<input type="text" class="form-control" id="parent_voting_table" name="parent_voting_table" value="<?php echo $this->_data['view_model']->get_parent_voting_table();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Turno Votacion">Turno Votacion </label>
								<input type="text" class="form-control" id="voter_turn" name="voter_turn" value="<?php echo $this->_data['view_model']->get_voter_turn();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Votó en Municipales2020">Votó en Municipales2020 </label>
								<input type="text" class="form-control" id="has_voted_municipal_2020" name="has_voted_municipal_2020" value="<?php echo $this->_data['view_model']->get_has_voted_municipal_2020();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Votó en Generales2020">Votó en Generales2020 </label>
								<input type="text" class="form-control" id="has_voted_general_2020" name="has_voted_general_2020" value="<?php echo $this->_data['view_model']->get_has_voted_general_2020();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Recibió llamado">Recibió llamado </label>
								<input type="text" class="form-control" id="has_contact_call" name="has_contact_call" value="<?php echo $this->_data['view_model']->get_has_contact_call();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Recibió encuesta">Recibió encuesta </label>
								<input type="text" class="form-control" id="has_contact_call_poll" name="has_contact_call_poll" value="<?php echo $this->_data['view_model']->get_has_contact_call_poll();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Recibió SMS">Recibió SMS </label>
								<input type="text" class="form-control" id="has_contact_sms" name="has_contact_sms" value="<?php echo $this->_data['view_model']->get_has_contact_sms();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Recibió encuesta SMS">Recibió encuesta SMS </label>
								<input type="text" class="form-control" id="has_contact_sms_poll" name="has_contact_sms_poll" value="<?php echo $this->_data['view_model']->get_has_contact_sms_poll();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Recibió correo">Recibió correo </label>
								<input type="text" class="form-control" id="has_contact_email" name="has_contact_email" value="<?php echo $this->_data['view_model']->get_has_contact_email();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
						</div>

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-primary" value="Buscar">
                        </div>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
        <div class="card"  id="voters_listing">
            <h5 class="card-header">
                <div class="float-left"><?php echo $view_model->get_heading();?></div>
                <div class="float-right"><a class="btn btn-primary btn-sm" target="__blank" href="/admin/voters/add"><i class="fas fa-plus-circle"></i></a></div>
                <div class="float-right"></div>
                <div class="clearfix"></div>
            </h5>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-striped">
                        <thead>
                        <?php
                        $order_by = $view_model->get_order_by();
                        $direction = $view_model->get_sort();
                        $model_base_url = $view_model->get_sort_base_url();
                        $field_column = $view_model->get_field_column();
                        $clean_mode = $view_model->get_format_layout();
                        $format_mode = '';
                        if ($clean_mode) {
                            $format_mode = '&layout_clean_mode=1';
                        }
                        foreach ($view_model->get_column() as $key => $data) {
                            $data_field = $field_column[$key];
                            if (strlen($order_by) < 1 || $data_field == '')
                            {
                                echo "<th>{$data}</th>";
                            }
                            else
                            {
                                if ($order_by === $data_field)
                                {
                                    if ($direction == 'ASC')
                                    {
                                        echo "<th><a href='{$model_base_url}?order_by={$data_field}{$format_mode}&direction=DESC'>{$data} <i class='fas fa-sort-up' style='vertical-align: -0.35em;'></i></a></th>";
                                    }
                                    else
                                    {
                                        echo "<th><a href='{$model_base_url}?order_by={$data_field}{$format_mode}&direction=ASC'>{$data} <i class='fas fa-sort-down' style='margin-bottom:3px;'></i></a></th>";
                                    }
                                }
                                else
                                {
                                    echo "<th><a href='{$model_base_url}?order_by={$data_field}{$format_mode}&direction=ASC'>{$data} <i class='fas fa-sort-down'  style='margin-bottom:3px;color:#e2e2e2;'></i></a></th>";
                                }
                            }
                        } ?>
                        </thead>
                        <tbody>
                        <?php foreach ($view_model->get_list() as $data) { ?>
                            <?php
                            echo '<tr>';
							echo "<td>{$data->id}</td>";
							echo "<td>{$data->avanza_member_id}</td>";
							echo "<td>{$data->government_id}</td>";
							echo "<td>{$data->first_name}</td>";
							echo "<td>{$data->last_name}</td>";
							echo "<td>{$view_model->married_status_mapping()[$data->married_status]}</td>";
							echo "<td>{$view_model->gender_mapping()[$data->gender]}</td>";
							echo "<td>{$data->date_of_birth}</td>";
							echo "<td>{$data->place_of_birth}</td>";
							echo "<td>{$view_model->member_pld_mapping()[$data->member_pld]}</td>";
							echo "<td>{$data->circunscripcion}</td>";
							echo "<td>{$data->age}</td>";
							echo "<td>{$data->address}</td>";
							echo "<td>{$data->address_number}</td>";
							echo "<td>{$data->recinto}</td>";
							echo "<td>{$data->recinto_name}</td>";
							echo "<td>{$data->recinto_address}</td>";
							echo "<td>{$data->colegio}</td>";
							echo "<td>{$data->sector}</td>";
							echo "<td>{$data->sector_name}</td>";
							echo "<td>{$data->voting_sub_sector_code}</td>";
							echo "<td>{$data->phone_1}</td>";
							echo "<td>{$view_model->phone_type_1_mapping()[$data->phone_type_1]}</td>";
							echo "<td>{$data->phone_2}</td>";
							echo "<td>{$view_model->phone_type_2_mapping()[$data->phone_type_2]}</td>";
							echo "<td>{$data->email}</td>";
							echo "<td>{$data->facebook}</td>";
							echo "<td>{$data->twitter}</td>";
							echo "<td>{$data->instagram}</td>";
							echo "<td>{$data->vote_in_primary}</td>";
							echo "<td>{$data->cordinator_id}</td>";
							echo "<td>{$view_model->is_cordinator_mapping()[$data->is_cordinator]}</td>";
							echo "<td>{$data->occupation_id}</td>";
							echo "<td>{$data->government_sponsor}</td>";
							echo "<td>{$view_model->is_vote_external_mapping()[$data->is_vote_external]}</td>";
							echo "<td>{$data->parent_voting_table}</td>";
							echo "<td>{$data->voter_turn}</td>";
							echo "<td>{$data->has_voted_municipal_2020}</td>";
							echo "<td>{$data->has_voted_general_2020}</td>";
							echo "<td>{$data->has_contact_call}</td>";
							echo "<td>{$data->has_contact_call_poll}</td>";
							echo "<td>{$data->has_contact_sms}</td>";
							echo "<td>{$data->has_contact_sms_poll}</td>";
							echo "<td>{$data->has_contact_email}</td>";
							echo '<td>';
							echo '<a class="btn btn-primary btn-sm" target="__blank" href="/admin/voters/edit/' . $data->id . '">Editar</a>';
							echo ' <a class="btn btn-warning btn-sm" target="__blank" href="/admin/voters/view/' . $data->id . '">Ver</a>';
							echo '</td>';
                            echo '</tr>';
                            ?>
                        <?php } ?>
                        </tbody>
                    </table>
                    <p class="pagination_custom"><?php echo $view_model->get_links(); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>