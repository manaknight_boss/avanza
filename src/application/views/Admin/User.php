<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<h2><?php echo $view_model->get_heading();?></h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card" id="user_filter_listing">
            <h5 class="card-header"><?php echo $view_model->get_heading();?> Buscar</h5>
            <div class="card-body">
                <?= form_open('/admin/users/0', ['method' => 'get']) ?>
                    <div class="row">
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="ID">ID </label>
								<input type="text" class="form-control" id="id" name="id" value="<?php echo $this->_data['view_model']->get_id();?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Correo">Correo </label>
								<input type="text" class="form-control" id="email" name="email" value="<?php echo $this->_data['view_model']->get_email();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Nombre">Nombre </label>
								<input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $this->_data['view_model']->get_first_name();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Apellido">Apellido </label>
								<input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $this->_data['view_model']->get_last_name();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Papel">Papel </label>
								<select name="role_id" class="form-control">
									<option value="">Todos</option>
									<?php foreach ($view_model->role_id_mapping() as $key => $value) {
										echo "<option value='{$key}' " . (($view_model->get_role_id() == $key && $view_model->get_role_id() != '') ? 'selected' : '') . "> {$value} </option>";
									}?>
								</select>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Estado">Estado </label>
								<select name="status" class="form-control">
									<option value="">Todos</option>
									<?php foreach ($view_model->status_mapping() as $key => $value) {
										echo "<option value='{$key}' " . (($view_model->get_status() == $key && $view_model->get_status() != '') ? 'selected' : '') . "> {$value} </option>";
									}?>
								</select>
							</div>
						</div>

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-primary" value="Buscar">
                        </div>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
        <div class="card"  id="user_listing">
            <h5 class="card-header">
                <div class="float-left"><?php echo $view_model->get_heading();?></div>
                <div class="float-right"><a class="btn btn-primary btn-sm" target="__blank" href="/admin/users/add"><i class="fas fa-plus-circle"></i></a></div>
                <div class="float-right"></div>
                <div class="clearfix"></div>
            </h5>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-striped">
                        <thead>
                        <?php
                        $order_by = $view_model->get_order_by();
                        $direction = $view_model->get_sort();
                        $model_base_url = $view_model->get_sort_base_url();
                        $field_column = $view_model->get_field_column();
                        $clean_mode = $view_model->get_format_layout();
                        $format_mode = '';
                        if ($clean_mode) {
                            $format_mode = '&layout_clean_mode=1';
                        }
                        foreach ($view_model->get_column() as $key => $data) {
                            $data_field = $field_column[$key];
                            if (strlen($order_by) < 1 || $data_field == '')
                            {
                                echo "<th>{$data}</th>";
                            }
                            else
                            {
                                if ($order_by === $data_field)
                                {
                                    if ($direction == 'ASC')
                                    {
                                        echo "<th><a href='{$model_base_url}?order_by={$data_field}{$format_mode}&direction=DESC'>{$data} <i class='fas fa-sort-up' style='vertical-align: -0.35em;'></i></a></th>";
                                    }
                                    else
                                    {
                                        echo "<th><a href='{$model_base_url}?order_by={$data_field}{$format_mode}&direction=ASC'>{$data} <i class='fas fa-sort-down' style='margin-bottom:3px;'></i></a></th>";
                                    }
                                }
                                else
                                {
                                    echo "<th><a href='{$model_base_url}?order_by={$data_field}{$format_mode}&direction=ASC'>{$data} <i class='fas fa-sort-down'  style='margin-bottom:3px;color:#e2e2e2;'></i></a></th>";
                                }
                            }
                        } ?>
                        </thead>
                        <tbody>
                        <?php foreach ($view_model->get_list() as $data) { ?>
                            <?php
                            echo '<tr>';
							echo "<td>{$data->id}</td>";
							echo "<td><div class='mkd-image-container'><img class='img-fluid' src='{$data->image}' onerror=\"if (this.src != '/uploads/placeholder.jpg') this.src = '/uploads/placeholder.jpg';\"/></div></td>";
							echo'<td>' . $data->email . "<br/>" . $data->first_name . "<br/>" . $data->last_name . "<br/>" . $view_model->role_id_mapping()[$data->role_id] . "<br/>" . $data->phone . "<br/>" . '</td>';
							echo "<td>{$view_model->status_mapping()[$data->status]}</td>";
							echo '<td>';
							echo '<a class="btn btn-primary btn-sm" target="__blank" href="/admin/users/edit/' . $data->id . '">Editar</a>';
							echo ' <a class="btn btn-warning btn-sm" target="__blank" href="/admin/users/view/' . $data->id . '">Ver</a>';
							$condition = ($data->status == 0)?TRUE:FALSE;
							echo ($condition) ? "<a class='btn btn-info btn-sm' target='_blank' href='/users/approve/'>Approve</a>" : '';
							echo '</td>';
                            echo '</tr>';
                            ?>
                        <?php } ?>
                        </tbody>
                    </table>
                    <p class="pagination_custom"><?php echo $view_model->get_links(); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>