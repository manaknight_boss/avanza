<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<h2><?php echo $title;?></h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
        <div class="card">
            <h5 class="card-header">
                <div class="float-left"><?php echo $title;?></div>
                <div class="float-right"></div>
                <div class="float-right"></div>
                <div class="clearfix"></div>
            </h5>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-striped">
                        <thead>
                        <?php
                        $heading = [
                          'Tipo',
                          'Estado',
                          'Circ.',
                          'Edad',
                          'Recinto',
                          'Colegio',
                          'Genero',
                          'E. Civil',
                          'Total',
                          'Resp. 1',
                          'Resp. 2',
                          'Resp. 3',
                          'Contestaron',
                          'Contactado',
                          'No existe'
                        ];
                        foreach ($heading as $data) {
                            echo "<th>{$data}</th>";
                        } ?>
                        </thead>
                        <tbody>
                        <?php foreach ($list as $data) { ?>
                            <?php
                            echo '<tr>';
                            echo "<td>{$data->type}</td>";
                            echo "<td>{$data->status}</td>";
                            echo "<td>{$data->circunscripcion}</td>";
                            echo "<td>{$data->age}</td>";
                            echo "<td>{$data->recinto}</td>";
                            echo "<td>{$data->college}</td>";
                            echo "<td>{$data->gender}</td>";
                            echo "<td>{$data->martial_status}</td>";
                            echo "<td>{$data->total}</td>";
                            echo "<td>{$data->respond_1}</td>";
                            echo "<td>{$data->respond_2}</td>";
                            echo "<td>{$data->respond_3}</td>";
                            echo "<td>{$data->engaged}</td>";
                            echo "<td>{$data->reached}</td>";
                            echo "<td>{$data->dne}</td>";
                            echo '</tr>';
                            ?>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>