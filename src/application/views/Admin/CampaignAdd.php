<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/admin/dashboard" class="breadcrumb-link">Tablero</a></li>
						<li class="breadcrumb-item"><a href="/admin/campaigns/0" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">Añadir</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <?php if (validation_errors()) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?= validation_errors() ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($error) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($success) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Añadir <?php echo $view_model->get_heading();?></h5>
                <div class="card-body">
                <?= form_open() ?>
				<div class="form-group">
					<label for="Nombre">Nombre </label>
					<input type="text" class="form-control" id="form_name" name="name" value="<?php echo set_value('name'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Tipo">Tipo </label>
					<select id="form_type" name="type" class="form-control">
						<option value='5'> Encuesta SMS </option>
					</select>
				</div>
				<div class="form-group">
					<label for="Circ.">Circ. </label>
					<select id="form_circunscripcion" name="circunscripcion" class="form-control">
						<?php foreach ($view_model->circunscripcion_mapping() as $key => $value) {
							if ($key == 0) {
								continue;
							}
							echo "<option value='{$key}'> {$value} </option>";
						}?>
					</select>
				</div>
				<div class="form-group">
					<label for="Fuente">Fuente </label>
					<select id="form_source_id" name="source_id" class="form-control">
						<?php foreach ($view_model->source_id_mapping() as $key => $value) {
							echo "<option value='{$key}'> {$value} </option>";
						}?>
					</select>
				</div>
				<div class="form-group">
					<label for="recintos">Recintos </label>
					<select id="form_recintos" name="recinto" class="form-control">
						<?php foreach ($recinto as $key => $value) {
							echo "<option value='{$key}'> {$value} </option>";
						}?>
					</select>
				</div>
				<div class="form-group">
					<label for="Edad">Edad </label>
					<select id="form_age" name="age" class="form-control">
						<?php foreach ($view_model->age_mapping() as $key => $value) {
							echo "<option value='{$key}'> {$value} </option>";
						}?>
					</select>
				</div>
				<div class="form-group">
					<label for="Colegio">Colegio </label>
					<select id="form_college" name="college" class="form-control">
						<?php foreach ($college as $key => $value) {
							echo "<option value='{$key}'> {$value} </option>";
						}?>
					</select>
				</div>
				<div class="form-group">
					<label for="Genero">Genero </label>
					<select id="form_gender" name="gender" class="form-control">
						<?php foreach ($view_model->gender_mapping() as $key => $value) {
							echo "<option value='{$key}'> {$value} </option>";
						}?>
					</select>
				</div>
				<div class="form-group">
					<label for="Estado Civil">Estado Civil </label>
					<select id="form_married_status" name="married_status" class="form-control">
						<?php foreach ($view_model->married_status_mapping() as $key => $value) {
							echo "<option value='{$key}'> {$value} </option>";
						}?>
					</select>
				</div>

				<div class="form-group">
					<label for="Contenido">Contenido </label>
					<textarea id='form_content' name='content' class='form-control' rows='5'><?php echo set_value('content'); ?></textarea>
				</div>

                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Enviar">
                </div>
                </form>
            </div>
        </div>
    </div>
</div>