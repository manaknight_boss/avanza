<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title">Lista de llamadas CSV </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/admin/dashboard" class="breadcrumb-link">Tablero</a></li>
						<li class="breadcrumb-item active" aria-current="page">Ver</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header"> Detalles</h5>
                <div class="card-body">
						<h6>Nombre:&nbsp; <?php echo $name;?></h6>
						<h6>Voz:&nbsp; <?php echo "<a href='$voice_file'> $voice_file</a>";?></h6>
						</div>
        </div>
    </div>
</div>
<div class="row">
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
        <div class="card">
            <h5 class="card-header">
								<div class="float-left"><?php
								echo 'Lista de Llamadas';
								?></div>
                <div class="float-right">
								</div>
                <div class="float-right"></div>
                <div class="clearfix"></div>
            </h5>
            <div class="card-body">

                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-striped">
                        <thead>
												<?php
                        $headers = ['Nombre', 'Voz Date',  'Telefono', 'Res. llamada'];
												foreach ($headers as $data) {
                            echo "<th>{$data}</th>";
                        } ?>
                        </thead>
                        <tbody>
												<?php
												foreach ($list as $data) { ?>
														<?php
                            echo '<tr>';
														echo "<td>{$data->government_id}</td>";
														echo "<td>";
                            echo $data->call_date;
														echo "</td>";
														echo "<td>{$data->phone_1}<br/>{$data->phone_2}</td>";
														echo '<td>';
															if ($data->call_result == 1)
															{
																echo 'Respondida';
															}
															else if ($data->call_result == 0)
															{
																echo 'Por llamar';
															}
															else
															{
																echo 'No respondió';
															}
														echo '</td>';
                            echo '</tr>';
                            ?>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
		</div>
	</div>
</div>