<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/admin/dashboard" class="breadcrumb-link">Tablero</a></li>
						<li class="breadcrumb-item"><a href="/admin/emails/0" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">Ver</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header"><?php echo $view_model->get_heading();?> Detalles</h5>
                <div class="card-body">
						<h6>Tipo de correo:&nbsp; <?php echo $view_model->get_slug();?></h6>
						<h6>Objetivo:&nbsp; <?php echo $view_model->get_subject();?></h6>
						<h6>Etiquetas de reemplazo:&nbsp; <?php echo $view_model->get_tag();?></h6>
						<h6>Cuerpo del correo:&nbsp; <?php echo $view_model->get_html();?></h6>

                </div>
        </div>
    </div>
</div>