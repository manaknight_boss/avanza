<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/admin/dashboard" class="breadcrumb-link">Tablero</a></li>
						<li class="breadcrumb-item"><a href="/admin/voters/0" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">Editar</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <?php if (validation_errors()) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?= validation_errors() ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($error) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($success) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Editar <?php echo $view_model->get_heading();?></h5>
                <div class="card-body">
                <?= form_open() ?>
				<div class="form-group">
					<label for="ID Avanza">ID Avanza </label>
					<input type="text" class="form-control" id="form_avanza_member_id" name="avanza_member_id" value="<?php echo set_value('avanza_member_id', $this->_data['view_model']->get_avanza_member_id());?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Cédula">Cédula </label>
					<input type="text" class="form-control" id="form_government_id" name="government_id" value="<?php echo set_value('government_id', $this->_data['view_model']->get_government_id());?>"/>
				</div>
				<div class="form-group">
					<label for="Nombre">Nombre </label>
					<input type="text" class="form-control" id="form_first_name" name="first_name" value="<?php echo set_value('first_name', $this->_data['view_model']->get_first_name());?>"/>
				</div>
				<div class="form-group">
					<label for="Apellidos">Apellidos </label>
					<input type="text" class="form-control" id="form_last_name" name="last_name" value="<?php echo set_value('last_name', $this->_data['view_model']->get_last_name());?>"/>
				</div>
				<div class="form-group">
					<label for="Estado Civil">Estado Civil </label>
					<select id="form_married_status" name="married_status" class="form-control">
						<?php foreach ($view_model->married_status_mapping() as $key => $value) {
							echo "<option value='{$key}' " . (($view_model->get_married_status() == $key && $view_model->get_married_status() != '') ? 'selected' : '') . "> {$value} </option>";
						}?>
					</select>
				</div>
				<div class="form-group">
					<label for="Genero">Genero </label>
					<select id="form_gender" name="gender" class="form-control">
						<?php foreach ($view_model->gender_mapping() as $key => $value) {
							echo "<option value='{$key}' " . (($view_model->get_gender() == $key && $view_model->get_gender() != '') ? 'selected' : '') . "> {$value} </option>";
						}?>
					</select>
				</div>
				<div class="form-group">
					<label for="Fecha nacimiento">Fecha nacimiento </label>
					<input type="date" class="form-control" id="form_date_of_birth" name="date_of_birth" value="<?php echo set_value('date_of_birth', $this->_data['view_model']->get_date_of_birth());?>"/>
				</div>
				<div class="form-group">
					<label for="Lugar Nacimiento">Lugar Nacimiento </label>
					<input type="text" class="form-control" id="form_place_of_birth" name="place_of_birth" value="<?php echo set_value('place_of_birth', $this->_data['view_model']->get_place_of_birth());?>"/>
				</div>
				<div class="form-group">
					<label for="Miembro PLD">Miembro PLD </label>
					<select id="form_member_pld" name="member_pld" class="form-control">
						<?php foreach ($view_model->member_pld_mapping() as $key => $value) {
							echo "<option value='{$key}' " . (($view_model->get_member_pld() == $key && $view_model->get_member_pld() != '') ? 'selected' : '') . "> {$value} </option>";
						}?>
					</select>
				</div>
				<div class="form-group">
					<label for="Circ.">Circ. </label>
					<input type="text" class="form-control" id="form_circunscripcion" name="circunscripcion" value="<?php echo set_value('circunscripcion', $this->_data['view_model']->get_circunscripcion());?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Edad">Edad </label>
					<input type="text" class="form-control" id="form_age" name="age" value="<?php echo set_value('age', $this->_data['view_model']->get_age());?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Direccion">Direccion </label>
					<input type="text" class="form-control" id="form_address" name="address" value="<?php echo set_value('address', $this->_data['view_model']->get_address());?>"/>
				</div>
				<div class="form-group">
					<label for="Numero">Numero </label>
					<input type="text" class="form-control" id="form_address_number" name="address_number" value="<?php echo set_value('address_number', $this->_data['view_model']->get_address_number());?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="recinto">recinto </label>
					<input type="text" class="form-control" id="form_recinto" name="recinto" value="<?php echo set_value('recinto', $this->_data['view_model']->get_recinto());?>"/>
				</div>
				<div class="form-group">
					<label for="Nombre del Recinto">Nombre del Recinto </label>
					<input type="text" class="form-control" id="form_recinto_name" name="recinto_name" value="<?php echo set_value('recinto_name', $this->_data['view_model']->get_recinto_name());?>"/>
				</div>
				<div class="form-group">
					<label for="Direccion Recinto">Direccion Recinto </label>
					<input type="text" class="form-control" id="form_recinto_address" name="recinto_address" value="<?php echo set_value('recinto_address', $this->_data['view_model']->get_recinto_address());?>"/>
				</div>
				<div class="form-group">
					<label for="colegio">colegio </label>
					<input type="text" class="form-control" id="form_colegio" name="colegio" value="<?php echo set_value('colegio', $this->_data['view_model']->get_colegio());?>"/>
				</div>
				<div class="form-group">
					<label for="sector">sector </label>
					<input type="text" class="form-control" id="form_sector" name="sector" value="<?php echo set_value('sector', $this->_data['view_model']->get_sector());?>"/>
				</div>
				<div class="form-group">
					<label for="Nombre de sector">Nombre de sector </label>
					<input type="text" class="form-control" id="form_sector_name" name="sector_name" value="<?php echo set_value('sector_name', $this->_data['view_model']->get_sector_name());?>"/>
				</div>
				<div class="form-group">
					<label for="codigo sub sector">codigo sub sector </label>
					<input type="text" class="form-control" id="form_voting_sub_sector_code" name="voting_sub_sector_code" value="<?php echo set_value('voting_sub_sector_code', $this->_data['view_model']->get_voting_sub_sector_code());?>"/>
				</div>
				<div class="form-group">
					<label for="Telefono 1">Telefono 1 </label>
					<input type="text" class="form-control" id="form_phone_1" name="phone_1" value="<?php echo set_value('phone_1', $this->_data['view_model']->get_phone_1());?>"/>
				</div>
				<div class="form-group">
					<label for="Tipo telefono 1">Tipo telefono 1 </label>
					<select id="form_phone_type_1" name="phone_type_1" class="form-control">
						<?php foreach ($view_model->phone_type_1_mapping() as $key => $value) {
							echo "<option value='{$key}' " . (($view_model->get_phone_type_1() == $key && $view_model->get_phone_type_1() != '') ? 'selected' : '') . "> {$value} </option>";
						}?>
					</select>
				</div>
				<div class="form-group">
					<label for="Telefono 2">Telefono 2 </label>
					<input type="text" class="form-control" id="form_phone_2" name="phone_2" value="<?php echo set_value('phone_2', $this->_data['view_model']->get_phone_2());?>"/>
				</div>
				<div class="form-group">
					<label for="Tipo telefono 2">Tipo telefono 2 </label>
					<select id="form_phone_type_2" name="phone_type_2" class="form-control">
						<?php foreach ($view_model->phone_type_2_mapping() as $key => $value) {
							echo "<option value='{$key}' " . (($view_model->get_phone_type_2() == $key && $view_model->get_phone_type_2() != '') ? 'selected' : '') . "> {$value} </option>";
						}?>
					</select>
				</div>
				<div class="form-group">
					<label for="Correo">Correo </label>
					<input type="text" class="form-control" id="form_email" name="email" value="<?php echo set_value('email', $this->_data['view_model']->get_email());?>"/>
				</div>
				<div class="form-group">
					<label for="facebook">facebook </label>
					<input type="text" class="form-control" id="form_facebook" name="facebook" value="<?php echo set_value('facebook', $this->_data['view_model']->get_facebook());?>"/>
				</div>
				<div class="form-group">
					<label for="twitter">twitter </label>
					<input type="text" class="form-control" id="form_twitter" name="twitter" value="<?php echo set_value('twitter', $this->_data['view_model']->get_twitter());?>"/>
				</div>
				<div class="form-group">
					<label for="instagram">instagram </label>
					<input type="text" class="form-control" id="form_instagram" name="instagram" value="<?php echo set_value('instagram', $this->_data['view_model']->get_instagram());?>"/>
				</div>
				<div class="form-group">
					<label for="Voto en primarias">Voto en primarias </label>
					<input type="text" class="form-control" id="form_vote_in_primary" name="vote_in_primary" value="<?php echo set_value('vote_in_primary', $this->_data['view_model']->get_vote_in_primary());?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Id coordinador">Id coordinador </label>
					<input type="text" class="form-control" id="form_cordinator_id" name="cordinator_id" value="<?php echo set_value('cordinator_id', $this->_data['view_model']->get_cordinator_id());?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Es coordinador">Es coordinador </label>
					<select id="form_is_cordinator" name="is_cordinator" class="form-control">
						<?php foreach ($view_model->is_cordinator_mapping() as $key => $value) {
							echo "<option value='{$key}' " . (($view_model->get_is_cordinator() == $key && $view_model->get_is_cordinator() != '') ? 'selected' : '') . "> {$value} </option>";
						}?>
					</select>
				</div>
				<div class="form-group">
					<label for="ID ocupacion">ID ocupacion </label>
					<input type="text" class="form-control" id="form_occupation_id" name="occupation_id" value="<?php echo set_value('occupation_id', $this->_data['view_model']->get_occupation_id());?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Aistencia de gobierno">Aistencia de gobierno </label>
					<input type="text" class="form-control" id="form_government_sponsor" name="government_sponsor" value="<?php echo set_value('government_sponsor', $this->_data['view_model']->get_government_sponsor());?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Vota en el exterior">Vota en el exterior </label>
					<select id="form_is_vote_external" name="is_vote_external" class="form-control">
						<?php foreach ($view_model->is_vote_external_mapping() as $key => $value) {
							echo "<option value='{$key}' " . (($view_model->get_is_vote_external() == $key && $view_model->get_is_vote_external() != '') ? 'selected' : '') . "> {$value} </option>";
						}?>
					</select>
				</div>
				<div class="form-group">
					<label for="Mesa madre">Mesa madre </label>
					<input type="text" class="form-control" id="form_parent_voting_table" name="parent_voting_table" value="<?php echo set_value('parent_voting_table', $this->_data['view_model']->get_parent_voting_table());?>"/>
				</div>
				<div class="form-group">
					<label for="Turno Votacion">Turno Votacion </label>
					<input type="text" class="form-control" id="form_voter_turn" name="voter_turn" value="<?php echo set_value('voter_turn', $this->_data['view_model']->get_voter_turn());?>"/>
				</div>
				<div class="form-group">
					<label for="Votó en Municipales2020">Votó en Municipales2020 </label>
					<input type="text" class="form-control" id="form_has_voted_municipal_2020" name="has_voted_municipal_2020" value="<?php echo set_value('has_voted_municipal_2020', $this->_data['view_model']->get_has_voted_municipal_2020());?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Votó en Generales2020">Votó en Generales2020 </label>
					<input type="text" class="form-control" id="form_has_voted_general_2020" name="has_voted_general_2020" value="<?php echo set_value('has_voted_general_2020', $this->_data['view_model']->get_has_voted_general_2020());?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Recibió llamado">Recibió llamado </label>
					<input type="text" class="form-control" id="form_has_contact_call" name="has_contact_call" value="<?php echo set_value('has_contact_call', $this->_data['view_model']->get_has_contact_call());?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Recibió encuesta">Recibió encuesta </label>
					<input type="text" class="form-control" id="form_has_contact_call_poll" name="has_contact_call_poll" value="<?php echo set_value('has_contact_call_poll', $this->_data['view_model']->get_has_contact_call_poll());?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Recibió SMS">Recibió SMS </label>
					<input type="text" class="form-control" id="form_has_contact_sms" name="has_contact_sms" value="<?php echo set_value('has_contact_sms', $this->_data['view_model']->get_has_contact_sms());?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Recibió encuesta SMS">Recibió encuesta SMS </label>
					<input type="text" class="form-control" id="form_has_contact_sms_poll" name="has_contact_sms_poll" value="<?php echo set_value('has_contact_sms_poll', $this->_data['view_model']->get_has_contact_sms_poll());?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>
				<div class="form-group">
					<label for="Recibió correo">Recibió correo </label>
					<input type="text" class="form-control" id="form_has_contact_email" name="has_contact_email" value="<?php echo set_value('has_contact_email', $this->_data['view_model']->get_has_contact_email());?>" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || (event.charCode == 45)"/>
				</div>


                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Enviar">
                </div>
                </form>
            </div>
        </div>
    </div>
</div>