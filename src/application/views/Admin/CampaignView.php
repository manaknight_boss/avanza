<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/admin/dashboard" class="breadcrumb-link">Tablero</a></li>
						<li class="breadcrumb-item"><a href="/admin/campaigns/0" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">Ver</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header"><?php echo $view_model->get_heading();?> Detalles</h5>
                <div class="card-body">
						<h6>Id:&nbsp; <?php echo $view_model->get_id();?></h6>
						<h6>Nombre:&nbsp; <?php echo $view_model->get_name();?></h6>
						<h6>Tipo:&nbsp; <?php echo $view_model->type_mapping()[$view_model->get_type()];?></h6>
						<h6>Circ.:&nbsp; <?php echo $view_model->circunscripcion_mapping()[$view_model->get_circunscripcion()];?></h6>
						<h6>Fuente:&nbsp; <?php echo $view_model->source_id_mapping()[$view_model->get_source_id()];?></h6>
						<h6>Recinto:&nbsp; <?php echo ($view_model->get_recinto() == 0) ? 'Todos' : $recinto[$view_model->get_recinto()];?></h6>
						<h6>Colegio:&nbsp; <?php echo ($view_model->get_college() == 0) ? 'Todos' : $college[$view_model->get_college()];?></h6>
						<h6>Genero:&nbsp; <?php echo $view_model->gender_mapping()[$view_model->get_gender()];?></h6>
						<h6>Edad:&nbsp; <?php echo $view_model->age_mapping()[$view_model->get_age()];?></h6>
						<h6>E. Civil:&nbsp; <?php echo $view_model->get_married_status();?></h6>
						<h6>Estado:&nbsp; <?php echo $view_model->status_mapping()[$view_model->get_status()];?></h6>
						<h6>Contenido:&nbsp; <?php echo $view_model->get_content();?></h6>
						<h6>Votantes:&nbsp; <?php echo $view_model->get_total();?></h6>
						<?php
						if ($view_model->get_status() == 0 && count($list) > 0)
						{
							echo '&nbsp;<a class="btn btn-danger btn-sm" href="/admin/campaign/build/' . $view_model->get_id() .  '">Iniciar campaña</a>';
						}
						?>

					</div>
        </div>
    </div>
</div>
<div class="row">
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
        <div class="card">
            <h5 class="card-header">
								<div class="float-left"><?php
								if ($view_model->get_type() == 2 || $view_model->get_type() == 4) {
								echo 'Lista de Llamadas';
								}
								else if ($view_model->get_type() == 3 || $view_model->get_type() == 5) {
									echo 'Lista de SMS';
								}
								else {
									echo 'Lista de correos';
								}
								?></div>
                <div class="float-right"></div>
                <div class="float-right"></div>
                <div class="clearfix"></div>
            </h5>
            <div class="card-body">

                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-striped">
                        <thead>
												<?php
												if ($view_model->get_type() == 2 || $view_model->get_type() == 4) {
													$headers = ['Cédula', 'Fecha Llamada',  'Telefono', 'Res. llamada', 'Acción' ];
												} else if ($view_model->get_type() == 3 || $view_model->get_type() == 5) {
													$headers = ['Cédula', 'SMSDate',  'Telefono', 'Res. SMS', 'Acción' ];
												} else {
													$headers = ['Cédula', 'CorreoDate',  'Telefono', 'Res. Correo', 'Acción' ];
												}
												foreach ($headers as $data) {
                            echo "<th>{$data}</th>";
                        } ?>
                        </thead>
                        <tbody>
												<?php
												foreach ($list as $data) { ?>
														<?php
                            echo '<tr>';
														echo "<td>{$data->government_id} ({$data->first_name} {$data->last_name})</td>";
														echo "<td>";
														if ($view_model->get_type() == 2 || $view_model->get_type() == 4) {
															echo $data->call_date;
														}
														if ($view_model->get_type() == 3 || $view_model->get_type() == 5) {
															echo $data->sms_date;
														}
														echo "</td>";
														echo "<td>{$data->phone_1}<br/>{$data->phone_2}</td>";
														echo '<td>';
														if ($view_model->get_type() == 2)
														{
															if ($data->call_result == 1)
															{
																echo 'Respondida';
															}
															else if ($data->call_result == 0)
															{
																echo 'Por llamar';
															}
															else
															{
																echo 'No respondió';
															}
														}
														if ($view_model->get_type() == 3)
														{
															if ($data->sms_result == 1)
															{
																echo 'Enviado';
															}
															else if ($data->sms_result == 0)
															{
																echo 'Por enviar';
															}
															else
															{
																echo 'No respondió';
															}
														}
														if ($view_model->get_type() == 4 || $view_model->get_type() == 5)
														{
															if ($data->poll_result == 1)
															{
																echo 'Resp. 1';
															}
															else if ($data->poll_result == 2)
															{
																echo 'Resp. 2';
															}
															else if ($data->poll_result == 3)
															{
																echo 'Resp. 3';
															}
															else
															{
																echo 'No respondió';
															}
														}
														echo '</td>';
														echo '<td>';
														if ($view_model->get_type() == 2 && $data->call_result == 0)
														{
															echo '<a class="btn btn-warning btn-sm" target="_blank" href="/admin/campaign/' . $campaign_id . '/call/' . $data->government_id . '">Llamar</a>';
														}
														if ($view_model->get_type() == 3 && $data->sms_result == 0)
														{
															echo '<a class="btn btn-warning btn-sm" target="_blank" href="/admin/campaign/' . $campaign_id . '/sms/' . $data->government_id . '">Enviar SMS</a>';
														}
														if ($view_model->get_type() == 4 && $data->poll_result == 0)
														{
															echo '<a class="btn btn-warning btn-sm" target="_blank" href="/admin/campaign/' . $campaign_id . '/callresponse/' . $data->government_id . '/1">Resp. 1</a>';
															echo '<a class="btn btn-warning btn-sm" target="_blank" href="/admin/campaign/' . $campaign_id . '/callresponse/' . $data->government_id . '/2">Resp. 2</a>';
															echo '<a class="btn btn-warning btn-sm" target="_blank" href="/admin/campaign/' . $campaign_id . '/callresponse/' . $data->government_id . '/3">Resp. 3</a>';
														}
														if ($view_model->get_type() == 5 && $data->poll_result == 0)
														{
															echo '<a class="btn btn-warning btn-sm" target="_blank" href="/admin/campaign/' . $campaign_id . '/smsresponse/' . $data->government_id . '/1">Resp. 1</a>';
															echo '<a class="btn btn-warning btn-sm" target="_blank" href="/admin/campaign/' . $campaign_id . '/smsresponse/' . $data->government_id . '/2">Resp. 2</a>';
															echo '<a class="btn btn-warning btn-sm" target="_blank" href="/admin/campaign/' . $campaign_id . '/smsresponse/' . $data->government_id . '/3">Resp. 3</a>';
														}
														echo '</td>';
                            echo '</tr>';
                            ?>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
		</div>
	</div>
</div>