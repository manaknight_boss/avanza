<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2>Tablero <a href="/admin/dashboard/real" class="btn btn-primary">Actualizar Tablero</a></h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="container">
    <div class="row">
    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 mt-4">
        <div class="card">
            <h5 class="card-header">SMS Global</h5>
            <div class="card-body" style="min-height:270px">
            <canvas id="sms_dashboard_chart"></canvas>
            <?php echo "<div id='total_voters' data-num='{$total_voters}'> Votantes: " . $total_voters;?></div>
            <?php echo "<div id='total_sms_reach' data-num='{$total_sms_reach}'> Contactados: " . $total_sms_reach;?></div>
            <?php echo "<div id='total_sms_no_response' data-num='{$total_sms_no_response}'> No Resp.: " . $total_sms_no_response;?></div>
            <?php echo "<div id='total_sms_sent' data-num='{$total_sms_sent}'> Enviados: " . $total_sms_sent;?></div>
            <?php echo "<div id='total_sms_poll' data-num='{$total_sms_poll}'> Encuestas: " . $total_sms_poll;?></div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 mt-4">
        <div class="card">
            <h5 class="card-header">Llamadas Global</h5>
            <div class="card-body" style="min-height:270px">
            <canvas id="voice_dashboard_chart"></canvas>
            <?php echo "<div id='total_voters' data-num='{$total_voters}'> Votantes: " . $total_voters;?></div>
            <?php echo "<div id='total_voice_reach' data-num='{$total_voice_reach}'> Contactados: " . $total_voice_reach;?></div>
            <?php echo "<div id='total_voice_no_response' data-num='{$total_voice_no_response}'> No Resp.: " . $total_voice_no_response;?></div>
            <?php echo "<div id='total_voice_sent' data-num='{$total_voice_sent}'> Enviados: " . $total_voice_sent;?></div>
            <?php echo "<div id='total_voice_poll' data-num='{$total_voice_poll}'> Encuestas: " . $total_voice_poll;?></div>
            </div>
        </div>
    </div>
    <!-- <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3 mt-3">
        <div class="card">
            <h5 class="card-header">Correos Global</h5>
            <div class="card-body" style="min-height:270px">
            <canvas id="email_dashboard_chart"></canvas>
            <?php echo "<div id='total_voters' data-num='{$total_voters}'> Votantes: " . $total_voters;?></div>
            <?php echo "<div id='total_email_reach' data-num='{$total_email_reach}'> Contactados: " . $total_email_reach;?></div>
            <?php echo "<div id='total_email_sent' data-num='{$total_email_sent}'> Enviados: " . $total_email_sent;?></div>
            </div>
        </div>
    </div> -->
    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 mt-4">
        <div class="card">
            <h5 class="card-header">Resultados encuestas</h5>
            <div class="card-body" style="min-height:270px">
                <canvas id="poll_dashboard_chart"></canvas>
                <?php echo "<div id='total_poll_voice' data-num='{$total_poll_voice}'> Encuestas voz: " . $total_poll_voice;?></div>
                <?php echo "<div id='total_poll_sms' data-num='{$total_poll_sms}'> Encuestas SMS: " . $total_poll_sms;?></div>
                <?php echo "<div id='total_poll_response_1' data-num='{$total_poll_response_1}'> Resp./Opcion 1: " . $total_poll_response_1;?></div>
                <?php echo "<div id='total_poll_response_2' data-num='{$total_poll_response_2}'> Resp./Opcion 2: " . $total_poll_response_2;?></div>
                <?php echo "<div id='total_poll_response_3' data-num='{$total_poll_response_3}'> Resp./Opcion 3: " . $total_poll_response_3;?></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
        <div class="card">
            <h5 class="card-header">Detalles Globales</h5>
        <div class="card-body" style="min-height:270px">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-Voz" role="tab" aria-controls="nav-Voz" aria-selected="true">Voz</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-SMS" role="tab" aria-controls="nav-SMS" aria-selected="false">SMS</a>
                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-Encuesta" role="tab" aria-controls="nav-Encuesta" aria-selected="false">Encuesta</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-Voz" role="tabpanel" aria-labelledby="nav-home-tab">
                <!-- voice start -->
                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-striped">
                        <thead>
                        <?php foreach ($voice_headers as $data) {
                            echo "<th>{$data}</th>";
                        } ?>
                        </thead>
                        <tbody>
                        <?php foreach ($voice_data as $data) { ?>
                            <?php
                            echo '<tr>';
							echo "<td>{$data['region']}</td>";
							echo "<td>{$data['voters']}</td>";
							echo "<td>{$data['sent']}</td>";
							echo "<td>{$data['reach']}</td>";
                            echo "<td>{$data['avanza_percentage']}</td>";
                            echo "<td>{$data['tactical_percentage']}</td>";
							echo "<td>{$data['progress']}</td>";
                            echo '</tr>';
                            ?>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- voice end -->
                </div>
                <div class="tab-pane fade" id="nav-SMS" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <!-- sms start -->
                    <div class="table-responsive">
                        <table class="table table-hover table-condensed table-striped">
                            <thead>
                            <?php foreach ($sms_headers as $data) {
                                echo "<th>{$data}</th>";
                            } ?>
                            </thead>
                            <tbody>
                            <?php foreach ($sms_data as $data) { ?>
                                <?php
                                echo '<tr>';
                                echo "<td>{$data['region']}</td>";
                                echo "<td>{$data['voters']}</td>";
                                echo "<td>{$data['sent']}</td>";
                                echo "<td>{$data['reach']}</td>";
                                echo "<td>{$data['avanza_percentage']}</td>";
                                echo "<td>{$data['tactical_percentage']}</td>";
                                echo "<td>{$data['progress']}</td>";
                                echo '</tr>';
                                ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- sms end -->
                </div>
                <div class="tab-pane fade" id="nav-Encuesta" role="tabpanel" aria-labelledby="nav-contact-tab">N/A</div>
            </div>
        </div>
        </div>
    </div>
</div>
</div>
<script>

</script>