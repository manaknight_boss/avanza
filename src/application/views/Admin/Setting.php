<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<h2>Ajustes</h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
        <div class="card">
            <h5 class="card-header">
                Configuracion
            </h5>
            <div class="card-body">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="form-group">
								<label for="Site Name">Site Name </label>
								<textarea id='site_name' name='site_name' data-id="1" class='form-control mkd-setting-change' rows='3'><?php echo $list['site_name']->value;?></textarea>
							</div>
						</div>
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="form-group">
								<label for="Site Logo">Site Logo </label>
								<textarea id='site_logo' name='site_logo' data-id="2" class='form-control mkd-setting-change' rows='3'><?php echo $list['site_logo']->value;?></textarea>
							</div>
						</div>
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="form-group">
								<label for="Maintenance">Maintenance </label>
								<select name="maintenance" class="form-control mkd-setting-select-change" data-id="3">
										<?php echo "<option value='0' " . (($list['maintenance']->key == 'maintenance' && $list['maintenance']->value == "0") ? 'selected' : '') . "> No </option>";?>
										<?php echo "<option value='1' " . (($list['maintenance']->key == 'maintenance' && $list['maintenance']->value == "1") ? 'selected' : '') . "> Si </option>";?>
								</select>
							</div>
						</div>
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="form-group">
								<label for="Version">Version </label>
								<textarea id='version' name='version' data-id="4" class='form-control mkd-setting-change' rows='3'><?php echo $list['version']->value;?></textarea>
							</div>
						</div>
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<div class="form-group">
								<label for="Copyright">Copyright </label>
								<textarea id='copyright' name='copyright' data-id="5" class='form-control mkd-setting-change' rows='3'><?php echo $list['copyright']->value;?></textarea>
							</div>
						</div>

            </div>
        </div>
    </div>
</div>