<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/admin/dashboard" class="breadcrumb-link">Tablero</a></li>
						<li class="breadcrumb-item"><a href="/admin/voters/0" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">Ver</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header"><?php echo $view_model->get_heading();?> Detalles</h5>
                <div class="card-body">
						<h6>ID:&nbsp; <?php echo $view_model->get_id();?></h6>
						<h6>ID Avanza:&nbsp; <?php echo $view_model->get_avanza_member_id();?></h6>
						<h6>Cédula:&nbsp; <?php echo $view_model->get_government_id();?></h6>
						<h6>Nombre:&nbsp; <?php echo $view_model->get_first_name();?></h6>
						<h6>Apellidos:&nbsp; <?php echo $view_model->get_last_name();?></h6>
						<h6>Estado Civil:&nbsp; <?php echo $view_model->married_status_mapping()[$view_model->get_married_status()];?></h6>
						<h6>Genero:&nbsp; <?php echo $view_model->gender_mapping()[$view_model->get_gender()];?></h6>
						<h6>Fecha nacimiento:&nbsp; <?php echo $view_model->get_date_of_birth();?></h6>
						<h6>Lugar Nacimiento:&nbsp; <?php echo $view_model->get_place_of_birth();?></h6>
						<h6>Miembro PLD:&nbsp; <?php echo $view_model->member_pld_mapping()[$view_model->get_member_pld()];?></h6>
						<h6>Circ.:&nbsp; <?php echo $view_model->get_circunscripcion();?></h6>
						<h6>Edad:&nbsp; <?php echo $view_model->get_age();?></h6>
						<h6>Direccion:&nbsp; <?php echo $view_model->get_address();?></h6>
						<h6>Numero:&nbsp; <?php echo $view_model->get_address_number();?></h6>
						<h6>recinto:&nbsp; <?php echo $view_model->get_recinto();?></h6>
						<h6>Nombre del Recinto:&nbsp; <?php echo $view_model->get_recinto_name();?></h6>
						<h6>Direccion Recinto:&nbsp; <?php echo $view_model->get_recinto_address();?></h6>
						<h6>colegio:&nbsp; <?php echo $view_model->get_colegio();?></h6>
						<h6>sector:&nbsp; <?php echo $view_model->get_sector();?></h6>
						<h6>Nombre de sector:&nbsp; <?php echo $view_model->get_sector_name();?></h6>
						<h6>codigo sub sector:&nbsp; <?php echo $view_model->get_voting_sub_sector_code();?></h6>
						<h6>Telefono 1:&nbsp; <?php echo $view_model->get_phone_1();?></h6>
						<h6>Tipo telefono 1:&nbsp; <?php echo $view_model->phone_type_1_mapping()[$view_model->get_phone_type_1()];?></h6>
						<h6>Telefono 2:&nbsp; <?php echo $view_model->get_phone_2();?></h6>
						<h6>Tipo telefono 2:&nbsp; <?php echo $view_model->phone_type_2_mapping()[$view_model->get_phone_type_2()];?></h6>
						<h6>Correo:&nbsp; <?php echo $view_model->get_email();?></h6>
						<h6>facebook:&nbsp; <?php echo $view_model->get_facebook();?></h6>
						<h6>twitter:&nbsp; <?php echo $view_model->get_twitter();?></h6>
						<h6>instagram:&nbsp; <?php echo $view_model->get_instagram();?></h6>
						<h6>Voto en primarias:&nbsp; <?php echo $view_model->get_vote_in_primary();?></h6>
						<h6>Id coordinador:&nbsp; <?php echo $view_model->get_cordinator_id();?></h6>
						<h6>Es coordinador:&nbsp; <?php echo $view_model->is_cordinator_mapping()[$view_model->get_is_cordinator()];?></h6>
						<h6>ID ocupacion:&nbsp; <?php echo $view_model->get_occupation_id();?></h6>
						<h6>Aistencia de gobierno:&nbsp; <?php echo $view_model->get_government_sponsor();?></h6>
						<h6>Vota en el exterior:&nbsp; <?php echo $view_model->is_vote_external_mapping()[$view_model->get_is_vote_external()];?></h6>
						<h6>Mesa madre:&nbsp; <?php echo $view_model->get_parent_voting_table();?></h6>
						<h6>Turno Votacion:&nbsp; <?php echo $view_model->get_voter_turn();?></h6>
						<h6>Votó en Municipales2020:&nbsp; <?php echo $view_model->get_has_voted_municipal_2020();?></h6>
						<h6>Votó en Generales2020:&nbsp; <?php echo $view_model->get_has_voted_general_2020();?></h6>
						<h6>Recibió llamado:&nbsp; <?php echo $view_model->get_has_contact_call();?></h6>
						<h6>Recibió encuesta:&nbsp; <?php echo $view_model->get_has_contact_call_poll();?></h6>
						<h6>Recibió SMS:&nbsp; <?php echo $view_model->get_has_contact_sms();?></h6>
						<h6>Recibió encuesta SMS:&nbsp; <?php echo $view_model->get_has_contact_sms_poll();?></h6>
						<h6>Recibió correo:&nbsp; <?php echo $view_model->get_has_contact_email();?></h6>

                </div>
        </div>
    </div>
</div>