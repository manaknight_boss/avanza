<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/admin/dashboard" class="breadcrumb-link">Tablero</a></li>
						<li class="breadcrumb-item"><a href="/admin/test" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">Añadir</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <?php if (validation_errors()) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?= validation_errors() ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($error) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($success) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Añadir <?php echo $view_model->get_heading();?></h5>
                <div class="card-body">
                <?= form_open() ?>
				<div class="form-group">
					<div class="mkd-upload-form-btn-wrapper">
						<label for="Archivo">Archivo</label>
						<button class="mkd-upload-btn">Cargar un Archivo</button>
						<input type="file" name="file_upload" id="file_upload" onchange="onFileUploaded(event, 'file')" accept=".gif,.jpg,.jpeg,.png,.doc,.docx,.pdf,.md,.txt,.rtf,.xls,.xlsx,.xml,.json,.html,.mp3,.mp4,.csv,.bmp,.mpeg,.ppt,.pptx,.svg,.wav,.webm,.weba,.woff,.tiff"/>
					<input type="hidden" id="file" name="file"/>
					<input type="hidden" id="file_id" name="file_id"/>
					<span id="file_text" class="mkd-upload-filename"></span>
					</div>
				</div>


                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Enviar">
                </div>
                </form>
            </div>
        </div>
    </div>
</div>