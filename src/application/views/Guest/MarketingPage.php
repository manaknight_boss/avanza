<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://bootstrapbay.gitlab.io/circular-free/favicon.ico">
    <title>Avanza360</title>


    <!-- Bootstrap core CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300|Open+Sans:300" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <link href="/assets/css/guest.css" rel="stylesheet">

</head>

<body class="circular-theme--two">

    <!-- 	header -->
    <div class="header" id="header">
        <nav class="navbar navbar-expand-lg navbar-transparent" id="navigation">
            <a class="navbar-brand d-lg-none" href="index.html#">
                <img src="/assets/image/Vector.png" class="d-inline-block align-top" alt="">
            </a>


            <div class="collapse navbar-collapse justify-content-between" id="navbarSupportedContent">
                <a class="navbar-brand order-1 d-none d-lg-block" href="index.html#">
                    <img src="/assets/image/Vector.png" class="d-inline-block align-top" alt="">
                </a>
                <ul class="navbar-nav order-2 justify-content-center">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.html#header">Inicio <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://avanza360.app/admin/login">Comunicación</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index.html#showcase">Votación</a>
                    </li>

                </ul>

            </div>
        </nav>

        <div class="container">
            <div class="header-text">
                <h1>Bienvenidos Avanza 360.</h1>
                <p>Elige tu solucion y avancemos.</p>
            </div>
            <div class="row header-btns">
                <div class="col-xs-12 col-md-7 col-lg-4 offset-lg-2">

                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>

    <!-- 	footer -->
    <div class="footer ">
        <div class="container-fluid ">
            <div class="row ">
                <div class="col-sm-12 col-md-3 logo ">
                    <img class="img-fluid " src="/assets/image/Vector.png " />
                </div>
                <div class="col-sm-12 col-md-6 links ">
                    <a href="index.html#header " class="active ">Inicio</a>
                    <a href="https://avanza360.app/admin/login ">Comunicación</a>
                    <a href="index.html#careers ">Votación</a>
                </div>
                <div class="col-sm-12 col-md-3 social">
                    <a href="javascript:void(0)">
                        <img class="img-fluid" src="/assets/image/facebook_logo.png">
                    </a>
                    <a href="javascript:void(0)">
                        <img class="img-fluid" src="/assets/image/twitter_logo.png">
                    </a>
                    <a href="javascript:void(0)">
                        <img class="img-fluid" src="/assets/image/instagram_logo.png">
                    </a>
                </div>
            </div>
            <div class="row ">
                <div class="col-md-4 offset-md-4 copyright ">
                    © Copyright
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Avanza 360.
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script src="/js/guest.js "></script>
</body>

</html>