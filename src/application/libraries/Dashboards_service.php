<?php
defined('BASEPATH') or exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Dashboard Class
 */
class Dashboards_service
{
  protected $_voter_model;

  public function get_voice_data($voter_model)
  {
      $result = [];
      $this->_voter_model = $voter_model;
      $result[] = $this->build_voice_region(1);
      $result[] = $this->build_voice_region(2);
      $result[] = $this->build_voice_region(3);
      return $result;
  }

    public function build_voice_region($region)
    {
      $result = [
        'region' => $region,
        'voters' => $this->_voter_model->count(['circunscripcion' => $region]),
        'sent' => $this->_voter_model->raw_query('SELECT count(*) as n FROM call_history')->result()[0]->n,
        'reach' => $this->_voter_model->raw_query("SELECT count(*) as n FROM voters_new WHERE circunscripcion={$region} AND (has_contact_call=1 OR has_contact_call_poll=1)")->result()[0]->n,
        'avanza_member' => $this->_voter_model->raw_query("SELECT count(*) as n FROM voters_new WHERE circunscripcion={$region} AND avanza_member_id>0")->result()[0]->n,
        'avanza_reach' => $this->_voter_model->raw_query("SELECT count(*) as n FROM voters_new WHERE circunscripcion={$region} AND avanza_member_id>0 AND (has_contact_call=1 OR has_contact_call_poll=1)")->result()[0]->n,
        'avanza_percentage' => '0%',
        'tactical_member' =>  $this->_voter_model->raw_query("SELECT count(*) as n FROM voters_new WHERE circunscripcion={$region} AND tactical_member_id > 0 ")->result()[0]->n,
        'tactical_reach' => $this->_voter_model->raw_query("SELECT count(*) as n FROM voters_new WHERE circunscripcion={$region} AND tactical_member_id>0 AND (has_contact_call=1 OR has_contact_call_poll=1)")->result()[0]->n,
        'tactical_percentage' => '0%',
      ];

      if ($result['avanza_reach'] > 0)
      {
        $result['avanza_percentage'] = number_format((int)$result['avanza_member'] / (int)$result['avanza_reach'] * 100, 4) . '%';
      }
      if ($result['tactical_reach'] > 0)
      {
        $result['tactical_percentage'] = number_format((int)$result['tactical_member'] / (int)$result['tactical_reach'] * 100, 4) . '%';
      }

      $result['progress'] = number_format((int)$result['reach'] / (int)$result['voters'] * 100, 4) . '%';

      return $result;
    }

    public function get_sms_data($voter_model)
    {
        $result = [];
        $this->_voter_model = $voter_model;
        $result[] = $this->build_sms_region(1);
        $result[] = $this->build_sms_region(2);
        $result[] = $this->build_sms_region(3);
        return $result;
    }

    public function build_sms_region($region)
    {
      $result = [
        'region' => $region,
        'voters' => $this->_voter_model->count(['circunscripcion' => $region]),
        'sent' => $this->_voter_model->raw_query('SELECT count(*) as n FROM sms_history')->result()[0]->n,
        'reach' => $this->_voter_model->raw_query("SELECT count(*) as n FROM voters_new WHERE circunscripcion={$region} AND ( has_contact_sms=1 OR has_contact_sms_poll=1)")->result()[0]->n,
        'avanza_member' => $this->_voter_model->raw_query("SELECT count(*) as n FROM voters_new WHERE circunscripcion={$region} AND avanza_member_id>0")->result()[0]->n,
        'avanza_reach' => $this->_voter_model->raw_query("SELECT count(*) as n FROM voters_new WHERE circunscripcion={$region} AND avanza_member_id>0 AND ( has_contact_sms=1 OR has_contact_sms_poll=1)")->result()[0]->n,
        'avanza_percentage' => '0%',
        'tactical_member' =>  $this->_voter_model->raw_query("SELECT count(*) as n FROM voters_new WHERE circunscripcion={$region} AND tactical_member_id > 0 ")->result()[0]->n,
        'tactical_reach' => $this->_voter_model->raw_query("SELECT count(*) as n FROM voters_new WHERE circunscripcion={$region} AND tactical_member_id>0 AND ( has_contact_sms=1 OR has_contact_sms_poll=1)")->result()[0]->n,
        'tactical_percentage' => '0%',
      ];

      if ($result['avanza_reach'] > 0)
      {
        $result['avanza_percentage'] = number_format((int)$result['avanza_member'] / (int)$result['avanza_reach'] * 100, 4) . '%';
      }
      if ($result['tactical_reach'] > 0)
      {
        $result['tactical_percentage'] = number_format((int)$result['tactical_member'] / (int)$result['tactical_reach'] * 100, 4) . '%';
      }

      $result['progress'] = number_format((int)$result['reach'] / (int)$result['voters'] * 100, 4) . '%';

      return $result;
    }
}