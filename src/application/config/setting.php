<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/*
| -------------------------------------------------------------------
| MIME TYPES
| -------------------------------------------------------------------
| This file contains an array of mime types.  It is used by the
| Upload class to help identify allowed file types.
|
*/
$config['setting'] = array(
	'site_name' => 'Manaknight Inc',
	'site_logo' => 'https://manaknightdigital.com/assets/img/logo.png',
	'maintenance' => '0',
	'version' => '1.0.0',
	'copyright' => 'Copyright © 2019 Manaknightdigital Inc. All rights reserved.',

);