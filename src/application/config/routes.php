<?php defined('BASEPATH') or exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['v1/api/normal/table/cordinator/municipal/(:any)/(:any)'] = 'Grassroot/Grassroot_voters_no_auth_api_controller/cordinator/$1/$2/2';
$route['v1/api/normal/table/cordinator/general/(:any)/(:any)'] = 'Grassroot/Grassroot_voters_no_auth_api_controller/cordinator/$1/$2/1';
$route['v1/api/table/cordinator/municipal/(:any)/(:any)'] = 'Grassroot/Grassroot_voters_api_controller/cordinator/$1/$2/2';
$route['v1/api/table/cordinator/general/(:any)/(:any)'] = 'Grassroot/Grassroot_voters_api_controller/cordinator/$1/$2/1';
$route['admin/call_history/deletecustom/(:num)/(:num)'] = 'Admin/Admin_call_history_controller/delete_custom/$1/$2';
$route['v1/api/normal/table/results/municipal/(:any)'] = 'Grassroot/Grassroot_voters_no_auth_api_controller/table/$1/2';
$route['admin/sms_history/deletecustom/(:num)/(:num)'] = 'Admin/Admin_sms_history_controller/delete_custom/$1/$2';
$route['v1/api/recinto/update/(:any)/(:any)/(:num)'] = 'Grassroot/Recinto_coordinator_api_controller/update/$1/$2/$3';
$route['v1/api/normal/table/results/general/(:any)'] = 'Grassroot/Grassroot_voters_no_auth_api_controller/table/$1/1';
$route['v1/api/admin/voters/autocomplete/(:any)'] = 'Admin/Admin_voters_controller/autocomplete/$1';
$route['v1/api/table/results/municipal/(:any)'] = 'Grassroot/Grassroot_voters_api_controller/table/$1/2';
$route['v1/api/normal/table/cordinator/(:any)'] = 'Grassroot/Grassroot_voters_no_auth_api_controller/cordinator_all/$1';
$route['v1/api/smscall/call/no_answer/(:num)'] = 'Guest/Sms_custom_api_controller/callno_answer/$1';
$route['v1/api/recinto/college/(:any)/(:any)'] = 'Grassroot/Recinto_coordinator_api_controller/personnel/$1/$2';
$route['v1/api/normal/voted/municipal/(:any)'] = 'Grassroot/Grassroot_voters_no_auth_api_controller/voted/$1/1';
$route['v1/api/grassroot/final/delete/(:num)'] = 'Grassroot/Grassroot_final_count_api_controller/delete/$1';
$route['v1/api/table/results/general/(:any)'] = 'Grassroot/Grassroot_voters_api_controller/table/$1/1';
$route['v1/api/smscall/sms/no_answer/(:num)'] = 'Guest/Sms_custom_api_controller/smsno_answer/$1';
$route['v1/api/normal/voted/general/(:any)'] = 'Grassroot/Grassroot_voters_no_auth_api_controller/voted/$1/2';
$route['v1/api/grassroot/final/view/(:num)'] = 'Grassroot/Grassroot_final_count_api_controller/view/$1';
$route['v1/api/grassroot/final/edit/(:num)'] = 'Grassroot/Grassroot_final_count_api_controller/edit/$1';
$route['v1/api/cordinator/dashboard/(:any)'] = 'Grassroot/Grassroot_coordinator_api_controller/coordinator/$1';
$route['grassroot/cordinator/(:num)/(:num)'] = 'Grassroot/Grassroot_dashboard_controller/cordinator/$1/$2/1';
$route['admin/campaigns/view/(:num)/(:num)'] = 'Admin/Admin_campaign_controller/view/$1/$2';
$route['v1/api/voice/call/response/(:num)'] = 'Guest/Voice_controller/call_callback/$1';
$route['v1/api/admin/settings/edit/(:num)'] = 'Admin/Admin_setting_controller/edit/$1';
$route['admin/campaign/(:num)/call/(:any)'] = 'Admin/Admin_campaign_controller/call_manually/$1/$2';
$route['v1/api/voice/call/message/(:num)'] = 'Guest/Voice_controller/get_call_message/$1';
$route['v1/api/normal/final/count/(:any)'] = 'Grassroot/Grassroot_voters_no_auth_api_controller/final/$1';
$route['admin/campaigns/add_custom/voice'] = 'Admin/Admin_campaign_controller/add_custom_voice';
$route['admin/campaign/(:num)/sms/(:any)'] = 'Admin/Admin_campaign_controller/sms_manually/$1/$2';
$route['admin/call_history/delete/(:num)'] = 'Admin/Admin_call_history_controller/delete/$1';
$route['v1/api/voice/call/custom/(:num)'] = 'Guest/Voice_controller/get_custom_call_play_message/$1';
$route['v1/api/thirdparty/voters/(:any)'] = 'Grassroot/Third_party_voters_api_controller/index/$1';
$route['v1/api/smscall/call/save/(:num)'] = 'Guest/Sms_custom_api_controller/callsave/$1';
$route['v1/api/smscall/call/fail/(:num)'] = 'Guest/Sms_custom_api_controller/callfail/$1';
$route['v1/api/recinto/personnel/(:any)'] = 'Grassroot/Recinto_coordinator_api_controller/recinto/$1';
$route['v1/api/recinto/dashboard/(:any)'] = 'Grassroot/Recinto_coordinator_api_controller/dashboard/$1';
$route['v1/api/normal/government/(:any)'] = 'Grassroot/Grassroot_voters_no_auth_api_controller/government/$1';
$route['v1/api/admin/voters/view/(:num)'] = 'Admin/Admin_voters_api_controller/view/$1';
$route['v1/api/admin/voters/edit/(:num)'] = 'Admin/Admin_voters_api_controller/edit/$1';
$route['grassroot/voters/voteNow/(:num)'] = 'Grassroot/Grassroot_voters_controller/voteNow/$1';
$route['grassroot/personnel/view/(:num)'] = 'Grassroot/Recinto_collegio_personnel_controller/view/$1';
$route['grassroot/personnel/edit/(:num)'] = 'Grassroot/Recinto_collegio_personnel_controller/edit/$1';
$route['admin/sms_history/delete/(:num)'] = 'Admin/Admin_sms_history_controller/delete/$1';
$route['v1/api/smscall/sms/save/(:num)'] = 'Guest/Sms_custom_api_controller/smssave/$1';
$route['v1/api/smscall/sms/fail/(:num)'] = 'Guest/Sms_custom_api_controller/smsfail/$1';
$route['v1/api/grassroot/voters/(:num)'] = 'Grassroot/Grassroot_voters_api_controller/index/$1';
$route['grassroot/recintos/edit/(:num)'] = 'Grassroot/Recinto_coordinator_controller/edit/$1';
$route['admin/campaigns/results/(:num)'] = 'Admin/Admin_campaign_controller/results/$1';
$route['admin/campaigns/add_custom/sms'] = 'Admin/Admin_campaign_controller/add_custom_sms';
$route['admin/call_history/view/(:num)'] = 'Admin/Admin_call_history_controller/view/$1';
$route['v1/api/voted/municipal/(:any)'] = 'Grassroot/Grassroot_voters_api_controller/voted/$1/1';
$route['v1/api/voice/call/play/(:num)'] = 'Guest/Voice_controller/get_call_play_message/$1';
$route['v1/api/recinto/profile/(:any)'] = 'Grassroot/Recinto_coordinator_api_controller/profile/$1';
$route['v1/api/grassroot/reset/(:num)'] = 'Grassroot/Grassroot_reset_api_controller/index/$1';
$route['v1/api/grassroot/final/(:num)'] = 'Grassroot/Grassroot_final_count_api_controller/index/$1';
$route['grassroot/users/delete/(:num)'] = 'Grassroot/Grassroot_user_controller/delete/$1';
$route['grassroot/final/delete/(:num)'] = 'Grassroot/Grassroot_final_count_controller/delete/$1';
$route['admin/sms_history/view/(:num)'] = 'Admin/Admin_sms_history_controller/view/$1';
$route['admin/custom_call/view/(:num)'] = 'Admin/Admin_dashboard_controller/custom_call/$1';
$route['admin/call_history/add/(:num)'] = 'Admin/Admin_call_history_controller/add_custom/$1';
$route['v1/api/smscall/customs/(num)'] = 'Guest/Sms_custom_api_controller/customs/$1';
$route['v1/api/admin/sms/view/(:num)'] = 'Admin/Admin_sms_api_controller/view/$1';
$route['v1/api/admin/sms/edit/(:num)'] = 'Admin/Admin_sms_api_controller/edit/$1';
$route['admin/sms_history/add/(:num)'] = 'Admin/Admin_sms_history_controller/add_custom/$1';
$route['v1/api/voted/general/(:any)'] = 'Grassroot/Grassroot_voters_api_controller/voted/$1/2';
$route['grassroot/users/view/(:num)'] = 'Grassroot/Grassroot_user_controller/view/$1';
$route['grassroot/users/edit/(:num)'] = 'Grassroot/Grassroot_user_controller/edit/$1';
$route['grassroot/final/view/(:num)'] = 'Grassroot/Grassroot_final_count_controller/view/$1';
$route['grassroot/final/edit/(:num)'] = 'Grassroot/Grassroot_final_count_controller/edit/$1';
$route['admin/campaigns/view/(:num)'] = 'Admin/Admin_campaign_controller/view/$1';
$route['admin/campaigns/edit/(:num)'] = 'Admin/Admin_campaign_controller/edit/$1';
$route['admin/campaign/build/(:num)'] = 'Admin/Admin_campaign_controller/change_status/$1/2';
$route['v1/api/sms/callback/(:num)'] = 'Guest/Voice_controller/sms_reply/$1';
$route['v1/api/grassroot/final/add'] = 'Grassroot/Grassroot_final_count_api_controller/add';
$route['v1/api/admin/voters/(:num)'] = 'Admin/Admin_voters_api_controller/index/$1';
$route['grassroot/personnel/(:num)'] = 'Grassroot/Recinto_collegio_personnel_controller/index/$1';
$route['admin/custom_call_list/add'] = 'Admin/Custom_call_list_controller/add';
$route['v1/api/normal/final/count'] = 'Grassroot/Grassroot_voters_no_auth_api_controller/add_final_count';
$route['v1/api/grassroot/register'] = 'Grassroot/Grassroot_register_api_controller';
$route['v1/api/file/import/(:any)'] = 'Guest/Image_controller/file_import/$1';
$route['grassroot/recintos/(:num)'] = 'Grassroot/Recinto_coordinator_controller/index/$1';
$route['admin/image/delete/(:num)'] = 'Admin/Admin_image_controller/delete/$1';
$route['admin/campaigns/add/voice'] = 'Admin/Admin_campaign_controller/add_voice';
$route['admin/call_history/(:num)'] = 'Admin/Admin_call_history_controller/index/$1';
$route['v1/api/grassroot/profile'] = 'Grassroot/Grassroot_voters_api_controller/profile';
$route['v1/api/government/(:any)'] = 'Grassroot/Grassroot_voters_api_controller/government/$1';
$route['grassroot/recinto/(:num)'] = 'Grassroot/Grassroot_dashboard_controller/recinto/$1';
$route['grassroot/dashboard/real'] = 'Grassroot/Grassroot_dashboard_controller/clear_cache';
$route['admin/voters/view/(:num)'] = 'Admin/Admin_voters_controller/view/$1';
$route['admin/voters/edit/(:num)'] = 'Admin/Admin_voters_controller/edit/$1';
$route['admin/sms_history/(:num)'] = 'Admin/Admin_sms_history_controller/index/$1';
$route['admin/emails/view/(:num)'] = 'Admin/Admin_email_controller/view/$1';
$route['admin/emails/edit/(:num)'] = 'Admin/Admin_email_controller/edit/$1';
$route['v1/api/grassroot/voters'] = 'Grassroot/Grassroot_voters_api_controller/index/0';
$route['v1/api/grassroot/forgot'] = 'Grassroot/Grassroot_forgot_api_controller';
$route['v1/api/cordinator/login'] = 'Grassroot/Grassroot_coordinator_api_controller/login';
$route['v1/api/admin/voters/add'] = 'Admin/Admin_voters_api_controller/add';
$route['v1/api/admin/sms/(:num)'] = 'Admin/Admin_sms_api_controller/index/$1';
$route['grassroot/voters/(:num)'] = 'Grassroot/Grassroot_voters_controller/index/$1';
$route['admin/users/view/(:num)'] = 'Admin/Admin_user_controller/view/$1';
$route['admin/users/edit/(:num)'] = 'Admin/Admin_user_controller/edit/$1';
$route['admin/image/view/(:num)'] = 'Admin/Admin_image_controller/view/$1';
$route['admin/campaigns/add/sms'] = 'Admin/Admin_campaign_controller/add_sms';
$route['v1/api/grassroot/token'] = 'Grassroot/Grassroot_login_api_controller/token';
$route['v1/api/grassroot/login'] = 'Grassroot/Grassroot_login_api_controller';
$route['v1/api/grassroot/final'] = 'Grassroot/Grassroot_final_count_api_controller/index/0';
$route['grassroot/users/(:num)'] = 'Grassroot/Grassroot_user_controller/index/$1';
$route['grassroot/reset/(:num)'] = 'Grassroot/Grassroot_reset_controller/index/$1';
$route['grassroot/final/(:num)'] = 'Grassroot/Grassroot_final_count_controller/index/$1';
$route['admin/test/view/(:num)'] = 'Admin/Admin_test_controller/view/$1';
$route['admin/test/edit/(:num)'] = 'Admin/Admin_test_controller/edit/$1';
$route['admin/custom_call_list'] = 'Admin/Custom_call_list_controller';
$route['admin/campaigns/(:num)'] = 'Admin/Admin_campaign_controller/index/$1';
$route['admin/call_history/add'] = 'Admin/Admin_call_history_controller/add';
$route['v1/api/voice/call/log'] = 'Guest/Voice_controller/get_call_logs';
$route['v1/api/smscall/custom'] = 'Guest/Sms_custom_api_controller/custom';
$route['admin/sms_history/add'] = 'Admin/Admin_sms_history_controller/add';
$route['admin/sms/view/(:num)'] = 'Admin/Admin_sms_controller/view/$1';
$route['admin/sms/edit/(:num)'] = 'Admin/Admin_sms_controller/edit/$1';
$route['v1/api/sms/replyfail'] = 'Guest/Voice_controller/sms_callback_fail';
$route['v1/api/recinto/login'] = 'Grassroot/Recinto_coordinator_api_controller/login';
$route['v1/api/assets/(:num)'] = 'Guest/Image_controller/paginate/$1';
$route['v1/api/admin/sms/add'] = 'Admin/Admin_sms_api_controller/add';
$route['admin/dashboard/real'] = 'Admin/Admin_dashboard_controller/clear_cache';
$route['v1/api/voice/gather'] = 'Guest/Voice_controller/gather';
$route['v1/api/image/upload'] = 'Guest/Image_controller';
$route['v1/api/admin/voters'] = 'Admin/Admin_voters_api_controller/index/0';
$route['grassroot/users/add'] = 'Grassroot/Grassroot_user_controller/add';
$route['grassroot/section/3'] = 'Grassroot/Grassroot_dashboard_controller/section/3';
$route['grassroot/section/2'] = 'Grassroot/Grassroot_dashboard_controller/section/2';
$route['grassroot/section/1'] = 'Grassroot/Grassroot_dashboard_controller/section/1';
$route['grassroot/personnel'] = 'Grassroot/Recinto_collegio_personnel_controller/index/0';
$route['grassroot/final/add'] = 'Grassroot/Grassroot_final_count_controller/add';
$route['grassroot/dashboard'] = 'Grassroot/Grassroot_dashboard_controller';
$route['admin/voters/(:num)'] = 'Admin/Admin_voters_controller/index/$1';
$route['admin/emails/(:num)'] = 'Admin/Admin_email_controller/index/$1';
$route['v1/api/file/upload'] = 'Guest/Image_controller/file_upload';
$route['v1/api/admin/token'] = 'Admin/Admin_login_api_controller/token';
$route['v1/api/admin/login'] = 'Admin/Admin_login_api_controller';
$route['grassroot/tactical'] = 'Grassroot/Grassroot_tactical_dashboard_controller/index';
$route['grassroot/register'] = 'Grassroot/Grassroot_register_controller';
$route['grassroot/recintos'] = 'Grassroot/Recinto_coordinator_controller/index/0';
$route['admin/users/(:num)'] = 'Admin/Admin_user_controller/index/$1';
$route['admin/report/users'] = 'Admin/Admin_num_users_report_controller/index';
$route['admin/image/(:num)'] = 'Admin/Admin_image_controller/index/$1';
$route['admin/call_history'] = 'Admin/Admin_call_history_controller/index/0';
$route['grassroot/profile'] = 'Grassroot/Grassroot_profile_controller';
$route['admin/sms_history'] = 'Admin/Admin_sms_history_controller/index/0';
$route['v1/api/sms/reply'] = 'Guest/Voice_controller/sms_callback';
$route['v1/api/admin/sms'] = 'Admin/Admin_sms_api_controller/index/0';
$route['grassroot/voters'] = 'Grassroot/Grassroot_voters_controller/index/0';
$route['grassroot/logout'] = 'Grassroot/Grassroot_login_controller/logout';
$route['grassroot/global'] = 'Grassroot/Grassroot_dashboard_controller/global_index/1';
$route['grassroot/forgot'] = 'Grassroot/Grassroot_forgot_controller';
$route['admin/voters/add'] = 'Admin/Admin_voters_controller/add';
$route['admin/sms/(:num)'] = 'Admin/Admin_sms_controller/index/$1';
$route['admin/emails/add'] = 'Admin/Admin_email_controller/add';
$route['grassroot/users'] = 'Grassroot/Grassroot_user_controller/index/0';
$route['grassroot/login'] = 'Grassroot/Grassroot_login_controller';
$route['grassroot/final'] = 'Grassroot/Grassroot_final_count_controller/index/0';
$route['admin/users/add'] = 'Admin/Admin_user_controller/add';
$route['admin/dashboard'] = 'Admin/Admin_dashboard_controller';
$route['admin/campaigns'] = 'Admin/Admin_campaign_controller/index/0';
$route['admin/test/add'] = 'Admin/Admin_test_controller/add';
$route['admin/settings'] = 'Admin/Admin_setting_controller/index';
$route['v1/api/assets'] = 'Guest/Image_controller/paginate/0';
$route['admin/sms/add'] = 'Admin/Admin_sms_controller/add';
$route['admin/profile'] = 'Admin/Admin_profile_controller';
$route['health_check'] = 'Health_check_controller/index';
$route['admin/voters'] = 'Admin/Admin_voters_controller/index/0';
$route['admin/logout'] = 'Admin/Admin_login_controller/logout';
$route['admin/emails'] = 'Admin/Admin_email_controller/index/0';
$route['admin/users'] = 'Admin/Admin_user_controller/index/0';
$route['admin/login'] = 'Admin/Admin_login_controller';
$route['admin/image'] = 'Admin/Admin_image_controller/index/0';
$route['admin/test'] = 'Admin/Admin_test_controller';
$route['admin/sms'] = 'Admin/Admin_sms_controller/index/0';
$route['privacy'] = 'Guest/Privacy_controller/index';
$route['about'] = 'Guest/About_controller';

$route['404_override'] = '';
$route['translate_uri_dashes'] = false;
$route['default_controller'] = 'Welcome/index';
