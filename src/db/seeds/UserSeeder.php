<?php


use Phinx\Seed\AbstractSeed;
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * User Seeder
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
		[
			'email' => 'admin@manaknight.com',
			'password' => str_replace('$2y$', '$2b$', password_hash('a123456', PASSWORD_BCRYPT)),
			'type' => 'n',
			'first_name' => 'Admin',
			'last_name' => 'Admin',
			'phone' => '12345678',
			'image' => 'https://i.imgur.com/AzJ7DRw.png',
			'image_id' => 1,
			'refer' => 'admin',
			'profile_id' => 0,
			'verify' => 1,
			'role_id' => 2,
			'stripe_id' => '',
			'status' => 1,
			'created_at' => date('Y-m-j'),
			'updated_at' => date('Y-m-j H:i:s'),
		],
		[
			'email' => 'member@manaknight.com',
			'password' => str_replace('$2y$', '$2b$', password_hash('a123456', PASSWORD_BCRYPT)),
			'type' => 'n',
			'first_name' => 'Admin',
			'last_name' => 'Admin',
			'phone' => '12345678',
			'image' => 'https://i.imgur.com/AzJ7DRw.png',
			'image_id' => 1,
			'refer' => 'member',
			'profile_id' => 0,
			'verify' => 1,
			'role_id' => 1,
			'stripe_id' => '',
			'status' => 1,
			'created_at' => date('Y-m-j'),
			'updated_at' => date('Y-m-j H:i:s'),
		],
		[
			'email' => 'grassroot@manaknight.com',
			'password' => str_replace('$2y$', '$2b$', password_hash('a123456', PASSWORD_BCRYPT)),
			'type' => 'n',
			'first_name' => 'grassroot',
			'last_name' => 'grassroot',
			'phone' => '12345678',
			'image' => 'https://i.imgur.com/AzJ7DRw.png',
			'image_id' => 1,
			'refer' => 'member',
			'profile_id' => 0,
			'verify' => 1,
			'role_id' => 1,
			'stripe_id' => '',
			'status' => 1,
			'created_at' => date('Y-m-j'),
			'updated_at' => date('Y-m-j H:i:s'),
		],

        ];
        $model = $this->table('user');
        $model->truncate();
        $model->insert($data)->save();
    }
}
