<?php
use Phinx\Migration\AbstractMigration;
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * RecintoCollegioPersonnel Migration
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class RecintoCollegioPersonnel extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $exists = $this->hasTable('recinto_collegio_personnel');
        if (!$exists)
        {
            $table = $this->table('recinto_collegio_personnel');
            $table->addColumn('recinto','integer')
		->addColumn('collegio','string',["limit" => 255])
		->addColumn('staff_lead_name','string',["limit" => 255])
		->addColumn('staff_lead_government_id','string',["limit" => 255])
		->addColumn('transmitter_name','string',["limit" => 255])
		->addColumn('transmitter_government_id','string',["limit" => 255])
		->addColumn('delegate_1_name','string',["limit" => 255])
		->addColumn('delegate_1_government_id','string',["limit" => 255])
		->addColumn('delegate_2_name','string',["limit" => 255])
		->addColumn('delegate_2_government_id','string',["limit" => 255])
		->addColumn('runner_1_name','string',["limit" => 255])
		->addColumn('runner_1_government_id','string',["limit" => 255])
		->addColumn('runner_2_name','string',["limit" => 255])
		->addColumn('runner_2_government_id','string',["limit" => 255])
		->addColumn('staff_lead_present','integer')
		->addColumn('transmitter_present','integer')
		->addColumn('delegate_1_present','integer')
		->addColumn('delegate_2_present','integer')
		->addColumn('runner_1_present','integer')
		->addColumn('runner_2_present','integer')
		->addColumn('created_at','date')
		->addColumn('updated_at','datetime')
		->create();
        }
    }

    public function down()
    {
        $this->table('recinto_collegio_personnel')->drop()->save();
    }
}
