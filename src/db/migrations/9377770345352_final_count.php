<?php
use Phinx\Migration\AbstractMigration;
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * FinalCount Migration
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class FinalCount extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $exists = $this->hasTable('final_count');
        if (!$exists)
        {
            $table = $this->table('final_count');
            $table->addColumn('table','string',["limit" => 50])
		->addColumn('file','text')
		->addColumn('file_id','integer')
		->addColumn('mayor_1','string',["limit" => 255])
		->addColumn('mayor_count_1','integer')
		->addColumn('mayor_2','string',["limit" => 255])
		->addColumn('mayor_count_2','integer')
		->addColumn('mayor_3','string',["limit" => 255])
		->addColumn('mayor_count_3','integer')
		->addColumn('mayor_4','string',["limit" => 255])
		->addColumn('mayor_count_4','integer')
		->addColumn('mayor_5','string',["limit" => 255])
		->addColumn('mayor_count_5','integer')
		->addColumn('mayor_6','string',["limit" => 255])
		->addColumn('mayor_count_6','integer')
		->addColumn('mayor_7','string',["limit" => 255])
		->addColumn('mayor_count_7','integer')
		->addColumn('mayor_8','string',["limit" => 255])
		->addColumn('mayor_count_8','integer')
		->addColumn('mayor_9','string',["limit" => 255])
		->addColumn('mayor_count_9','integer')
		->addColumn('mayor_10','string',["limit" => 255])
		->addColumn('mayor_count_10','integer')
		->addColumn('senator_1','string',["limit" => 255])
		->addColumn('senator_count_1','integer')
		->addColumn('senator_2','string',["limit" => 255])
		->addColumn('senator_count_2','integer')
		->addColumn('senator_3','string',["limit" => 255])
		->addColumn('senator_count_3','integer')
		->addColumn('senator_4','string',["limit" => 255])
		->addColumn('senator_count_4','integer')
		->addColumn('senator_5','string',["limit" => 255])
		->addColumn('senator_count_5','integer')
		->addColumn('senator_6','string',["limit" => 255])
		->addColumn('senator_count_6','integer')
		->addColumn('senator_7','string',["limit" => 255])
		->addColumn('senator_count_7','integer')
		->addColumn('senator_8','string',["limit" => 255])
		->addColumn('senator_count_8','integer')
		->addColumn('senator_9','string',["limit" => 255])
		->addColumn('senator_count_9','integer')
		->addColumn('senator_10','string',["limit" => 255])
		->addColumn('senator_count_10','integer')
		->addColumn('regional_1','string',["limit" => 255])
		->addColumn('regional_count_1','integer')
		->addColumn('regional_2','string',["limit" => 255])
		->addColumn('regional_count_2','integer')
		->addColumn('regional_3','string',["limit" => 255])
		->addColumn('regional_count_3','integer')
		->addColumn('regional_4','string',["limit" => 255])
		->addColumn('regional_count_4','integer')
		->addColumn('regional_5','string',["limit" => 255])
		->addColumn('regional_count_5','integer')
		->addColumn('regional_6','string',["limit" => 255])
		->addColumn('regional_count_6','integer')
		->addColumn('regional_7','string',["limit" => 255])
		->addColumn('regional_count_7','integer')
		->addColumn('regional_8','string',["limit" => 255])
		->addColumn('regional_count_8','integer')
		->addColumn('regional_9','string',["limit" => 255])
		->addColumn('regional_count_9','integer')
		->addColumn('regional_10','string',["limit" => 255])
		->addColumn('regional_count_10','integer')
		->addColumn('regional_11','string',["limit" => 255])
		->addColumn('regional_count_11','integer')
		->addColumn('regional_12','string',["limit" => 255])
		->addColumn('regional_count_12','integer')
		->addColumn('regional_13','string',["limit" => 255])
		->addColumn('regional_count_13','integer')
		->addColumn('regional_14','string',["limit" => 255])
		->addColumn('regional_count_14','integer')
		->addColumn('regional_15','string',["limit" => 255])
		->addColumn('regional_count_15','integer')
		->addColumn('created_at','date')
		->addColumn('updated_at','datetime')
		->addIndex(["table"], ['unique' => true])
		->create();
        }
    }

    public function down()
    {
        $this->table('final_count')->drop()->save();
    }
}
