<?php
use Phinx\Migration\AbstractMigration;
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * RecintoCoordinator Migration
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class RecintoCoordinator extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $exists = $this->hasTable('recinto_coordinator');
        if (!$exists)
        {
            $table = $this->table('recinto_coordinator');
            $table->addColumn('recinto','integer')
		->addColumn('recinto_name','string',["limit" => 255])
		->addColumn('collegio','integer',["limit" => 255])
		->addColumn('coordinator_name','string',["limit" => 255])
		->addColumn('coordinator_government_id','string',["limit" => 255])
		->addColumn('password','string',["limit" => 255])
		->addColumn('personel','integer')
		->addColumn('assigned','integer')
		->addColumn('present','integer')
		->addColumn('created_at','date')
		->addColumn('updated_at','datetime')
		->create();
        }
    }

    public function down()
    {
        $this->table('recinto_coordinator')->drop()->save();
    }
}
