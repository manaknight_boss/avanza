<?php
use Phinx\Migration\AbstractMigration;
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * CallHistory Migration
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class CallHistory extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $exists = $this->hasTable('call_history');
        if (!$exists)
        {
            $table = $this->table('call_history');
            $table->addColumn('government_id','string',["limit" => 255])
		->addColumn('phone_1','string',["limit" => 255])
		->addColumn('phone_2','string',["limit" => 255])
		->addColumn('sid','string',["limit" => 255])
		->addColumn('call_date','date', ['null' => true])
		->addColumn('campaign_id','integer')
		->addColumn('call_result','integer')
		->addColumn('call_type','integer')
		->addColumn('poll_result','integer')
		->addColumn('created_at','date')
		->addColumn('updated_at','datetime')
		->create();
        }
    }

    public function down()
    {
        $this->table('call_history')->drop()->save();
    }
}
