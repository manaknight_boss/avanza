<?php
use Phinx\Migration\AbstractMigration;
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Campaign Migration
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Campaign extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $exists = $this->hasTable('campaign');
        if (!$exists)
        {
            $table = $this->table('campaign');
            $table->addColumn('name','string',["limit" => 255])
		->addColumn('type','integer')
		->addColumn('status','integer')
		->addColumn('circunscripcion','integer')
		->addColumn('age','integer')
		->addColumn('recinto','integer')
		->addColumn('source_id','integer')
		->addColumn('gender','integer')
		->addColumn('married_status','integer')
		->addColumn('college','integer')
		->addColumn('content','text')
		->addColumn('current_page','integer')
		->addColumn('total','integer')
		->addColumn('count_query','text')
		->addColumn('query','text')
		->addColumn('created_at','date')
		->addColumn('updated_at','datetime')
		->create();
        }
    }

    public function down()
    {
        $this->table('campaign')->drop()->save();
    }
}
