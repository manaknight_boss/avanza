<?php
use Phinx\Migration\AbstractMigration;
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * VotersNew Migration
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class VotersNew extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $exists = $this->hasTable('voters_new');
        if (!$exists)
        {
            $table = $this->table('voters_new');
            $table->addColumn('avanza_member_id','integer')
		->addColumn('tactical_member_id','integer')
		->addColumn('government_id','string',["limit" => 255])
		->addColumn('first_name','string',["limit" => 255])
		->addColumn('last_name','string',["limit" => 255])
		->addColumn('married_status','string',["limit" => 1])
		->addColumn('gender','string',["limit" => 1])
		->addColumn('date_of_birth','date', ['null' => true])
		->addColumn('place_of_birth','string',["limit" => 255])
		->addColumn('member_pld','integer')
		->addColumn('circunscripcion','integer')
		->addColumn('age','integer')
		->addColumn('address','string',["limit" => 255])
		->addColumn('address_number','integer')
		->addColumn('recinto','string',["limit" => 255])
		->addColumn('recinto_name','string',["limit" => 255])
		->addColumn('recinto_address','string',["limit" => 255])
		->addColumn('colegio','string',["limit" => 255])
		->addColumn('sector','string',["limit" => 255])
		->addColumn('sector_name','string',["limit" => 255])
		->addColumn('voting_sub_sector_code','string',["limit" => 255])
		->addColumn('phone_1','string',["limit" => 255])
		->addColumn('phone_type_1','integer')
		->addColumn('phone_2','string',["limit" => 255])
		->addColumn('phone_type_2','integer')
		->addColumn('email','string',["limit" => 255])
		->addColumn('facebook','string',["limit" => 255])
		->addColumn('twitter','string',["limit" => 255])
		->addColumn('instagram','string',["limit" => 255])
		->addColumn('vote_in_primary','integer')
		->addColumn('cordinator_id','integer')
		->addColumn('is_cordinator','integer')
		->addColumn('tactical_cordinator_id','integer')
		->addColumn('is_tactical_cordinator','integer')
		->addColumn('occupation_id','integer')
		->addColumn('government_sponsor','integer')
		->addColumn('is_vote_external','integer')
		->addColumn('parent_voting_table','string',["limit" => 255])
		->addColumn('voter_turn','string',["limit" => 255])
		->addColumn('has_voted_municipal_2020','integer')
		->addColumn('has_voted_general_2020','integer')
		->addColumn('has_contact_call','integer')
		->addColumn('has_contact_call_poll','integer')
		->addColumn('has_contact_sms','integer')
		->addColumn('has_contact_sms_poll','integer')
		->addColumn('has_contact_email','integer')
		->create();
        }
    }

    public function down()
    {
        $this->table('voters_new')->drop()->save();
    }
}
