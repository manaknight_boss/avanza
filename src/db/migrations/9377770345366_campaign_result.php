<?php
use Phinx\Migration\AbstractMigration;
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * CampaignResult Migration
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class CampaignResult extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $exists = $this->hasTable('campaign_result');
        if (!$exists)
        {
            $table = $this->table('campaign_result');
            $table->addColumn('campaign_id','integer')
		->addColumn('type','integer')
		->addColumn('status','integer')
		->addColumn('circunscripcion','integer')
		->addColumn('age','integer')
		->addColumn('sector','integer')
		->addColumn('college','integer')
		->addColumn('gender','integer')
		->addColumn('martial_status','integer')
		->addColumn('total','integer')
		->addColumn('respond_1','integer')
		->addColumn('respond_2','integer')
		->addColumn('respond_3','integer')
		->addColumn('engaged','integer')
		->addColumn('reached','integer')
		->addColumn('dne','integer')
		->addColumn('created_at','date')
		->addColumn('updated_at','datetime')
		->create();
        }
    }

    public function down()
    {
        $this->table('campaign_result')->drop()->save();
    }
}
