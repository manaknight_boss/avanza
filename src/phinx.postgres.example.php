<?php
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
return [
    'paths'        => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
        'seeds'      => '%%PHINX_CONFIG_DIR%%/db/seeds',
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_database'        => 'development',
        'development'             => [
            'adapter' => 'pgsql',
            'host'    => 'localhost',
            'name'    => 'manaknight',
            'user'    => 'manaknight',
            'pass'    => '',
            'port'    => 5432,
            'charset' => 'utf8'
        ]
    ],
];